-- 游戏初始化
--技能、物品不得在此代码~


-- Set this to true if you want to see a complete debug output of all events/processes done by ccc
-- You can also change the cvar 'ccc_spew' at any time to 1 or 0 for output/no output
CCC_DEBUG_SPEW = false 

if ccc == nil then
    DebugPrint( '[CCC] creating ccc game mode' )
    _G.ccc = class({})
end

-- This library allow for easily delayed/timed actions
require('libraries/timers')
-- This library can be used for advancted physics/motion/collision of units.  See PhysicsReadme.txt for more information.
require('libraries/physics')
-- This library can be used for advanced 3D projectile systems.
require('libraries/projectiles')
-- This library can be used for sending panorama notifications to the UIs of players/teams/everyone
require('libraries/notifications')
-- This library can be used for starting customized animations on units from lua
require('libraries/animations')
-- This library can be used for performing "Frankenstein" attachments on units
require('libraries/attachments')
--require('libraries/bheap')

-- These internal libraries set up ccc's events and processes.  Feel free to inspect them/change them if you need to.
require('internal/ccc')
require('internal/events')
require('internal/util')
-- settings.lua is where you can specify many different properties for your game mode and is one of the core ccc files.
require('settings')
-- events.lua is where you can specify the actions to be taken when any event occurs and is one of the core ccc files.
require('events')
----------------------------------------------------------自定义
require('items.Item_Init')
require('abilities.Ability_Init')
require('sys.System_Init')
--加载技能和物品的文件
CustomAbility:Require( )
CustomItem:Require( )
CustomSystem:Require( )
------------------------------------------------------------
--[[
  This function should be used to set up Async precache calls at the beginning of the gameplay.

  In this function, place all of your PrecacheItemByNameAsync and PrecacheUnitByNameAsync.  These calls will be made
  after all players have loaded in, but before they have selected their heroes. PrecacheItemByNameAsync can also
  be used to precache dynamically-added datadriven abilities instead of items.  PrecacheUnitByNameAsync will 
  precache the precache{} block statement of the unit and all precache{} block statements for every Ability# 
  defined on the unit.

  This function should only be called once.  If you want to/need to precache more items/abilities/units at a later
  time, you can call the functions individually (for example if you want to precache units in a new wave of
  holdout).

  This function should generally only be used if the Precache() function in addon_game_mode.lua is not working.
]]
function ccc:PostLoadPrecache()
  DebugPrint("[CCC] Performing Post-Load precache")    
  --PrecacheItemByNameAsync("item_example_item", function(...) end)
  --PrecacheItemByNameAsync("example_ability", function(...) end)

  --PrecacheUnitByNameAsync("npc_dota_hero_viper", function(...) end)
  --PrecacheUnitByNameAsync("npc_dota_hero_enigma", function(...) end)
end

--[[
  This function is called once and only once as soon as the first player (almost certain to be the server in local lobbies) loads in.
  It can be used to initialize state that isn't initializeable in Initccc() but needs to be done before everyone loads in.
]]
function ccc:OnFirstPlayerLoaded()
  DebugPrint("[CCC] First Player has loaded")
end

--[[
  This function is called once and only once after all players have loaded into the game, right as the hero selection time begins.
  It can be used to initialize non-hero player state or adjust the hero selection (i.e. force random etc)
]]
function ccc:OnAllPlayersLoaded()
  DebugPrint("[CCC] All Players have loaded into the game")
end

--[[
  This function is called once and only once for every player when they spawn into the game for the first time.  It is also called
  if the player's hero is replaced with a new hero for any reason.  This function is useful for initializing heroes, such as adding
  levels, changing the starting gold, removing/adding abilities, adding physics, etc.

  The hero parameter is the hero entity that just spawned in
]]
function ccc:OnHeroInGame(hero)
  DebugPrint("[CCC] Hero spawned in game for first time -- " .. hero:GetUnitName())

  -- This line for example will set the starting gold of every hero to 500 unreliable gold
  hero:SetGold(0, false)


  -- These lines will create an item and add it to the player, effectively ensuring they start with the item
  -- local item = CreateItem("item_example_item", hero, hero)
  -- hero:AddItem(item)

  --[[ --These lines if uncommented will replace the W ability of any hero that loads into the game
    --with the "example_ability" ability

  local abil = hero:GetAbilityByIndex(1)
  hero:RemoveAbility(abil:GetAbilityName())
  hero:AddAbility("example_ability")]]
end

--[[
  This function is called once and only once when the game completely begins (about 0:00 on the clock).  At this point,
  gold will begin to go up in ticks if configured, creeps will spawn, towers will become damageable etc.  This function
  is useful for starting any game logic timers/thinkers, beginning the first round, etc.
]]
function ccc:OnGameInProgress()
  DebugPrint("[CCC] The game has officially begun")

  Timers:CreateTimer(30, -- Start this timer 30 game-time seconds later
    function()
      DebugPrint("This function is called 30 seconds after the game begins, and every 30 seconds thereafter")
      return 30.0 -- Rerun this timer every 30 game-time seconds 
    end)
end



-- This function initializes the game mode and is called before anyone loads into the game
-- It can be used to pre-initialize any values/tables that will be needed later
function ccc:Initccc()
  ccc = self
  DebugPrint('[CCC] Starting to load ccc ccc...')
  ccc:_Initccc()
  ------------------------------------------自定义
  --系统初始化
  CustomSystem:System_Init(  )
  --技能脚本初始化
  CustomAbility:Ability_Init()
  --物品脚本初始化
  CustomItem:Item_Init()
  -----------------------------------------------
  -- Commands can be registered for debugging purposes or as functions that can be called by the custom Scaleform UI
--  Convars:RegisterCommand( "command_example", Dynamic_Wrap(ccc, 'ExampleConsoleCommand'), "A console command example", FCVAR_CHEAT )
  DebugPrint('[CCC] Done loading ccc ccc!\n\n')
end
-- This is an example console command
function ccc:ExampleConsoleCommand()
  print( '******* Example Console Command ***************' )
--  local cmdPlayer = Convars:GetCommandClient()
  if cmdPlayer then
    local playerID = cmdPlayer:GetPlayerID()
    if playerID ~= nil and playerID ~= -1 then
      -- Do something here for the player who called this command
      PlayerResource:ReplaceHeroWith(playerID, "npc_dota_hero_viper", 1000, 1000)
    end
  end

  print( '*********************************************' )
end
