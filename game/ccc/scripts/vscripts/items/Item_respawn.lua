function Reincarnation( event )
    local caster = event.caster
    local attacker = event.attacker
    local ability = event.ability
    local casterHP = caster:GetHealth()
    local reincarnate = caster:FindAbilityByName("tauren_chieftain_reincarnation")
    local item_chongsheng = caster:FindAbilityByName("item_chongsheng")

    -- print("item Reincarnate",item_chongsheng,reincarnate)
    if caster.chongsheng or caster:IsIllusion() then
        return
    elseif reincarnate and reincarnate:IsCooldownReady() and reincarnate:GetLevel() >=1 then
        -- event.ability = reincarnate
        -- Reincarnation_ability( event )
        return
    end

    if casterHP == 0  then
        -- print("item Reincarnate")
        -- Variables for Reincarnation
        local reincarnate_time = ability:GetSpecialValueFor( "reincarnate_time" )
        --local casterGold = caster:GetGold()
        local respawnPosition = caster:GetAbsOrigin()
        local itemnum = 0
        local itemname = tostring(event.ability:GetAbilityName())
        
        chongsheng_spend( caster, itemname )

        -- Kill, counts as death for the player but doesn't count the kill for the killer unit
        caster:SetHealth(1)
        caster.chongsheng = true
        caster:ForceKill(true) 

        -- Set the gold back
        --caster:SetGold(casterGold, false)

        -- Set the short respawn time and respawn position
        if caster:IsRealHero() then
            caster:SetTimeUntilRespawn(reincarnate_time) 
            caster:SetRespawnPosition(respawnPosition)
        else
            Timers:CreateTimer(reincarnate_time, function()
                caster:RespawnUnit()
            end)
        end

        ---[[不朽之守护音效特效
        local model = "models/props_gameplay/tombstoneb01.vmdl"
        local grave = Entities:CreateByClassname("prop_dynamic")
        grave:SetModel(model)
        grave:SetAbsOrigin(respawnPosition)

        local particle1 = ParticleManager:CreateParticle( "particles/items_fx/aegis_timer.vpcf", PATTACH_ABSORIGIN_FOLLOW, grave )
        ParticleManager:SetParticleControl(particle1, 0, respawnPosition)
        ParticleManager:SetParticleControl(particle1, 1, Vector(reincarnate_time, 0, 0))

        Timers:CreateTimer(reincarnate_time - 2, function()
            
            StartSoundEventFromPosition("DOTAMusic_Hero.Reincarnate", respawnPosition)
        end)

        Timers:CreateTimer(reincarnate_time, function()
            local particle2 = ParticleManager:CreateParticle( "particles/items_fx/aegis_respawn_timer.vpcf", PATTACH_ABSORIGIN_FOLLOW, caster )
            ParticleManager:SetParticleControl(particle2, 0, respawnPosition)
            grave:RemoveSelf()
        end)
        --]] 

        --[[骷髅王复活特效音效
        local particleName = "particles/units/heroes/hero_skeletonking/wraith_king_reincarnate.vpcf"
        caster.ReincarnateParticle = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN_FOLLOW, caster )
        ParticleManager:SetParticleControl(caster.ReincarnateParticle, 0, respawnPosition)
        ParticleManager:SetParticleControl(caster.ReincarnateParticle, 1, Vector(slow_radius,0,0))

        Timers:CreateTimer(reincarnate_time, function() 
            ParticleManager:DestroyParticle(caster.ReincarnateParticle, false)
            --caster:SetBuybackEnabled(true)
        end)

         
        local model = "models/props_diretide/coffin_animatronic.vmdl"
        local grave = Entities:CreateByClassname("prop_dynamic")
        grave:SetModel(model)
        grave:SetAbsOrigin(respawnPosition)

        local particleName = "particles/units/heroes/hero_skeletonking/skeleton_king_death_bits.vpcf"
        local particle1 = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN, caster )
        ParticleManager:SetParticleControl(particle1, 0, respawnPosition)

        local particleName = "particles/units/heroes/hero_skeletonking/skeleton_king_death_dust.vpcf"
        local particle2 = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN_FOLLOW, caster )
        ParticleManager:SetParticleControl(particle2, 0, respawnPosition)

        local particleName = "particles/units/heroes/hero_skeletonking/skeleton_king_death_dust_reincarnate.vpcf"
        local particle3 = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN_FOLLOW, caster )
        ParticleManager:SetParticleControl(particle3 , 0, respawnPosition)   

        caster:EmitSound("Hero_SkeletonKing.Reincarnate")
        caster:EmitSound("Hero_SkeletonKing.Death")

        Timers:CreateTimer(reincarnate_time, function()
            caster:EmitSound("Hero_SkeletonKing.Reincarnate.Stinger")
            --caster:RespawnUnit()
            grave:RemoveSelf()
            --caster:SetHealth(500) 
                
        end)
        --]]
    end
end 

function chongsheng_spend( caster, itemname )
    for itemSlot = 0, 5, 1 do 
        --print("itemSlot1",itemSlot)
        if caster ~= nil then 
            local Item = caster:GetItemInSlot( itemSlot )  
            if Item ~= nil and Item:GetName() == itemname then
                if  Item:GetCurrentCharges() == 1 then
                    --print("GetCurrentCharges")
                    caster:RemoveItem(Item)
                    --print("RemoveItem")
                    return
                end
            end
        end
    end
    for itemSlot = 0, 5, 1 do
        --print("itemSlot2",itemSlot) 
        if caster ~= nil then 
            local Item = caster:GetItemInSlot( itemSlot )  
            if Item ~= nil and Item:GetName() == itemname then
                if  Item:GetCurrentCharges() == 2 then
                    --print("SetCurrentCharges")
                    Item:SetCurrentCharges(1)
                    return
                end
            end
        end
    end
end

function Reincarnation_ability( event )
    local caster = event.caster
    local attacker = event.attacker
    local ability = event.ability
    local cooldown = ability:GetCooldown(ability:GetLevel() - 1)
    local casterHP = caster:GetHealth()
    local casterMana = caster:GetMana()
    local abilityManaCost = ability:GetManaCost( ability:GetLevel() - 1 )

    -- Change it to your game needs
    -- local respawnTimeFormula = caster:GetLevel() * 4
    -- local reincarnate_time = ability:GetLevelSpecialValueFor("reincarnate_time", (ability:GetLevel() - 1))
    
    -- print("ability Reincarnate",casterHP == 0,ability:IsCooldownReady(),casterMana >= abilityManaCost)
    if casterHP == 0 and ability:IsCooldownReady() and casterMana >= abilityManaCost  then
        -- print("ability Reincarnate")
        -- Variables for Reincarnation
        local reincarnate_time = ability:GetLevelSpecialValueFor( "reincarnate_time", ability:GetLevel() - 1 )
        --local casterGold = caster:GetGold()
        local respawnPosition = caster:GetAbsOrigin()
        
        -- Start cooldown on the passive
        ability:StartCooldown(cooldown)

        -- Kill, counts as death for the player but doesn't count the kill for the killer unit
        caster:SetHealth(1)
        caster.chongsheng_ability = true
        caster.chongsheng = true
        caster:Kill(caster, nil)

        -- Set the gold back
        --caster:SetGold(casterGold, false)

        -- Set the short respawn time and respawn position
        caster:SetTimeUntilRespawn(reincarnate_time) 
        caster:SetRespawnPosition(respawnPosition) 

        -- Particle
        local particleName = "particles/units/heroes/hero_skeletonking/wraith_king_reincarnate.vpcf"
        caster.ReincarnateParticle = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN_FOLLOW, caster )
        ParticleManager:SetParticleControl(caster.ReincarnateParticle, 0, respawnPosition)
        ParticleManager:SetParticleControl(caster.ReincarnateParticle, 1, Vector(500,0,0))
        ParticleManager:SetParticleControl(caster.ReincarnateParticle, 1, Vector(500,500,0))

        -- End Particle after reincarnating
        Timers:CreateTimer(reincarnate_time, function() 
            ParticleManager:DestroyParticle(caster.ReincarnateParticle, false)
        end)

        -- Grave and rock particles
        -- The parent "particles/units/heroes/hero_skeletonking/skeleton_king_death.vpcf" misses the grave model
        local model = "models/props_gameplay/tombstoneb01.vmdl"
        local grave = Entities:CreateByClassname("prop_dynamic")
        grave:SetModel(model)
        grave:SetAbsOrigin(respawnPosition)

        local particleName = "particles/units/heroes/hero_skeletonking/skeleton_king_death_bits.vpcf"
        local particle1 = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN, caster )
        ParticleManager:SetParticleControl(particle1, 0, respawnPosition)

        local particleName = "particles/units/heroes/hero_skeletonking/skeleton_king_death_dust.vpcf"
        local particle2 = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN_FOLLOW, caster )
        ParticleManager:SetParticleControl(particle2, 0, respawnPosition)

        local particleName = "particles/units/heroes/hero_skeletonking/skeleton_king_death_dust_reincarnate.vpcf"
        local particle3 = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN_FOLLOW, caster )
        ParticleManager:SetParticleControl(particle3 , 0, respawnPosition)

        -- End grave after reincarnating
        Timers:CreateTimer(reincarnate_time, function() grave:RemoveSelf() end)     

        -- Sounds
        caster:EmitSound("Hero_SkeletonKing.Reincarnate")
        caster:EmitSound("Hero_SkeletonKing.Death")
        Timers:CreateTimer(reincarnate_time, function()
            caster:EmitSound("Hero_SkeletonKing.Reincarnate.Stinger")
        end)

    elseif casterHP == 0 then
        -- On Death without reincarnation, set the respawn time to the respawn time formula
        -- caster:SetTimeUntilRespawn(respawnTimeFormula)
    end 


end