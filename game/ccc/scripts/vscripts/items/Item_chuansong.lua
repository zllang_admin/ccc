function chuansong( keys )
	local caster = keys.caster
	local point_ori = keys.target_points[1]
	local ori_c   = caster:GetOrigin() 
	local radius = keys.abilityradius
	local point_youjun = ori_c
    local target = keys.target
    local c_owner = caster:GetPlayerOwner() or caster:GetOwner() 
    local c_team = caster:GetTeam()
    
    if target == caster then
        if c_team == DOTA_TEAM_GOODGUYS then
            point_ori = Entities:FindByName(nil, "ent_dota_fountain_good"):GetAbsOrigin()
        elseif c_team == DOTA_TEAM_BADGUYS then
            point_ori = Entities:FindByName(nil, "ent_dota_fountain_bad"):GetAbsOrigin()
        end
    end

	local ability = keys.ability
    local yanchi = ability:GetSpecialValueFor("yanchi") 
    --local yanchi = ability:GetLevelSpecialValueFor("yanchi", (ability:GetLevel() - 1))
  
    local t_target =  DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC 
                   
    local t_team   = DOTA_UNIT_TARGET_TEAM_FRIENDLY
                   --DOTA_UNIT_TARGET_TEAM_ENEMY 
                   --DOTA_UNIT_TARGET_TEAM_BOTH                    
    
    ---[[全扫法
    local youjun = FindUnitsInRadius( c_team, point_ori, caster, 20000, t_team, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_CLOSEST, false )	
	
	if youjun[1] then
		local i = 1
		--不能以飞行单位作为传送点
        while youjun[i]:HasFlyMovementCapability() or youjun[i]:GetClassname() == "npc_dota_ward_base" or youjun[i]:GetClassname() == "npc_dota_ward_base_truesight" or youjun[i]:GetUnitName()=="npc_dota_shadow_shaman_ward_1" do
			i = i + 1
		end
        point_youjun=youjun[i]:GetAbsOrigin()
        if youjun[i]:IsBuilding() and ( point_ori - point_youjun ):Length2D() <=300 then
            
         	point_youjun=point_ori
        end
    end
    --]]
    --[[递进法
    for dist=100,1000000,100 do
    	local youjun = FindUnitsInRadius( c_team, point_ori, caster, dist, t_team, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false )	
		for n=1,#youjun do
			if youjun[n]:IsAncient() or youjun[n]:IsOther()==true then
				youjun[n]=nil
				--print("delete")
			end
		end
		--print(dist,youjun[1])
		if youjun[1] ~=nil then
			local shortest=math.huge
			for _,t in pairs(youjun) do
				local x = t: GetAbsOrigin().x
             	local y = t: GetAbsOrigin().y
             	local len = (point_ori.x-x)*(point_ori.x-x) + (point_ori.y-y)*(point_ori.y-y)
				if   len < shortest  then
                   	shortest = len
                   	point_youjun=t:GetOrigin()
             	end
             	if t:IsBuilding() == true and dist<=100 then
             		point_youjun=point_ori
             	end
			end
			break
		end
    end
    --]]



    
    local ti  = FindUnitsInRadius( c_team, ori_c, caster, radius, t_team, t_target, DOTA_UNIT_TARGET_FLAG_PLAYER_CONTROLLED + DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false )
    --FindUnitsInRadius(int a, Vector b, handle c, float d, int e, int f, int g, int h, bool i) 
    --Finds the units in a given radius with the given flags. 
    --( iTeamNumber, vPosition, hCacheUnit, flRadius, iTeamFilter, iTypeFilter, iFlagFilter, iOrder, bCanGrowCache )
    for _,t in pairs(ti) do          
        local t_owner = t:GetPlayerOwner() or t:GetOwner()   
        if t:HasMovementCapability() and t_owner == c_owner and not t:HasModifier("modifier_queenofpain_qunmoluanwu") and not IsDummyUnit(t) then 
            local ori_r = t:GetOrigin()

            --Timers:CreateTimer(yanchi, function()
            		
            --local ori = Vector(point_youjun.x + ori_r.x - ori_c.x, point_youjun.y + ori_r.y - ori_c.y, point_youjun.z )
            local cha = Vector(ori_r.x-ori_c.x,ori_r.y-ori_c.y,0):Normalized()*200
		    local ori = point_youjun + cha

            --print(t,"2")
            --被吹风状态时被传送不会运行吹风的下降功能
            t.uped = nil

            t:Purge(true,true,false,true,true)
            t:RemoveModifierByName("modifier_ensnare_datadriven")

            t:Interrupt()

            FindClearSpaceForUnit(t, ori, true)
            ProjectileManager:ProjectileDodge(t)

            --drain particle
            local pa = tye or PATTACH_ABSORIGIN_FOLLOW
                    --PATTACH_WORLDORIGIN
                    --PATTACH_OVERHEAD_FOLLOW
            Timers:CreateTimer(yanchi, function()
                local p2 = ParticleManager:CreateParticle( "particles/ch3c/chuansong/lion_spell_mana_drain_demon.vpcf", pa, caster )
                --ParticleManager:SetParticleControlEnt( p2, 0, caster, pa, "attach_hitloc", ori_c, true)
                ParticleManager:SetParticleControl( p2, 0, ori_c )
                ParticleManager:SetParticleControlEnt( p2, 1, t, pa, "attach_hitloc", ori, true)
            
                --ParticleManager:SetParticleControl( p2, 1, ori + Vector(0,0,100) )
                Timers:CreateTimer(0.3, function()
                    ParticleManager:DestroyParticle(p2, false)
                    ParticleManager:ReleaseParticleIndex(p2)
                end)
            end)        
        end
    end
    local p3 = ParticleManager:CreateParticle( "particles/ch3c/chuansong/teleport_end.vpcf", PATTACH_ABSORIGIN_FOLLOW, caster )
    ParticleManager:DestroyParticle(p3, false)
    
    local p4 = ParticleManager:CreateParticle( "particles/econ/events/ti5/teleport_start_l_ti5.vpcf", PATTACH_CUSTOMORIGIN, caster )
    ParticleManager:SetParticleControl( p4, 0, ori_c )

end

starttime={}  --记录每个单位使用道具的时间点

function cd(keys)  --使用传送后运行
    local caster = keys.caster
    if caster:HasModifier("modifier_item_chuansong_cd"  ) then
        starttime[caster] = GameRules:GetGameTime()
        --print(starttime[caster])
        swap_to_item(keys, "item_chuansong","item_chuansong_inactive",keys.cool)
    end
    --swap_to_item(keys,"item_chuansong_inactive", "item_chuansong")
end

function equip(keys)    --装备道具后运行
    local caster = keys.caster
    if caster:HasModifier("modifier_item_chuansong_cd"  ) then
        local timeleft = keys.cool + starttime[caster] - GameRules:GetGameTime()  --剩余的冷却时间
        swap_to_item(keys, "item_chuansong","item_chuansong_inactive",timeleft)
    else 
        swap_to_item(keys,"item_chuansong_inactive","item_chuansong",nil)   --冷却完成
    end
end

function swap_to_item(keys, ItemNameA,ItemNameB,timeleft)   --将所有A道具换为B道具的函数（keys，旧道具，新道具，剩余冷却时间）
    for i=0, 5, 1 do  --Fill all empty slots in the player's inventory with "dummy" items.防止格子位置变化。
        local current_item = keys.caster:GetItemInSlot(i)
        if current_item == nil then
            keys.caster:AddItem(CreateItem("item_dummy", keys.caster, keys.caster))
        end
    end
    
    for i=0, 5, 1 do  --将所有A道具换为B道具
        local current_item = keys.caster:GetItemInSlot(i)
        if current_item ~= nil then
            --print(current_item:GetName())
            if current_item:GetName() == ItemNameA then
                keys.caster:RemoveItem(current_item)
                keys.caster:AddItem(CreateItem(ItemNameB, keys.caster, keys.caster))
                --print(ItemNameA,ItemNameB)
            end
            local newitem = keys.caster:GetItemInSlot(i)  
            if timeleft ~= nil and newitem:GetName() == ItemNameB then   --用剩余的冷却时间设置冷却时间
                --newitem:EndCooldown()
                --print("EndCooldown")
                -- newitem:StartCooldown(timeleft) 
            end
        end
    end

    for i=0, 5, 1 do  --Remove all dummy items from the player's inventory.防止格子位置变化。
        local current_item = keys.caster:GetItemInSlot(i)
        if current_item ~= nil then
            if current_item:GetName() == "item_dummy" then
                keys.caster:RemoveItem(current_item)
            end
        end
    end
end

function  pull( keys )
    local caster = keys.caster
    local ori_c   = caster:GetOrigin() 
    local radius = keys.abilityradius
    local ability = keys.ability
    local c_owner = caster:GetPlayerOwner() or caster:GetOwner() 
    ability.pass = false
    --local yanchi = ability:GetLevelSpecialValueFor("yanchi", (ability:GetLevel() - 1))
       
    local target = keys.target

    local c_team   = caster:GetTeam()

    local ti  = FindUnitsInRadius( c_team, ori_c, caster, radius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC , DOTA_UNIT_TARGET_FLAG_PLAYER_CONTROLLED + DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false )
    --FindUnitsInRadius(int a, Vector b, handle c, float d, int e, int f, int g, int h, bool i) 
    --Finds the units in a given radius with the given flags. 
    --( iTeamNumber, vPosition, hCacheUnit, flRadius, iTeamFilter, iTypeFilter, iFlagFilter, iOrder, bCanGrowCache )
    for _,t in pairs(ti) do
        local t_owner = t:GetPlayerOwner() or t:GetOwner()   
        if t:HasMovementCapability() and t_owner == c_owner and not t:HasModifier("modifier_queenofpain_qunmoluanwu") and not IsDummyUnit(t) then       
            local ori_r = t:GetOrigin()
            local p = ParticleManager:CreateParticle( "particles/ch3c/chuansong/lion_spell_mana_drain_demon.vpcf", PATTACH_CUSTOMORIGIN_FOLLOW, t )
            ParticleManager:SetParticleControl( p, 0, ori_r )
            ParticleManager:SetParticleControl( p, 1, ori_c )
            Timers:CreateTimer(keys.yanchi, function()
                ParticleManager:DestroyParticle(p, false)
                ParticleManager:ReleaseParticleIndex(p)
            end)
        end
    end
end

function OnInterrupted( keys )
    
end

function PassToTarget( keys )
    local caster = keys.caster
    local target = keys.target
    local item = keys.ability
    local player = caster:GetPlayerOwner() or caster:GetOwner() 
    local playerID = player:GetPlayerID()  
    local c_owner = caster:GetPlayerOwner() or caster:GetOwner() 
    local t_owner = target:GetPlayerOwner() or target:GetOwner()   
    if target and IsValidEntity(target) and target:IsAlive() and target.HasInventory and target:HasInventory() and c_owner == t_owner then
        caster:MoveToNPCToGiveItem(target, item)
    else
        SendErrorMessage(playerID, "#dota_hud_error_target_cant_take_items")
        caster:Interrupt()
    end
end