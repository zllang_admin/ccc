function StrengthBook(keys)
	local caster = keys.caster
	local ability = keys.ability
	local bonus = ability:GetSpecialValueFor( "bonus" )
	caster:ModifyStrength(bonus)
end

function AgilityBook(keys)
	local caster = keys.caster
	local ability = keys.ability
	local bonus = ability:GetSpecialValueFor( "bonus" )
	caster:ModifyAgility(bonus)
end

function IntellectBook(keys)
	local caster = keys.caster
	local ability = keys.ability
	local bonus = ability:GetSpecialValueFor( "bonus" )
	caster:ModifyIntellect(bonus)
end

function ExperienceBook(keys)
	local caster = keys.caster
	local ability = keys.ability
	local bonus = ability:GetSpecialValueFor( "bonus" )
	caster:AddExperience(bonus,0,false,false)
end

function LifeBook(keys)
	local caster = keys.caster
	local ability = keys.ability
	local bonus = ability:GetSpecialValueFor( "bonus" )
	if caster.LifeBonusTime == nil then
		caster.LifeBonusTime = 0
	end
	caster.LifeBonusTime = caster.LifeBonusTime + 1

	caster:SetMaxHealth(caster:GetMaxHealth()+bonus*caster.LifeBonusTime)
end

function AllBook(keys)
	local caster = keys.caster
	local ability = keys.ability
	local strength = ability:GetSpecialValueFor( "strength" )
	local agility = ability:GetSpecialValueFor( "agility" )
	local intellect = ability:GetSpecialValueFor( "intellect" )
	caster:ModifyStrength(strength)
	caster:ModifyAgility(agility)
	caster:ModifyIntellect(intellect)
end