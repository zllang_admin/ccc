--文件命名徐修改:Item_shouwei.lua

function shengming( keys )
	local caster = keys.caster
	--[[可叠加版
    local target = keys.target_entities
	for i,unit in pairs(target) do
		if unit:IsAlive() then
			local x = unit:GetMaxHealth()*0.02
			unit:Heal(x,caster)
			local pfxPath = string.format("particles/msg_fx/msg_%s.vpcf",  "heal")
		    local pidx = ParticleManager:CreateParticle(pfxPath, PATTACH_ABSORIGIN_FOLLOW, unit) -- target:GetOwner()
		    local digits = #tostring(math.ceil(x))
		    ParticleManager:SetParticleControl(pidx, 1, Vector(tonumber(0), tonumber(x), tonumber(nil)))
		    ParticleManager:SetParticleControl(pidx, 2, Vector(1.5, digits+1, 0))
		    ParticleManager:SetParticleControl(pidx, 3, Vector(0, 255, 0))
		end
	end
	--]]
	---[[不可叠加版
	local unit = keys.target
	if unit:IsAlive() then
		local x = unit:GetMaxHealth()*0.02
		unit:Heal(x,caster)
		local pfxPath = string.format("particles/msg_fx/msg_%s.vpcf",  "heal")
		local pidx = ParticleManager:CreateParticle(pfxPath, PATTACH_ABSORIGIN_FOLLOW, unit) -- target:GetOwner()
	    local digits = #tostring(math.ceil(x))
	    ParticleManager:SetParticleControl(pidx, 1, Vector(tonumber(0), tonumber(x), tonumber(nil)))
	    ParticleManager:SetParticleControl(pidx, 2, Vector(1.5, digits+1, 0))
	    ParticleManager:SetParticleControl(pidx, 3, Vector(0, 255, 0))
	end
	--]]
end
function mofa( keys )
	local caster = keys.caster
    local target = keys.target_entities
	for i,unit in pairs(target) do
		if unit:IsAlive() then
			local x = unit:GetMaxMana()*0.02
			unit:GiveMana(x)
			local pfxPath = string.format("particles/msg_fx/msg_%s.vpcf",  "heal")
		    local pidx = ParticleManager:CreateParticle(pfxPath, PATTACH_ABSORIGIN_FOLLOW, unit) -- target:GetOwner()
		    local digits = #tostring(math.ceil(x))
		    ParticleManager:SetParticleControl(pidx, 1, Vector(tonumber(0), tonumber(x), tonumber(nil)))
		    ParticleManager:SetParticleControl(pidx, 2, Vector(1.5, digits+1, 0))
		    ParticleManager:SetParticleControl(pidx, 3, Vector(0, 255, 255))
		end
	end
end