function tiaodao( keys )
    local caster = keys.caster
    local point_ori   = keys.target_points[1]
    local ori_c   = caster:GetOrigin() 
    local weiyi = Vector((point_ori.x - ori_c.x), (point_ori.y - ori_c.y), 0 )
    local juli =weiyi:Length2D()
    local min=keys.min
    local max=keys.max
    local ori
    local forward = caster:GetForwardVector()
    if juli == 0 then
        ori = forward*min+ori_c 
    elseif juli<min then
        ori = weiyi*min/juli+ori_c       
    elseif juli>max then
        ori = weiyi*max/juli+ori_c   
    else
        ori = point_ori  
    end
    ProjectileManager:ProjectileDodge(caster)
    FindClearSpaceForUnit(caster, ori, true)
    
end