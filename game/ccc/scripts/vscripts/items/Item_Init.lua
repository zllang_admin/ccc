if CustomItem == nil then CustomItem=class({}) end 

function CustomItem:Item_Precache( context )
	--预加载特效、模型、音效等。
	--例如：
	--PrecacheResource("particle", "particles/econ/generic/generic_aoe_explosion_sphere_1/generic_aoe_explosion_sphere_1.vpcf", context)
  	--PrecacheResource("particle_folder", "particles/test_particle", context)  
	--PrecacheResource("model_folder", "particles/heroes/antimage", context)
	--PrecacheResource("model", "particles/heroes/viper/viper.vmdl", context)
	PrecacheResource( "soundfile", "soundevents/game_sounds_heroes/game_sounds_warlock.vsndevts", context )




end
function CustomItem:Item_Init(  )
	--初始化物品相关脚本,功能等同于addon_game_mode.lua中的Init。
	--例如：
		--监听事件：ListenToGameEvent("player_reconnected", Dynamic_Wrap(ccc, 'OnPlayerReconnect'), self)
		--初始化某函数：Init()
	ListenToGameEvent('dota_item_purchased', Dynamic_Wrap(CustomItem, 'OnItemPurchased'), self)
	ListenToGameEvent('dota_link_clicked', Dynamic_Wrap(CustomItem, 'OnDotaLinkClicked'), self)
	ListenToGameEvent('dota_player_shop_changed', Dynamic_Wrap(CustomItem, 'OnDotaLinkClicked'), self)
	ListenToGameEvent('gameui_activated', Dynamic_Wrap(CustomItem, 'OnDotaLinkClicked'), self)

--	GameRules:GetGameModeEntity():SetExecuteOrderFilter( Dynamic_Wrap( CustomItem, "ExecuteOrderFilter" ), self )

	ListenToGameEvent('npc_spawned', Dynamic_Wrap(CustomItem, 'OnNPCSpawned'), self)


end
function CustomItem:Require( )
	-- 加载脚本文件至主系统，功能等同与在addon_game_mode.lua 中require。
	--例如：require('xxxx.xxx.lua')


	
end
function CustomItem:OnNPCSpawned( event )
	---[[
	hSpawnedUnit = EntIndexToHScript( event.entindex )

	if hSpawnedUnit:IsOwnedByAnyPlayer() then
		if hSpawnedUnit.chongsheng then
            hSpawnedUnit.chongsheng = false
            if not hSpawnedUnit.chongsheng_ability then
            	hSpawnedUnit:SetHealth(500)
            else
            	hSpawnedUnit.chongsheng_ability = false
            end  
		end
		--local newItem = CreateItem( "item_chongsheng", nil, nil )
	    --hPlayerHero:AddItem( newItem )
	end
	--]]
end
function CustomItem:OnItemPurchased(keys)
	-- 当物品购买
	--dprint("OnItemPurchased",keys)
	return true
end
function CustomItem:ExecuteOrderFilter( keys )
	--命令过滤器
	--dprint("ExecuteOrderFilter",keys)
	return true 
end
function CustomItem:OnDotaLinkClicked( keys )
	--dprint("#####################",keys)
end