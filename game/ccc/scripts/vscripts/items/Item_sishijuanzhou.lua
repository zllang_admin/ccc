--[[
	Author: Noya
	Date: 21.01.2015.
	Resurrects units near the caster, using the corpse mechanic.
]]
function sishijuanzhou( event )
	--print("animate")
	local caster = event.caster
	local ability = event.ability
	local player = caster:GetPlayerOwner() or caster:GetOwner()
	local playerID = player:GetPlayerID()
	local team = event.caster:GetTeamNumber()
	local duration = ability:GetLevelSpecialValueFor( "chixu", ability:GetLevel() - 1 )
	local radius = ability:GetLevelSpecialValueFor( "banjing", ability:GetLevel() - 1 )
	local max_units_resurrected = ability:GetLevelSpecialValueFor( "shuliang", ability:GetLevel() - 1 )

	-- Find all corpse entities in the radius and start the counter of units resurrected.
	local targets = Entities:FindAllByNameWithin("npc_dota_creature", caster:GetAbsOrigin(), radius)
	local units_resurrected = 0

	-- Go through the units
	for _, unit in pairs(targets) do
		if units_resurrected < max_units_resurrected and unit.corpse_expiration ~= nil then

			-- The corpse has a unit_name associated.
			local resurected = CreateUnitByName(unit.unit_name, unit:GetAbsOrigin(), true, caster, caster, team)
			resurected:SetControllableByPlayer(playerID, true)

			-- Apply modifiers for the summon properties
			resurected:AddNewModifier(caster, ability, "modifier_kill", {duration = duration})
			resurected:AddNewModifier(caster, ability, "modifier_phased", {duration = 0.03})
			ability:ApplyDataDrivenModifier(caster, resurected, "modifier_item_sishijuanzhou", nil)

			-- Leave no corpses
			resurected.no_corpse = true
			unit:RemoveSelf()

			-- Increase the counter of units resurrected
			units_resurrected = units_resurrected + 1
		end
	end
end
-- Denies casting if no corpses near, with a message
function sishijuanzhouPrecast( event )
	--print("pre")
	--print(CORPSE_MODEL)
	local caster = event.caster
	local ability = event.ability
	local radius = ability:GetLevelSpecialValueFor( "banjing", ability:GetLevel() - 1 )
	local targets = Entities:FindAllByNameWithin("npc_dota_creature", caster:GetAbsOrigin(), radius)
	for _, unit in pairs(targets) do
		if unit:GetUnitName() == "npc_dummy_corpse" then
			return
		end
	end
	local player = caster:GetPlayerOwner() or caster:GetOwner()
	local pID = player:GetPlayerID()
	event.caster:Interrupt()
	SendErrorMessage(pID, "#dota_hud_error_no_corpses")
end