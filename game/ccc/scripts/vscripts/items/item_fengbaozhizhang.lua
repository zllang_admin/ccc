--require("utils/utils_print")
--require('utils/timers')
function fengbaozhizhang_01(keys)  --风暴之杖  无需修改

	local  ThisUsed = FengUsedTimes
	local height = 380
	local chixu=keys.chixu
	local caster=EntIndexToHScript(keys.caster_entindex)
	local target=keys.target


	target:Interrupt()

	--该单位被吹次数
	if target.FengedTimes == nil then
		target.FengedTimes = 0
	end

	--这是第几次被吹
	local ThisUsed = target.FengedTimes + 1

	target:Purge(true,true,false,true,true)
	local target_point =target:GetOrigin()
	target.fengParticle = ParticleManager:CreateParticle("particles/items_fx/cyclone.vpcf", PATTACH_CUSTOMORIGIN, hIceEffect) 	
	    ParticleManager:SetParticleControl(target.fengParticle, 0,target_point)
	    ParticleManager:SetParticleControl(target.fengParticle, 1, target_point)
    fengbaozhizhang_xuanzhuan(target,height,chixu)
    fengbaozhizhang_up(target)
    Timers:CreateTimer(chixu-0.5,function()
    	---如果该计时器所处的吹风次数等于现在的被吹次数，则该计时器有效
    	--print(target:HasModifier("fengbaozhizhang_buff_01"))
    	if ThisUsed == target.FengedTimes then
    		fengbaozhizhang_down(target)
    		return nil
    	end
    end)   	
end
function fengbaozhizhang_xuanzhuan(target,height,chixu)
	-- body
	local now_height = 0
	target.xuanzhuanover = false 
	local forwardvec = target:GetForwardVector()
	local pervec = Vector(0,0,60)
	local origin = target:GetAbsOrigin()
	Timers:CreateTimer(chixu-0.1,function()
		--如果该计时器所处的吹风次数等于现在的被吹次数，则该计时器有效
		if ThisUsed == target.FengedTimes then
			if target.xuanzhuanover == false then
				target.xuanzhuanover = true
			end
		end
	end)
	ang_left =QAngle(0, 80, 0)
    GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("fengbaozhizhang_01"),function()  	 
	    point_rotate = RotatePosition(origin, ang_left, forwardvec)
	    target:SetForwardVector(point_rotate)
	    forwardvec=forwardvec+point_rotate
	    origin = target:GetAbsOrigin()
	    local groundHeight = GetGroundHeight(origin,nil)
	    if origin.z - groundHeight ~= 380 then
	    	--print("fixHeight")
	    	target:SetAbsOrigin(Vector(origin.x,origin.y,groundHeight+380))
	    end
		if target.xuanzhuanover == true then 
			target.xuanzhuanover = nil
			return nil 
		end 
	    return 0.01
    end,0)
end
function fengbaozhizhang_up(target)
	-- body
	local target_ori = target:GetAbsOrigin()
    target:SetAbsOrigin(target_ori+Vector(0,0,380))
    target.uped = true
end
function fengbaozhizhang_down(target)  --耗时0.5秒
	-- body
	if target.fengParticle then
		ParticleManager:DestroyParticle(target.fengParticle,true)
		--print("Destroy")
		target.fengParticle = nil
	end
	if target.xuanzhuanover ~= false or not target.uped then
		return
	end
	local target_ori = target:GetAbsOrigin() 
	local delta = 0
    GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("fengbaozhizhang_02"),function()  	 

	    local target_ori = target:GetAbsOrigin()
	    target:SetAbsOrigin(target_ori-Vector(0,0,36)) 
	    delta=delta + 36
	    if delta>360 then 
	    	target:SetAbsOrigin(GetGroundPosition(target_ori,target))
	    	return nil 
	    end 
	    return 0.02
    end,0)
    target.uped = nil
end

function fengbaozhizhang_end( keys )
	local target = keys.target
	fengbaozhizhang_down(target)
	if target.xuanzhuanover == false then
		target.xuanzhuanover = true
	end
end