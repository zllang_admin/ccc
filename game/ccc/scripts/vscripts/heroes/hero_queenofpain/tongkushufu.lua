function tongkushufu_start( keys )
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	---[[
	local particleName = "particles/units/heroes/hero_shadowshaman/shadowshaman_shackle_2.vpcf"
	local particle = ParticleManager:CreateParticle(particleName, PATTACH_ABSORIGIN_FOLLOW, target)
  	ability.particle = particle
  	ParticleManager:SetParticleControlEnt(particle, 0, caster, PATTACH_POINT_FOLLOW, "attach_hitloc", caster:GetAbsOrigin(), true)
  	ParticleManager:SetParticleControlEnt(particle, 1, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), true)
  	ParticleManager:SetParticleControlEnt(particle, 3, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), true)
  	ParticleManager:SetParticleControlEnt(particle, 4, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), true)
  	ParticleManager:SetParticleControlEnt(particle, 5, caster, PATTACH_POINT_FOLLOW, "attach_attack1", caster:GetAbsOrigin(), true)
  	ParticleManager:SetParticleControlEnt(particle, 6, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), true)
	--]]
end

function tongkushufu_end( keys )
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	
	ParticleManager:DestroyParticle(ability.particle, false)
end

function tongkushufu_interval( keys )
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	local shanghai = ability:GetSpecialValueFor("shanghai")
	local MaxHealth = target:GetMaxHealth()

	local damage = {
            victim = target,
            attacker = caster,
            damage = shanghai / 100 * MaxHealth,
            damage_type = DAMAGE_TYPE_PURE,
            ability = ability
        }
    ApplyDamage(damage)
end