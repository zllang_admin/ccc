function bingfengzhiqiu( keys )
	local caster = keys.caster
	local point = keys.target_points[1]
	local ability = keys.ability
	local casterLoc = caster:GetAbsOrigin()
	local direction = (point - casterLoc):Normalized()
	local jiange = ability:GetSpecialValueFor("jiange")
	local chushijuli = ability:GetSpecialValueFor("chushijuli")
	local shuliang = ability:GetSpecialValueFor("shuliang")
	local bingqiusudu = ability:GetSpecialValueFor("bingqiusudu")
	local binglijuli = ability:GetSpecialValueFor("binglijuli")
	local binglisudu = ability:GetSpecialValueFor("binglisudu")
	local shanghaibanjing = ability:GetSpecialValueFor("shanghaibanjing")
	local boshu = ability:GetSpecialValueFor("boshu")
	local spawnLoc = direction * chushijuli + casterLoc + Vector(0,0,128)
	local angel_cha = 360 / shuliang
	local count = 0

	local bingqiu_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_winter_wyvern/wyvern_splinter_2.vpcf", PATTACH_ABSORIGIN, caster)
	ParticleManager:SetParticleControl(bingqiu_particle, 0, spawnLoc)
	ParticleManager:SetParticleControl(bingqiu_particle, 1, direction * bingqiusudu )

	Timers:CreateTimer(jiange, function()
		spawnLoc = spawnLoc + bingqiusudu * jiange * direction
		local velocityVec = Vector(1,0,0)
		for i=1,shuliang do
			velocityVec = RotatePosition(Vector(0,0,0), QAngle( 0, angel_cha, 0 ), velocityVec)
			-- print(spawnLoc,velocityVec)
			local projectileTable = {
				Ability = ability,
				EffectName = "particles/units/heroes/hero_winter_wyvern/wyvern_splinter_blast_2.vpcf",
				vSpawnOrigin = spawnLoc,
				fDistance = binglijuli,
				fStartRadius = shanghaibanjing,
				fEndRadius = shanghaibanjing,
				Source = caster,
				bHasFrontalCone = false,
				bReplaceExisting = false,
				bProvidesVision = false,
				iUnitTargetTeam = DOTA_UNIT_TARGET_TEAM_ENEMY,
				iUnitTargetType = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
				vVelocity = velocityVec * binglisudu
			}
			ProjectileManager:CreateLinearProjectile( projectileTable )
		end

		local explosion_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_winter_wyvern/wyvern_splinter_2_explosion.vpcf", PATTACH_ABSORIGIN, caster)
		ParticleManager:SetParticleControl(explosion_particle, 1, spawnLoc )
		StartSoundEventFromPosition("Hero_Winter_Wyvern.SplinterBlast.Splinter", spawnLoc)
		
		count = count + 1
		if count >= boshu then
			ParticleManager:DestroyParticle(bingqiu_particle, false)
			return nil
		else
			return jiange
		end
	end)
end