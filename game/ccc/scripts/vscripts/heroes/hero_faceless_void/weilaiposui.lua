function weilaiposui_interval( keys )
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	local shanghai = ability:GetSpecialValueFor("shanghai")
	
	local abilityPoints = target:GetAbilityPoints()

	if not target:HasModifier("modifier_faceless_void_weilaiposui") then
		return
	end

	if not target.weilaiposui_abilitypoints then
		target.weilaiposui_abilitypoints = 0
	end

	local attribute_bonus_level = 0
	if not target.weilaiposui_abilitypoints_particle then
		local pfxPath = "particles/ch3c/fv/shadow_demon_shadow_poison_stackui_3.vpcf"
		target.weilaiposui_abilitypoints_particle = ParticleManager:CreateParticle(pfxPath, PATTACH_OVERHEAD_FOLLOW, target)

		local attribute_bonus_ability = target:FindAbilityByName("attribute_bonus")
		if attribute_bonus_ability then
			attribute_bonus_level = attribute_bonus_ability:GetLevel()
		end
	end

	if abilityPoints > 0 or attribute_bonus_level > 0 then
		-- print(abilityPoints,attribute_bonus_level)
		local damage_times = abilityPoints + attribute_bonus_level

		local damage = {
            victim = target,
            attacker = caster,
            damage = shanghai * damage_times,
            damage_type = DAMAGE_TYPE_PHYSICAL,
            ability = ability
        }
    	ApplyDamage(damage)

    	target.weilaiposui_abilitypoints = target.weilaiposui_abilitypoints + abilityPoints

    	local shiwei = math.floor(target.weilaiposui_abilitypoints / 10)
    	local gewei = target.weilaiposui_abilitypoints % 10
    	-- print(target.weilaiposui_abilitypoints,shiwei,gewei)

    	ParticleManager:SetParticleControl(target.weilaiposui_abilitypoints_particle, 1, Vector(shiwei, gewei, 0))

		local dagon_particle = ParticleManager:CreateParticle("particles/items_fx/dagon_2.vpcf",  PATTACH_ABSORIGIN_FOLLOW, caster)
		ParticleManager:SetParticleControlEnt(dagon_particle, 1, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), false)
		local particle_effect_intensity = (100 * abilityPoints)  --Control Point 2 in Dagon's particle effect takes a number between 400 and 800, depending on its level.
		ParticleManager:SetParticleControl(dagon_particle, 2, Vector(particle_effect_intensity))
		
		caster:EmitSound("DOTA_Item.Dagon.Activate")
		target:EmitSound("DOTA_Item.Dagon5.Target")

		target:SetAbilityPoints(0)

		local modifier = target:FindModifierByName("modifier_abilitypoint")
		if modifier then
			modifier:ForceRefresh()
		end
		
	end
end

function weilaiposui_destroy( keys )
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability

	ParticleManager:DestroyParticle(target.weilaiposui_abilitypoints_particle, false)
	ParticleManager:ReleaseParticleIndex(target.weilaiposui_abilitypoints_particle)
	target.weilaiposui_abilitypoints_particle = nil

	target:SetAbilityPoints(target.weilaiposui_abilitypoints)
	target.weilaiposui_abilitypoints = nil
	
end