function huoyanxuanwo_start( keys )
	local caster = keys.caster
	local point = keys.target_points[1]
	local ability = keys.ability
	local chixu = ability:GetSpecialValueFor("chixu")
	local caster_team = caster:GetTeam()

	ability.point = point

	local particle = ParticleManager:CreateParticle("particles/econ/items/legion/legion_weapon_voth_domosh/legion_duel__2ring_arcana.vpcf", PATTACH_ABSORIGIN, caster)
    ParticleManager:SetParticleControl(particle, 0, point)
    ParticleManager:SetParticleControl(particle, 7, point)

    local dummy = CreateUnitByName("npc_dummy_rocket", point, false, caster, caster, caster_team) 
    ability:ApplyDataDrivenModifier(caster, dummy, "modifier_lina_huoyanxuanwi", nil)
    StartSoundEvent("Hero_Invoker.ChaosMeteor.Impact", dummy)
    StartSoundEvent("Hero_Batrider.Firefly.loop", dummy)
    Timers:CreateTimer(chixu, function()
    	ParticleManager:DestroyParticle(particle, false)
    	dummy:RemoveSelf()
    	-- ability.point = nil
   	end)
end

function huoyanxuanwo_interval( keys )
	local caster = keys.caster
	local ability = keys.ability
	local point = ability.point
	local target = keys.target
	local targetLoc = target:GetAbsOrigin()
	local zhongxin = ability:GetSpecialValueFor("zhongxin")
	local shanghai = ability:GetSpecialValueFor("shanghai")
	local jiange = ability:GetSpecialValueFor("jiange")

	local juli = (point - targetLoc):Length2D()
	-- print("interval",zhongxin,juli,targetLoc,point,jiange,shanghai)
	if juli >= zhongxin then
		if not target:HasModifier("modifier_lina_huoyanxuanwi_3") then
			ability:ApplyDataDrivenModifier(caster, target, "modifier_lina_huoyanxuanwi_3", nil)
		end
		local damage = {
            victim = target,
            attacker = caster,
            damage = shanghai * jiange,
            damage_type = DAMAGE_TYPE_PURE,
            ability = ability
        }
    	ApplyDamage(damage)
	else
		target:RemoveModifierByName("modifier_lina_huoyanxuanwi_3")
	end
end

function huoyanxuanwo_check( keys )
	local caster = keys.caster
	local ability = keys.ability

	if not caster:HasModifier("modifier_lina_huoyanxuanwi_2") then
		caster:RemoveModifierByName("modifier_lina_huoyanxuanwi_3")
	end

end