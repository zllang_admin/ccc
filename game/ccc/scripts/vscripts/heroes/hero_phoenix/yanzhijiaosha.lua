--施法前摇
function yanzhijiaosha_phase( keys )
	local caster = keys.caster
	local ability = keys.ability
	StartAnimation(keys.caster, {duration=0.6, activity=ACT_DOTA_INTRO, rate=1})
	local particleName = "particles/ch3c/phoenix/shadow_demon_disruption_refract_2.vpcf"
	ability.particle = ParticleManager:CreateParticle(particleName, PATTACH_ABSORIGIN_FOLLOW, caster)
   	-- ParticleManager:SetParticleControl(ability.particle, 0, caster:GetAbsOrigin())

end

--开始施法
function yanzhijiaosha_start( keys )
	local caster = keys.caster
	local ability = keys.ability
	local target = keys.target
	local caster_position = caster:GetAbsOrigin()
	local shuliang = ability:GetSpecialValueFor("shuliang")
	local banjing = ability:GetSpecialValueFor("banjing")
	local xuanzhuanbanjing = ability:GetSpecialValueFor("xuanzhuanbanjing")
	local start_height = ability:GetSpecialValueFor("start_height")

	local angel_cha = 360 / shuliang
	ability.yanzhijiaosha_targetsIndex = {}
	ability.yanzhijiaosha_points = {}
	ability.yanzhijiaosha_next_height = {}
	ability.yanzhijiaoshaAngle = 0
	for i=1,shuliang do
		ability.yanzhijiaosha_targetsIndex[i] = nil
		if i == 1 then
			ability.yanzhijiaosha_points[i] = caster_position + Vector(xuanzhuanbanjing,0,0)
		else
			ability.yanzhijiaosha_points[i] = RotatePosition(caster_position, QAngle( 0, angel_cha, 0 ), ability.yanzhijiaosha_points[i-1])
		end
		ability.yanzhijiaosha_next_height[i] = start_height + caster_position.z
	end
	ability.yanzhijiaoshaTime = 0
	ability.yanzhijiaoshaAngle = 0

	caster:SetAbsOrigin(caster_position + Vector(0,0,start_height))

	StartSoundEvent("Hero_Batrider.FlamingLasso.Loop", caster)

	Timers:CreateTimer(4,function()
        if caster:HasModifier("modifier_phoenix_yanzhijiaosha_channel") then
        	StopSoundEvent("Hero_Batrider.FlamingLasso.Loop", caster)
			StartSoundEvent("Hero_Batrider.FlamingLasso.Loop", caster)
        	return 4
        else
       		return nil
       	end
    end)

	StartAnimation(caster, {duration=1.9, activity=ACT_DOTA_CAST_ABILITY_5, rate=0.38})
    Timers:CreateTimer(2,function()
        if caster:HasModifier("modifier_phoenix_yanzhijiaosha_channel") then
        	EndAnimation(caster)
        	StartAnimation(caster, {duration=1.9, activity=ACT_DOTA_CAST_ABILITY_5, rate=0.38})
        	return 2
        else
       		return nil
       	end
    end)


 --    local particleName = "particles/ch3c/phoenix/shadow_demon_disruption_refract_2.vpcf"
	-- ability.particle = ParticleManager:CreateParticle(particleName, PATTACH_ABSORIGIN_FOLLOW, caster)
   	-- ParticleManager:SetParticleControl(ability.particle, 0, caster:GetAbsOrigin())
end

--持续施法循环运行
function yanzhijiaosha_search( keys )
	-- print("search")
	local caster = keys.caster
	local ability = keys.ability
	local caster_position = caster:GetAbsOrigin()
	local jiaodu = ability:GetSpecialValueFor("jiaodu")
	local max_height = ability:GetSpecialValueFor("max_height")
	local min_height = ability:GetSpecialValueFor("min_height")
	local chuizhisudu = ability:GetSpecialValueFor("chuizhisudu")
	local banjing = ability:GetSpecialValueFor("banjing")
	local shuliang = ability:GetSpecialValueFor("shuliang")
	local xuanzhuanbanjing = ability:GetSpecialValueFor("xuanzhuanbanjing")
	local frame = 0.04

	if not ability:IsChanneling() then
		caster:RemoveModifierByName("modifier_phoenix_yanzhijiaosha_channel")
		-- print("RemoveModifier")
		return
	end

	caster_position.z = GetGroundHeight(caster_position, caster)
	local angel_cha = 360 / shuliang
	local angle_frame_cha = jiaodu * frame
	ability.yanzhijiaoshaTime = ability.yanzhijiaoshaTime + frame
	ability.yanzhijiaoshaAngle = ability.yanzhijiaoshaAngle + angle_frame_cha

	local enemies = FindUnitsInRadius( caster:GetTeamNumber(),
                                      caster_position,
                                      nil,
                                      banjing,
                                      DOTA_UNIT_TARGET_TEAM_ENEMY ,
                                      DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
                                      DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
                                      FIND_CLOSEST,
                                      false )

	for i=1,shuliang do
		if ability.yanzhijiaoshaTime >= 0.5 then
			ability.yanzhijiaosha_next_height[i] = RandomInt(min_height, max_height) + caster_position.z
			-- ability.yanzhijiaosha_next_height[i] = 0
			if i == shuliang then
				ability.yanzhijiaoshaTime = 0
			end
		end

		local height_cha = ability.yanzhijiaosha_next_height[i]-ability.yanzhijiaosha_points[i].z
		local direction = Vector(0,0,height_cha):Normalized()

		local move_vector = direction * chuizhisudu * frame
		if math.abs(height_cha) < math.abs(move_vector.z) then
			move_vector.z = height_cha
		end

		if i == 1 then
			ability.yanzhijiaosha_points[i] =  Vector(caster_position.x, caster_position.y+xuanzhuanbanjing, ability.yanzhijiaosha_points[i].z)
			ability.yanzhijiaosha_points[i] = RotatePosition(caster_position, QAngle( 0, ability.yanzhijiaoshaAngle, 0 ), ability.yanzhijiaosha_points[i])	
		else
			local rotateVector = RotatePosition(caster_position, QAngle( 0, angel_cha, 0 ), ability.yanzhijiaosha_points[i-1])
			ability.yanzhijiaosha_points[i] = Vector(rotateVector.x, rotateVector.y, ability.yanzhijiaosha_points[i].z)
		end

		ability.yanzhijiaosha_points[i] = ability.yanzhijiaosha_points[i] + move_vector

		if ability.yanzhijiaosha_targetsIndex[i] == nil then
			for k,enemy in pairs(enemies) do
				if (not enemy:HasModifier("modifier_phoenix_yanzhijiaosha")) and (not IsDummyUnit(enemy)) then
					-- print("ApplyDataDrivenModifier")
					ability:ApplyDataDrivenModifier(caster, enemy, "modifier_phoenix_yanzhijiaosha", {})
					ability.yanzhijiaosha_targetsIndex[i] = enemy:GetEntityIndex()
					enemy.yanzhijiaosha_index = i
					break
				end
			end	
		else
			local target = EntIndexToHScript(ability.yanzhijiaosha_targetsIndex[i])
			if target and IsValidEntity(target) and target:IsAlive() then
				target:SetForwardVector( caster:GetAbsOrigin() - ability.yanzhijiaosha_points[i])
				target:SetAbsOrigin(ability.yanzhijiaosha_points[i])
			else
				ability.yanzhijiaosha_targetsIndex[i] = nil
				return
			end
		end
	end

end

--目标被束缚创造modifier时
function yanzhijiaosha_create( keys )
	-- print("create")
	local caster = keys.target
	local ability = keys.ability
	local target = keys.target
	local caster_position = caster:GetAbsOrigin()
	
	
end

--目标解除束缚销毁modifier时
function yanzhijiaosha_destroy( keys )
	-- print("destroy")
	local caster = keys.target
	local ability = keys.ability
	local target = keys.target

	local forwardvec = caster:GetAbsOrigin() - ability.yanzhijiaosha_points[target.yanzhijiaosha_index]

	ability.yanzhijiaosha_targetsIndex[target.yanzhijiaosha_index] = nil
	target.yanzhijiaosha_index = nil
	-- target:InterruptMotionControllers(true)

	forwardvec = target:GetForwardVector()
	forwardvec = Vector(forwardvec.x,forwardvec.y,0):Normalized()
	target:SetForwardVector(forwardvec)
	FindClearSpaceForUnit(target, target:GetAbsOrigin(), true)

end

--水平运动
function yanzhijiaosha_horizonal( keys )
	-- print("horizonal")
	local caster = keys.caster
	local ability = keys.ability
	local target = keys.target
	local caster_position = caster:GetAbsOrigin()

	if ability.yanzhijiaosha_points and target.yanzhijiaosha_index and ability.yanzhijiaosha_points[target.yanzhijiaosha_index] then
		target:SetForwardVector( caster_position - ability.yanzhijiaosha_points[target.yanzhijiaosha_index])
		target:SetAbsOrigin(ability.yanzhijiaosha_points[target.yanzhijiaosha_index])	
	end
end

--垂直运动
function yanzhijiaosha_vertical( keys )
	-- print("horizonal")
	local caster = keys.caster
	local ability = keys.ability
	local target = keys.target

	if ability.yanzhijiaosha_points and target.yanzhijiaosha_index and ability.yanzhijiaosha_points[target.yanzhijiaosha_index] then
		target:SetAbsOrigin(ability.yanzhijiaosha_points[target.yanzhijiaosha_index])
	end
end

--持续施法结束
function yanzhijiaosha_end( keys )
	-- print("end")
	local caster = keys.caster
	local ability = keys.ability

	for k,entityIndex in pairs(ability.yanzhijiaosha_targetsIndex) do
		local unit = EntIndexToHScript(entityIndex)
		unit:RemoveModifierByName("modifier_phoenix_yanzhijiaosha")
	end

	FindClearSpaceForUnit(caster, caster:GetAbsOrigin(), true)
	StopSoundEvent("Hero_Batrider.FlamingLasso.Loop", caster)
	-- ParticleManager:DestroyParticle(ability.particle, false)
	---[[
	ability.yanzhijiaosha_targetsIndex = nil
	-- ability.yanzhijiaosha_points = nil
	ability.yanzhijiaoshaTime = 0
	-- ability.yanzhijiaosha_next_height = nil
	ability.yanzhijiaoshaAngle = 0
	--]]
end