if Filter == nil then Filter=class({}) end

function Filter:Init()
	GameRules:GetGameModeEntity():SetExecuteOrderFilter( Dynamic_Wrap( Filter, "ExecuteOrderFilter" ), self )
	GameRules:GetGameModeEntity():SetDamageFilter( Dynamic_Wrap(Filter, "FilterDamage" ), self )
	GameRules:GetGameModeEntity():SetModifyExperienceFilter( Dynamic_Wrap(Filter, "FilterExperience" ), self )
	GameRules:GetGameModeEntity():SetModifyGoldFilter( Dynamic_Wrap(Filter, "FilterGold" ), self )
	-- GameRules:GetGameModeEntity():SetModifierGainedFilter( Dynamic_Wrap(Filter, "FilterModifier" ), self )
	LinkLuaModifier( "modifier_orb_range", "sys/modifiers/modifier_orb_range",LUA_MODIFIER_MOTION_NONE )
	CustomGameEventManager:RegisterListener("move_to_invulnerable",Dynamic_Wrap(Filter,"MoveToInvulnerable"))
	-- CustomGameEventManager:RegisterListener("pass_chuansong",Dynamic_Wrap(Filter,"SetPassChuansong"))
	
end

function Filter:ExecuteOrderFilter( keys )
	-- print("ExecuteOrderFilter")
	-- PrintTable(keys)
	-- print("DOTA_UNIT_ORDER_RADAR",DOTA_UNIT_ORDER_RADAR)
	if keys.order_type==DOTA_UNIT_ORDER_GLYPH then
		return false
	elseif keys.order_type==DOTA_UNIT_ORDER_RADAR then
		return false
	elseif keys.order_type==DOTA_UNIT_ORDER_ATTACK_TARGET then 
		local target=EntIndexToHScript(keys.entindex_target)
		local attackable_units = {}
		local all_attackable = true
		for k,v in pairs(keys.units) do
			local attacker=EntIndexToHScript(v)
			local player = attacker:GetPlayerOwner() or attacker:GetOwner()
			local playerID
			if player and player:IsPlayer() then
				playerID = player:GetPlayerID()
			end
			if not FiltShenShengHuJia( attacker,target ) then
				if playerID and all_attackable then
					SendErrorMessage(playerID, "#error_attack_building_with_divine_shield")
				end
				all_attackable = false
			elseif not FiltOrb( attacker,target ) then
				if playerID and all_attackable then
					SendErrorMessage(playerID, "#error_melee_attack_flying_unit")
				end
				all_attackable = false
			else
				table.insert(attackable_units, attacker)
			end
		end
		if not all_attackable then
			for k,v in pairs(attackable_units) do
				v:MoveToTargetToAttack(target)
			end
			return false
		end
	elseif keys.order_type==DOTA_UNIT_ORDER_MOVE_TO_TARGET then
		local target=EntIndexToHScript(keys.entindex_target)
		for k,v in pairs(keys.units) do
			local mover=EntIndexToHScript(v)
			local loadAbility = mover:FindAbilityByName("flying_mount_load")
			if loadAbility then
				mover:CastAbilityOnTarget(target, loadAbility, mover:GetPlayerOwnerID())
				return false
			end
		end
	end
	return true
end

function Filter:FilterDamage(filterTable)
	local attacker_index = filterTable["entindex_attacker_const"]
	if not attacker_index then
		return true
	end
	local attacker = EntIndexToHScript( attacker_index )
	local damagetype = filterTable["damagetype_const"]
	if attacker:IsHero() then
    	if damagetype == DAMAGE_TYPE_MAGICAL or damagetype == DAMAGE_TYPE_PURE then
    	filterTable["damage"] = filterTable["damage"]/(1+((attacker:GetIntellect()/16)/100))
    	end
	end
	return true
end

function Filter:FilterExperience(filterTable)

	return false
end

function Filter:FilterGold(filterTable)
	-- print("FilterGold")
	-- PrintTable(filterTable)
	local reason = filterTable.reason_const
	local reliable = filterTable.reliable
	local gold = filterTable.gold
	if reason == 11 and reliable == 1 then
		return false
	elseif reason == 12 and reliable == 1 then
		return false
	elseif reason == 13 then
		return true
	else
		return true
	end
end

function IsHaveOrb( unit )
	if unit:HasModifier("modifier_item_bingqiu") or unit:HasModifier("modifier_item_dianqiu") or unit:HasModifier("modifier_item_huoqiu") or unit:HasModifier("modifier_item_huodao") then
		return true
	else
		return false
	end
end

function FiltOrb( attacker,target )
	if IsValidEntity(target) and target:GetClassname()~="dota_item_drop" and target.HasFlyMovementCapability and target:HasFlyMovementCapability() then
		if attacker:GetAttackCapability()==DOTA_UNIT_CAP_MELEE_ATTACK then
			if IsHaveOrb(attacker) then
				attacker:SetAttackCapability(DOTA_UNIT_CAP_RANGED_ATTACK)
				attacker.IsMelee = true
				attacker:AddNewModifier(attacker, nil, "modifier_orb_range", {})
			else 
				return false
			end
		else
			if attacker.IsMelee and not IsHaveOrb(attacker) then
				attacker:SetAttackCapability(DOTA_UNIT_CAP_MELEE_ATTACK)
				attacker:RemoveModifierByName("modifier_orb_range")
			end
		end
	else
		if attacker.IsMelee then
			attacker:SetAttackCapability(DOTA_UNIT_CAP_MELEE_ATTACK)
			attacker:RemoveModifierByName("modifier_orb_range")
		end
	end
	return true
end

function FiltShenShengHuJia( attacker,target )
	if attacker:HasModifier("modifier_shengqishi_shenshenghujia") then
		if target.IsTower and target:IsTower() then
			return false
		elseif target.GetUnitName and target:GetUnitName()=="npc_building_shangdianshouwei" then
			return false
		end
	end
	return true
end

function Filter:MoveToInvulnerable(keys)
	-- print("MoveToInvulnerable")
	-- PrintTable(keys)
	local selectedEntities = keys.selectedEntities
	local target = EntIndexToHScript(keys.targetIndex)
	-- Timers:CreateTimer(1,function()
	if not target then
		return
	end
	for k,v in pairs(selectedEntities) do
		ExecuteOrderFromTable({
    		UnitIndex=   v,
      		OrderType=   DOTA_UNIT_ORDER_MOVE_TO_TARGET,
      		TargetIndex= target:GetEntityIndex(),
    	})
	end
	-- end)
end


function Filter:SetPassChuansong(keys)
	-- print("SetPassChuansong")
	-- PrintTable(keys)
	-- local mainSelected = keys.mainSelected
	-- local target = EntIndexToHScript(keys.targetIndex)
	-- for k,v in pairs(selectedEntities) do
	-- 	ExecuteOrderFromTable({
 --    		UnitIndex=   v,
 --      		OrderType=   DOTA_UNIT_ORDER_MOVE_TO_TARGET,
 --      		TargetIndex= target:GetEntityIndex(),
 --    	})
	-- end
end

function Filter:FilterModifier( keys )
	print("FilterModifier")
	PrintTable(keys)
	return true
end

