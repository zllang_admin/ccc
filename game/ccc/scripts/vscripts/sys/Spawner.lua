-- Copyright (c) 2015-2016 野猪大大(zllang)
--================================================================================================================
--=======================================||刷怪器||===============================================================
--================================================================================================================
if Spawner==nil then Spawner=class({}) end
--======================================||单位数据||===================================================
--[[战场野怪出生点、初始路径点]]
Spawner.WavePoint={
  --{出生点名字,初始路径点名字}
	{"Light_mid_1","Light_mid_2",DOTA_TEAM_GOODGUYS},
	{"Light_up_1","Light_up_2",DOTA_TEAM_GOODGUYS},
	{"Light_down_1","Light_down_2",DOTA_TEAM_GOODGUYS},
	{"Dark_mid_1","Dark_mid_2",DOTA_TEAM_BADGUYS},
	{"Dark_up_1","Dark_up_2",DOTA_TEAM_BADGUYS},
	{"Dark_down_1","Dark_down_2",DOTA_TEAM_BADGUYS},
}
--[[线上小兵]]
Spawner.Wave={
--  {"怪物名字",开始刷怪时间,间隔时间,数量,地点}
	--{UnitName="npc_dota_creep_jinlong",StartTime=35,DeltaTime=1150,Num=2},
---[[光明中路
	{UnitName="npc_dota_creep_goodguys_melee",StartTime=53,DeltaTime=53,Num=5,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_goodguys_ranged_upgraded_mega",StartTime=63,DeltaTime=63,Num=5,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_huoqiang",StartTime=73,DeltaTime=73,Num=3,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_nvlieshao",StartTime=83,DeltaTime=83,Num=3,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_paijipaoxiaodui",StartTime=123,DeltaTime=123,Num=2,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_jiaoyingshouqishi",StartTime=133,DeltaTime=133,Num=2,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_qishi",StartTime=143,DeltaTime=143,Num=2,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_deluyi",StartTime=153,DeltaTime=153,Num=2,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_qimeila",StartTime=200,DeltaTime=200,Num=2,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_dayuren",StartTime=280,DeltaTime=280,Num=2,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_honglong",StartTime=570,DeltaTime=570,Num=2,WavePosition="Light_mid_1"},
	{UnitName="npc_dota_creep_jinlong",StartTime=1150,DeltaTime=1150,Num=2,WavePosition="Light_mid_1"},
	-- --]]
---[[光明上路
	{UnitName="npc_dota_creep_goodguys_melee",StartTime=53,DeltaTime=53,Num=4,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_goodguys_ranged_upgraded_mega",StartTime=63,DeltaTime=63,Num=4,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_huoqiang",StartTime=73,DeltaTime=73,Num=2,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_nvlieshao",StartTime=83,DeltaTime=83,Num=2,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_paijipaoxiaodui",StartTime=123,DeltaTime=123,Num=1,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_jiaoyingshouqishi",StartTime=133,DeltaTime=133,Num=1,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_qishi",StartTime=143,DeltaTime=143,Num=1,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_deluyi",StartTime=153,DeltaTime=153,Num=1,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_qimeila",StartTime=200,DeltaTime=200,Num=1,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_dayuren",StartTime=280,DeltaTime=280,Num=1,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_honglong",StartTime=570,DeltaTime=570,Num=1,WavePosition="Light_up_1"},
	{UnitName="npc_dota_creep_jinlong",StartTime=1150,DeltaTime=1150,Num=1,WavePosition="Light_up_1"},
	-- --]]
---[[光明下路
	{UnitName="npc_dota_creep_goodguys_melee",StartTime=53,DeltaTime=53,Num=4,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_goodguys_ranged_upgraded_mega",StartTime=63,DeltaTime=63,Num=4,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_huoqiang",StartTime=73,DeltaTime=73,Num=2,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_nvlieshao",StartTime=83,DeltaTime=83,Num=2,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_paijipaoxiaodui",StartTime=123,DeltaTime=123,Num=1,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_jiaoyingshouqishi",StartTime=133,DeltaTime=133,Num=1,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_qishi",StartTime=143,DeltaTime=143,Num=1,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_deluyi",StartTime=153,DeltaTime=153,Num=1,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_qimeila",StartTime=200,DeltaTime=200,Num=1,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_dayuren",StartTime=280,DeltaTime=280,Num=1,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_honglong",StartTime=570,DeltaTime=570,Num=1,WavePosition="Light_down_1"},
	{UnitName="npc_dota_creep_jinlong",StartTime=1150,DeltaTime=1150,Num=1,WavePosition="Light_down_1"},
	--]]	
---[[黑暗中路
	{UnitName="npc_dota_creep_badguys_melee_diretide",StartTime=53,DeltaTime=53,Num=5,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_badguys_melee_upgraded_mega",StartTime=63,DeltaTime=63,Num=5,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_xuejuemo",StartTime=73,DeltaTime=73,Num=3,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_lueduozhe",StartTime=83,DeltaTime=83,Num=3,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_fensuizhe",StartTime=123,DeltaTime=123,Num=2,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_shixianggui",StartTime=133,DeltaTime=133,Num=2,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_zengwu",StartTime=143,DeltaTime=143,Num=2,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_niutouren",StartTime=153,DeltaTime=153,Num=2,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_binglong",StartTime=200,DeltaTime=200,Num=2,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_keduoshou",StartTime=280,DeltaTime=280,Num=2,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_lvlong",StartTime=570,DeltaTime=570,Num=2,WavePosition="Dark_mid_1"},
	{UnitName="npc_dota_creep_lanlong",StartTime=1150,DeltaTime=1150,Num=2,WavePosition="Dark_mid_1"},
	--]]
---[[黑暗上路
	{UnitName="npc_dota_creep_badguys_melee_diretide",StartTime=53,DeltaTime=53,Num=4,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_badguys_melee_upgraded_mega",StartTime=63,DeltaTime=63,Num=4,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_xuejuemo",StartTime=73,DeltaTime=73,Num=2,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_lueduozhe",StartTime=83,DeltaTime=83,Num=2,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_fensuizhe",StartTime=123,DeltaTime=123,Num=1,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_shixianggui",StartTime=133,DeltaTime=133,Num=1,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_zengwu",StartTime=143,DeltaTime=143,Num=1,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_niutouren",StartTime=153,DeltaTime=153,Num=1,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_binglong",StartTime=200,DeltaTime=200,Num=1,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_keduoshou",StartTime=280,DeltaTime=280,Num=1,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_lvlong",StartTime=570,DeltaTime=570,Num=1,WavePosition="Dark_up_1"},
	{UnitName="npc_dota_creep_lanlong",StartTime=1150,DeltaTime=1150,Num=1,WavePosition="Dark_up_1"},
    --]]
---[[黑暗下路
	{UnitName="npc_dota_creep_badguys_melee_diretide",StartTime=53,DeltaTime=53,Num=4,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_badguys_melee_upgraded_mega",StartTime=63,DeltaTime=63,Num=4,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_xuejuemo",StartTime=73,DeltaTime=73,Num=2,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_lueduozhe",StartTime=83,DeltaTime=83,Num=2,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_fensuizhe",StartTime=123,DeltaTime=123,Num=1,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_shixianggui",StartTime=133,DeltaTime=133,Num=1,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_zengwu",StartTime=143,DeltaTime=143,Num=1,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_niutouren",StartTime=153,DeltaTime=153,Num=1,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_binglong",StartTime=200,DeltaTime=200,Num=1,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_keduoshou",StartTime=280,DeltaTime=280,Num=1,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_lvlong",StartTime=570,DeltaTime=570,Num=1,WavePosition="Dark_down_1"},
	{UnitName="npc_dota_creep_lanlong",StartTime=1150,DeltaTime=1150,Num=1,WavePosition="Dark_down_1"},
	--]]
}
--[[副本野怪]]
Spawner.Creep={
--  {"怪物名字",出生点名字,是否重生,开始时间,重生间隔,朝向}  注:若不重生，则DeltaTime=0
---[[
	{UnitName="npc_dota_creep_heilong",SpawnerPoint="Mid_dragon",Respawn=false,StartTime=1,DeltaTime=0,Num=1},
	{UnitName="npc_building_gaodengjinglingfangyuta",SpawnerPoint="Mid_dragon",Respawn=false,StartTime=1.5,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_heifuyou",SpawnerPoint="Up_dragon",Respawn=false,StartTime=2,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_heifuyou",SpawnerPoint="Down_dragon",Respawn=false,StartTime=2.5,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_nitankuilei",SpawnerPoint="Light_stone",Respawn=false,StartTime=0,DeltaTime=0,Num=1,Team=DOTA_TEAM_GOODGUYS},
	{UnitName="npc_dota_creep_nitankuilei",SpawnerPoint="Dark_stone",Respawn=false,StartTime=0,DeltaTime=0,Num=1,Team=DOTA_TEAM_BADGUYS},
	{UnitName="npc_dota_creep_chailangshouwangzhe",SpawnerPoint="15_boss",Respawn=false,StartTime=15,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_juxingkulouzhanshi",SpawnerPoint="15_melee1",Respawn=false,StartTime=15.1,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_juxingkulouzhanshi",SpawnerPoint="15_melee2",Respawn=false,StartTime=15.2,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_juxingkulouzhanshi",SpawnerPoint="15_melee3",Respawn=false,StartTime=15.3,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_juxingkulouzhanshi",SpawnerPoint="15_melee4",Respawn=false,StartTime=15.4,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_juxingkulouzhanshi",SpawnerPoint="15_melee5",Respawn=false,StartTime=15.5,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_juxingkulouzhanshi",SpawnerPoint="15_melee6",Respawn=false,StartTime=15.6,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_huoyangongjianshou",SpawnerPoint="15_ranged1",Respawn=false,StartTime=15.7,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_huoyangongjianshou",SpawnerPoint="15_ranged2",Respawn=false,StartTime=15.8,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_huoyangongjianshou",SpawnerPoint="15_ranged3",Respawn=false,StartTime=15.9,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_huoyangongjianshou",SpawnerPoint="15_ranged4",Respawn=false,StartTime=16.0,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_huoyangongjianshou",SpawnerPoint="15_ranged5",Respawn=false,StartTime=16.1,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_huoyangongjianshou",SpawnerPoint="15_ranged6",Respawn=false,StartTime=16.2,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_huoyangongjianshou",SpawnerPoint="15_ranged7",Respawn=false,StartTime=16.3,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_huoyangongjianshou",SpawnerPoint="15_ranged8",Respawn=false,StartTime=16.4,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_kongbushuangdongzhilang",SpawnerPoint="40_1",Respawn=false,StartTime=40,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_kongbushuangdongzhilang",SpawnerPoint="40_2",Respawn=false,StartTime=40.1,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_kongbushuangdongzhilang",SpawnerPoint="40_3",Respawn=false,StartTime=40.2,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_kongbushuangdongzhilang",SpawnerPoint="40_4",Respawn=false,StartTime=40.3,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_kongbushuangdongzhilang",SpawnerPoint="40_5",Respawn=false,StartTime=40.4,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_kongbushuangdongzhilang",SpawnerPoint="40_6",Respawn=false,StartTime=40.5,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_kongbushuangdongzhilang",SpawnerPoint="40_7",Respawn=false,StartTime=40.6,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_duyezhizhu",SpawnerPoint="20_boss",Respawn=false,StartTime=20,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_yanshikuilei",SpawnerPoint="20_melee1",Respawn=false,StartTime=20.1,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_yanshikuilei",SpawnerPoint="20_melee2",Respawn=false,StartTime=20.2,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_yanshikuilei",SpawnerPoint="20_melee3",Respawn=false,StartTime=20.3,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_yanshikuilei",SpawnerPoint="20_melee4",Respawn=false,StartTime=20.4,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_yanshikuilei",SpawnerPoint="20_melee5",Respawn=false,StartTime=20.5,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shenyuanlingzhuyouling",SpawnerPoint="20_ranged1",Respawn=false,StartTime=20.6,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shenyuanlingzhuyouling",SpawnerPoint="20_ranged2",Respawn=false,StartTime=20.7,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shenyuanlingzhuyouling",SpawnerPoint="20_ranged3",Respawn=false,StartTime=20.8,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shenyuanlingzhuyouling",SpawnerPoint="20_ranged4",Respawn=false,StartTime=20.9,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_huimieshouwei",SpawnerPoint="30_boss",Respawn=false,StartTime=30,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shenyuanlingzhuyouling",SpawnerPoint="30_shenyuanlingzhuyouling1",Respawn=false,StartTime=30.1,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shenyuanlingzhuyouling",SpawnerPoint="30_shenyuanlingzhuyouling2",Respawn=false,StartTime=30.2,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shenyuanlingzhuyouling",SpawnerPoint="30_shenyuanlingzhuyouling3",Respawn=false,StartTime=30.3,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shenyuanlingzhuyouling",SpawnerPoint="30_shenyuanlingzhuyouling4",Respawn=false,StartTime=30.4,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_kuangbaoyuansu",SpawnerPoint="30_kuangbaoyuansu1",Respawn=false,StartTime=30.5,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_kuangbaoyuansu",SpawnerPoint="30_kuangbaoyuansu2",Respawn=false,StartTime=30.6,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_kuangbaoyuansu",SpawnerPoint="30_kuangbaoyuansu3",Respawn=false,StartTime=30.7,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_gongchengkuilei",SpawnerPoint="30_gongchengkuilei1",Respawn=false,StartTime=30.8,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_gongchengkuilei",SpawnerPoint="30_gongchengkuilei2",Respawn=false,StartTime=30.9,DeltaTime=0,Num=1},
	{UnitName="npc_dota_warlock_golem_1",SpawnerPoint="30_golem1",Respawn=false,StartTime=31.0,DeltaTime=0,Num=1},
	{UnitName="npc_dota_warlock_golem_1",SpawnerPoint="30_golem2",Respawn=false,StartTime=31.1,DeltaTime=0,Num=1},
	{UnitName="npc_dota_warlock_golem_1",SpawnerPoint="30_golem3",Respawn=false,StartTime=31.2,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_chailangcike",SpawnerPoint="30_chailangcike1",Respawn=false,StartTime=31.3,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_chailangcike",SpawnerPoint="30_chailangcike2",Respawn=false,StartTime=31.4,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shirenguishouling",SpawnerPoint="30_shirenguishouling1",Respawn=false,StartTime=31.5,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shirenguishouling",SpawnerPoint="30_shirenguishouling2",Respawn=false,StartTime=31.6,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shirenguishouling",SpawnerPoint="30_shirenguishouling3",Respawn=false,StartTime=31.7,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shirenguishouling",SpawnerPoint="30_shirenguishouling4",Respawn=false,StartTime=31.8,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shirenguimofashi",SpawnerPoint="30_shirenguimofashi1",Respawn=false,StartTime=31.9,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_heianwuzhe",SpawnerPoint="30_heianwuzhe1",Respawn=false,StartTime=32.0,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_heianwuzhe",SpawnerPoint="30_heianwuzhe2",Respawn=false,StartTime=32.1,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_heianwuzhe",SpawnerPoint="30_heianwuzhe3",Respawn=false,StartTime=32.2,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_heianwuzhe",SpawnerPoint="30_heianwuzhe4",Respawn=false,StartTime=32.3,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_heianwuzhe",SpawnerPoint="30_heianwuzhe5",Respawn=false,StartTime=32.4,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shumoyingzimushi",SpawnerPoint="30_shumoyingzimushi1",Respawn=false,StartTime=32.5,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shumoyingzimushi",SpawnerPoint="30_shumoyingzimushi2",Respawn=false,StartTime=32.6,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shumoyingzimushi",SpawnerPoint="30_shumoyingzimushi3",Respawn=false,StartTime=32.7,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shumoyingzimushi",SpawnerPoint="30_shumoyingzimushi4",Respawn=false,StartTime=32.8,DeltaTime=0,Num=1},
	{UnitName="npc_dota_creep_shumoyingzimushi",SpawnerPoint="30_shumoyingzimushi5",Respawn=false,StartTime=32.9,DeltaTime=0,Num=1},
	{UnitName="npc_boss_long",SpawnerPoint="shengli_1",Respawn=true,StartTime=45,DeltaTime=600,Num=1},
	{UnitName="npc_boss_hongmao",SpawnerPoint="shengli_2",Respawn=true,StartTime=46,DeltaTime=600,Num=1},
	{UnitName="npc_boss_tumao",SpawnerPoint="shengli_3",Respawn=true,StartTime=47,DeltaTime=600,Num=1},
	{UnitName="npc_boss_lanmao",SpawnerPoint="shengli_4",Respawn=true,StartTime=48,DeltaTime=600,Num=1},
	--]]
}
--======================================||刷怪过程||===================================================
--[[当前时间]]
Spawner.CurrentTime=nil
--[[任务]]
Spawner.Task=heap(function ( a,b )return true end)
--[[思考集合]]
Spawner.Tick={}
--单帧任务执行数量限制
Spawner.FrameNum=1
--[[初始化刷怪器]]
num=1
function Spawner:Init()
	print("Spawner:Init()")
	self:Think()
	self:InitTask()
	--[[添加单位死亡监听]]
	ListenToGameEvent("entity_killed", Dynamic_Wrap(Spawner, "OnUnitKilled"), self)
end
--[[刷怪逻辑]]
function Spawner:InitTask()
	---[[初始化小兵刷怪
	for k,v in pairs(self.WavePoint) do
		--保留名字判断一波兵出生地点名字是否相同，将地点存到[4]
		--self.WavePoint[k][1]=Entities:FindByName(nil,v[1]):GetAbsOrigin()
		self.WavePoint[k][2]=Entities:FindByName(nil,v[2])
		self.WavePoint[k][4]=Entities:FindByName(nil,v[1]):GetAbsOrigin()
	end
	for _,v in pairs(self.Wave) do 
		for i=1,v.Num do
			self:AddThink(v,function(TaskTime)
					if Spawner.CurrentTime>=TaskTime then 
						return true
					else 
						return false
					end
				end,
				function (Content,c)
					for i,j in pairs(Spawner.WavePoint) do
						if v.WavePosition==j[1] then
							Content.Position=j[4]
							Content.InitPoint=j[2]
							Content.Team=j[3]							
							local unit=Spawner:Spawn(Content)
							unit.Content=Content
							unit.Wave=true
							unit.Onkilled=function()
								--c.TaskTime=c.TaskTime+c.Delta
							end						
						end
					end
				end)
		end
		
	end
	--]]
	--[[初始化副本野怪刷怪]]
	for _,v in pairs(self.Creep) do
		--print("SpawnerPoint",v,v.SpawnerPoint)
		v.SpawnerPoint=Entities:FindByName(nil,v.SpawnerPoint):GetAbsOrigin()
		self:AddThink(v,
			function(TaskTime)
				if Spawner.CurrentTime>=TaskTime then 
					return true
				else 
					return false
				end
			end,
			function ( Content ,c)
				Content.Position=Content.SpawnerPoint
				Content.InitPoint=nil
				Content.Team=Content.Team or DOTA_TEAM_NEUTRALS
				if Content.Forward == nil then
					Content.Forward=Vector(0,-1,0)
				end
				local unit=Spawner:Spawn(Content)
				unit.Content=Content
				unit.Creep=true
				unit.Onkilled=function()
				-- print("c.Respawn",c.Respawn)
				if c.Respawn then
					c.added = false
					c.TaskTime=Spawner.CurrentTime+c.Delta
					-- table.insert(self.Tick,c)
				end
				end			
			end)

	end
end
--[[刷怪]]
function Spawner:Spawn(Content)
	local unit = CreateUnitByName(Content.UnitName, Content.Position, false, nil, nil,Content.Team)
    if unit:HasModifier("modifier_invulnerable") then
    	unit:RemoveModifierByName("modifier_invulnerable")
    end
    -- print("Spawner:Spawn",Spawner.MagicImmuneTeam,Content.Team)
    if Spawner.MagicImmuneTeam == Content.Team then
    	local item = CreateItem("item_shenshengxunzhang", nil, nil)
    	item:ApplyDataDrivenModifier(unit, unit, "modifier_item_shenshengxunzhang", {})
    	item:RemoveSelf()
    end
    if Content.Forward then
    	unit:SetForwardVector(Content.Forward)
    end
    FindClearSpaceForUnit(unit,unit:GetAbsOrigin(), false) 
    if Content.InitPoint then
    	Timers:CreateTimer(0.1,function()
    		CreepAI:Start( unit,Content.InitPoint:GetAbsOrigin() )
    	end)
    end
    return unit
end
--======================================||通用任务框架||===================================================
--[[主循环]]
function Spawner:Think()
	GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("SpawnerTick"),function()
		--[[更新时间]]
		if GameRules:State_Get() == DOTA_GAMERULES_STATE_PRE_GAME or GameRules:State_Get() == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then		
			Spawner.CurrentTime=GameRules:GetDOTATime(false,true) + PRE_GAME_TIME -  COUNT_DOWN --7秒为小精灵不可行动时间
		else
			Spawner.CurrentTime=0
		end
		--print("Time",Spawner.CurrentTime)
		--[[执行任务]]
		Spawner:DoTask()
		return FrameTime()
	end,0)
	GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("SpawnerThinker"),function()
		--[[思考]]
		Spawner:DoThink()
		return FrameTime()
	end,0)
end
--[[添加刷怪思考]]
function Spawner:AddThink( Content,FunLogic,FunMain )
	local c={}
	c.InitTime=Content.StartTime 
	c.Delta=Content.DeltaTime
	c.TaskTime=c.InitTime
	c.Content=Content
	c.added=false
	c.Respawn=Content.Respawn
	c.Logic=function()
		-- print("FunLogic",c.TaskTime,Spawner.CurrentTime,c.added)
		if FunLogic(c.TaskTime)and not c.added then
			local fun = function ()
				FunMain(c.Content,c)
				-- print("FunMain")
				c.callback()
			end
			Spawner:AddTask(fun)
			-- print("c.added=true")
			c.added=true
		end
	end
	c.callback=function()
		-- print("c.callback",c.Delta,c.Respawn)
		if c.Delta==0 then 
			return 
		end
		if c.Respawn then
			return
		end
		c.TaskTime=c.TaskTime+c.Delta
		c.added=false
	end
	table.insert(self.Tick,c)
end
--[[添加任务]]
function Spawner:AddTask(fun)
	self.Task:insert(nil,fun)
end
--[[执行任务]]
function Spawner:DoTask()
	local i=0
	while(not self.Task:empty())
	do
		local _,fun=self.Task:pop()
		fun()
		i=i+1
		if i>=self.FrameNum then 
			i=0
			break 
		end
	end
end
--[[执行逻辑思考]]
function Spawner:DoThink()
	if not IsEmpty(self.Tick) then
		for _,v in pairs(self.Tick) do
			v.Logic()
		end
		
	end 
end
--======================================||事件监听响应||==============================================
function Spawner:OnUnitKilled( keys )
	if not keys.entindex_killed or not keys.entindex_attacker then
		return
	end
	local entity_killed = EntIndexToHScript(keys.entindex_killed)
    local entity_attacker = EntIndexToHScript(keys.entindex_attacker)
    if entity_killed.Creep then 
    	entity_killed.Onkilled()
    end
end
--======================================||其他||=====================================================
function IsEmpty(t)
	return next(t)==nil 
end