--Copyright (c) 2016-2017 野猪大大(zllang)
--================================================================================================================
--=======================================||玩家信息类||===========================================================
--================================================================================================================

--[[  API说明：使用时不要忘记判断返回是否为空，为空时说明服务器有问题，偶尔会发生的。所以不要影响正常游戏，若获取
				失败，界面应显示：正在连接服务器。。。
		
		1、GameRules:GetGameModeEntity().DB.IsVipPlayer(SteamID)   ------------玩家是否为VIP
		2、GameRules:GetGameModeEntity().DB.GetPlayerScore(SteamID)   ---------获取该玩家当前积分
		3、GameRules:GetGameModeEntity().DB.GetPlayerCoin(SteamID)   ----------获取该玩家当前3c币

		4、GameRules:GetGameModeEntity().DB.UpdateToServer()   ----------------更新数据到服务器，在游戏结束时调用

		5、GameRules:GetGameModeEntity().DB.AskForUpdate(steamid)   -----------主动从服务器获取最新信息，方便及时更
																				新在线数据到本地
		6、GameRules:GetGameModeEntity().DB.GetAllServerInfo(SteamID)   -------获取所有玩家在线数据

		7、GameRules:GetGameModeEntity().DB.GetRankings()   -------------------获取排行榜，前20个。

		8、GameRules:GetGameModeEntity().DB.GetConnectState()   ----------------获取服务器连接状态

		9、GameRules:GetGameModeEntity().DB.Reward(steamid,RewardType, RewardValue) 
		-------设置奖励：
						RewardType："Coin"、"Score"和"VIP" 参数为字符串，记住！
						RewardValue：VIP时，可以为任意内容。
]]

--================================================================================================================
--[[记录玩家所有信息，包括多英雄信息、状态信息、外建数据库信息等]]
if PlayerInfo==nil then PlayerInfo=class({}) end
--设置是否开启调试
PlayerInfo.Debug =true 

--[[初始化]]

function PlayerInfo:Init()
	GameRules:GetGameModeEntity().DB=self:InitDataBase()
	
	
	-- --[[测试代码]]
	-- if PlayerInfo.Debug==true then
	-- 	Convars:RegisterCommand( "servertest",function( )
	--         local cmdPlayer = Convars:GetCommandClient() 
	--         local hero =cmdPlayer:GetAssignedHero()  
	--         local SteamID=130672686
	--         if cmdPlayer then
	--             print("Reward........")
	--             GameRules:GetGameModeEntity().DB.Reward(SteamID,"Coin", 50)
	--             GameRules:GetGameModeEntity().DB.Reward(SteamID,"Score", 100)
	--             GameRules:GetGameModeEntity().DB.UpdateToServer()
	--         end
	--     end,"server test",0)

	-- end
	--[[测试代码]]
	-- if PlayerInfo.Debug==true then
	-- 	Convars:RegisterCommand( "RankTest",function( )
	--         local cmdPlayer = Convars:GetCommandClient() 
	--         local hero =cmdPlayer:GetAssignedHero()  
	--         local SteamID=130672686
	--         if cmdPlayer then
	--             print("Reward........")
	--             RewardSystem:GameOver_Mortality(DOTA_TEAM_GOODGUYS)
	--         end
	--     end,"Reward test",0)

	-- end
end
--称号系统
function PlayerInfo:OnHeroInGame( hero )
	print('PlayerInfo:OnHeroInGame')
	if hero.IsRealHero and hero:IsRealHero() and not hero.worldPanel then
		print('It is Hero')
     hero.worldPanel = WorldPanels:CreateWorldPanelForAll(
      {layout = "file://{resources}/layout/custom_game/worldpanels/Designation.xml",
        entity = hero:GetEntityIndex(),
        entityHeight = 210,
      })

  end
end
--[[数据缓存]]

function PlayerInfo:InitDataBase()
	--数据库“DB”放置于GameRules:GetGameModeEntity()，以便全局获取。
	GameRules:GetGameModeEntity().DB={}
	local DB={}
	local DataBase={}
	local Rankings={}
	local Rankings_str={}
	local ReconnectTimes=0
	DB.IsConnect=false
	--缓存服务器上的信息
	--[[
			DB.Server[steamid].IsVip :是否为vip玩家
			DB.Server[steamid].Score :积分	
			DB.Server[steamid].Coin :金币	
	]]
	DB.Server={}
	--获取玩家vip状态
	DB.IsVipPlayer=function ( steamid )
		if DB.IsConnect==false then return false end			
		return DataBase[steamid].IsVIP	
	end
	--设置玩家vip状态
	DB.SetVipPlayer=function ( steamid ,IsVIP)
		DataBase[steamid].IsVIP=IsVIP
	end
	--获取服务器连接状态
	DB.GetConnectState=function()
		return DB.IsConnect
	end
	--获取玩家积分
	DB.GetPlayerScore=function ( steamid )
		if DB.IsConnect==false then return false end		
		return DataBase[steamid].Score
	end
	--设置玩家积分
	DB.SetPlayerScore=function ( steamid ,Score)
		DataBase[steamid].Score=Score
	end
	--获取玩家金币
	DB.GetPlayerCoin=function ( steamid )
		if DB.IsConnect==false then return false end		
		return DataBase[steamid].Coin
	end
	--设置玩家金币
	DB.SetPlayerCoin=function ( steamid ,Coin)
		DataBase[steamid].Coin=Coin
	end
	--获取当前所有玩家数据
	DB.GetAllServerInfo=function ()
		return DataBase
	end
	--获取排行榜
	DB.GetRankings=function()
		return Rankings
	end
	--更新排行榜到本地
	DB.GetRankingsFromServer=function()
		Server:GetRankings(function (result)
			--如果获取排行榜失败，那么10秒后重试
			if result == false then 
				GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("markings"),function()
					if ReconnectTimes > 5 then 
						GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("markings1"),function() 
							DB.GetRankingsFromServer()
							ReconnectTimes=ReconnectTimes+1
						end,10)
						return nil 
					elseif ReconnectTimes > 10 then
						print('connect to server feild,this board game no score!')
						return nil 
					end  
					ReconnectTimes=ReconnectTimes+1
					DB.GetRankingsFromServer()
				end,10)
			else
				ReconnectTimes=0
				Rankings=result
				--dprint('Rankings',Rankings)
				local Rank_str={}
				for k,v in pairs(Rankings) do
					Rank_str[k]=v
					Rank_str[k][1]=tostring(Rank_str[k][1])
				end
				Rankings_str=Rank_str
				GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("UpdateRankingsToPUI"),function()
				    DB.UpdateRankingsToPUI()						
				end,1)	
				if DB.IsConnect==false then 
					DB.IsConnect=true
				end
			end
		end)
	end
	--设置奖励
	DB.Reward = function(steamid,RewardType, RewardValue)
		if DB.IsConnect==false then print('connect server feild') return nil end
		if not DataBase[steamid].Score or not  DataBase[steamid].Coin then print('Rewrad Field:No Server Data!') end
		if RewardType=="Coin" then DataBase[steamid].Coin=DataBase[steamid].Coin+RewardValue end
		if RewardType=="Score" then DataBase[steamid].Score=DataBase[steamid].Score+RewardValue end
		if RewardType=="VIP" then DataBase[steamid].IsVIP=true end
	end
	--主动申请重新从服务器更新数据库
	DB.AskForUpdate=function ( steamid )
		Server:GetUserInfo(steamid,function( result )
			if not result then
				--若获取信息失败，则30秒后重试。
				GameRules:GetGameModeEntity():SetContextThink("retry AskForUpdate",function()
					DB.AskForUpdate(steamid)
				end,30)
			else				
				DB.IsConnect=true
				print('Game Mode:',_G.DeathMatch)
				if _G.DeathMatch then 				
					DB.SetPlayerScore(steamid,result.ScoreD)
					DB.SetVipPlayer(steamid,result.IsVIP)
					DB.SetPlayerCoin(steamid,result.Coin)
				else
					DB.SetPlayerScore(steamid,result.Score)
					DB.SetVipPlayer(steamid,result.IsVIP)
					DB.SetPlayerCoin(steamid,result.Coin)
				end

				GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("UpdateOneToPUI"),function()
				    DB.UpdateOneToPUI(steamid)		
				end,1)		
			end
		end)
	end
	--更新排行榜数据到UI
	DB.UpdateRankingsToPUI=function ()
		print('###UpdateRankingsToPUI####')

		for k,playerinfo in pairs(DB.player) do
			local playerID=DB.player[k].playerID
			local player=PlayerResource:GetPlayer(playerID)

			CustomGameEventManager:Send_ServerToPlayer(player,"UpdataRankings",
			{
				Rank=Rankings_str,
				Death=_G.DeathMatch
			})
		end
	end
	--更新全部数据到UI
	DB.UpdateAllToPUI=function ()		
		for k,playerinfo in pairs(DB.player) do
			local playerID=DB.player[k].playerID
			local player=PlayerResource:GetPlayer(playerID)
			CustomGameEventManager:Send_ServerToPlayer(player,"UpdataPlayerInfo",
			{
				score=DB.GetPlayerScore(k),
				coin=DB.GetPlayerCoin(k),
				IsVIP=DB.IsVipPlayer(k),
				SteamID=tostring(k)
			})
		end
	end
	--更新一个数据到UI
	DB.UpdateOneToPUI=function (steamid)				
		local playerID=DB.player[steamid].playerID
		local player=PlayerResource:GetPlayer(playerID)
		CustomGameEventManager:Send_ServerToPlayer(player,"UpdataPlayerInfo",
		{
			score=DB.GetPlayerScore( steamid ),
			coin=DB.GetPlayerCoin(steamid),
			IsVIP=DB.IsVipPlayer(steamid),
			SteamID=tostring(steamid) 
		})	
	end
	--更新数据到服务器
	DB.UpdateToServer=function()

		for steamid,data in pairs(DataBase) do
			if _G.DeathMatch then
				data.ScoreD=data.Score
				data.Score=nil 
			end
			Server:UpdataData(steamid,data,function (  )
				print('updata to server success!')
				for k,v in pairs(GameRules:GetGameModeEntity().DB.player) do
					DB.AskForUpdate(k)
					DB.UpdateOneToPUI(k)	
				end
			end)
		end
	end
	--替换多控系统中的信息记录，统一管理。
	--老版本的 GameRules:GetGameModeEntity() 换成 GameRules:GetGameModeEntity().DB
	DB.MultiInfoInit=function()
		--在 DOTA_GAMERULES_STATE_CUSTOM_GAME_SETUP 游戏状态时调用。
		DB.HeroNum={}
		DB.Heros={}
		DB.HeroNum[DOTA_TEAM_GOODGUYS]=0
		DB.HeroNum[DOTA_TEAM_BADGUYS]=0
		DB.Heros[DOTA_TEAM_GOODGUYS]={}
		DB.Heros[DOTA_TEAM_BADGUYS]={}
		if DB.player ==nil then DB.player={} end
		for playerID = 0, DOTA_MAX_TEAM_PLAYERS do
	    	if  PlayerResource:IsValidPlayerID(playerID) and PlayerResource:IsValidPlayer(playerID) then
	    		local player=PlayerResource:GetPlayer(playerID)
			    local steamid=PlayerResource:GetSteamAccountID(playerID)			
			    if steamid then
				    DataBase[steamid]={}
				    DB.player[steamid]={} 
			    	DB.player[steamid].hero={}
			    	DB.player[steamid].hero_entityindex={}
			    	DB.player[steamid].hero_num=0
			    	DB.player[steamid].playerID=playerID
			    	DB.player[steamid].connect=true
			    end 
	    	end
	    end
	end
	DB.GetInfoFirst=function()		
		--服务器获取排行榜
		DB.GetRankingsFromServer()
		for playerID = 0, DOTA_MAX_TEAM_PLAYERS do
	    	if  PlayerResource:IsValidPlayerID(playerID) and PlayerResource:IsValidPlayer(playerID) then
	    		local player=PlayerResource:GetPlayer(playerID)
			    local steamid=PlayerResource:GetSteamAccountID(playerID)
			    if steamid then 
			    	DB.AskForUpdate(steamid)
			    end 
	    	end
	    end
	end
	return DB
end

