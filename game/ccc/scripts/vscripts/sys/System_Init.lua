if CustomSystem == nil then CustomSystem=class({}) end 

function CustomSystem:System_Precache( context )
	--预加载特效、模型、音效等。
	--例如：
	--PrecacheResource("particle", "particles/econ/generic/generic_aoe_explosion_sphere_1/generic_aoe_explosion_sphere_1.vpcf", context)
  	--PrecacheResource("particle_folder", "particles/test_particle", context)  
	--PrecacheResource("model_folder", "particles/heroes/antimage", context)
	--PrecacheResource("model", "particles/heroes/viper/viper.vmdl", context)
	PrecacheUnitByNameSync("npc_dota_hero_ancient_apparition", context)
  	PrecacheUnitByNameSync("npc_dota_hero_enigma", context)
  	PrecacheUnitByNameSync("npc_dota_hero_wisp", context)
  	--单位攻击弹道
  	PrecacheResource( "particle", "particles/units/heroes/hero_winter_wyvern/winter_wyvern_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_visage/visage_familiar_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/base_attacks/ranged_siege_bad.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_jakiro/jakiro_base_attack_fire.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_weaver/weaver_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/neutral_fx/thunderlizard_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_luna/luna_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_visage/visage_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_sniper/sniper_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/base_attacks/ranged_siege_good.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_troll_warlord/troll_warlord_base_attack_glow.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_witchdoctor/witchdoctor_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_enchantress/enchantress_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/neutral_fx/gnoll_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_clinkz/clinkz_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_razor/razor_base_attack_timed.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_venomancer/venomancer_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_morphling/morphling_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/neutral_fx/black_dragon_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/neutral_fx/satyr_trickster_projectile.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_dragon_knight/dragon_knight_elder_dragon_fire.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_dragon_knight/dragon_knight_elder_dragon_corrosive.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_dragon_knight/dragon_knight_elder_dragon_frost.vpcf", context )
  	PrecacheResource( "particle", "particles/econ/items/keeper_of_the_light/kotl_weapon_arcane_staff/keeper_base_attack_arcane_staff.vpcf", context )
  	PrecacheResource( "particle", "particles/neutral_fx/black_dragon_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/neutral_fx/black_drake_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_jakiro/jakiro_base_attack_fire.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_chaos_knight/chaos_knight_weapon_blur.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_lone_druid/lone_druid_attack_start.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_drow/drow_base_attack.vpcf", context )
  	PrecacheResource( "particle", "particles/base_attacks/ranged_tower_good.vpcf", context )
  	PrecacheResource( "particle", "particles/base_attacks/ranged_tower_bad.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_windrunner/windrunner_base_attack.vpcf", context )
  	
  	--单位其他特效
  	PrecacheResource( "particle", "particles/units/heroes/hero_pudge/pudge_death_dust.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_winter_wyvern/wyvern_spawn.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_winter_wyvern/wyvern_flying_alt_wings.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_clinkz/clinkz_searing_arrow.vpcf", context )
  	PrecacheResource( "particle", "particles/units/heroes/hero_omniknight/omniknight_repel_buff.vpcf", context )
  	PrecacheUnitByNameSync("npc_building_gaodengjinglingfangyuta", context)

  	for _,v in pairs(HeroList_Custom) do
  		-- print("PrecacheUnitByNameSync",_,v)
    	PrecacheUnitByNameSync(v, context)
 	end

	--读取单位kv
	GameRules.UnitKV = LoadKeyValues("scripts/npc/npc_units_custom.txt")
	-- print("GameRules.UnitKV",GameRules.UnitKV)
 	for k,_ in pairs(GameRules.UnitKV) do
 		-- print(k)
    	PrecacheUnitByNameSync(k, context)
 	end

end
function CustomSystem:System_Init(  )
	self:InitConstant()
	--初始化模块相关脚本,功能等同于addon_game_mode.lua中的Init。
	--例如：
		--监听事件：ListenToGameEvent("player_reconnected", Dynamic_Wrap(ccc, 'OnPlayerReconnect'), self)
		--初始化某函数：Init()
	--监听单位死亡事件
	ListenToGameEvent('entity_killed', Dynamic_Wrap(CustomSystem, 'OnEntityKilled'), self)
	--监听单位出生事件
	ListenToGameEvent('npc_spawned', Dynamic_Wrap(CustomSystem, 'OnNPCSpawned'), self)
	--监听游戏状态变化
	ListenToGameEvent('game_rules_state_change', Dynamic_Wrap(CustomSystem, 'OnGameRulesStateChange'), self)
	ListenToGameEvent("dota_player_shop_changed", Dynamic_Wrap(CustomSystem, 'On_dota_player_shop_changed'), self)
	--监听英雄学习技能
	ListenToGameEvent('dota_player_learned_ability', Dynamic_Wrap(CustomSystem, 'OnPlayerLearnedAbility'), self)
	--监听英雄升级
	ListenToGameEvent('dota_player_gained_level', Dynamic_Wrap(CustomSystem, 'OnPlayerLevelUp'), self)
	--监听队伍杀敌事件
	ListenToGameEvent( "dota_team_kill_credit", Dynamic_Wrap( CustomSystem, 'OnTeamKillCredit' ), self )
	--监听补刀事件
	-- ListenToGameEvent("last_hit", Dynamic_Wrap(CustomSystem, 'On_last_hit'), self)
	--玩家信息面板加载完毕
	CustomGameEventManager:RegisterListener("PlayerInfoPanelLoaded", Dynamic_Wrap(CustomSystem,"PlayerInfoPanelLoaded"))
	CustomGameEventManager:RegisterListener("RankingsPanelLoaded", Dynamic_Wrap(CustomSystem,"RankingsPanelLoaded"))


	LinkLuaModifier( "modifier_abilitypoint", "sys/modifiers/modifier_abilitypoint.lua", LUA_MODIFIER_MOTION_NONE )
	--初始化通讯
	CommunicateInit()
	--多控系统初始化
	MultiHeroControlSystem:Init()
	--玩家信息
	PlayerInfo:Init()
	--初始化商店
	Shop:Init()
	--ShopPanel:Init()
	--初始化刷怪器
	Spawner:Init()
	--过滤器初始化
	Filter:Init()
	--经验系统初始化
	ExperienceSystem:Init()
	--物品掉落系统初始化
	DropItem:Init()
end
--初始化常量
function CustomSystem:InitConstant()
	--选择英雄倒计时
	COUNT_DOWN = 7
	--是否为死亡模式
	-- print("GetMapName",GetMapName())
	_G.DeathMatch = GetMapName()=="death_match"
	--死亡模式杀敌人数
	self.TEAM_KILLS_TO_WIN = 7
	--胜利信息
	self.m_VictoryMessages = {}
	self.m_VictoryMessages[DOTA_TEAM_GOODGUYS] = "#VictoryMessage_GoodGuys"
	self.m_VictoryMessages[DOTA_TEAM_BADGUYS]  = "#VictoryMessage_BadGuys"
	self.CLOSE_TO_VICTORY_THRESHOLD = 5
	--加密模块执行状态
	_G.ServerStatus=false

	if not DeathMatch then
		GameRules:SetGoldPerTick(0)
		GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_GOODGUYS, 3)
		GameRules:SetCustomGameTeamMaxPlayers(DOTA_TEAM_BADGUYS, 3)
	end
	-- DeathMatch = false

	_G.RandomHero = _G.DeathMatch
end

function CustomSystem:Require( )
	-- 加载脚本文件至主系统，功能等同与在addon_game_mode.lua 中require。
	--例如：	require('xxxx.xxx.lua')
	heap=require('libraries.bheap')
	require('libraries.dkjson')
	require('sys.MultiHeroControlSystem')
	require('sys.HeroList_Custom')
	require('sys.corpses')
	require('sys.messages')
	require('sys.HeroHandler')
	require('sys.Utils')
	require('sys.Shop')
	require('sys.Spawner')
	require('sys.Filter')
	require('sys.ExperienceSystem')
	require('sys.DropItem')
	require('sys.creep_ai')
	require('sys.neutral_ai')
	require('sys.AbilityAi')
	require('sys.PlayerInfo')
	require('libraries.containers')
	require('libraries.lockbox.init')
	require('libraries.worldpanels')
	require('sys.RewardSystem')

	
end
--============================================================================================================
--============================================================================================================
--                                          以下为事件响应
--============================================================================================================
--============================================================================================================
--====================[[英雄信息面板加载完成事件响应]]
function CustomSystem:PlayerInfoPanelLoaded( event )
	if _G.ServerStatus==false then 
		GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("PlayerInfoPanelLoaded"),function()
			self:PlayerInfoPanelLoaded( event )
		end,1)
		return nil
	end
	local steamid=PlayerResource:GetSteamAccountID(event.PlayerID)	
	GameRules:GetGameModeEntity().DB.UpdateOneToPUI(steamid)

end
function CustomSystem:RankingsPanelLoaded( event )
	if _G.ServerStatus==false then 
		GameRules:GetGameModeEntity():SetContextThink(DoUniqueString("PlayerInfoPanelLoaded"),function()
			self:PlayerInfoPanelLoaded( event )
		end,1)
		return nil
	end
	local steamid=PlayerResource:GetSteamAccountID(event.PlayerID)	
	GameRules:GetGameModeEntity().DB.UpdateRankingsToPUI()

end
--====================[[单位死亡事件响应]] 
function CustomSystem:OnEntityKilled( event )
	local killed = EntIndexToHScript(event.entindex_killed)
	if killed:IsRealHero() and not DeathMatch then
		CustomGameEventManager:Send_ServerToAllClients("SetRespawn", {unit=killed:GetEntityIndex(),Respawn=killed.chongsheng})
		CustomGameEventManager:Send_ServerToAllClients("SetBuyBacker", {unit=killed:GetEntityIndex(),Buyable=false})
	elseif killed:HasInventory() and (not killed.chongsheng) and (not killed:IsRealHero()) then
		for i=0, 5, 1 do
        	local current_item = killed:GetItemInSlot(i)
        	if current_item then
        		-- print("DropItemAtPositionImmediate")
            	killed:DropItemAtPositionImmediate(current_item, killed:GetAbsOrigin())
        	end
    	end
	end

	Timers:CreateTimer(0.1, function() 
		if LeavesCorpse( killed ) then
			-- Create and set model
			--print("Create and set model")
			local corpse = CreateUnitByName("npc_dummy_corpse", killed:GetAbsOrigin(), true, nil, nil,	DOTA_TEAM_NOTEAM)		
			local i = RandomInt(1, #CORPSE_MODEL)

			corpse:SetModel(CORPSE_MODEL[i])
			corpse:SetModelScale(CORPSE_SCALE[i])
			corpse:SetForwardVector(RandomVector(1))
			corpse:GetAbsOrigin()
			corpse:SetAbsOrigin(corpse:GetAbsOrigin()+Vector(0,0,-4))

			-- Set the corpse invisible until the dota corpse disappears
			--local abilityEx = corpse:AddAbility("dummy_corpse")
    		--abilityEx:SetLevel(1)
			corpse:AddNoDraw()			
			-- Keep a reference to its name and expire time
			corpse.corpse_expiration = GameRules:GetGameTime() + CORPSE_DURATION
			corpse.unit_name = killed:GetUnitName()
			-- Set custom corpse visible
			Timers:CreateTimer(3, function() if IsValidEntity(corpse) then 
				corpse:RemoveNoDraw() 				
				end end)
			-- Remove itself after the corpse duration
			Timers:CreateTimer(CORPSE_DURATION, function()
				if corpse and IsValidEntity(corpse) then
					corpse:RemoveSelf()
				end
			end)
		end
	end)
	if not killed.chongsheng and killed:IsRealHero() then
		if DeathMatch then
			if killed:IsOwnedByAnyPlayer() and killed:IsRealHero() then
    			killed:SetTimeUntilRespawn(5)
    		end
    	else
			local BuyBackGold = 200 * ( 1.1 + 0.1 * (killed:GetLevel() - 1) )
			if BuyBackGold > 2000 then
				BuyBackGold = 2000
			end
			BuyBackCost[killed:GetEntityIndex()] = BuyBackGold
			CustomGameEventManager:Send_ServerToAllClients("SetBuyBackCost", {unit=killed:GetEntityIndex(),BuyBackCost=BuyBackGold})
			--测试用
			if killed:IsOwnedByAnyPlayer() and killed:IsRealHero() then
    			killed:SetTimeUntilRespawn(90)
    		end
    	end
    end

    local killedEntityName = killed:GetName()
   	if killedEntityName == "jiqiang_radiant" then
   		local jiqiang_trigger = Entities:FindByName(nil, "trigger_jiqiang_radiant")
   		if IsValidEntity(jiqiang_trigger) then
   			jiqiang_trigger:SetAbsOrigin(Vector(8000,8000,-300))
   			Timers:CreateTimer(0.1, function() 
   				jiqiang_trigger:RemoveSelf()
   			end)
   		end
   	elseif killedEntityName == "jiqiang_dire" then
   		local jiqiang_trigger = Entities:FindByName(nil, "trigger_jiqiang_dire")
   		if IsValidEntity(jiqiang_trigger) then
   			jiqiang_trigger:SetAbsOrigin(Vector(8000,8000,-300))
   			Timers:CreateTimer(0.1, function() 
   				jiqiang_trigger:RemoveSelf()
   			end)
   		end
   	end
end
--====================[[单位出生事件响应]] 
function CustomSystem:OnNPCSpawned( keys )
	local npc = EntIndexToHScript(keys.entindex)
	----------------判断是否为英雄第一次出生--------------------------
	if npc:IsRealHero() and npc.cFirstSpawned == nil then           --
	    npc.cFirstSpawned = true 									--
	    CustomSystem:OnHeroInGame(npc) 								--
	end 															--
	------------------------------------------------------------------

	----------------判断是否为中立生物-------------------------------
	if IsValidAlive(npc) and npc:GetTeam() == DOTA_TEAM_NEUTRALS then
		--不延迟的话开始ai时该单位还没出生
		Timers:CreateTimer(0.04, function()
				NeutralAI:Start( npc )
			end)
	end
	----------------------------------------------------------------

	----------------判断是否有相关技能-------------------------------
	if npc:HasAbility("luna_moon_glaive") then
		local abilityEx = npc:FindAbilityByName("luna_moon_glaive")
    	if abilityEx then
    		abilityEx:SetLevel(1)
    	end
	end
	if npc:HasAbility("binglong_shuangdonghuxi") then
		local abilityEx = npc:FindAbilityByName("binglong_shuangdonghuxi")
    	if abilityEx then
    		abilityEx:SetLevel(1)
    	end
	end
	----------------------------------------------------------------

	---------------判断单位名字-------------------------------------
	--[[
	if npc:GetUnitName() == "npc_building_shangdianshouwei" then
		Timers:CreateTimer(0.04, function()
			AbilityAI:Start(npc)
		end)
	end
	--]]
	---------------------------------------------------------------

	---------------判断买活-------------------------------------
	if npc:IsRealHero() and npc.IsBuyBacked then
		npc.IsBuyBacked = false
		Timers:CreateTimer(0.04, function()
			npc:SetHealth(npc:GetMaxHealth() / 2)
			npc:SetMana(npc:GetMaxMana() / 2)
		end)
	end
	---------------------------------------------------------------
end
--====================[[当英雄进入游戏中]] 
function CustomSystem:OnHeroInGame( _hero )	
	local hero = _hero
	--hero:SetBaseDamageMin(100000)
	--hero:SetBaseDamageMax(150000)
	local playerID=hero:GetPlayerID() 
	local HeroName = hero:GetClassname()
	if hero:FindAbilityByName("dummy_elf") then
		MultiHeroControlSystem:CameraMoveToUnit(hero:GetPlayerID(),hero)
		return
  	end
	--初始化英雄
	HeroHandler:Init(hero)
	--添加背包
	Shop:OnHeroInGame( hero )
	local NotIllusion = true
	if GameRules:GetGameModeEntity().DB.Heros then
		local allHeroes = GameRules:GetGameModeEntity().DB.Heros[hero:GetTeam()]
		for _,v in pairs(allHeroes) do
			if EntIndexToHScript(v):GetClassname() == HeroName then
				NotIllusion = false
			end
		end
	else
		NotIllusion = false
	end
	if FirstHero then
		Timers:CreateTimer(0.1,function (  )
			CustomGameEventManager:Send_ServerToPlayer(PlayerResource:GetPlayer(hero:GetPlayerID()),"select_unit",{unitIndex=hero:GetEntityIndex()})		
		end)
	end
	
	if(HeroName == "npc_dota_hero_drow_ranger") then
    	local abilityEx = hero:FindAbilityByName("es_heianzhijian")
    	if abilityEx then
    		abilityEx:SetLevel(1)
    	end
    	abilityEx = hero:FindAbilityByName("youxia_fuzhou")
    	if abilityEx then
    		abilityEx:SetLevel(1)
    	end
  	elseif(HeroName == "npc_dota_hero_windrunner") then
    	local abilityEx = hero:FindAbilityByName("ls_shuangdongzhijian")
    	if abilityEx then
    		abilityEx:SetLevel(1)
    	end
    	abilityEx = hero:FindAbilityByName("youxia_fuzhou")
    	if abilityEx then
    		abilityEx:SetLevel(1)
    	end
  	elseif(HeroName == "npc_dota_hero_juggernaut") then
    	local abilityEx = hero:FindAbilityByName("slardar_amplify_damage")
    	if abilityEx then
    		abilityEx:SetLevel(1)
    	end
  	elseif(HeroName == "npc_dota_hero_alchemist") then
    	local abilityEx = hero:FindAbilityByName("lianjinshuhsi_dianjinshu")
    	if abilityEx then
    		abilityEx:SetLevel(1)
    	end
    elseif(HeroName == "npc_dota_hero_disruptor") then
    	local abilityEx = hero:FindAbilityByName("far_seer_far_sight")
    	if abilityEx then
    		abilityEx:SetLevel(1)
    	end
    elseif(HeroName == "npc_dota_hero_shadow_shaman") then
    	local abilityEx = hero:FindAbilityByName("luna_moon_glaive")
    	if abilityEx then
    		abilityEx:SetLevel(1)
    	end
  	end

  	if not DeathMatch then
		local item = CreateItem("item_chongsheng", hero, hero)
   		hero:AddItem(item)
	end
	hero:AddAbility("attribute_bonus")
  	hero:AddNewModifier(hero,nil,"modifier_abilitypoint",{})
  	--奖励系统
  	RewardSystem:OnHeroInGame( _hero )
  	--称号 有点小问题，待修复
  	if HeroName ~= "npc_dota_hero_wisp" then
  		--PlayerInfo:OnHeroInGame( hero )
  	end
end
--====================[[当游戏状态变化]]
function CustomSystem:OnGameRulesStateChange( keys )
	--设置时间为0点
	GameRules:SetTimeOfDay(0)
	--print('OnGameRulesStateChange:')
	--DeepPrintTable(keys)
	local newState = GameRules:State_Get()
	-- print("OnGameRulesStateChange",newState)
	-- print(DOTA_GAMERULES_STATE_STRATEGY_TIME,DOTA_GAMERULES_STATE_WAIT_FOR_PLAYERS_TO_LOAD,DOTA_GAMERULES_STATE_INIT,DOTA_GAMERULES_STATE_POST_GAME)
    if newState == DOTA_GAMERULES_STATE_PRE_GAME then 
    	--多控初始化
     	-- MultiHeroControlSystem:PlayerControlInit() 
      	Timers:CreateTimer(function()
        	MultiHeroControlSystem:PlayerInit()
      	end)

      	--设置选英雄倒计时
      	--StopSoundEvent("announcer_announcer_choose_hero",nil)
      	local countDown = COUNT_DOWN
      	Timers:CreateTimer(1,function()
      		countDown = countDown - 1
        	if countDown > 0 then
        		EmitGlobalSound("General.CastFail_AbilityInCooldown")
        		
        		Notifications:TopToAll({text=countDown, duration=1.0,style={color="red",["font-size"]="110px"},})
        		return 1
        	else
        		EmitAnnouncerSound("Loot_Drop_Stinger_Short")
        		countDown = "Begin" 
        		Notifications:TopToAll({text=countDown, duration=1.0,style={color="red",["font-size"]="110px"},})
        		return nil
        	end
      	end)
      	--去除所有塔的真实视域、高塔保护
    	local alltower = Entities:FindAllByClassname("npc_dota_tower")
    	for _,v in pairs(alltower) do 
    		
    		v:RemoveModifierByName("modifier_tower_truesight_aura")
    		v:RemoveModifierByName("modifier_tower_aura")
    	end

    	local jiqiang_radiant = Entities:FindByName(nil,"jiqiang_radiant")
    	local jiqiang_dire = Entities:FindByName(nil,"jiqiang_dire")
		Timers:CreateTimer(0.04, function()
			AbilityAI:Start(jiqiang_radiant)
			AbilityAI:Start(jiqiang_dire)
		end)

		local allbuilding = Entities:FindAllByClassname("npc_dota_building")
		for _,v in pairs(allbuilding) do 
    		-- print("allbuilding",v,v:GetUnitName())
    		v:RemoveModifierByName("modifier_invulnerable")
    	end

    	local allFountain = Entities:FindAllByClassname("ent_dota_fountain")
		for _,v in pairs(allFountain) do 
    		--print("allFountain",v,v:GetUnitName())
    		v:RemoveModifierByName("modifier_fountain_aura")
    	end

    	if DeathMatch then
    		-- TEAM_KILLS_TO_WIN = 3
    		self.TEAM_KILLS_TO_WIN = PlayerResource:GetPlayerCount() * 10
    		CustomNetTables:SetTableValue( "game_state", "victory_condition", { kills_to_win = self.TEAM_KILLS_TO_WIN } );
    		-- TEAM_KILLS_TO_WIN = nil
    	end

    	Timers:CreateTimer(1,function()
    		for k,v in pairs(_G.ModelAnimationGroup) do
    			local unit = EntIndexToHScript(v)
    			if unit and IsValidEntity(unit) then
    				StartAnimation(unit, {duration=-1, activity=ACT_DOTA_IDLE, rate=1})
    			end
    		end
    	end)

    elseif newState == DOTA_GAMERULES_STATE_HERO_SELECTION then 
        MultiHeroControlSystem:PrePlayerInit() 
    elseif newState == DOTA_GAMERULES_STATE_CUSTOM_GAME_SETUP then
    	GameRules:GetGameModeEntity().DB.MultiInfoInit() 
    	GameRules:GetGameModeEntity().DB.GetInfoFirst()
    elseif newState == DOTA_GAMERULES_STATE_POST_GAME then
    	print('<<<<<<<<<<<<<<DOTA_GAMERULES_STATE_POST_GAME')
    	--GameRules:GetGameModeEntity().DB.UpdateToServer()
    elseif newState == DOTA_GAMERULES_STATE_LAST then
    	print('<<<<<<<<<<<<<<DOTA_GAMERULES_STATE_LAST')
    	--GameRules:GetGameModeEntity().DB.UpdateToServer()
    end   

    
end

function CustomSystem:OnTeamKillCredit( event )
	-- print( "OnKillCredit" )
	-- PrintTable(event)
	if not DeathMatch then
		return
	end
	-- print("IsValidPlayerID",PlayerResource:IsValidPlayerID(-1))

	local nKillerID = event.killer_userid
	local nTeamID = event.teamnumber
	local nTeamKills = event.herokills
	local nKillsRemaining = self.TEAM_KILLS_TO_WIN - nTeamKills

	local broadcast_kill_event =
	{
		killer_id = event.killer_userid,
		team_id = event.teamnumber,
		team_kills = nTeamKills,
		kills_remaining = nKillsRemaining,
		victory = 0,
		close_to_victory = 0,
		very_close_to_victory = 0,
	}

	if nKillsRemaining <= 0 then
		GameRules:SetCustomVictoryMessage( self.m_VictoryMessages[nTeamID] )
		--GameRules:SetGameWinner( nTeamID )
		RewardSystem:GameOver_Mortality(nTeamID)
		broadcast_kill_event.victory = 1
	elseif nKillsRemaining == 1 then
		EmitGlobalSound( "ui.npe_objective_complete" )
		broadcast_kill_event.very_close_to_victory = 1
	elseif nKillsRemaining <= self.CLOSE_TO_VICTORY_THRESHOLD then
		EmitGlobalSound( "ui.npe_objective_given" )
		broadcast_kill_event.close_to_victory = 1
	end

	CustomGameEventManager:Send_ServerToAllClients( "kill_event", broadcast_kill_event )
end

--当英雄升级
function CustomSystem:OnPlayerLevelUp(keys)
  --print('[CCC] OnPlayerLevelUp')
  --PrintTable(keys)

  local player = EntIndexToHScript(keys.player)
  local level = keys.level
  local playerID = player:GetPlayerID()
  RefreshModifierAbilityPoint( playerID )
end

--当英雄学习技能
function CustomSystem:OnPlayerLearnedAbility( keys)
  --print('[CCC] OnPlayerLearnedAbility')
  --PrintTable(keys)

  local player = EntIndexToHScript(keys.player)
  local abilityname = keys.abilityname
  local playerID = player:GetPlayerID()
  RefreshModifierAbilityPoint( playerID )
end


--============================================================================================================
--============================================================================================================
--                                          其他
--============================================================================================================
--============================================================================================================
function RefreshModifierAbilityPoint( playerID)
	local steamid=PlayerResource:GetSteamAccountID(playerID)
	if GameRules:GetGameModeEntity().DB.player[steamid] == nil then
		local heroes = HeroList:GetAllHeroes()
		for k,v in pairs(heroes) do
			local modifier = v:FindModifierByName("modifier_abilitypoint")
			if modifier then
				modifier:ForceRefresh()
			end
		end
	else
		local heroes = GameRules:GetGameModeEntity().DB.player[steamid].hero_entityindex
		for k,v in pairs(heroes) do
			-- local EntIndexToHScript(v):FindModifierByName("modifier_abilitypoint"):ForceRefresh()
			local modifier = EntIndexToHScript(v):FindModifierByName("modifier_abilitypoint")
			if modifier then
				modifier:ForceRefresh()
			end
		end
	end
end