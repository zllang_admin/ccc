require('internal/util')
if modifier_tianyouwudi == nil then modifier_tianyouwudi=class({}) end

function modifier_tianyouwudi:OnCreated(kv)
	if IsServer() then 
		local caster = self:GetCaster()
		self.Particle = ParticleManager:CreateParticle("particles/units/heroes/hero_omniknight/omniknight_repel_buff.vpcf", PATTACH_POINT_FOLLOW, caster)
	end 
end
function modifier_tianyouwudi:OnRefresh( kv )
	if IsServer() then 
		
	end 
end
function modifier_tianyouwudi:OnDestroy( kv )
	if IsServer() then 
		ParticleManager:DestroyParticle(self.Particle, false)
	end 
end
--[[
function modifier_tianyouwudi:GetEffectName(kv)
	if IsServer() then 
		return "particles/generic_gameplay/generic_stunned.vpcf"
	end 
end
function modifier_tianyouwudi:GetEffectAttachType(kv)
	if IsServer() then 
		return PATTACH_OVERHEAD_FOLLOW
	end 
end
--]]
--======================================================常规====================================================

function modifier_tianyouwudi:IsHidden()
	return false
end
function modifier_tianyouwudi:IsPurgable()
	return false
end
function modifier_tianyouwudi:IsPurgeException()
	return false
end
function modifier_tianyouwudi:RemoveOnDeath()
	return true
end
function modifier_tianyouwudi:GetModifierIncomingDamage_Percentage()
	if IsServer() then
		return -100
	end
end
function modifier_tianyouwudi:DeclareFunctions()
	local funcs={
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE
		}
	return funcs
end
function modifier_tianyouwudi:CheckState() 
    local state = {
   		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
   		[MODIFIER_STATE_ATTACK_IMMUNE] = true,
    }
    return state
end

function modifier_tianyouwudi:OnIntervalThink()
	
end

function modifier_tianyouwudi:GetTexture(kv)
	return "omniknight_repel"
end