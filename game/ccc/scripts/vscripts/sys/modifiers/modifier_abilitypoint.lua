require('internal/util')
if modifier_abilitypoint == nil then modifier_abilitypoint=class({}) end

function modifier_abilitypoint:OnCreated(kv)
	if IsServer() then 
		self:SetStackCount( self:GetParent():GetAbilityPoints() )
		self:GetParent():CalculateStatBonus()
		--self:StartIntervalThink(0.1)
	end 
end

function modifier_abilitypoint:OnRefresh( kv )
	if IsServer() then 
		self:SetStackCount( self:GetParent():GetAbilityPoints() )
		self:GetParent():CalculateStatBonus()
	end 
end

--======================================================属性修正====================================================
--[[属性+2×]]
function modifier_abilitypoint:GetModifierBonusStats_Strength( params )
	if IsServer() then
		return self:GetStackCount() * 2
	end
end

function modifier_abilitypoint:GetModifierBonusStats_Agility( params )
	if IsServer() then
		return self:GetStackCount() * 2
	end
end

function modifier_abilitypoint:GetModifierBonusStats_Intellect( params )
	if IsServer() then
		return self:GetStackCount() * 2
	end
end
--======================================================常规====================================================

function modifier_abilitypoint:IsHidden()
	return ( self:GetStackCount() == 0 )
end
function modifier_abilitypoint:IsPurgable()
	return false
end
function modifier_abilitypoint:IsPurgeException()
	return false
end
function modifier_abilitypoint:RemoveOnDeath()
	return false
end
function modifier_abilitypoint:DeclareFunctions()
	local funcs={
			MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
			MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
			MODIFIER_PROPERTY_STATS_INTELLECT_BONUS
		}
	return funcs
end


function modifier_abilitypoint:OnIntervalThink()
	if IsServer() then
		self:ForceRefresh()
	end
end


function modifier_abilitypoint:GetTexture(kv)
	return "invoker_invoke"
end