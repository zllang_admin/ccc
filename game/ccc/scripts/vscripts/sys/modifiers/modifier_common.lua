require('internal/util')
if modifier_common == nil then modifier_common=class({}) end

function modifier_common:OnCreated(kv)
	if IsServer() then 
		
	end 
end
function modifier_common:OnRefresh( kv )
	if IsServer() then 
		
	end 
end
--================================================飞行、地面攻击行为================================================
function modifier_common:OnAttackStart(keys)
	if IsServer() then 
		local attacker=keys.attacker
		local target=keys.target	
		local player = attacker:GetOwner()
		local playerID
		if player and player:IsPlayer() then
			playerID = player:GetPlayerID()
		end
		--print("playerID",playerID)
		if not FiltShenShengHuJia( attacker,target ) then
			if playerID then
				SendErrorMessage(playerID, "#error_attack_building_with_divine_shield")
			end
			attacker:Stop()
			local order =
		    {
		        UnitIndex = keys.attacker:entindex(),
		        OrderType = DOTA_UNIT_ORDER_STOP 
		    }
		    ExecuteOrderFromTable( order )
		elseif not FiltOrb( attacker,target ) then
			if playerID then
				SendErrorMessage(playerID, "#error_melee_attack_flying_unit")
			end
			attacker:Stop()
			local order =
		    {
		        UnitIndex = keys.attacker:entindex(),
		        OrderType = DOTA_UNIT_ORDER_STOP 
		    }
		    ExecuteOrderFromTable( order )
		end
	end 
end
--======================================================属性修正====================================================
--[[力量加血修正 每点力量加血20->30]]
function modifier_common:GetModifierHealthBonus()
	if IsServer() then
		return 10*self:GetCaster():GetStrength()
	end
end
--[[力量生命回复修正 每点力量回复0.03->0.1]]
function modifier_common:GetModifierConstantHealthRegen( )
	if IsServer() then
		return 0.07*self:GetCaster():GetStrength()
	end
end
--[[敏捷攻速修正 每点敏捷  （无需修正）]]
-- function modifier_common:GetModifierAttackSpeedBonus_Constant( )	
-- end	
--[[敏捷移动速修正 每点敏捷0->0.1]]
function modifier_common:GetModifierMoveSpeedBonus_Constant( )
	if IsServer() then
		return 0.1*self:GetCaster():GetAgility()
	end
end	
--[[敏捷护甲修正 每点敏捷1/7->0.3]]
function modifier_common:GetModifierPhysicalArmorBonus(  )
	if IsServer() then
		return self:GetCaster():GetAgility()*(0.3-1/7)
	end
end
--[[智力魔法值修复 每点智力最大魔法奖励12->15]]
function modifier_common:GetModifierManaBonus()
	if IsServer() then
		return 3*self:GetCaster():GetIntellect()
	end
end
--[[智力魔法回复修复 每点智力魔法回复奖励0.04->0.05]]
function modifier_common:GetModifierConstantManaRegen()
	if IsServer() then
		return 0.01*self:GetCaster():GetIntellect()
	end
end
--[[主属性攻击加成修复 无需修复]]
--======================================================常规====================================================

function modifier_common:IsHidden()
	return true
end
function modifier_common:IsPurgable()
	return false
end
function modifier_common:IsPurgeException()
	return false
end
function modifier_common:RemoveOnDeath()
	return false
end
function modifier_common:DeclareFunctions()
	local funcs={
			MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
			MODIFIER_PROPERTY_HEALTH_REGEN_CONSTANT,
			MODIFIER_PROPERTY_HEALTH_BONUS,
			MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT,
			MODIFIER_PROPERTY_MOVESPEED_BONUS_CONSTANT,
			MODIFIER_PROPERTY_PHYSICAL_ARMOR_BONUS,
			MODIFIER_PROPERTY_MANA_REGEN_CONSTANT,
			MODIFIER_PROPERTY_MANA_BONUS,
			MODIFIER_EVENT_ON_ATTACK_START
		}
	return funcs
end


function modifier_common:OnIntervalThink()
	
end
