
--尸体模型
if CORPSE_MODEL == nil then
    CORPSE_MODEL = {}
    CORPSE_MODEL[1] = "models/creeps/neutral_creeps/n_creep_troll_skeleton/n_creep_troll_skeleton_fx.vmdl"
    CORPSE_MODEL[2] = "models/props_bones/badside_bones001.vmdl"
    CORPSE_MODEL[3] = "models/props_bones/badside_bones002.vmdl"
    CORPSE_MODEL[4] = "models/props_bones/badside_bones003.vmdl"
    CORPSE_MODEL[5] = "models/props_bones/badside_bones004.vmdl"
    CORPSE_MODEL[6] = "models/props_bones/badside_bones005.vmdl"
    CORPSE_MODEL[7] = "models/props_bones/bones001_set_tintable.vmdl"
    CORPSE_MODEL[8] = "models/props_bones/bones002_set_tintable.vmdl"
    CORPSE_MODEL[9] = "models/props_bones/bones003_set_tintable.vmdl"
    CORPSE_MODEL[10] = "models/props_bones/bones004_set_tintable.vmdl"
    CORPSE_MODEL[11] = "models/props_bones/bones008_tintable.vmdl"
    CORPSE_MODEL[12] = "models/props_structures/skeleton001.vmdl"
    
end

--尸体大小
if CORPSE_SCALE == nil then
    CORPSE_SCALE = {}
    CORPSE_SCALE[1] = 1
    CORPSE_SCALE[2] = 0.4
    CORPSE_SCALE[3] = 1
    CORPSE_SCALE[4] = 0.5
    CORPSE_SCALE[5] = 0.8
    CORPSE_SCALE[6] = 0.5
    CORPSE_SCALE[7] = 0.4
    CORPSE_SCALE[8] = 1
    CORPSE_SCALE[9] = 0.5
    CORPSE_SCALE[10] = 0.8
    CORPSE_SCALE[11] = 0.6
    CORPSE_SCALE[12] = 0.4
end

--尸体时间
CORPSE_DURATION = 22

function SetNoCorpse( event )
    event.target.no_corpse = true
end

function FindCorpseInRadius( origin, radius )
    return Entities:FindByModelWithin(nil, CORPSE_MODEL, origin, radius) 
end

-- Custom Corpse Mechanic
function LeavesCorpse( unit )

    if not unit or not IsValidEntity(unit) then
        return false

    -- Heroes don't leave corpses (includes illusions)
    elseif unit:IsHero() then
        return false

    -- Ignore buildings 
    elseif unit.GetInvulnCount ~= nil then
        return false

    -- Ignore custom buildings
    elseif unit:IsTower() then
        return false

    -- Ignore units that start with dummy keyword   
    elseif string.find(unit:GetUnitName(), "dummy") then
        return false

    -- Ignore units that were specifically set to leave no corpse
    elseif unit.no_corpse then
        return false

    -- Read the LeavesCorpse KV
    elseif unit.chongsheng then
        return false

    --has respawn
    else
        ---[[
        local unit_info = GameRules.UnitKV[unit:GetUnitName()]
        --print("unit_info",unit:GetUnitName(),unit_info["LeavesCorpse"])
        if unit_info and unit_info["LeavesCorpse"] and unit_info["LeavesCorpse"] == 1 then
            return true
        --[[CanRespawn会导致单位死亡不被销毁
        if unit:UnitCanRespawn() == false then
            return false
            --return true
            --]]
        else
            -- Leave corpse     
            return false
        end
    end
end