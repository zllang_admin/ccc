if HeroList_Custom == nil then HeroList_Custom=class({}) end 
if HeroSelect ==nil then HeroSelect=class({}) end 
HeroList_Custom={
--light
	[1]="npc_dota_hero_omniknight",
	[2]="npc_dota_hero_keeper_of_the_light",
	[3]="npc_dota_hero_windrunner",
	[4]="npc_dota_hero_alchemist",
    [5]="npc_dota_hero_leshrac", 
	[6]="npc_dota_hero_sven" ,
	[13]="npc_dota_hero_rubick" ,
	[14]="npc_dota_hero_crystal_maiden" ,
	[15]="npc_dota_hero_phoenix" ,
	[16]="npc_dota_hero_queenofpain" ,
	[21]="npc_dota_hero_invoker" ,
--dark
	[7]="npc_dota_hero_abaddon" ,
	[8]="npc_dota_hero_disruptor",
	[9]="npc_dota_hero_drow_ranger" ,
	[10]="npc_dota_hero_shadow_shaman"  ,
	[11]="npc_dota_hero_juggernaut" ,
	[12]="npc_dota_hero_centaur"  ,
	[17]="npc_dota_hero_faceless_void" ,
	[18]="npc_dota_hero_lina" ,
	[19]="npc_dota_hero_nevermore" ,
	[20]="npc_dota_hero_lich" ,
	[22]="npc_dota_hero_abyssal_underlord" ,
}
HeroSelect.IsFirstHero={}
print("HeroList_Custom is loaded Success!")

HeroList_Custom_Left = {}

for k,v in pairs(HeroList_Custom) do
	HeroList_Custom_Left[k] = k 
end
function CreateRandomHero( playerID )
	print("CreateRandomHero",playerID,#HeroList_Custom_Left)
	if #HeroList_Custom_Left >= 1 then
		local player=PlayerResource:GetPlayer(playerID)
		local gold =PlayerResource:GetGold(playerID)

		local hero_left_index = RandomInt(1, #HeroList_Custom_Left)
		local hero_index = HeroList_Custom_Left[hero_left_index]
		table.remove(HeroList_Custom_Left,hero_left_index)
		player:MakeRandomHeroSelection()

		Timers:CreateTimer(1,function ()
			-- player:MakeRandomHeroSelection()
			local hero = PlayerResource:ReplaceHeroWith(playerID,HeroList_Custom[hero_index],0,0)
				
			PlayerResource:SetGold(playerID,gold,false)
			hero:SetOwner(player)
			hero:SetControllableByPlayer(playerID,true)
			
			--[[移动英雄到对应阵营的出生点]]
			if PlayerResource:GetTeam(playerID) == DOTA_TEAM_GOODGUYS then 
				FindClearSpaceForUnit(hero,Entities:FindByName(nil, MultiHeroControlSystem.BrightHeroSpawner):GetAbsOrigin(),false)
			else
				FindClearSpaceForUnit(hero, Entities:FindByName(nil, MultiHeroControlSystem.DarkHeroSpawner):GetAbsOrigin(),false)
			end
			--[[选择该英雄]] 	
			MultiHeroControlSystem:SelectUnit(player,hero)
			--[[移动镜头到该英雄处]]
			MultiHeroControlSystem:CameraMoveToUnit(playerID,hero)
			--[[通知PUI创建对应界面元素,掉线重连失效点]]
			
		    --CustomGameEventManager:Send_ServerToPlayer(player,"create_hero",{heroname=HeroList_Custom[hero_index],entindex=hero:GetEntityIndex(),player=player})
	    	MultiHeroControlSystem:AddHero(playerID,HeroList_Custom[hero_index],hero:GetEntityIndex())	
	    	local cc=CreateCommunicate(player)
	    	print("CreateCommunicate",CreateCommunicate)
			cc.Send({listener="OnCreateHero",heroname=HeroList_Custom[hero_index],entindex=hero:GetEntityIndex(),player=player})
		end)

		local Dstr1="test"..hero_index
    	local Dstr2="hero_select_"..hero_index
		if hero_index < 10 then
			Dstr1="test0"..hero_index
			Dstr2="hero_select_0"..hero_index
		end
    	print(Dstr1,Dstr2)
		Entities:FindByName(nil,Dstr1):Destroy()
		Entities:FindByName(nil,Dstr2):Destroy()
	end
end