AI_THINK_INTERVAL = 0.5
AI_STATE_IDLE = 0
AI_STATE_AGGRESSIVE = 1
AI_STATE_RETURNING = 2
AI_STATE_SLEEPING = 3

AI_AGGRESIVE_TIME = 5

NeutralAI = {}
NeutralAI.__index = NeutralAI

function NeutralAI:Start( unit )
    --print("Starting NeutralAI for "..unit:GetUnitName().." "..unit:GetEntityIndex())

    local ai = {}
    setmetatable( ai, NeutralAI )

    ai.unit = unit --The unit this AI is controlling
    ai.stateThinks = { --Add thinking functions for each state
        [AI_STATE_IDLE] = 'IdleThink',
        [AI_STATE_AGGRESSIVE] = 'AggressiveThink',
        [AI_STATE_RETURNING] = 'ReturningThink',
        [AI_STATE_SLEEPING] = 'SleepThink'
    }

    unit.state = AI_STATE_IDLE
    unit.spawnPos = unit:GetAbsOrigin()
    unit.campCenter = FindCreepCampCenter(unit)
    unit.allies = FindAllUnitsAroundPoint(unit, unit.campCenter, 1000)
    --print("unit.allies",#unit.allies,unit:GetName())
    unit.acquireRange = unit:GetAcquisitionRange()
    unit.aggroRange = 200 --Range an enemy unit has to be for the group to go from IDLE to AGGRESIVE
    unit.leashRange = 1000 --Range from spawnPos to go from AGGRESIVE to RETURNING
    unit.aggressiveTime = 0 --离开leashRange时间，达到5秒则返回

    -- Disable normal ways of acquisition
    unit:SetIdleAcquire(false)
    unit:SetAcquisitionRange(0)

    --Start thinking
    Timers:CreateTimer(function()
    	-- print("unit thinking",unit.state)
        return ai:GlobalThink()
    end)

    return ai
end

function NeutralAI:GlobalThink()
    local unit = self.unit

    if (not IsValidAlive(unit)) or unit:IsControllableByAnyPlayer() then
        return nil
    end

    -- print("state",unit.state,unit:GetUnitName(),unit:GetAggroTarget())
    --Execute the think function that belongs to the current state
    Dynamic_Wrap(NeutralAI, self.stateThinks[ unit.state ])( self )
    
    return AI_THINK_INTERVAL
end

function NeutralAI:IdleThink()
    local unit = self.unit

    unit.allies = FindAllUnitsAroundPoint(unit, unit.campCenter, 1000)
    
    -- Sleep
    if unit:GetUnitLabel() == "Sleepy" and not GameRules:IsDaytime() then
        unit:RemoveModifierByName("modifier_neutral_idle_aggro")
        ApplyModifier(unit, "modifier_neutral_sleep")
        unit.state = AI_STATE_SLEEPING
        return true
    end

    local target = FindAttackableEnemies( unit, unit.acquireRange )
    -- print("target",target)
    --Start attacking as a group
    if target then
        for _,v in pairs(unit.allies) do
            if IsValidAlive(v) then
                if v.state == AI_STATE_IDLE or v.state == AI_STATE_SLEEPING then
                    v:RemoveModifierByName("modifier_neutral_sleep")
                    v:RemoveModifierByName("modifier_neutral_idle_aggro")
                    v:MoveToTargetToAttack(target)
                    v.aggroTarget = target
                    v.state = AI_STATE_AGGRESSIVE
                end
            end
        end
        return true       
    end
    ApplyModifier(unit, "modifier_neutral_idle_aggro")
    return true
end

function NeutralAI:SleepThink()
    local unit = self.unit

    -- Wake up
    if GameRules:IsDaytime() then
        unit:RemoveModifierByName("modifier_neutral_sleep")

        unit.state = AI_STATE_IDLE
        return true
    end
end

function NeutralAI:AggressiveThink()
    local unit = self.unit

    --Check if the unit has walked outside its leash range
    local distanceFromSpawn = ( unit.spawnPos - unit:GetAbsOrigin() ):Length2D()
    if distanceFromSpawn >= unit.leashRange then
        unit.aggressiveTime = unit.aggressiveTime + AI_THINK_INTERVAL    
    else
        unit.aggressiveTime = 0
    end

    if unit.aggressiveTime >= AI_AGGRESIVE_TIME then
        unit:MoveToPosition( unit.spawnPos )
        unit.state = AI_STATE_RETURNING
        unit.aggroTarget = nil
        return true
    end

    -- print(unit.aggroTarget,target)
    -- Use the acquisition range to find enemies while in aggro state
    local target = FindAttackableEnemies( unit, unit.acquireRange )
    --Check if the unit's target is still alive
    if not IsValidAlive(unit.aggroTarget) then
        -- If there is no other valid target, return
        if not target then
            unit:MoveToPosition( unit.spawnPos )
            unit.state = AI_STATE_RETURNING
            unit.aggroTarget = nil    
        else
            if unit:GetCurrentActiveAbility() then
                return true
            end
            unit:MoveToTargetToAttack(target)
            unit.aggroTarget = target
        end
        return true
    
    -- If the current aggro target is still valid
    else
        if target then
            if unit:GetCurrentActiveAbility() then
                return true
            end

            local range_to_current_target = unit:GetRangeToUnit(unit.aggroTarget)
            local range_to_closest_target = unit:GetRangeToUnit(target)

            -- If the range to the current target exceeds the attack range of the attacker, and there is a possible target closer to it, attack that one instead
            if range_to_current_target > unit:GetAttackRange() and range_to_current_target > range_to_closest_target then
                unit:MoveToTargetToAttack(target)
                unit.aggroTarget = target
            else
                unit:MoveToTargetToAttack(unit.aggroTarget)
            end
        else  
            if unit:GetCurrentActiveAbility() then
                return true
            end
            if unit.aggroTarget then
                unit:MoveToTargetToAttack(unit.aggroTarget)
            end
            -- Can't attack the current target and there aren't more targets close
            if not unit:HasAttackCapability() or unit.aggroTarget:HasModifier("modifier_invisible") then
                unit:MoveToPosition( unit.spawnPos )
                unit.state = AI_STATE_RETURNING
                unit.aggroTarget = nil
            end
        end
    end
    -- print("unit.aggroTarget",unit.aggroTarget)
    StartAbilityAi(unit)
    return true
end

function NeutralAI:ReturningThink()
    local unit = self.unit

    --Check if the AI unit has reached its spawn location yet
    if ( unit.spawnPos - unit:GetAbsOrigin() ):Length2D() < 10 then
        --Go into the idle state
        unit.state = AI_STATE_IDLE
        ApplyModifier(unit, "modifier_neutral_idle_aggro")
        return true
    -- elseif ( unit.spawnPos - unit:GetAbsOrigin() ):Length2D() < unit.leashRange  then
    --     --Go into the idle state
    --     ApplyModifier(unit, "modifier_neutral_idle_aggro")
    --     return true
    else
        unit:MoveToPosition( unit.spawnPos )
        return true
    end
end

-- Return a valid attackable unit or nil if there are none
function FindAttackableEnemies( unit, radius )
    local enemies = FindEnemiesInRadius( unit, radius )
    -- print("enemies",enemies)
    for _,target in pairs(enemies) do
        if unit:HasAttackCapability() and not target:HasModifier("modifier_invisible") and not (unit:GetAttackCapability()==DOTA_UNIT_CAP_MELEE_ATTACK and target:HasFlyMovementCapability()) then
            return target
        end
    end
    return nil
end

-- Looks for the center minimap_ unit
function FindCreepCampCenter( unit )
    local units = FindUnitsInRadius(DOTA_TEAM_NEUTRALS, unit:GetAbsOrigin(), nil, 1000, DOTA_UNIT_TARGET_TEAM_FRIENDLY, 
                                    DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD, FIND_CLOSEST, false)
    for _,neutral in pairs(units) do
        if string.match(neutral:GetUnitName(), "minimap_") then
            return neutral:GetAbsOrigin()
        end
    end
end

function FindAllUnitsAroundPoint( unit, point, radius )
    local team = unit:GetTeamNumber()
    local target_type = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING--DOTA_UNIT_TARGET_ALL
    local flags = DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES
    return FindUnitsInRadius(team, unit:GetAbsOrigin(), nil, radius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, target_type, flags, FIND_ANY_ORDER, false)
end

-- Returns all visible enemies in radius of the unit
function FindEnemiesInRadius( unit, radius )
    local team = unit:GetTeamNumber()
    local position = unit:GetAbsOrigin()
    local target_type = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC +DOTA_UNIT_TARGET_BUILDING
    local flags = DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE + DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_NOT_ATTACK_IMMUNE
    return FindUnitsInRadius(team, position, nil, radius, DOTA_UNIT_TARGET_TEAM_ENEMY, target_type, flags, FIND_CLOSEST, false)
end

function IsValidAlive( unit )
    return (IsValidEntity(unit) and unit:IsAlive())
end

function ApplyModifier( unit, modifier_name )
    local item = CreateItem("item_apply_modifiers", nil, nil)
    item:ApplyDataDrivenModifier(unit, unit, modifier_name, {})
    item:RemoveSelf()
end

function WakeUp( event )
    local unit = event.unit
    local attacker = event.attacker

    for _,v in pairs(unit.allies) do
        if IsValidAlive(v) then
            v:RemoveModifierByName("modifier_neutral_sleep")
            v.state = AI_STATE_AGGRESSIVE
            if not IsValidAlive(v.aggroTarget) then
                v:MoveToTargetToAttack(attacker)
                v.aggroTarget = attacker
            end
        end
    end
end

function NeutralAggro( event )
    local unit = event.unit
    local attacker = event.attacker

    for _,v in pairs(unit.allies) do
        if IsValidAlive(v) and (v.state == AI_STATE_IDLE or v.state == AI_STATE_RETURNING ) then
            v:RemoveModifierByName("modifier_neutral_idle_aggro")
            v.state = AI_STATE_AGGRESSIVE
            if not IsValidAlive(v.aggroTarget) then
                v:MoveToTargetToAttack(attacker)
                v.aggroTarget = attacker
            end
        end
    end
end