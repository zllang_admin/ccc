--[[自定义经验分配系统]]
if ExperienceSystem == nil then ExperienceSystem=class({}) end 
--[[经验比例]]
ExperienceSystem.Ratio={
						[1]=105,
						[2]=202,
						[3]=294,
						[4]=384,
						[5]=475,
						[6]=540,
						[7]=595,
						[8]=650,
						[9]=705,
						[10]=760
					}

function ExperienceSystem:Init()
	ListenToGameEvent('entity_killed', Dynamic_Wrap(ExperienceSystem, 'OnEntityKilled'), self)
end
function ExperienceSystem:OnEntityKilled( keys)
	if not keys.entindex_killed or not keys.entindex_attacker then
		return
	end
	local killed = EntIndexToHScript(keys.entindex_killed)
	local killer=EntIndexToHScript(keys.entindex_attacker)
	local experience=killed:GetDeathXP()
	local hero=FindUnitsInRadius(killer:GetTeam(), killed:GetAbsOrigin() , nil, 1200 , DOTA_UNIT_TARGET_TEAM_FRIENDLY	, DOTA_UNIT_TARGET_HERO, 0, 0, false)	
	
	local heroNum=GameRules:GetGameModeEntity().DB.HeroNum[killer:GetTeam()]
	if heroNum==nil or heroNum==0 then return end
	if heroNum>=10 then heroNum =10 end
	if killed:IsRealHero() then
		KillFilter( killed, killer )
	end
	if hero[1] then 
		for k,v in pairs(hero) do
			v:AddExperience(0.01*experience*ExperienceSystem.Ratio[heroNum]/#hero,0,false,false)
		end
	else 
		if killer:GetTeam()~=DOTA_TEAM_BADGUYS and killer:GetTeam() ~= DOTA_TEAM_GOODGUYS   then return end
		for k,v in pairs(GameRules:GetGameModeEntity().DB.Heros[killer:GetTeam()]) do
			EntIndexToHScript(v):AddExperience(0.01*experience*ExperienceSystem.Ratio[heroNum]/heroNum,0,false,false)
		end
	end
end		

function KillFilter( killed, killer )
	local kill_gold = 500
	local teammate_gold = 200
  	local killer_player = killer:GetPlayerOwner() or killer:GetOwner()
  	local killer_playerID = -1
  	if killer_player then
  		-- print("killer_player",killer_player,killer_player:IsPlayer())
  		killer_playerID = killer_player:GetPlayerID()
  	end
  	local killer_team = killer:GetTeam()
	local killed_team = killed:GetTeam()
	if (killer_team == DOTA_TEAM_GOODGUYS and killed_team == DOTA_TEAM_BADGUYS) or (killer_team == DOTA_TEAM_BADGUYS and killed_team == DOTA_TEAM_GOODGUYS) then
		if PlayerResource:IsValidPlayerID(killer_playerID) then
			PlayerResource:ModifyGold(killer_playerID, kill_gold, false, 12)
			SendOverheadEventMessage( nil, OVERHEAD_ALERT_GOLD, killed, kill_gold, nil )
		end
		for playerID = 0, DOTA_MAX_TEAM_PLAYERS do
    		if PlayerResource:IsValidPlayerID(playerID) and PlayerResource:IsValidPlayer(playerID) then
    			local player=PlayerResource:GetPlayer(playerID)   
    			print("player",player,playerID)   
    			if player and playerID ~= killer_playerID and player:GetTeam() == killer_team then
    				PlayerResource:ModifyGold(playerID, teammate_gold, false, 12)
					-- SendOverheadEventMessage( player, OVERHEAD_ALERT_GOLD, player, teammate_gold, player )
	    		end
	        end
	    end
    end
end