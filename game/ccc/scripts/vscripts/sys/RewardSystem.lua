--Copyright (c) 2016-2017 野猪大大(zllang)

--奖赏系统，本系统将包括，积分奖励，金币奖励，VIP福利等功能
--简单的积分系统，算法不一定完美。。勉强够用吧，要刷分的，刷刷刷。。。。

if RewardSystem ==nil then RewardSystem =class({}) end 

RewardSystem.Winner=0 --游戏未结束
RewardSystem.IsValidGame=true
RewardSystem.Result={}
RewardSystem.Result.HighMarkStart=nil
RewardSystem.Result.IsNormalEnding=false
--赢家是强队
RewardSystem.RewardTypeHW={
	[1]=3,
	[2]=1,
	[3]=1,
	[4]=1,
	[5]=0
}
RewardSystem.RewardTypeHL={
	[1]=1,
	[2]=0,
	[3]=0,
	[4]=0,
	[5]= -1
}
--赢家是弱队
RewardSystem.RewardTypeLW={
	[1]=12,
	[2]=8,
	[3]=6,
	[4]=4,
	[5]=2
}
RewardSystem.RewardTypeLL={
	[1]=  0,
	[2]= -1,
	[3]= -2,
	[4]= -3,
	[5]= -3
}
--[[遗迹呗摧毁的时候调用]]
_G.GameResult=function( team )
	print('relic has destroyed!')
	if team == 1 then 
		RewardSystem.Winner=DOTA_TEAM_BADGUYS

	elseif team==2 then 
        RewardSystem.Winner=DOTA_TEAM_GOODGUYS
	end
	local Winner=RewardSystem.Winner
	--print('The Winner is:',Winner)
	--print('_G.GameResult start to call GameOver()')
	RewardSystem:GameOver()
end
--[[死亡模式结束]]
--在死亡模式中调用，参数为winner:DOTA_TEAM_GOODGUYS 或者 DOTA_TEAM_BADGUYS
function RewardSystem:GameOver_Mortality(Winner)
	print('GameOver in Mortality')
	RewardSystem.Winner=Winner
	--print('The Winner is:',Winner)
	self.Result.IsNormalEnding=true
	--print('GameOver_Mortality() start to call GameOver()')
	self:GameOver()
end
--游戏结束
function RewardSystem:GameOver()
	--print('GameOver() is called!')
	--print('IsValidGame:',self.IsValidGame)
	if self.IsValidGame==true then
		--print('Start to CalculateScore,call CalculateScore()')
		local good,bad=self:CalculateScore()
		--print('Start to Reward,call Reward(good,bad)')
		self:Reward(good,bad)
		--print('Reward success,start to set the winner!')
		GameRules:SetGameWinner(self.Winner)
	end	
end
--完成奖励
function RewardSystem:Reward( good,bad )
	--如果本局有效，则进行奖励
	--print('Reward is starting......')
	if self.IsValidGame==true then 
		local Winner=self.Winner
		--print('The winner is:',Winner)
		local HighMarkStart=self.Result.HighMarkStart
		--print('The HighMark team is:',HighMarkStart,DOTA_TEAM_GOODGUYS)
		if Winner==HighMarkStart and Winner==DOTA_TEAM_GOODGUYS then 
			for i=1,#good do
				GameRules:GetGameModeEntity().DB.Reward(good[i].SteamID,'Score', self.RewardTypeHW[i])
			end
			for i=1,#bad do
				GameRules:GetGameModeEntity().DB.Reward(bad[i].SteamID,'Score', self.RewardTypeHL[i])
			end
		elseif Winner==HighMarkStart and Winner==DOTA_TEAM_BADGUYS then
			for i=1,#good do
				GameRules:GetGameModeEntity().DB.Reward(good[i].SteamID,'Score', self.RewardTypeHL[i])
			end
			for i=1,#bad do
				GameRules:GetGameModeEntity().DB.Reward(bad[i].SteamID,'Score', self.RewardTypeHW[i])
			end
		
		elseif Winner~=HighMarkStart and Winner==DOTA_TEAM_GOODGUYS then
			for i=1,#good do
				GameRules:GetGameModeEntity().DB.Reward(good[i].SteamID,'Score', self.RewardTypeLW[i])
			end
			for i=1,#bad do
				GameRules:GetGameModeEntity().DB.Reward(bad[i].SteamID,'Score', self.RewardTypeLL[i])
			end
		
		elseif Winner~=HighMarkStart and Winner==DOTA_TEAM_BADGUYS then
			for i=1,#good do
				GameRules:GetGameModeEntity().DB.Reward(good[i].SteamID,'Score', self.RewardTypeLL[i])
			end
			for i=1,#bad do
				GameRules:GetGameModeEntity().DB.Reward(bad[i].SteamID,'Score', self.RewardTypeLW[i])
			end
		
		end
		--print('start to UpdateToServer....')
		GameRules:GetGameModeEntity().DB.UpdateToServer()
	end
end
--检查玩家在线状态
function RewardSystem:CheckPlayerOnline()
	--print('start to check player online state.....')
	if self.IsValidGame==true then 
		local G_count=0
		local B_count=0
		local count_G = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_GOODGUYS)
	  	for i=1,count_G do
	  		local pid = PlayerResource:GetNthPlayerIDOnTeam(DOTA_TEAM_GOODGUYS, i)
	  		if PlayerResource:GetConnectionState(pid) == 2 then
	   	 		G_count=G_count+1  	
	   	 	end 			  	 	
	  	end
	  	local count_B = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_BADGUYS)
	  	for i=1,count_B do
	  		local pid = PlayerResource:GetNthPlayerIDOnTeam(DOTA_TEAM_BADGUYS, i)
	  		if PlayerResource:GetConnectionState(pid) == 2 then
	   	 		B_count=B_count+1  	
	   	 	end 			  	 	
	  	end
	  	--print('The player online count is(G/B):',G_count,'  ',B_count)
	  	if G_count < 1  then 
	  		self.Winner=DOTA_TEAM_BADGUYS
	  		self:GameOver()
	  	elseif B_count < 1 then 
	  		self.Winner=DOTA_TEAM_GOODGUYS
	  		self:GameOver()
	  	end
	end
end
--检查是否为单人模式或非对战
function RewardSystem:CheckGameMode()
	-- 单人或者单队伍模式，不记录积分
	--print('start to check game mode....')
	local count_G = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_GOODGUYS)
	local count_B = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_BADGUYS)
	--print('check player count:',count_G,'  ',count_B)
	if count_G==0 or count_B==0 then 
		self.IsValidGame=false 
		print('This is a single player mode,no score!')
	else
		print('This game is multi player mode,score mode!')
	end
end
function RewardSystem:PlayerDisconnect( keys )
	--当掉线时，设置该玩家掉线
	--触发检查玩家在线状态，若某一方全部掉线，设置输赢
	--print('PlayerDisconnect!,start to check online player count..')
	self:CheckPlayerOnline()

end
function RewardSystem:PlayerReconnect( keys )
	--当重连接时，恢复在线
end
function RewardSystem:OnHeroInGame( keys )
	--print('OnHeroInGame!start to check gamemode!')
	self:CheckGameMode()
	--计算开局强队，即队伍综合分数较高的。
	--print('start to call Powerhouse() to calculate Powerhouse..')
	self.Result.HighMarkStart=self:Powerhouse()
end
--[[计算队伍初始积分均值]]
function RewardSystem:CalculateAverageScore()
	--print('starting CalculateAverageScore....')
	local count = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_GOODGUYS)
	local Goodguys = 0
  	for i=1,count do
  		local pid = PlayerResource:GetNthPlayerIDOnTeam(DOTA_TEAM_GOODGUYS, i) 
		if type( GameRules:GetGameModeEntity().DB.GetPlayerScore(PlayerResource:GetSteamAccountID(pid)) ) ~= "number" then 
			Goodguys = Goodguys+1000
		else 
   	 		Goodguys = Goodguys+GameRules:GetGameModeEntity().DB.GetPlayerScore(PlayerResource:GetSteamAccountID(pid))  
   	 	end 	 		  	 	
  	end
  	local AverGoodguys=Goodguys/5
    count = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_BADGUYS)
	local Badguys = 0
  	for i=1,count do
  		local pid = PlayerResource:GetNthPlayerIDOnTeam(DOTA_TEAM_BADGUYS, i)
  		if type( GameRules:GetGameModeEntity().DB.GetPlayerScore(PlayerResource:GetSteamAccountID(pid)) ) ~= "number" then 
			Badguys = Badguys+1000
		else
   	 		Badguys = Badguys+GameRules:GetGameModeEntity().DB.GetPlayerScore(PlayerResource:GetSteamAccountID(pid))
   	 	end   	 		  	 	
  	end
  	local AverBadguys=Badguys/5
  	--print('CalculateAverageScore Result(G/B):',AverGoodguys,'  ',AverBadguys)
  	return AverGoodguys,AverBadguys
end
--[[计算强队]]
function RewardSystem:Powerhouse( )
	local good,bad=self:CalculateAverageScore()
	if good>=bad then 
		return DOTA_TEAM_GOODGUYS
	else
		return DOTA_TEAM_BADGUYS
	end
end
--[[计算玩家得分]]
function RewardSystem:CalculateScore()
	----print('starting to CalculateScore......')
	local Goodguys={}
	local Badguys={}
	local count = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_GOODGUYS)
  	for i=1,count do
  		local pid = PlayerResource:GetNthPlayerIDOnTeam(DOTA_TEAM_GOODGUYS, i)
  		if 	Goodguys[i]== nil then Goodguys[i]={} end
  		Goodguys[i].SteamID=PlayerResource:GetSteamAccountID(pid)
  		local GoldRatio=0
  		if self:TeamTotalGold(DOTA_TEAM_GOODGUYS)~=0 then 
  			GoldRatio=5*4*PlayerResource:GetTotalEarnedGold(pid)/self:TeamTotalGold(DOTA_TEAM_GOODGUYS)
  		end
   	 	Goodguys[i].Score = (PlayerResource:GetKills(pid)+1)*8-PlayerResource:GetDeaths(pid)*2+ GoldRatio	 		  	 	
  	end
  	Goodguys=self:bubble_sort(Goodguys)
  	count = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_BADGUYS)
  	for i=1,count do
  		local pid = PlayerResource:GetNthPlayerIDOnTeam(DOTA_TEAM_BADGUYS, i)
  		if 	Badguys[i]== nil then Badguys[i]={} end
  		Badguys[i].SteamID=PlayerResource:GetSteamAccountID(pid)
  		local GoldRatio=0
  		if self:TeamTotalGold(DOTA_TEAM_GOODGUYS)~=0 then 
  			GoldRatio=5*4*PlayerResource:GetTotalEarnedGold(pid)/self:TeamTotalGold(DOTA_TEAM_GOODGUYS)
  		end
   	 	Badguys[i].Score = (PlayerResource:GetKills(pid)+1)*8-PlayerResource:GetDeaths(pid)*2+ GoldRatio	 		  	 	
  	end
  	Badguys=self:bubble_sort(Badguys)
  	return Goodguys,Badguys
end
--[[计算队伍金币总收入]]
function RewardSystem:TeamTotalGold( team )
	--print('TeamTotalGold.....')
	local count = PlayerResource:GetPlayerCountForTeam(team)
	local gold=0
  	for i=1,count do
  		local pid = PlayerResource:GetNthPlayerIDOnTeam(team, i)
   	 	gold = gold+PlayerResource:GetTotalEarnedGold(pid) 		  	 	
  	end
  	return gold
end
function RewardSystem:bubble_sort(arr)
	local tmp = {}
	for i=1,#arr-1 do
		for j=1,#arr-i do
			if arr[j].Score < arr[j+1].Score then
				tmp = arr[j]
				arr[j] = arr[j+1]
				arr[j+1] = tmp
			end
		end
	end
	return arr
end


