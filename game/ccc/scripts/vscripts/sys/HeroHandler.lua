-- Copyright (c) 2015-2016 野猪大大(zllang)
if HeroHandler ==nil then 
	HeroHandler=class({}) 
end
LinkLuaModifier("modifier_common","sys/modifiers/modifier_common.lua",LUA_MODIFIER_MOTION_NONE)

function HeroHandler:Init(hero)
	-- 初始化英雄
	--添加通用技能
	if hero:IsRealHero() then 
		hero:AddAbility("ability_common")		
		local ability=hero:FindAbilityByName("ability_common"):SetLevel(1)
		hero:AddNewModifier(hero,ability,"modifier_common",{})
		HeroHandler:AddFunction(hero)
		MultiHeroControlSystem:CameraMoveToUnit(hero:GetPlayerID(),hero)
	end 
end
function HeroHandler:AddFunction( hero )
	-- 添加方法
	for _,v in pairs(self) do
		table.insert(hero,v)
	end
end