CREEP_AI_THINK_INTERVAL = 1
CREEP_AI_STATE_MOVE = 0
CREEP_AI_STATE_AGGRESSIVE = 1

CreepAI = {}
CreepAI.__index = CreepAI

function CreepAI:Start( unit,FinalPosition )
    --print("Starting CreepAI for "..unit:GetUnitName().." "..unit:GetEntityIndex())

    local ai = {}
    setmetatable( ai, CreepAI )

    ai.unit = unit --The unit this AI is controlling
    ai.stateThinks = { --Add thinking functions for each state
        [CREEP_AI_STATE_MOVE] = 'MoveThink',
        [CREEP_AI_STATE_AGGRESSIVE] = 'AggressiveThink',
    }

    unit.state = CREEP_AI_STATE_MOVE
    unit.acquireRange = unit:GetAcquisitionRange()
    unit.FinalPosition = FinalPosition

    -- Disable normal ways of acquisition
    unit:SetIdleAcquire(false)
    unit:SetAcquisitionRange(0)

    --Start thinking
    Timers:CreateTimer(0.04,function()
    	--print("unit thinking",unit:GetName(),unit.state)
        return ai:GlobalThink()
    end)

    return ai
end

function CreepAI:GlobalThink()
    local unit = self.unit

    if (not unit) or (not IsValidAlive(unit)) or (not unit:IsAlive()) or unit:IsControllableByAnyPlayer() then
        return nil
    end

    if not unit.aggroTarget or not IsValidAlive(unit.aggroTarget) or not unit.aggroTarget:IsAlive() then
        unit.aggroTarget = nil
    end
    --print("state",unit.state,unit:GetUnitName(),unit:GetAggroTarget())
    --Execute the think function that belongs to the current state
    Dynamic_Wrap(CreepAI, self.stateThinks[ unit.state ])( self )
    return CREEP_AI_THINK_INTERVAL
end

function CreepAI:MoveThink()
    local unit = self.unit

    local targetID = FindCreepAggroTarget( unit )
        
    if targetID then
        local target = EntIndexToHScript(targetID)
        --print("target",target,target:GetUnitName())
        if unit.state == CREEP_AI_STATE_MOVE then
            unit:MoveToTargetToAttack(target)
            unit.aggroTarget = target
            unit.state = CREEP_AI_STATE_AGGRESSIVE
        end   
    else
        unit.aggroTarget = nil
        if (not unit.ReachFinalPosition) and (unit.FinalPosition - unit:GetAbsOrigin()):Length2D() < 150 then
            unit.ReachFinalPosition = true
            if unit:GetTeam() == DOTA_TEAM_GOODGUYS then
                unit.FinalPosition = Entities:FindByName(nil,"dota_badguys_fort"):GetAbsOrigin()
            elseif unit:GetTeam() == DOTA_TEAM_BADGUYS then
                unit.FinalPosition = Entities:FindByName(nil,"dota_goodguys_fort"):GetAbsOrigin()
            end
        end
        unit:MoveToPosition(unit.FinalPosition)
    end

    return true
end

function CreepAI:AggressiveThink()
    local unit = self.unit

    local targetID = FindCreepAggroTarget( unit )
    if targetID then
        if unit:GetCurrentActiveAbility() then
            return true
        end
        local target = EntIndexToHScript(targetID)
        if unit.state == CREEP_AI_STATE_AGGRESSIVE then
            unit:MoveToTargetToAttack(target)
            unit.aggroTarget = target
            unit.state = CREEP_AI_STATE_AGGRESSIVE
            StartAbilityAi(unit)
        end  
    else
        unit.state = CREEP_AI_STATE_MOVE
        unit.aggroTarget = nil
        unit:MoveToPosition(unit.FinalPosition)
    end
    return true
end

function FindCreepAggroTarget( unit )
    local enemies = FindEnemiesInRadius( unit, unit.acquireRange )
    local team = unit:GetTeam()
    local AggroLevels = {}
    for k,v in pairs(enemies) do
        local AggroTarger = v:GetAggroTarget()
        
        if not (v:HasFlyMovementCapability() and unit:GetAttackCapability()==DOTA_UNIT_CAP_MELEE_ATTACK or (not IsValidAlive(v))) then
            local targetID = v:GetEntityIndex()
            if AggroTarger and AggroTarger:GetTeam() == team then 
                if AggroTarger:IsRealHero() then
                    AggroLevels[targetID] = 3
                elseif AggroTarger:GetTeam() == v:GetTeam() then
                    AggroLevels[targetID] = 0
                else
                    AggroLevels[targetID] = 2
                end
            else
                AggroLevels[targetID] = 1
            end
        end
    end

    for k,v in pairs(AggroLevels) do
        if v == 3 then
            return k
        end
    end

    --print("IsAttacking",unit:IsAttacking())
    if unit:IsAttacking() and unit.aggroTarget and IsValidAlive(unit.aggroTarget) then
        local aggroTargetIndex = unit.aggroTarget:GetEntityIndex()
        if AggroLevels[aggroTargetIndex] and AggroLevels[aggroTargetIndex]>0 then
            return aggroTargetIndex
        end
    end

    for k,v in pairs(AggroLevels) do
        if v == 2 or v == 1 then
            return k
        end
    end

    for k,v in pairs(AggroLevels) do
        if v == 1 then
            return k
        end
    end

    for k,v in pairs(AggroLevels) do
        if v == 0 then
            return k
        end
    end

    return nil
end