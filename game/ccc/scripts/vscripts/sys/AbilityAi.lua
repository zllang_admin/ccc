function StartAbilityAi( unit )
    -- print("StartAbilityAi")
	local ability = unit:FindAbilityByName("deluyi_paoxiao")
	local aggroTarget = unit.aggroTarget
    if ability and aggroTarget and ability:IsFullyCastable() then
    	if not unit:HasModifier("modifier_deluyi_paoxiao") then
    		CastAbilityAnyType( ability,unit,target,position )
    		return
    	end
    end

    ability = unit:FindAbilityByName("mud_golem_hurl_boulder")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	if not aggroTarget:IsStunned() then
    		CastAbilityAnyType( ability,unit,aggroTarget,position )
    		return
    	end
    end

    ability = unit:FindAbilityByName("brewmaster_storm_cyclone")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	if not aggroTarget:IsStunned() then
    		CastAbilityAnyType( ability,unit,aggroTarget,position )
    		return
    	end
    end 

    --[[
    ability = unit:FindAbilityByName("duyezhizhu_baofengxue")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	CastAbilityAnyType( ability,unit,aggroTarget,aggroTarget:GetAbsOrigin() )
    	return
    end
    --]]
    
    ability = unit:FindAbilityByName("clinkz_searing_arrows")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	CastAbilityAnyType( ability,unit,aggroTarget,position )
    	return
    end

    ability = unit:FindAbilityByName("satyr_soulstealer_mana_burn")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	CastAbilityAnyType( ability,unit,aggroTarget,position )
    	return
    end

    ability = unit:FindAbilityByName("heianwuzhe_canfei")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	if not aggroTarget:HasModifier("modifier_heianwuzhe_canfei") then
    		CastAbilityAnyType( ability,unit,aggroTarget,position )
    		return
    	end
    end

    ability = unit:FindAbilityByName("forest_troll_high_priest_heal")
    if ability and ability:IsFullyCastable() then
    	CastAbilityAnyType( ability,unit,aggroTarget,position )
    	return
    end

    ability = unit:FindAbilityByName("satyr_hellcaller_shockwave")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	CastAbilityAnyType( ability,unit,aggroTarget,position )
    	return
    end

    ability = unit:FindAbilityByName("harpy_storm_chain_lightning")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	CastAbilityAnyType( ability,unit,aggroTarget,position )
    	return
    end

    ability = unit:FindAbilityByName("warlock_rain_of_chaos")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	CastAbilityAnyType( ability,unit,aggroTarget,aggroTarget:GetAbsOrigin() )
    	return
    end

    ability = unit:FindAbilityByName("lich_frost_nova")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	CastAbilityAnyType( ability,unit,aggroTarget,position )
    	return
    end

    ability = unit:FindAbilityByName("shirenguishouling_jiuchangenxu")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	if not aggroTarget:HasModifier("modifier_shirenguishouling_jiuchangenxu") then
    		CastAbilityAnyType( ability,unit,aggroTarget,position )
    		return
    	end
    end

    ability = unit:FindAbilityByName("polar_furbolg_ursa_warrior_thunder_clap")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	CastAbilityAnyType( ability,unit,aggroTarget,position )
    	return
    end

    ability = unit:FindAbilityByName("lion_impale")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	CastAbilityAnyType( ability,unit,aggroTarget,aggroTarget:GetAbsOrigin() )
    	return
    end

    ability = unit:FindAbilityByName("centaur_khan_war_stomp")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	local radius = ability:GetSpecialValueFor("radius")
    	if FindEnemiesInRadius( unit, radius ) then
    		CastAbilityAnyType( ability,unit,aggroTarget,position )
    		return
    	end
    end

    ability = unit:FindAbilityByName("shenyuanlingzhuyouling_zuzhou")
    if ability and aggroTarget and ability:IsFullyCastable() then
    	if not aggroTarget:HasModifier("modifier_shenyuanlingzhuyouling_zuzhou") then
    		CastAbilityAnyType( ability,unit,aggroTarget,position )
    		return
    	end
    end

    ability = unit:FindAbilityByName("shouweipaodan")
    if ability and aggroTarget and ability:IsFullyCastable() then
        if not aggroTarget:IsStunned() then
            -- print("shouweipaodan")
            CastAbilityAnyType( ability,unit,aggroTarget,position )
            return
        end
    end

    ability = unit:FindAbilityByName("storm_spirit_static_remnant")
    if ability and aggroTarget and ability:IsFullyCastable() then
        if not aggroTarget:IsStunned() then
            -- print("shouweipaodan")
            CastAbilityAnyType( ability,unit,aggroTarget,position )
            return
        end
    end

    ability = unit:FindAbilityByName("storm_spirit_electric_vortex")
    if ability and aggroTarget and ability:IsFullyCastable() then
        if not aggroTarget:IsStunned() then
            -- print("shouweipaodan")
            CastAbilityAnyType( ability,unit,aggroTarget,position )
            return
        end
    end

    ability = unit:FindAbilityByName("ember_spirit_searing_chains")
    if ability and aggroTarget and ability:IsFullyCastable() then
        if not aggroTarget:IsStunned() then
            -- print("shouweipaodan")
            CastAbilityAnyType( ability,unit,aggroTarget,position )
            return
        end
    end

    ability = unit:FindAbilityByName("ember_spirit_sleight_of_fist")
    if ability and aggroTarget and ability:IsFullyCastable() then
        if not aggroTarget:IsStunned() then
            -- print("shouweipaodan")
            CastAbilityAnyType( ability,unit,aggroTarget,position )
            return
        end
    end

    ability = unit:FindAbilityByName("ember_spirit_flame_guard")
    if ability and aggroTarget and ability:IsFullyCastable() then
        if not aggroTarget:IsStunned() then
            -- print("shouweipaodan")
            CastAbilityAnyType( ability,unit,aggroTarget,position )
            return
        end
    end
end

function CastAbilityAnyType( ability,unit,target,position )
	if unit:GetCurrentActiveAbility() or ability:IsInAbilityPhase() or ability:IsChanneling() then
		return
	elseif target and ((target:IsMagicImmune() and not CheckBitBand( ability:GetAbilityTargetFlags(),DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES)) or target:IsInvulnerable()) then
		return
	end
	-- print("GetBehavior()",ability:GetBehavior())
	-- print("NO_TARGET",DOTA_ABILITY_BEHAVIOR_NO_TARGET)
	-- print("UNIT_TARGET",DOTA_ABILITY_BEHAVIOR_UNIT_TARGET)
	-- print("POINT",DOTA_ABILITY_BEHAVIOR_POINT)
	-- print("AUTOCAST",DOTA_ABILITY_BEHAVIOR_AUTOCAST)
	local behaviors = ability:GetBehavior()
	if CheckBitBand( behaviors,DOTA_ABILITY_BEHAVIOR_AUTOCAST ) then
		if not ability:GetAutoCastState() then
			-- print("ToggleAutoCast()")
			ability:ToggleAutoCast()
		end
	elseif CheckBitBand( behaviors,DOTA_ABILITY_BEHAVIOR_NO_TARGET ) then
		-- print("CastAbilityNoTarget")
		unit:CastAbilityNoTarget(ability, -1)
	elseif CheckBitBand( behaviors,DOTA_ABILITY_BEHAVIOR_UNIT_TARGET ) then
		-- print("CastAbilityOnTarget")
		unit:CastAbilityOnTarget(target, ability, -1)
	elseif CheckBitBand( behaviors,DOTA_ABILITY_BEHAVIOR_POINT ) then
		-- print("CastAbilityOnPosition")
		-- print(ability:GetAbilityName())
        if not position then
            position = target:GetAbsOrigin()
        end
		unit:CastAbilityOnPosition(position, ability, -1)
	end	
end

function CheckBitBand( behaviors,monoBehavior )
	return monoBehavior == bit.band( monoBehavior, behaviors )
end

AbilityAI = {}
AbilityAI.__index = AbilityAI

ABILITTY_AI_STATE_AGGRESSIVE = 1
ABILITTY_AI_THINK_INTERVAL = 1

function AbilityAI:Start(unit)
    print("Starting AbilityAI for "..unit:GetUnitName().." "..unit:GetEntityIndex())

    local ai = {}
    setmetatable( ai, AbilityAI )

    ai.unit = unit --The unit this AI is controlling

    ai.stateThinks = { --Add thinking functions for each state
        [ABILITTY_AI_STATE_AGGRESSIVE] = 'AggressiveThink',
    }

    unit.state = ABILITTY_AI_STATE_AGGRESSIVE
    unit.acquireRange = unit:GetAcquisitionRange()
    --Start thinking
    Timers:CreateTimer(0.04,function()
        --print("unit thinking",unit:GetName(),unit.state)
        return ai:GlobalThink()
    end)

    return ai
end

function AbilityAI:GlobalThink()
    local unit = self.unit

    if not unit or not IsValidAlive(unit) or not unit:IsAlive() then
        return nil
    end

    if not unit.aggroTarget or not IsValidAlive(unit.aggroTarget) or not unit.aggroTarget:IsAlive() then
        unit.aggroTarget = nil
    end
    --print("state",unit.state,unit:GetUnitName(),unit:GetAggroTarget())
    --Execute the think function that belongs to the current state
    Dynamic_Wrap(AbilityAI, self.stateThinks[ unit.state ])( self )
    return ABILITTY_AI_THINK_INTERVAL
end

function AbilityAI:AggressiveThink()
    local unit = self.unit

    local aggroTarget = unit:GetAttackTarget()
 
    if not aggroTarget then
        return true
    else
        -- print("aggroTarget",aggroTarget,IsCastableOnTarget( aggroTarget ))
        if IsCastableOnTarget( aggroTarget ) then
            unit.aggroTarget = aggroTarget 
            StartAbilityAi( unit )
        else
            local enemies = FindEnemiesInRadius( unit, unit.acquireRange )
            for k,v in pairs(enemies) do
                if IsCastableOnTarget( v ) then
                    unit.aggroTarget = aggroTarget 
                    StartAbilityAi( unit )
                    return true
                end
            end
        end
    end

    return true
end

function IsCastableOnTarget( target )
    if target and IsValidAlive(target) and target:IsAlive() and (not (target:IsMagicImmune() or target:IsInvulnerable())) then
        return true
    else
        return false
    end
end