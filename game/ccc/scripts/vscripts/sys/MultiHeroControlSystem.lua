-- Copyright (c) 2015-2016 野猪大大(zllang)
--=============================================================================================================
--=========================================多控系统============================================================
--=============================================================================================================
require('libraries/timers')
if MultiHeroControlSystem==nil then MultiHeroControlSystem=class({}) end

LinkLuaModifier("modifier_commandrestricted","sys/modifiers/modifier_commandrestricted.lua",LUA_MODIFIER_MOTION_NONE)

	_G.player={} 
--===========================================配置==============================================================
	--[[最大英雄数量]]
	MultiHeroControlSystem.MaxPlayerNum=20
	--[[黑暗精灵出生点]]
	MultiHeroControlSystem.DarkElfSpawner="eif_spawn_2"
	--[[光明精灵出生点]]
	MultiHeroControlSystem.BrightElfSpawner="eif_spawn_1"
	--[[黑暗英雄出生点]]
	MultiHeroControlSystem.DarkHeroSpawner="hero_spawn_2"
	--[[光明英雄出生点]]
	MultiHeroControlSystem.BrightHeroSpawner="hero_spawn_1"
--========================================多控系统初始化=======================================================
function MultiHeroControlSystem:Init()
	--GameRules:GetGameModeEntity().player={}
	--测试事件1
	CustomGameEventManager:RegisterListener("test", Dynamic_Wrap(MultiHeroControlSystem,"test01"))
	--测试事件2
	CustomGameEventManager:RegisterListener("test1", Dynamic_Wrap(MultiHeroControlSystem,"test02"))
	--移动相机至单位事件
	CustomGameEventManager:RegisterListener( "center_hero_camera", Dynamic_Wrap(MultiHeroControlSystem,"CameraMoveToHeroByName"))
	--移动相机至单位事件BY UNiT
	CustomGameEventManager:RegisterListener( "center_hero_camera_unit", Dynamic_Wrap(MultiHeroControlSystem,"_CameraMoveToUnit"))
	--对头像施法
	CustomGameEventManager:RegisterListener( "cast_on_heroimage", Dynamic_Wrap(MultiHeroControlSystem,"CastOnHeroimage"))
    --添加小精灵
    CustomGameEventManager:RegisterListener( "create_hero_lua", Dynamic_Wrap(MultiHeroControlSystem,"Create_Elf"))
    --玩家掉线重连
    -- ListenToGameEvent("player_reconnected", Dynamic_Wrap(MultiHeroControlSystem, 'On_player_reconnected '), self)
end

--========================================临时测试函数=========================================================

local HeroRandon=function ( )
	--测试
	HeroList_Custom={
		[1]="npc_dota_hero_abaddon",
		[2]="npc_dota_hero_alchemist",
		[3]="npc_dota_hero_antimage",
		[4]="npc_dota_hero_bane",
	    [5]="npc_dota_hero_beastmaster", 
		[6]="npc_dota_hero_bloodseeker" ,
		[7]="npc_dota_hero_chen" ,
		[8]="npc_dota_hero_crystal_maiden",
		[9]="npc_dota_hero_dark_seer" ,
		[10]="npc_dota_hero_dazzle"  ,
		[11]="npc_dota_hero_dragon_knight" ,
		[12]="npc_dota_hero_doom_bringer"  
	}
	local LastIndex=1
	return function ( )
		local a=HeroList_Custom[LastIndex]
		if LastIndex >= 12 then 
			LastIndex=1 
		else 
			LastIndex=LastIndex+1
		end 
		return a
	end
end
function MultiHeroControlSystem:test01(keys)
  	--print("############################### test001")
  	local player=PlayerResource:GetPlayer(keys.playerid)
  	local hero = player:GetAssignedHero()
	 
	local cor  = CreateUnitByName("npc_dota_courier", hero:GetAbsOrigin(), true, nil, nil, hero:GetTeam())
	cor:SetOwner(hero)
	cor:SetControllableByPlayer(hero:GetPlayerID(),true)
end
--临时测试函数
function MultiHeroControlSystem:test02(keys)
	local info=function(playerid)
		print("------------------------keys:----------------------------")
		PrintTable(keys)
		print("----------------------------------------------------")
	  	local _player=PlayerResource:GetPlayer(playerid)
	  	dprint("hero_num",_G.player[_player].hero_num)
	    dprint("hero:",_G.player[_player].hero[(_G.player[_player].hero_num)])
	    print("----------------------_G.player:------------------------")
	    PrintTable(_G.player[_player])
	    print("----------------------------------------------------")
	end
    for _,v in pairs(keys) do
		info(v)
	end
end
--========================================通用方法=======================================================
--[[选中单位，触发PUI选中单位事件]]
function MultiHeroControlSystem:SelectUnit(player,unit)      
	local cc=CreateCommunicate(player)
	cc.Send({listener="OnSelectUnit",entindex=unit:GetEntityIndex()})
end
--[[设置相机移动到英雄处]]
function MultiHeroControlSystem:CameraMoveToHeroByName(keys,callback)
	local heroname = keys.heroname
	local heroes = Entities:FindAllByName(heroname)
	local playerid = keys.playerid	
	for k,foundhero in pairs(heroes) do
		if foundhero:GetPlayerOwnerID() == playerid and not foundhero:IsIllusion() then				
			PlayerResource:SetCameraTarget(playerid, foundhero)
			Timers:CreateTimer(0.05,function()
				PlayerResource:SetCameraTarget(playerid, nil)
			end)
			return
		end	
	end
end
function MultiHeroControlSystem:_CameraMoveToUnit(keys)
	local unit=EntIndexToHScript(keys.hero_entindex) 
	MultiHeroControlSystem:CameraMoveToUnit(keys.playerid,unit)
end
function MultiHeroControlSystem:CameraMoveToUnit(playerid,unit)
	-- 设置相机移动到英雄处		
	-- print(unit,unit:IsIllusion())
	Timers:CreateTimer(0.05,function()
		PlayerResource:SetCameraTarget(playerid, unit)
	end)	
	Timers:CreateTimer(0.1,function()
		PlayerResource:SetCameraTarget(playerid, nil)
	end)	
end
--============================================玩家预初始化======================================================
function MultiHeroControlSystem:PrePlayerInit()	
	print("preplayerinit")
 	for playerID = 0, DOTA_MAX_TEAM_PLAYERS do
    	if  PlayerResource:IsValidPlayerID(playerID) and PlayerResource:IsValidPlayer(playerID) then
    		local player=PlayerResource:GetPlayer(playerID)
    		-- print("playerID",playerID)
    		if RandomHero then  
    			CreateRandomHero( playerID )
    		else
    			-- local wisp=CreateHeroForPlayer("npc_dota_hero_wisp",player)
				player:MakeRandomHeroSelection()
    		end
    		PlayerResource:SetGold(playerID,0,false)
        end
    end

end  
function MultiHeroControlSystem:PlayerInit()
	print("playerinit")
	--print("NOW STEP: MultiHeroControlSystem:PlayerInit()")
	--[[获取所有玩家ID   DOTA_MAX_TEAM_PLAYERS]]
    for playerID = 0, DOTA_MAX_TEAM_PLAYERS do
    	if  PlayerResource:IsValidPlayerID(playerID) and PlayerResource:IsValidPlayer(playerID) then
    		local player=PlayerResource:GetPlayer(playerID) 
    		local team = PlayerResource:GetTeam(playerID)
    		local position = nil 
            if team == DOTA_TEAM_GOODGUYS then 
            	local spawner = Entities:FindByName(nil, self.BrightElfSpawner)           	
            	position=spawner:GetAbsOrigin()
            elseif team == DOTA_TEAM_BADGUYS then 
            	local spawner =Entities:FindByName(nil, self.DarkElfSpawner)
            	position=spawner:GetAbsOrigin()
            else
            	print("Error: MultiHeroControlSystem:PlayerInit() not the right team!")
            	return nil
            end
            local hero = player:GetAssignedHero()
            if (not hero) or hero:GetUnitName() ~= "npc_dota_hero_wisp" then  
            	-- print("MultiHeroControlSystem:PlayerInit()  Not have valid hero!")  
            	return nil 
			end
			local wisp_ability = hero:FindAbilityByName("dummy_elf")
    		wisp_ability:SetLevel(1)
    		hero:SetAbilityPoints(0)
			MultiHeroControlSystem:SelectUnit(player,hero)           
			-- print("FindClearSpaceForUnit", hero, position)
            FindClearSpaceForUnit(hero, position, false)                             
            Timers:CreateTimer(function()
            	hero:AddNewModifier(hero,nil,"modifier_commandrestricted",{duration=COUNT_DOWN})
            	Timers:CreateTimer(COUNT_DOWN,function()
            		hero:RemoveModifierByName("modifier_commandrestricted")
            	end)
            	self:CameraMoveToUnit(playerID, hero) 	
                --CustomGameEventManager:Send_ServerToPlayer(player,"create_elf",{heroname=hero:GetUnitName(),entindex=hero:GetEntityIndex()})
            	local cc=CreateCommunicate(player)
				cc.Send({listener="OnCreateElf",heroname=hero:GetUnitName(),entindex=hero:GetEntityIndex()})
            end)
        end
    end
end
--==================[[玩家控制初始化]]
function MultiHeroControlSystem:PlayerControlInit()
	--print("NOW STEP: MultiHeroControlSystem:PlayerControlInit(player)")
	GameRules:GetGameModeEntity().HeroNum={}
	GameRules:GetGameModeEntity().Heros={}
	GameRules:GetGameModeEntity().HeroNum[DOTA_TEAM_GOODGUYS]=0
	GameRules:GetGameModeEntity().HeroNum[DOTA_TEAM_BADGUYS]=0
	GameRules:GetGameModeEntity().Heros[DOTA_TEAM_GOODGUYS]={}
	GameRules:GetGameModeEntity().Heros[DOTA_TEAM_BADGUYS]={}
	for playerID = 0, DOTA_MAX_TEAM_PLAYERS do
    	if  PlayerResource:IsValidPlayerID(playerID) and PlayerResource:IsValidPlayer(playerID) then
    		local player=PlayerResource:GetPlayer(playerID)
		    local steamid=PlayerResource:GetSteamAccountID(playerID)
		    GameRules:GetGameModeEntity().player[steamid]={}
		    if steamid then 
		    	GameRules:GetGameModeEntity().player[steamid].hero={}
		    	GameRules:GetGameModeEntity().player[steamid].hero_entityindex={}
		    	GameRules:GetGameModeEntity().player[steamid].hero_num=0
		    end 
    	end
    end   
end
--========================================英雄相关=======================================================
--==================[[添加英雄]]
function MultiHeroControlSystem:CreateHero(hero_index,playerID)
	--print("NOW STEP: MultiHeroControlSystem:CreateHero(hero_index,playerID)")
	local RemoveELF=true
	local hero
	local player=PlayerResource:GetPlayer(playerID)
	local gold =PlayerResource:GetGold(playerID)
	--[[如果为第一个英雄，则替换英雄，如果不为第一个英雄，则创建新的英雄]]
	-- print(HeroSelect.IsFirstHero[playerID])
	if HeroSelect.IsFirstHero[playerID]==nil then
		hero = PlayerResource:ReplaceHeroWith(playerID,HeroList_Custom[hero_index],0,0)	   
	   	HeroSelect.IsFirstHero[playerID]=false
	   	RemoveELF=false
	else		
		-- print("CreateHeroForPlayer", player)
		hero = CreateHeroForPlayer(HeroList_Custom[hero_index],player)		 
	end
	PlayerResource:SetGold(playerID,gold,false)
	hero:SetOwner(player)
	hero:SetControllableByPlayer(playerID,true)
	self:AddHero(playerID,HeroList_Custom[hero_index],hero:GetEntityIndex())	
	--[[移动英雄到对应阵营的出生点]]
	if PlayerResource:GetTeam(playerID) == DOTA_TEAM_GOODGUYS then 
		FindClearSpaceForUnit(hero,Entities:FindByName(nil, self.BrightHeroSpawner):GetAbsOrigin(),false)
	else
		FindClearSpaceForUnit(hero, Entities:FindByName(nil, self.DarkHeroSpawner):GetAbsOrigin(),false)
	end
	--[[选择该英雄]] 	
	self:SelectUnit(player,hero)
	--[[移动镜头到该英雄处]]
	self:CameraMoveToUnit(playerID,hero)
	--[[通知PUI创建对应界面元素,掉线重连失效点]]
	Timers:CreateTimer(function ()	
	    --CustomGameEventManager:Send_ServerToPlayer(player,"create_hero",{heroname=HeroList_Custom[hero_index],entindex=hero:GetEntityIndex(),player=player})
	    local cc=CreateCommunicate(player)
		cc.Send({listener="OnCreateHero",heroname=HeroList_Custom[hero_index],entindex=hero:GetEntityIndex(),player=player})
	end)
	--[[返回是否需要删除小精灵]]
	print("RemoveELF",RemoveELF)
	return RemoveELF
end
--==================[[多英雄信息记录]]
function MultiHeroControlSystem:AddHero(playerID,heroname,hero_entindex)
	print("NOW STEP: MultiHeroControlSystem:AddHero(playerID,heroname)")
	local _player=PlayerResource:GetPlayer(playerID)
	local Team=PlayerResource:GetTeam(playerID)
	if not _player then 
		dprint("MultiHeroControlSystem:AddHero(playerID,heroname)","Error")
	end 
	--[[steam账号]]
	local steamid=PlayerResource:GetSteamAccountID(playerID)
	local steamid64 = PlayerResource:GetSteamID(playerID)
	-- print("steamid",playerID,steamid,steamid64)
	--[[该玩家英雄数量]]
	GameRules:GetGameModeEntity().DB.player[steamid].hero_num=GameRules:GetGameModeEntity().DB.player[steamid].hero_num+1
	--[[该玩家英雄名字]]
	GameRules:GetGameModeEntity().DB.player[steamid].hero[(GameRules:GetGameModeEntity().DB.player[steamid].hero_num)]=heroname
	--[[该玩家英雄的实体]]
	GameRules:GetGameModeEntity().DB.player[steamid].hero_entityindex[heroname]=hero_entindex
	--[[队伍]]
	GameRules:GetGameModeEntity().DB.HeroNum[Team]=GameRules:GetGameModeEntity().DB.HeroNum[Team]+1
	--[[队伍英雄]]
	table.insert(GameRules:GetGameModeEntity().DB.Heros[Team],hero_entindex)
	local teamText = ""
	if Team == DOTA_TEAM_GOODGUYS then
		teamText = "#DOTA_GoodGuys"
	elseif Team == DOTA_TEAM_BADGUYS then
		teamText = "#DOTA_BadGuys"
	end

	local player_hero_num = GameRules:GetGameModeEntity().DB.player[steamid].hero_num
	if player_hero_num > 1 then
		steamid = tostring(steamid)
		local time = GameRules:GetDOTATime(false,false)
		local minute = math.floor(time / 60)
		local second = math.floor(time - minute * 60)
		Notifications:TopToAll({text=tostring(minute), duration=8})
		Notifications:TopToAll({text="#UI_minutes", continue=true})
		Notifications:TopToAll({text=tostring(second), continue=true})
		Notifications:TopToAll({text="#UI_seconds", continue=true})
		Notifications:TopToAll({player=steamid, continue=true,style={width="40px",height="40px"}})
		Notifications:TopToAll({text="#Player_hero_num", continue=true})
		Notifications:TopToAll({text=player_hero_num, continue=true})
		Notifications:TopToAll({text="#player_hero"..player_hero_num, continue=true})
	end
	--队伍经验比例信息
	local team_hero_num = GameRules:GetGameModeEntity().DB.HeroNum[Team]
	Notifications:TopToAll({text=teamText,  duration=5})
    Notifications:TopToAll({text="#Team_hero_num", continue=true})
    Notifications:TopToAll({text=team_hero_num, continue=true})
    Notifications:TopToAll({text="#Experience_ratio", continue=true})
    if team_hero_num > 10 then
    	team_hero_num = 10
    end
    Notifications:TopToAll({text=ExperienceSystem.Ratio[team_hero_num], continue=true})
    EmitGlobalSound("HeroPicker.Selected")
end
--==================[[英雄复活]
function MultiHeroControlSystem:Revive_Hero(keys)
	-- 复活英雄

end
--========================================小精灵相关=======================================================
function MultiHeroControlSystem:Create_elf(keys)
	--para:keys.playerID 玩家ID 
	-- 产生小精灵
	-- print("Create_elf")
	local playerID=keys.playerID
	if RandomHero then
    	CreateRandomHero( playerID )
    	return
    end 
	local player = PlayerResource:GetPlayer(playerID)
	local position
	local team = PlayerResource:GetTeam(playerID)
	if keys.type~=nil then 
		if keys.type==1 then 
			team=DOTA_TEAM_GOODGUYS
		elseif keys.type==2 then 
			team=DOTA_TEAM_BADGUYS
		end
	end 
    if team == DOTA_TEAM_GOODGUYS then 
    	local spawner = Entities:FindByName(nil, "eif_spawn_1")           	
    	position=spawner:GetAbsOrigin()
    elseif team == DOTA_TEAM_BADGUYS then 
    	local spawner =Entities:FindByName(nil, "eif_spawn_2")
    	position=spawner:GetAbsOrigin()
    else
    	print("Error: MultiHeroControlSystem:PlayerInit() not the right team!")
    	return nil
    end
    local gold = PlayerResource:GetGold(playerID)
	local wisp = CreateHeroForPlayer("npc_dota_hero_wisp",player)
	PlayerResource:SetGold(playerID,gold,false)
	wisp:SetControllableByPlayer(playerID,true)
	--CustomGameEventManager:Send_ServerToPlayer(player,"create_elf",{heroname=wisp:GetUnitName(),entindex=wisp:GetEntityIndex()})
	local cc=CreateCommunicate(player)
	cc.Send({listener="OnCreateElf",heroname=wisp:GetUnitName(),entindex=wisp:GetEntityIndex()})
	--[[马甲技能，无血条、无敌]]
	local wisp_ability=wisp:FindAbilityByName("dummy_elf")
    wisp_ability:SetLevel(1)
    wisp:SetAbilityPoints(0)
	FindClearSpaceForUnit(wisp,position,false)
	Timers:CreateTimer(0.1, function()
		self:CameraMoveToUnit(playerID,wisp)
		end)
    self:SelectUnit(player,wisp)
end
function MultiHeroControlSystem:Create_Elf(keys)
	-----------------事件响应函数：添加小精灵
	local playerID=keys.playerid
	local player=PlayerResource:GetPlayer(playerID)
	local steamid=PlayerResource:GetSteamAccountID(playerID)
	if GameRules:GetGameModeEntity().DB.player[steamid].hero_num >= MultiHeroControlSystem.MaxPlayerNum then return end  --限制最大英雄数	
    MultiHeroControlSystem:Create_elf({playerID=playerID})
end


--===========================================能量圈触发响应=======================================================
function Trigger_hero_select(keys)
	-- PrintTable(keys)
	local activator=keys.activator  
	local caller=keys.caller
	local str=caller:GetName()
	local lenth=string.len(caller:GetName())
	local str_sub=string.sub(caller:GetName(), lenth-1, lenth)
    local hero_index=tonumber(str_sub) --选择的英雄的序号
    -- print("Trigger_hero_select",str,lenth,str_sub,hero_index)
	--[[替换小精灵为所选英雄]]   
    if activator:GetUnitName() ~= "npc_dota_hero_wisp" or activator.selecting then 
    	return nil 
    end
    activator.selecting = true    
    local playerID=activator:GetPlayerID()
    local player=PlayerResource:GetPlayer(playerID)
    local _entindex=activator:GetEntityIndex()     
	
    PrecacheUnitByNameAsync( HeroList_Custom[hero_index] , function()

		if MultiHeroControlSystem:CreateHero(hero_index,playerID) then
    		activator:RemoveSelf()
    	end	
	end, playerID)
	
    --[[告知PUI删除该小精灵面板]]
    --CustomGameEventManager:Send_ServerToPlayer(player,"remove_elf",{entindex = _entindex})
    local cc=CreateCommunicate(player)
	cc.Send({listener="OnRemoveElf",entindex = _entindex})
	--[[删除模型，或者单位，地形完成后修改成正式版]]
    local Dstr1="test"..str_sub
    local Dstr2="testl"..str_sub
    local Dstr3=str
	Entities:FindByName(nil,Dstr1):Destroy()
	-- Entities:FindByName(nil,Dstr2):Destroy()
	Entities:FindByName(nil,Dstr3):Destroy()
end

--玩家掉线重连
function MultiHeroControlSystem:On_player_reconnected (keys)
  	--print("[CCC] player_reconnected")
  	--PrintTable(data)
  	--print("PrintTable over")
  
  	local playerID = keys.PlayerID
  	local player=PlayerResource:GetPlayer(playerID)
	local steamid=PlayerResource:GetSteamAccountID(playerID)
	local hero_num = GameRules:GetGameModeEntity().player[steamid].hero_num
	--当玩家重连后跟新其ID
	GameRules:GetGameModeEntity().DB.player[steamid].PlayerID=playerID

	-- for i=1,hero_num do
	-- 	local heroname = GameRules:GetGameModeEntity().player[steamid].hero[i]
	-- 	local hero_entindex = GameRules:GetGameModeEntity().player[steamid].hero_entityindex[heroname]
	-- 	local cc=CreateCommunicate(player)
	-- 	cc.Send({listener="OnCreateHero",heroname=heroname,entindex=hero_entindex,player=player})
	-- end
  	
end

--点击头像施法
function MultiHeroControlSystem:CastOnHeroimage( keys )
	-- print("CastOnHeroimage")
	local caster = EntIndexToHScript(keys.caster)
	local target = EntIndexToHScript(keys.target)
	local ability = EntIndexToHScript(keys.ability)
	local playerID = keys.PlayerID
	local behaviors = ability:GetBehavior()
	local targetTeam = ability:GetAbilityTargetTeam()
	-- print(targetTeam,targetTeam ~= DOTA_UNIT_TARGET_TEAM_BOTH,targetTeam ~= DOTA_UNIT_TARGET_TEAM_FRIENDLY,targetTeam ~= DOTA_UNIT_TARGET_TEAM_NONE)
	if targetTeam ~= DOTA_UNIT_TARGET_TEAM_BOTH and targetTeam ~= DOTA_UNIT_TARGET_TEAM_FRIENDLY and targetTeam ~= DOTA_UNIT_TARGET_TEAM_NONE then
		SendErrorMessage(playerID, "#dota_hud_error_cant_cast_on_ally")
		return 
	elseif not ability:IsCooldownReady() then
		SendErrorMessage(playerID, "#dota_hud_error_ability_in_cooldown")
		return
	elseif not ability:IsOwnersManaEnough() then
		SendErrorMessage(playerID, "#dota_hud_error_not_enough_mana")
		return
	elseif caster:IsSilenced() then
		SendErrorMessage(playerID, "#dota_hud_error_unit_silenced")
		return	
	elseif not ability:IsFullyCastable() then
		SendErrorMessage(playerID, "#dota_hud_error_cant_cast_on_other")
		return	
	end

	if CheckBitBand( behaviors,DOTA_ABILITY_BEHAVIOR_UNIT_TARGET ) then
		caster:CastAbilityOnTarget(target, ability, playerID)
	elseif CheckBitBand( behaviors,DOTA_ABILITY_BEHAVIOR_POINT ) then
		caster:CastAbilityOnPosition(target:GetAbsOrigin(), ability, playerID)
	end	
end