if DropItem == nil then DropItem=class({}) end 

function DropItem:Init()
	--print("DropItem:Init")
	ListenToGameEvent('entity_killed', Dynamic_Wrap(DropItem, 'OnEntityKilled'), self)
end
function DropItem:OnEntityKilled( keys)
	local killed = EntIndexToHScript(keys.entindex_killed)
	local position = killed:GetAbsOrigin()
	if killed:HasModifier("modifier_siwangqishi_caozongsishi") or killed:HasModifier("modifier_item_sishijuanzhou") then
		return
	end
	if killed:GetUnitName() == "npc_dota_creep_nitankuilei" then
		local items = {"item_dayao","item_dayao","item_dayao","item_dayao","item_dayao","item_dayao"}
		DropItems( items, position, 6 )
		-- DropItemInChance( "item_gloves",position, 100 )
	elseif killed:GetUnitName() == "npc_dota_creep_heilong" then
		local items = {"item_dayao","item_yiliaoshi"}
		DropItems( items, position, 2 )
	elseif killed:GetUnitName() == "npc_dota_creep_heifuyou" then
		local items = {"item_mofashi","item_yiliaoshi"}
		DropItems( items, position, 2 )
	elseif killed:GetUnitName() == "npc_dota_creep_kongbushuangdongzhilang" then
		local item = "item_lingxiuzhishu"
		DropItemInChance( item, position, 20 )
		DropItemInChance( "item_momianbaoshi", position, 1 )
		
	elseif killed:GetUnitName() == "npc_dota_creep_duyezhizhu" then
		DropItemInChance( "item_nailao", position, 100 )
		DropItemInChance( "item_nailao", position, 100 )
		DropItemInChance( "item_nailao", position, 100 )
		DropItemInChance( "item_nailao", position, 100 )
		DropItemInChance( "item_nailao", position, 100 )
		DropItemInChance( "item_nailao", position, 100 )
		DropItemInChance( "item_shanbi", position, 50 )
	elseif killed:GetUnitName() == "npc_dota_creep_shirenguishouling" then
		local items = {"item_mofashi","item_yiliaoshi","item_chongsheng"}
		DropItems( items, position, 1 )
	elseif killed:GetUnitName() == "npc_dota_creep_kuangbaoyuansu" then
		local items = {"item_zhiliaoshouwei","item_mofashouwei"}
		DropItems( items, position, 1 )
	elseif killed:GetUnitName() == "npc_dota_creep_huimieshouwei" then
		if DropItem.huimieshouwei_death then
			local items = {"item_emodiaoxiang"}
			DropItems( items, position, 1 )
		else
			local items = {"item_emodiaoxiang","item_xieehudun","item_dayao"}
			DropItems( items, position, 3 )
			DropItem.huimieshouwei_death = true
		end
	elseif killed:GetUnitName() == "npc_dota_warlock_golem_1" then
		local item = "item_emoyanshi"
		DropItemInChance( item, position, 100 )
	elseif killed:GetUnitName() == "npc_dota_creep_gongchengkuilei" then
		local item = "item_suduzhichui"
		if DropItem.gongchengkuilei_death then
			item = "item_liliangzhichui"
		end
		DropItemInChance( item, position, 100 )
		DropItem.gongchengkuilei_death = true
	elseif killed:GetUnitName() == "npc_dota_creep_chailangshouwangzhe" then
		DropItemInChance( "item_siwangzhishu", position, 100 )
		DropItemInChance( "item_siwangzhishu", position, 100 )
		DropItemInChance( "item_yiliaoshi",position, 50 )
		DropItemInChance( "item_gloves",position, 50 )	
	elseif killed:GetUnitName() == "npc_boss_long" then
		DropItemInChance( "item_shenshengxunzhang", position, 100 )
		local killer=EntIndexToHScript(keys.entindex_attacker)
		local killer_team = killer:GetTeam()
		local teamText = ""
		if killer_team == DOTA_TEAM_GOODGUYS then
			teamText = "#DOTA_GoodGuys"
		elseif Team == DOTA_TEAM_BADGUYS then
			killer_team = "#DOTA_BadGuys"
		end
		Notifications:TopToAll({text=teamText,  duration=5})
		Notifications:TopToAll({text="#ShengLiFangjian", continue=true})
		EmitGlobalSound("Loot_Drop_Stinger_Arcana")
	elseif killed:GetUnitName() == "npc_dota_creep_shirenguimofashi" then
		DropItemInChance( "item_sishijuanzhou",position, 50 )	
	end
end	

function DropItems( items, position, itemNum )
	if itemNum >= #items then
		for _,itemName in pairs(items) do
			local item = CreateItem(itemName, nil, nil)
			CreateItemOnPositionSync(position + RandomVector(50), item)
		end
	elseif itemNum < #items then
		randomitems = RandomIndex(#items,itemNum)
		for _,itemIndex in pairs(randomitems) do
			local itemName = items[itemIndex]
			local item = CreateItem(itemName, nil, nil)
			CreateItemOnPositionSync(position + RandomVector(50), item)
		end
	end
end

--从1到tabNum中随机选取indexNum个不重复的数并返回一个表
function RandomIndex(tabNum,indexNum)
	indexNum = indexNum or tabNum
	local t = {}
	local rt = {}
	for i = 1,indexNum do
		local ri = RandomInt(1,tabNum + 1 - i)
		local v = ri
		for j = 1,tabNum do
			if not t[j] then
				ri = ri - 1
				if ri == 0 then
					table.insert(rt,j)
					t[j] = true
				end
			end
		end
	end
	return rt
end

function DropItemInChance( itemName, position, chance )
	if RandomInt(1,100) <= chance then
		local item = CreateItem(itemName, nil, nil)
		CreateItemOnPositionSync(position + RandomVector(50), item)
	end
end