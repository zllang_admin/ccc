require("libraries/worldpanels")

if not WorldPanelExample then
  WorldPanelExample = class({})
end

function WorldPanelExample:OnNPCSpawned(keys)
  --Apply a worldpanel health bar to every spawned hero unit
  local npc = EntIndexToHScript(keys.entindex)

  if npc.IsRealHero and npc:IsRealHero() and not npc.worldPanel then

     npc.worldPanel = WorldPanels:CreateWorldPanelForAll(
      {layout = "file://{resources}/layout/custom_game/worldpanels/Designation.xml",
        entity = npc:GetEntityIndex(),
        entityHeight = 210,
      })

  end

end

if not WorldPanelExample.initialized then

  ListenToGameEvent('npc_spawned', Dynamic_Wrap(WorldPanelExample, 'OnNPCSpawned'), WorldPanelExample)
  WorldPanelExample.initialized = true
end