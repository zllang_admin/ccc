function TouchTeleport(keys)
    -- print("TouchTeleport")
	local activator=keys.activator  
	local caller=keys.caller
	local str=caller:GetName()
	local lenth=string.len(caller:GetName())
	local houzhui=string.sub(caller:GetName(), lenth-2, lenth)
    local qianzhui = string.sub(caller:GetName(), 1, lenth-3)
    --local hero_index=tonumber(str_sub) --选择的英雄的序号
    local targetEnt = nil
    local playerID = activator:GetPlayerID()
    local MingYun = false
    if not playerID or IsDummyUnit( activator ) or activator:IsIllusion() then
        return
    end
    if houzhui == "_in" then
        if qianzhui == "mingyun" then
            if not DeathMatch then
                SendErrorMessage(playerID, "#DeathMatchOnly")
                return
            elseif caller.IsClose then
                SendErrorMessage(playerID, "#FateRoomClose")
                return
            end
            local MingYunCost = 200
            local gold = PlayerResource:GetGold(playerID)
            if gold < MingYunCost then
                SendErrorMessage(playerID, "#DOTA_Good_Req_200")
            else
                PlayerResource:SpendGold(playerID,MingYunCost,4)
                Containers:EmitSoundOnClient(playerID,"General.Buy")
                targetEnt =  Entities:FindByName(nil, str .. "_ent")
                MingYun = true
                caller.IsClose = true
                -- print("caller",caller:GetName())
            end
        elseif  qianzhui == "shengli" then
            local RequireLevel = 60
            if not DeathMatch then
                SendErrorMessage(playerID, "#DeathMatchOnly")
                return
            elseif not (activator:IsRealHero() and activator:GetLevel()>=RequireLevel) then
                SendErrorMessage(playerID, "#DOTA_LevelUp_Req_"..RequireLevel)
                return
            end
            targetEnt =  Entities:FindByName(nil, str .. "_ent")
        else
            local RequireLevel = tonumber(qianzhui)
            local s1 = RequireLevel
            if not (activator:IsRealHero() and activator:GetLevel()>=RequireLevel) then
                --print(" SendErrorMessage","#DOTA_LevelUp_Req")
                SendErrorMessage(playerID, "#DOTA_LevelUp_Req_"..s1)           
            else
                targetEnt =  Entities:FindByName(nil, str .. "_ent")
            end
        end
    elseif houzhui == "out" then
        qianzhui = string.sub(caller:GetName(), 1, lenth-4)
        targetEnt =  Entities:FindByName(nil, str .. "_ent")
    end

    -- print("TouchTeleport",str,qianzhui,houzhui,targetEnt)
    if targetEnt then
        local ts = ParticleManager:CreateParticle( TeleportStartParticle[qianzhui], PATTACH_ABSORIGIN, activator )
        ParticleManager:SetParticleControl( ts, 0, caller:GetAbsOrigin() )

        -- FindClearSpaceForUnit(activator, targetEnt:GetAbsOrigin(), true)
        activator:Interrupt()

        local te = ParticleManager:CreateParticle( TeleportEndParticle[qianzhui], PATTACH_ABSORIGIN, activator )
        ParticleManager:SetParticleControl( te, 0, targetEnt:GetAbsOrigin() )

        Timers:CreateTimer(0.5,function (  )
            EmitSoundOn("Portal.Hero_Disappear", caller)
            EmitSoundOn("Portal.Hero_Appear", targetEnt)
            ProjectileManager:ProjectileDodge(activator)
            FindClearSpaceForUnit(activator, targetEnt:GetAbsOrigin(), true)
            MultiHeroControlSystem:CameraMoveToUnit(playerID,activator)
            activator:Interrupt()
            ParticleManager:DestroyParticle(ts, false)
            ParticleManager:DestroyParticle(te, false)
            if MingYun == true then
                MingYunFangJian( activator,playerID )
            end
        end)
        
    end
  
end

function MingYunFangJian( activator,playerID )
    -- print("MingYunFangJian")
    local MingYunTime = 8
    local MingYunCloseTime = 30
    activator:AddNewModifier(activator,nil,"modifier_commandrestricted",{duration=MingYunTime})
    activator:SetForwardVector(Vector(0,1,0))
    local targetEnt =  Entities:FindByName(nil, "minyun_out_ent")
    local caller =  Entities:FindByName(nil, "mingyun_in")
    local qianzhui = "mingyun"
    local ts
    local te
    local p1 = ParticleManager:CreateParticle( "particles/items_fx/aegis_lvl_1000_ambient.vpcf", PATTACH_ABSORIGIN, activator )
    local p2 = ParticleManager:CreateParticle( "particles/themed_fx/cny_firecrackers_radend.vpcf", PATTACH_ABSORIGIN, activator )
    local p3 = ParticleManager:CreateParticle( "particles/units/heroes/hero_oracle/oracle_false_promise.vpcf", PATTACH_ABSORIGIN, activator )
    local dummy = CreateUnitByName( "npc_dummy_rocket", caller:GetAbsOrigin(), false, nil, nil, DOTA_TEAM_NOTEAM ) 
    local p4 = ParticleManager:CreateParticle( "particles/units/heroes/hero_oracle/oracle_fatesedict.vpcf", PATTACH_POINT, dummy)
    ParticleManager:SetParticleControl( p4, 0, caller:GetAbsOrigin() )
    

    -- local playerName = PlayerResource:GetPlayerName(playerID)
    -- print("playerName",playerName)
    -- Notifications:TopToAll({text=playerName, duration=5})
    Notifications:TopToAll({hero=activator:GetClassname(), imagestyle="landscape", duration=5,style={width="100px",height="64px"}})
    Notifications:TopToAll({text="#GoIntoFateRoom", continue=true})
    EmitGlobalSound("compendium_points")

    Timers:CreateTimer(MingYunCloseTime,function (  )
        Notifications:TopToAll({text="#FateRoomOpen", continue=true})
        caller.IsClose = false
        dummy:RemoveSelf()
        EmitGlobalSound("compendium_points")
    end)
    Timers:CreateTimer(MingYunTime-0.5,function (  )
        ts = ParticleManager:CreateParticle( TeleportStartParticle[qianzhui], PATTACH_ABSORIGIN, activator )
        ParticleManager:SetParticleControl( ts, 0, caller:GetAbsOrigin() )

        activator:Interrupt()

        te = ParticleManager:CreateParticle( TeleportEndParticle[qianzhui], PATTACH_ABSORIGIN, activator )
        ParticleManager:SetParticleControl( te, 0, targetEnt:GetAbsOrigin() )
        
    end)
    Timers:CreateTimer(MingYunTime,function (  )
        EmitSoundOn("Portal.Hero_Disappear", caller)
        EmitSoundOn("Portal.Hero_Appear", targetEnt)
            
        

        local event = MingYunEvent[RandomInt(1,#MingYunEvent)]
        -- print(event)

        Notifications:TopToAll({text="#InFateRoom", duration=5})
        Notifications:TopToAll({hero=activator:GetClassname(), imagestyle="landscape", continue=true,style={width="100px",height="64px"}})
        EmitGlobalSound("compendium_points")

        if string.sub(event, 1, 4) == "item" then
            local item = CreateItem(event,activator,activator)
            Timers:CreateTimer(0.04,function (  )
                Containers:AddItemToUnit(activator, item)
            end)
            Notifications:TopToAll({text="#Get", continue=true})
            Notifications:TopToAll({item=event, continue=true})
        elseif string.sub(event, 1, 8) == "modifier" then
            activator:AddNewModifier(activator,nil,event,{duration=60})
            Notifications:TopToAll({text="#Get", continue=true})
            local image = string.sub(event, 10, string.len(event))
            Notifications:TopToAll({image="file://{images}/spellicons/"..image..".png", continue=true,style={width="64px",height="64px"}})
        elseif event == "death" then
            activator:Kill(nil,activator)
            Notifications:TopToAll({text="#dota_hud_error_unit_dead", continue=true})
        elseif event == "givemoney" then    
            for playerID = 0, DOTA_MAX_TEAM_PLAYERS do
                if  PlayerResource:IsValidPlayerID(playerID) and PlayerResource:IsValidPlayer(playerID) then
                    local player=PlayerResource:GetPlayer(playerID)
                    if player:GetTeam()~=activator:GetTeam() then
                        PlayerResource:ModifyGold(playerID, 500, false, 0)
                    end
                end
            end
            Notifications:TopToAll({text="#give_money_500", continue=true})
        elseif event == "nothing" then
            Notifications:TopToAll({text="#nothing_happen", continue=true})
        elseif string.sub(event, 1, 6) == "travel"  then
            local targetEntName = string.sub(event, 8, string.len(event))
            targetEnt = Entities:FindByName(nil, targetEntName)
            Notifications:TopToAll({text="#teleported_to", continue=true})
            Notifications:TopToAll({text="#"..targetEntName, continue=true})
        end

        FindClearSpaceForUnit(activator, targetEnt:GetAbsOrigin(), true)
        MultiHeroControlSystem:CameraMoveToUnit(playerID,activator)
        activator:Interrupt()
        ParticleManager:DestroyParticle(ts, false)
        ParticleManager:DestroyParticle(te, false)
        ParticleManager:DestroyParticle(p1, false)
        ParticleManager:DestroyParticle(p2, false)
        ParticleManager:DestroyParticle(p3, false)
    end)
end

TeleportStartParticle = {}
TeleportStartParticle["15"] = "particles/items2_fx/teleport_start.vpcf"
TeleportStartParticle["20"] = "particles/econ/events/ti4/teleport_start_ti4.vpcf"
TeleportStartParticle["30"] = "particles/econ/events/fall_major_2015/teleport_start_fallmjr_2015.vpcf"
TeleportStartParticle["40"] = "particles/econ/events/league_teleport_2014/teleport_start_league.vpcf"
TeleportStartParticle["mingyun"] = "particles/econ/events/ti5/teleport_start_lvl2_ti5.vpcf"
TeleportStartParticle["shengli"] = "particles/econ/events/ti6/teleport_start_ti6_lvl3.vpcf"

TeleportEndParticle = {}
TeleportEndParticle["15"] = "particles/items2_fx/teleport_end.vpcf"
TeleportEndParticle["20"] = "particles/econ/events/ti4/teleport_end_ti4.vpcf"
TeleportEndParticle["30"] = "particles/econ/events/fall_major_2015/teleport_end_fallmjr_2015.vpcf"
TeleportEndParticle["40"] = "particles/econ/events/league_teleport_2014/teleport_end_league.vpcf"
TeleportEndParticle["mingyun"] = "particles/econ/events/ti5/teleport_end_lvl2_ti5.vpcf"
TeleportEndParticle["shengli"] = "particles/econ/events/ti6/teleport_end_ti6_lvl3.vpcf"

MingYunEvent = {
    "item_chuansong",
    "item_chongsheng",
    "item_feng",
    "item_jinghua",
    "item_zhiliaoshouwei",
    "item_mofashouwei",
    "item_ward_sentry",
    "item_chenmo",
    "item_tiaodao",
    "item_yaodai",
    "item_xixue",
    "item_huoyi",
    "item_huodao",
    "item_xie",
    "item_shanbi",
    "item_dianqiu",
    "item_bingqiu",
    "item_huoqiu",
    "item_mofashi",
    "item_jingyanzhishu",
    "item_liliangzhishu",
    "item_minjiezhishu",
    "item_zhilizhishu",
    "item_lingxiuzhishu",
    "item_shengmingzhishu",
    "item_jiqiangshouwei",
    "item_huoyanshouwei",
    "item_momian",
    "modifier_rune_doubledamage",
    "modifier_rune_regen",
    "modifier_rune_haste",
    "modifier_rune_invis",
    "modifier_rune_arcane",
    "death",
    "death",
    "death",
    "death",
    "givemoney",
    "givemoney",
    "nothing",
    "travel_dota_badguys_fort",
    "travel_dota_goodguys_fort",
    "travel_15_in_ent",
    "travel_20_in_ent",
    "travel_30_in_ent",
    "travel_40_in_ent",
}