-- Copyright (c) 2015-2016 野猪大大(zllang)
--=============================================常用工具函数========================================================
function Split(szFullString, szSeparator)  
		local nFindStartIndex = 1  
		local nSplitIndex = 1  
		local nSplitArray = {}  
		while true do  
		    local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)  
		    if not nFindLastIndex then  
		    	nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))  
		    break  
		    end  
		   nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)  
		   nFindStartIndex = nFindLastIndex + string.len(szSeparator)  
		   nSplitIndex = nSplitIndex + 1  
		end  
		return nSplitArray  
end
function  Date2UnixTime(year , mon , day , hour , min , sec )
    local  iy , im
    local INT=function( num )
  		local _a,_b=math.modf(num)
  		return _a
    end
    if tonumber(mon) > 2  then
        iy = tonumber(year)
    	im = tonumber(mon)
    else
    	iy = tonumber(year) - 1
    	im = tonumber(mon) + 12
    end
    local a = INT(iy / 100)
    a = 2 - a + INT(a/4)
    local JD = INT(365.25*(iy + 4716)) + INT(30.60001*(im + 1)) + tonumber(day) + a -1524.5
    local UnixTime = INT( (JD-2440587.5)*86400) + tonumber(hour)*3600 + tonumber(min)*60 + tonumber(sec)
    return UnixTime
end 

function GetUnixTime()
	local data=Split(GetSystemDate(),'/')
	local time=Split(GetSystemTime(),':')
	local unixtime=Date2UnixTime('20'..data[3],data[1],data[2],time[1],time[2],time[3])
	return unixtime
end

--=============================================================================================================
--========================================lua to js通讯封装====================================================
--[[使用方法：
	Lua to js：
	local cc=CreateCommunicate(Player)
	cc.Send({数据},function(data){
		回调执行内容
	})
	监听js：
	AddCommunicateListener("监听器名字",function(data){
		监听处理内容
	})
]]
--[[
	CommunicateInit():                        初始化通讯层。
	cc:CommunicateSwitchboard(Data)：         通讯分发。                  
	AddCommunicateListener(name,func):        添加交互监听。   
	Feedback(data):                           交互反馈。
	cc=CreateCommunicate(Player)：            创建交互实例。
	TaskStack():                              建立任务堆。
]]
--=============================================================================================================
if cc == nil then cc=class({}) end

-- Communicate type:
GET=0        	--需要PUI返回数据
SEND=1	     	--仅发送数据
JSGET=2      	--js需要返回
JSSEND=3        --js仅发送

ccDeBug=false   --debug开关
--=============================================初始化==========================================================
function CommunicateInit()
	_G.TaskStack=TaskStack()
	_G.CMCStack=TaskStack()
	--[[js 和 lua 交互通道]]
	CustomGameEventManager:RegisterListener("SendToLua",Dynamic_Wrap(cc,"CommunicateSwitchboard"))
end
--=============================================过程函数========================================================

--[[交互中转器]]
function cc:CommunicateSwitchboard( Data )
	if ccDeBug then dprint("CommunicateSwitchboard",Data) end
	if Data == nil then PayError("lua get a nil data from PUI!") return end 
	if Data.head == GET and Data.back then 
		local Callback=TaskStack.PopByIndex(Data.back)
		Callback(Data)
	elseif Data.head ==SEND then 
		--[[PUI成功接受数据]]
		--[[无需任何处理]]
	elseif Data.head ==JSSEND then 
		--[[PUI TO LUA 仅发送]]
		for _,v in pairs(CMCStack.GetAllItem()) do
			if Data.body.listener== v.listener then 
				v.func(Data)
			end 
		end
	elseif Data.head ==JSGET then 
		--[[PUI TO LUA 需返回]]
		--Feedback( Data )
		for _,v in pairs(CMCStack.GetAllItem()) do
			if Data.body.listener== v.listener then 
				v.func(Data,Feedback)
			end 
		end
	end	
end
-- [[添加交互监听]]
function AddCommunicateListener( name,func )
	CMCStack.Push({listener=name,func=func})
end
-- [[交互反馈]]
local Feedback=function(data)
	if ccDeBug then dprint("Feedback",data) end	
	local Player=PlayerResource:GetPlayer(data.PlayerID)
	CustomGameEventManager:Send_ServerToPlayer(Player,"SendToJs",data)
end
--[[创建交互]]
function CreateCommunicate(Player)
	--[[不给参数时，默认发送至所有客户端]]
	local cc={}
	cc.Send=function ( Data,Callback)
		if type(Callback) ~= "function" and Callback ~= nil then PayError("Callback must be function type!") return end
		if Data==nil then PayError("No data to send!") return end 
		data={}
		--[[data.head:  0:交互型，必须受到返回值。]]
		--[[data.back:  回调]]
		--[[data.body:数据内容]]
		if Callback ~= nil then 
			data.head=GET
			data.back=TaskStack.Push(Callback)
		else
			data.back=nil 
			data.head=SEND
		end 
		data.body=Data
		if Player == nil then 
			CustomGameEventManager:Send_ServerToAllClients("SendToJs",data)
		else
			CustomGameEventManager:Send_ServerToPlayer(Player,"SendToJs",data)
		end
	end
	return cc
end
--[[交互任务堆]]
function TaskStack()
	local _Stack={}
	local top=1
	local cc=class({})
	cc.Push=function ( item )
		_Stack[top]=item
		local Index=top
		top=top+1
		return Index
	end
	cc.PopByIndex=function ( Index )
		local back=_Stack[Index]
		_Stack[Index]=nil
		return back
	end
	cc.GetAllItem=function (  )
		return _Stack
	end
	return cc
end
