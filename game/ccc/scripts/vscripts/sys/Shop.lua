--=======================================================================================================
--==========================================商店=========================================================
--=======================================================================================================
if Shop==nil then Shop=class({}) end
--==========================================商店列表=====================================================
function Shop:CreateHomeShopList( unit )
	local ItemList={
		  "item_chuansong",
      "item_chongsheng",
	  	"item_feng",
      "item_jinghua",
      "item_zhiliaoshouwei",
      "item_mofashouwei",
      "item_ward_sentry",
      "item_chenmo",
      "item_tiaodao",
      "item_yaodai",
	  	"item_xixue",
	  	-- "item_huoyi",
	  	"item_huodao",
	  	"item_xie",
	  	"item_shanbi",
	  	"item_dianqiu",
	  	"item_bingqiu",
	  	"item_huoqiu",
	  	"item_mofashi",
      "item_jingyanzhishu",
      "item_liliangzhishu",
      "item_minjiezhishu",
      "item_zhilizhishu",
      "item_lingxiuzhishu",
      "item_shengmingzhishu", 
	}
	--PrintTable(ItemList)
	local Items={}
	local prices={}
	local stocks={}
	local _item
  local StockTimes = {}
	for i,v in pairs(ItemList) do
			_item=CreateItem(v,unit,unit)
			--print('item:',_item:GetEntityIndex())
			Items[i]=_item
			prices[i]=_item:GetCost()

      if v == "item_chongsheng" then
        if  DeathMatch then
			    stocks[_item:GetEntityIndex()]= 0
          _item.MaxStock = 0
        else
          stocks[_item:GetEntityIndex()]= 3
          _item.MaxStock = 3
        end
        StockTimes[_item:GetEntityIndex()] = 60
      elseif v == "item_mofashouwei" then
        stocks[_item:GetEntityIndex()]=3
        _item.MaxStock = 3
        StockTimes[_item:GetEntityIndex()] = 40
      elseif v == "item_zhiliaoshouwei" then
        stocks[_item:GetEntityIndex()]=3
        _item.MaxStock = 3
        StockTimes[_item:GetEntityIndex()] = 20
      elseif v == "item_feng" then
        stocks[_item:GetEntityIndex()]=1
        _item.MaxStock = 1
        StockTimes[_item:GetEntityIndex()] = 120
      elseif v == "item_jinghua" then
        stocks[_item:GetEntityIndex()]=1
        _item.MaxStock = 1
        StockTimes[_item:GetEntityIndex()] = 60
      end
			--_item:RemoveSelf()
		
	end
	return Items,prices,stocks,StockTimes
end


function Shop:CreateSideShopList( unit )
  local ItemList={
      "item_feng",
      "item_chenmo",
      "item_yaodai",
      "item_huodao",
      "item_shanbi",
      "item_xixue",
      "item_dianqiu",
      "item_bingqiu",
      "item_huoqiu",
      "item_mofashi",
      "item_ward_sentry",
  }
  --PrintTable(ItemList)
  local Items={}
  local prices={}
  local stocks={}
  local _item
  local StockTimes = {}
  for i,v in pairs(ItemList) do
      _item=CreateItem(v,unit,unit)
      --print('item:',_item:GetEntityIndex())
      Items[i]=_item
      prices[i]=_item:GetCost()

      if v == "item_chongsheng" then
        if  DeathMatch then
          stocks[_item:GetEntityIndex()]= 0
          _item.MaxStock = 0
        else
          stocks[_item:GetEntityIndex()]= 3
          _item.MaxStock = 3
        end
        StockTimes[_item:GetEntityIndex()] = 60
      elseif v == "item_mofashouwei" then
        stocks[_item:GetEntityIndex()]=3
        _item.MaxStock = 3
        StockTimes[_item:GetEntityIndex()] = 40
      elseif v == "item_zhiliaoshouwei" then
        stocks[_item:GetEntityIndex()]=3
        _item.MaxStock = 3
        StockTimes[_item:GetEntityIndex()] = 20
      elseif v == "item_feng" then
        stocks[_item:GetEntityIndex()]=1
        _item.MaxStock = 1
        StockTimes[_item:GetEntityIndex()] = 120
      elseif v == "item_jinghua" then
        stocks[_item:GetEntityIndex()]=1
        _item.MaxStock = 1
        StockTimes[_item:GetEntityIndex()] = 60
      end
      --_item:RemoveSelf()
    
  end
  return Items,prices,stocks,StockTimes
end

function Shop:CreateSecretShopList( unit )
  local ItemList={
      "item_chuansong",
      "item_chongsheng",
      "item_jinghua",
      "item_zhiliaoshouwei",
      "item_mofashouwei",
      "item_tiaodao",
      "item_huoyi",
      "item_xie",
  }
  --PrintTable(ItemList)
  local Items={}
  local prices={}
  local stocks={}
  local _item
  local StockTimes = {}
  for i,v in pairs(ItemList) do
      _item=CreateItem(v,unit,unit)
      --print('item:',_item:GetEntityIndex())
      Items[i]=_item
      prices[i]=_item:GetCost()

      if v == "item_chongsheng" then
        if  DeathMatch then
          stocks[_item:GetEntityIndex()]= 0
          _item.MaxStock = 0
        else
          stocks[_item:GetEntityIndex()]= 3
          _item.MaxStock = 3
        end
        StockTimes[_item:GetEntityIndex()] = 60
      elseif v == "item_mofashouwei" then
        stocks[_item:GetEntityIndex()]=3
        _item.MaxStock = 3
        StockTimes[_item:GetEntityIndex()] = 40
      elseif v == "item_zhiliaoshouwei" then
        stocks[_item:GetEntityIndex()]=3
        _item.MaxStock = 3
        StockTimes[_item:GetEntityIndex()] = 20
      elseif v == "item_feng" then
        stocks[_item:GetEntityIndex()]=1
        _item.MaxStock = 1
        StockTimes[_item:GetEntityIndex()] = 120
      elseif v == "item_jinghua" then
        stocks[_item:GetEntityIndex()]=1
        _item.MaxStock = 1
        StockTimes[_item:GetEntityIndex()] = 60
      end
      --_item:RemoveSelf()
    
  end
  return Items,prices,stocks,StockTimes
end


--=======================================================================================================
--=============================================实体商店==================================================
--=======================================================================================================
Shop.CurrentHero={}

function RandomItem(owner)
  local id = RandomInt(1,29)
  local name = Containers.itemIDs[id]
  return CreateItem(name, owner, owner)
end

function CreateLootBox(loc)
  local phys = CreateItemOnPositionSync(loc:GetAbsOrigin(), nil)
  phys:SetForwardVector(Vector(0,-1,0))
  phys:SetModelScale(1.5)

  local items = {}
  local slots = {1,2,3,4}
  for i=1,RandomInt(1,3) do
    items[table.remove(slots, RandomInt(1,#slots))] = RandomItem()
  end

  local cont = Containers:CreateContainer({
    layout =      {2,2},
    --skins =       {"Hourglass"},
    headerText =  "Loot Box",
    buttons =     {"Take All"},
    position =    "entity", --"mouse",--"900px 200px 0px",
    OnClose = function(playerID, container)
     -- print("Closed")

      if next(container:GetAllOpen()) == nil and #container:GetAllItems() == 0 then
        container:GetEntity():RemoveSelf()
        container:Delete()
        loc.container = nil

        Timers:CreateTimer(7, function()
          CreateLootBox(loc)
        end)
      end
    end,
    OnOpen = function(playerID, container)
      --print("Loot box opened")
    end,
    closeOnOrder= true,
    items = items,
    entity = phys,
    range = 150,
    OnButtonPressed = function(playerID, container, unit, button, buttonName)
      if button == 1 then
        local items = container:GetAllItems()
        for _,item in ipairs(items) do
          container:RemoveItem(item)
          Containers:AddItemToUnit(unit,item)
        end

        container:Close(playerID)
      end
    end,
    OnEntityOrder = function(playerID, container, unit, target)
      --print("ORDER ACTION loot box: ", playerID)
      container:Open(playerID)
      unit:Stop()
    end
  })

  loc.container = cont
  loc.phys = phys
end

function CreateShop(ii)
  local sItems = {}
  local prices = {}
  local stocks = {}
  local StockTimes = {}
  for _,i in ipairs(ii) do
    item = CreateItem(i[1], unit, unit)
    local index = item:GetEntityIndex()
    sItems[#sItems+1] = item
    if i[2] ~= nil then prices[index] = i[2] end
    if i[3] ~= nil then stocks[index] = i[3] end
    if i[4] ~= nil then StockTimes[index] = i[3] end
  end

  return sItems, prices, stocks, StockTimes  
end

function Shop:OpenInventory(args)
  local pid = args.PlayerID
  local unitIndex=args.unitIndex
  pidInventory[unitIndex]:Open(unitIndex)
end

function Shop:DefaultInventory(args)
  local pid = args.PlayerID
  local hero = PlayerResource:GetSelectedHeroEntity(pid)

  local di = defaultInventory[pid]
  local msg = "Default Inventory Set To Container Inventory"
  if di then
    Containers:SetDefaultInventory(hero, nil)
    defaultInventory[pid] = false
    msg = "Default Inventory Set To DOTA Inventory"
  else
    Containers:SetDefaultInventory(hero, pidInventory[pid])
    defaultInventory[pid] = true
  end

  Notifications:Top(pid, {text=msg,duration=5})
end
--[[初始化]]
function Shop:Init()
	--[[添加监听]]
  local abilityEx  = nil
	AddCommunicateListener("ShopHandle",Shop.ShopHandler)
	-- register listeners
  CustomGameEventManager:RegisterListener("OpenInventory", Dynamic_Wrap(Shop, "OpenInventory"))
  CustomGameEventManager:RegisterListener("DefaultInventory", Dynamic_Wrap(Shop, "DefaultInventory"))

  Containers:SetDisableItemLimit(true)
  Containers:UsePanoramaInventory(true)
  -- create initial stuff
  lootSpawns = Entities:FindAllByName("loot_spawn")
  itemDrops = Entities:FindAllByName("item_drops")
  contShopRadEnt = Entities:FindByName(nil, "container_shop_radiant")
  contShopDireEnt = Entities:FindByName(nil, "container_shop_dire")
  SideShopEnt1 = Entities:FindByName(nil, "side_shop1")
  SideShopEnt2 = Entities:FindByName(nil, "side_shop2")
  SecretShopEnt1 = Entities:FindByName(nil, "secret_shop1")
  SecretShopEnt2 = Entities:FindByName(nil, "secret_shop2")
  JiqiangRadiant = Entities:FindByName(nil, "jiqiang_radiant")
  JiqiangDire = Entities:FindByName(nil, "jiqiang_dire")

  privateBankEnt = Entities:FindByName(nil, "private_bank")
  sharedBankEnt = Entities:FindByName(nil, "shared_bank")
  


--============================光明商店======================================================
  local sItems,prices,stocks,StockTimes = self:CreateHomeShopList( ) 


  contShopRadEnt = CreateUnitByName("npc_dummy_shop", contShopRadEnt:GetAbsOrigin(), false, nil, nil, DOTA_TEAM_GOODGUYS)
  contShopRadEnt:AddNewModifier(viper, nil, "modifier_shopkeeper", {})
  contShopRadEnt.IsUnitShopOpen = {}
  contShopRadEnt.headerText =  "#light_shop_barrack"
  contShopRadEnt:SetModel("models/heroes/shopkeeper/shopkeeper.vmdl")
  contShopRadEnt:SetOriginalModel("models/heroes/shopkeeper/shopkeeper.vmdl")
  Timers:CreateTimer(1,function()
    contShopRadEnt:SetModelScale(1.5)
    -- StartAnimation(contShopRadEnt, {duration=-1, activity=ACT_DOTA_IDLE, rate=1})
    contShopRadEnt:SetForwardVector(Vector(0,-1,0))
    end)

  contRadiantShop = Containers:CreateShop({
    layout =      {5,5,5,5,5},
    skins =       {},
    headerText =  "#light_shop",
    pids =        {},
    unitIndex=    contShopRadEnt:GetEntityIndex(),
    position =     "0px 45px 0px",--"entity",
    entity =      contShopRadEnt,
    items =       sItems,
    prices =      prices,
    stocks =      stocks,
    StockTimes =   StockTimes,
    closeOnOrder= true,
    range =       500,
    OnSelect  =   function(playerID, container, selected)
    	--print("OnSelect",selected:GetEntityIndex(),container:IsOpen(selected:GetEntityIndex()))
      if PlayerResource:GetTeam(playerID) == DOTA_TEAM_GOODGUYS then
        local keys = {PlayerID=playerID,ShopIndex=contShopRadEnt:GetEntityIndex()}
        if not container:IsOpen(playerID) and not ShopPanel:IsOpen(contShopRadEnt,playerID) then
          container:Open(selected:GetEntityIndex(),playerID)
          ShopPanel:CloseAllUnitShop(keys)
          ShopPanel:OpenUnitShop(keys)
          Containers:EmitSoundOnClient(playerID,"Shop.PanelUp")
        else
          container:Close(container:GetUnitIndex(),playerID)
          ShopPanel:CloseUnitShop(keys)
          Containers:EmitSoundOnClient(playerID,"Shop.PanelDown")
        end
      end 
    end,
    OnDeselect =  function(playerID, container,deselected)
    	--print("OnDeselect",container:GetUnitIndex())
      if PlayerResource:GetTeam(playerID) == DOTA_TEAM_GOODGUYS then
        --container:Close(container:GetUnitIndex(),playerID)
      end
    end,
    OnEntityOrder=function(playerID, container, unit, target)
    --[[
    print("OnEntityOrder")
      local unitIndex=unit:GetEntityIndex()
      if PlayerResource:GetTeam(playerID) == DOTA_TEAM_GOODGUYS then
        container:Open(unitIndex,playerID)
        unit:Stop()
      else
        Containers:DisplayError(playerID, "#dota_hud_error_unit_command_restricted")
      end 
    --]]
    end,
  })

  contShopRadEnt.container = contRadiantShop

--============================黑暗商店======================================================
  sItems,prices,stocks,StockTimes = self:CreateHomeShopList( ) 

  contShopDireEnt = CreateUnitByName("npc_dummy_shop", contShopDireEnt:GetAbsOrigin(), false, nil, nil, DOTA_TEAM_BADGUYS)
  contShopDireEnt:AddNewModifier(viper, nil, "modifier_shopkeeper", {})
  contShopDireEnt.IsUnitShopOpen = {}
  contShopDireEnt.headerText =  "#dark_shop_barrack"
  contShopDireEnt:SetModel("models/heroes/shopkeeper_dire/shopkeeper_dire.vmdl")
  contShopDireEnt:SetOriginalModel("models/heroes/shopkeeper_dire/shopkeeper_dire.vmdl")
  Timers:CreateTimer(1,function()
    contShopDireEnt:SetModelScale(1.5)
    -- StartAnimation(contShopDireEnt, {duration=-1, activity=ACT_DOTA_IDLE, rate=1})
    contShopDireEnt:SetForwardVector(Vector(0,-1,0))
    end)
  
  contShopDire = Containers:CreateShop({
    layout =      {5,5,5,5,5},
    skins =       {},
    headerText =  "#dark_shop",
    pids =        {},
    unitIndex=    contShopDireEnt:GetEntityIndex(),
    position =    "0px 45px 0px",--"entity",
    entity =      contShopDireEnt,
    items =       sItems,
    prices =      prices,
    stocks =      stocks,
    StockTimes =   StockTimes,
    closeOnOrder= true,
    range =       500,
    OnSelect  =   function(playerID, container, selected)
      --print("OnSelect",selected:GetEntityIndex(),container:IsOpen(selected:GetEntityIndex()))
      if PlayerResource:GetTeam(playerID) == DOTA_TEAM_BADGUYS then
        local keys = {PlayerID=playerID,ShopIndex=contShopDireEnt:GetEntityIndex()}
        if not container:IsOpen(playerID) and not ShopPanel:IsOpen(contShopDireEnt,playerID) then
          container:Open(selected:GetEntityIndex(),playerID)
          ShopPanel:CloseAllUnitShop(keys)
          ShopPanel:OpenUnitShop(keys)
          Containers:EmitSoundOnClient(playerID,"Shop.PanelUp")
        else
          container:Close(container:GetUnitIndex(),playerID)
          ShopPanel:CloseUnitShop(keys)
          Containers:EmitSoundOnClient(playerID,"Shop.PanelDown")
        end
      end
    end,
    OnDeselect =  function(playerID, container,deselected)
      --print("OnDeselect",container:GetUnitIndex())
      if PlayerResource:GetTeam(playerID) == DOTA_TEAM_BADGUYS then
        --container:Close(container:GetUnitIndex(),playerID)
      end
    end,
    OnEntityOrder=function(playerID, container, unit, target)
    --[[
      local unitIndex=unit:GetEntityIndex()
      if PlayerResource:GetTeam(playerID) == DOTA_TEAM_BADGUYS then
        container:Open(unitIndex,playerID)
        unit:Stop()
      else
        Containers:DisplayError(playerID, "#dota_hud_error_unit_command_restricted")
      end
    --]]
    end,
  })

  contShopDireEnt.container = contShopDire

--============================野外商店1======================================================
  sItems,prices,stocks,StockTimes = self:CreateSideShopList( ) 

  SideShopEnt1 = CreateUnitByName("npc_dummy_shop", SideShopEnt1:GetAbsOrigin(), false, nil, nil, DOTA_TEAM_NOTEAM)
  SideShopEnt1:AddNewModifier(viper, nil, "modifier_shopkeeper", {})
  SideShopEnt1.IsUnitShopOpen = {}
  SideShopEnt1.headerText =  "#side_shop_barrack"
  SideShopEnt1:SetModel("models/props_structures/secretshop_asian001.vmdl")
  SideShopEnt1:SetOriginalModel("models/props_structures/secretshop_asian001.vmdl")
  SideShopEnt1:SetModelScale(1)
  Timers:CreateTimer(1,function()
    -- StartAnimation(SideShopEnt1, {duration=-1, activity=ACT_DOTA_IDLE, rate=1})
    SideShopEnt1:SetForwardVector(Vector(0,-1,0))
  end)
  
  SideShop1 = Containers:CreateShop({
    layout =      {3,3,3,3},
    skins =       {},
    headerText =  "#side_shop",
    pids  =        {},
    unitIndex=    SideShopEnt1:GetEntityIndex(),
    position =    "0px 45px 0px",--"entity",
    entity =      SideShopEnt1,
    items =       sItems,
    prices =      prices,
    stocks =      stocks,
    StockTimes =   StockTimes,
    closeOnOrder= true,
    range =       500,
    OnSelect  =   function(playerID, container, selected)
        --print("OnSelect",selected:GetEntityIndex(),container:IsOpen(selected:GetEntityIndex()))
        local keys = {PlayerID=playerID,ShopIndex=SideShopEnt1:GetEntityIndex()}
        if not container:IsOpen(playerID) and not ShopPanel:IsOpen(SideShopEnt1,playerID) then
          container:Open(selected:GetEntityIndex(),playerID)
          ShopPanel:CloseAllUnitShop(keys)
          ShopPanel:OpenUnitShop(keys)
          Containers:EmitSoundOnClient(playerID,"Shop.PanelUp")
        else
          container:Close(container:GetUnitIndex(),playerID)
          ShopPanel:CloseUnitShop(keys)
          Containers:EmitSoundOnClient(playerID,"Shop.PanelDown")
        end
    end,
    OnDeselect =  function(playerID, container,deselected)
        --container:Close(container:GetUnitIndex(),playerID)     
    end,
    OnEntityOrder=function(playerID, container, unit, target)
    --[[
        local unitIndex=unit:GetEntityIndex()
      container:Open(unitIndex,playerID)
      unit:Stop()
    --]]
    end,
  })

  SideShopEnt1.container = SideShop1

--============================野外商店2======================================================
  SideShopEnt2 = CreateUnitByName("npc_dummy_shop", SideShopEnt2:GetAbsOrigin(), false, nil, nil, DOTA_TEAM_NOTEAM)
  SideShopEnt2:AddNewModifier(viper, nil, "modifier_shopkeeper", {})
  SideShopEnt2.IsUnitShopOpen = {}
  SideShopEnt2.headerText =  "#side_shop_barrack"
  SideShopEnt2:SetModel("models/props_structures/secretshop_radiant003.vmdl")
  SideShopEnt2:SetOriginalModel("models/props_structures/secretshop_radiant003.vmdl")
  SideShopEnt2:SetModelScale(1)
  StartAnimation(SideShopEnt2, {duration=-1, activity=ACT_DOTA_IDLE, rate=1})
  SideShopEnt2:SetForwardVector(Vector(0,-1,0))

  SideShop2 = Containers:CreateShop({
    layout =      {3,3,3,3},
    skins =       {},
    headerText =  "#side_shop",
    pids  =        {},
    unitIndex=    SideShopEnt2:GetEntityIndex(),
    position =    "0px 45px 0px",--"entity",
    entity =      SideShopEnt2,
    items =       sItems,
    prices =      prices,
    stocks =      stocks,
    StockTimes =   StockTimes,
    closeOnOrder= true,
    range =       500,
    OnSelect  =   function(playerID, container, selected)
        --print("OnSelect",selected:GetEntityIndex(),container:IsOpen(selected:GetEntityIndex()))
        local keys = {PlayerID=playerID,ShopIndex=SideShopEnt2:GetEntityIndex()}
        if not container:IsOpen(playerID) and not ShopPanel:IsOpen(SideShopEnt2,playerID) then
          container:Open(selected:GetEntityIndex(),playerID)
          ShopPanel:CloseAllUnitShop(keys)
          ShopPanel:OpenUnitShop(keys)
          Containers:EmitSoundOnClient(playerID,"Shop.PanelUp")
        else
          container:Close(container:GetUnitIndex(),playerID)
          ShopPanel:CloseUnitShop(keys)
          Containers:EmitSoundOnClient(playerID,"Shop.PanelDown")
        end
    end,
    OnDeselect =  function(playerID, container,deselected)
        --container:Close(container:GetUnitIndex(),playerID)     
    end,
    OnEntityOrder=function(playerID, container, unit, target)
    --[[
        local unitIndex=unit:GetEntityIndex()
      container:Open(unitIndex,playerID)
      unit:Stop()
    --]]
    end,
  })

  SideShopEnt2.container = SideShop2

--============================神秘商店1======================================================
  sItems,prices,stocks,StockTimes = self:CreateSecretShopList()

  SecretShopEnt1 = CreateUnitByName("npc_dummy_shop", SecretShopEnt1:GetAbsOrigin(), false, nil, nil, DOTA_TEAM_NOTEAM)
  SecretShopEnt1:AddNewModifier(viper, nil, "modifier_shopkeeper", {})

  SecretShopEnt1:SetModel("models/props_structures/secretshop_radiant001.vmdl")
  SecretShopEnt1:SetOriginalModel("models/props_structures/secretshop_radiant001.vmdl")
  SecretShopEnt1:SetModelScale(1)
  StartAnimation(SecretShopEnt1, {duration=-1, activity=ACT_DOTA_IDLE, rate=1})
  SecretShopEnt1:SetForwardVector(Vector(0,-1,0))

  SecretShop1 = Containers:CreateShop({
    layout =      {2,2,2,2},
    skins =       {},
    headerText =  "#secret_shop",
    pids  =        {},
    unitIndex=    SecretShopEnt1:GetEntityIndex(),
    position =    "0px 45px 0px",--"entity",
    entity =      SecretShopEnt1,
    items =       sItems,
    prices =      prices,
    stocks =      stocks,
    StockTimes =   StockTimes,
    closeOnOrder= true,
    range =       500,
    OnSelect  =   function(playerID, container, selected)
        --print("OnSelect",selected:GetEntityIndex(),container:IsOpen(selected:GetEntityIndex()))
        if not container:IsOpen(playerID) then
          container:Open(selected:GetEntityIndex(),playerID)
          Containers:EmitSoundOnClient(playerID,"Shop.Panelup")
        else
          container:Close(container:GetUnitIndex(),playerID)
          Containers:EmitSoundOnClient(playerID,"Shop.PanelDown")
        end
        local keys = {PlayerID=playerID}
        ShopPanel:CloseAllUnitShop(keys)
          
    end,
    OnDeselect =  function(playerID, container,deselected)
        --container:Close(container:GetUnitIndex(),playerID)     
    end,
    OnEntityOrder=function(playerID, container, unit, target)
    --[[
        local unitIndex=unit:GetEntityIndex()
      container:Open(unitIndex,playerID)
      unit:Stop()
    --]]
    end,
  })

  SecretShopEnt1.container = SecretShop1

--============================神秘商店2======================================================
  SecretShopEnt2 = CreateUnitByName("npc_dummy_shop", SecretShopEnt2:GetAbsOrigin(), false, nil, nil, DOTA_TEAM_NOTEAM)
  SecretShopEnt2:AddNewModifier(viper, nil, "modifier_shopkeeper", {})

  SecretShopEnt2:SetModel("models/props_structures/secretshop_radiant002.vmdl")
  SecretShopEnt2:SetOriginalModel("models/props_structures/secretshop_radiant002.vmdl")
  SecretShopEnt2:SetModelScale(1)
  StartAnimation(SecretShopEnt2, {duration=-1, activity=ACT_DOTA_IDLE, rate=1})
  SecretShopEnt2:SetForwardVector(Vector(0,-1,0))


  SecretShop2 = Containers:CreateShop({
    layout =      {2,2,2,2},
    skins =       {},
    headerText =  "#secret_shop",
    pids  =        {},
    unitIndex=    SecretShopEnt2:GetEntityIndex(),
    position =    "0px 45px 0px",--"entity",
    entity =      SecretShopEnt2,
    items =       sItems,
    prices =      prices,
    stocks =      stocks,
    StockTimes =   StockTimes,
    closeOnOrder= true,
    range =       500,
    OnSelect  =   function(playerID, container, selected)
        --print("OnSelect",selected:GetEntityIndex(),container:IsOpen(selected:GetEntityIndex()))
        if not container:IsOpen(playerID) then
          container:Open(selected:GetEntityIndex(),playerID)
          Containers:EmitSoundOnClient(playerID,"Shop.Panelup")
        else
          container:Close(container:GetUnitIndex(),playerID)
          Containers:EmitSoundOnClient(playerID,"Shop.PanelDown")
        end
        local keys = {PlayerID=playerID}
        ShopPanel:CloseAllUnitShop(keys)
    end,
    OnDeselect =  function(playerID, container,deselected)
        --container:Close(container:GetUnitIndex(),playerID)     
    end,
    OnEntityOrder=function(playerID, container, unit, target)
    --[[
        local unitIndex=unit:GetEntityIndex()
      container:Open(unitIndex,playerID)
      unit:Stop()
    --]]
    end,
  })

  SecretShopEnt2.container = SecretShop2

  ShopPanel:Init()
end  
function Shop:OnHeroInGame( hero )
	-- create inventory
  local pid = hero:GetPlayerID()
  local unitIndex=hero:GetEntityIndex()
 -- local c = Containers:CreateContainer({
 --    layout =      {3,4,4},
 --    skins =       {},
 --    headerText =  "我的背包",
 --    pids =        {pid},
 --    unitIndex=    unitIndex,
 --    entity =      hero,
 --    closeOnOrder = false,
 --    position =    "1200px 600px 0px",
 --    OnDragWorld = true,
 --  })

 --  pidInventory[unitIndex] = c

 --  local item = CreateItem("item_chuansong", hero, hero)

 --  c:AddItem(item, 4)

  -- privateBank[unitIndex] = Containers:CreateContainer({
  --   layout =      {4,4,4,4},
  --   headerText =  "私人仓库",
  --   pids =        {pid},
  --   position =    "entity", --"200px 200px 0px",
  --   entity =      privateBankEnt,
  --   closeOnOrder= true,
  --   forceOwner =  hero,
  --   forcePurchaser=hero,
  --   range =       250,
  --   OnEntityOrder =function(unitIndex, container, unit, target)
  --     print("ORDER ACTION private bank: ", unitIndex)
  --     if privateBank[unitIndex] then 
  --       privateBank[unitIndex]:Open(unitIndex)
  --     end
  --     unit:Stop()
  --   end,
  -- })

  -- defaultInventory[unitIndex] = false
  Containers:SetDefaultInventory(hero, nil)
end


if LOADED then
  return
end
LOADED = true

pidInventory = {}
lootSpawns = nil
itemDrops = nil
privateBankEnt = nil
sharedBankEnt = nil
contShopRadEnt = nil
contShopDireEnt = nil
SideShopEnt1 = nil
SideShopEnt2 = nil
SecretShopEnt1 = nil
SecretShopEnt2 = nil
JiqiangRadiant = nil
JiqiangDire = nil

contShopRad = nil
contShopDire = nil
SideShop1 = nil
SideShop2 = nil
SecretShop1 = nil
SecretShop2 = nil
sharedBank = nil
privateBank = {}

defaultInventory = {}

--=======================================================================================================
--=========================================雇佣兵商店（Panel）=================================================
--=======================================================================================================
if ShopPanel==nil then ShopPanel=class({}) end



function ShopPanel:Init()
  CustomGameEventManager:RegisterListener("OpenUnitShop",Dynamic_Wrap(ShopPanel,"OpenUnitShop"))
  CustomGameEventManager:RegisterListener("SetAllIsOpenFalse",Dynamic_Wrap(ShopPanel,"SetAllIsOpenFalse"))
  CustomGameEventManager:RegisterListener("SetupShops",Dynamic_Wrap(ShopPanel,"SetupShops"))
  -- CustomGameEventManager:RegisterListener("BuyElf",Dynamic_Wrap(ShopPanel,"OnBuyElf"))
  CustomGameEventManager:RegisterListener("BuyBack",Dynamic_Wrap(ShopPanel,"BuyBack"))
  CustomGameEventManager:RegisterListener("BuyMercenaries",Dynamic_Wrap(ShopPanel,"BuyMercenaries"))
  CustomGameEventManager:RegisterListener("OnShopClick",Dynamic_Wrap(ShopPanel,"OnShopClick"))
  CustomGameEventManager:RegisterListener("OnUnitShopClick",Dynamic_Wrap(ShopPanel,"OnUnitShopClick"))
  CustomGameEventManager:RegisterListener("OnFlyingMountClick",Dynamic_Wrap(ShopPanel,"OnFlyingMountClick"))
  CustomGameEventManager:RegisterListener("OnShopKeyDown",Dynamic_Wrap(ShopPanel,"OnShopKeyDown"))
  CustomGameEventManager:RegisterListener("SetUnitShopOpenState",Dynamic_Wrap(ShopPanel,"SetUnitShopOpenState"))
  CustomGameEventManager:RegisterListener("SetContainerOpenState",Dynamic_Wrap(ShopPanel,"SetContainerOpenState"))
  -- CustomGameEventManager:RegisterListener("OnCourierSelected",Dynamic_Wrap(ShopPanel,"OnCourierSelected"))

  ShopPanel.Cooldown={}
  ShopPanel.Kucun={}
  BuyBackCost = {}

  for i = 1,8,1 do
    ShopPanel.Cooldown[i]={}
    ShopPanel.Kucun[i]={}
  end

  ShopIndexs = {
    contShopRadEnt:GetEntityIndex(),
    contShopDireEnt:GetEntityIndex(),
    SideShopEnt1:GetEntityIndex(),
    SideShopEnt2:GetEntityIndex(),
  }
  for _,ShopIndex in pairs(ShopIndexs) do
    --print("ShopIndex",ShopIndex)
  --初始单位冷却 1光明小精灵 2黑暗小精灵 3萨满祭司 4巨魔巫医 5树妖 6花岗岩傀儡 7树魔影子牧师 8地精工兵
    ShopPanel.Cooldown[1][ShopIndex]=0
    ShopPanel.Cooldown[2][ShopIndex]=0
    ShopPanel.Cooldown[3][ShopIndex]=0
    ShopPanel.Cooldown[4][ShopIndex]=0
    ShopPanel.Cooldown[5][ShopIndex]=440
    ShopPanel.Cooldown[6][ShopIndex]=0
    ShopPanel.Cooldown[7][ShopIndex]=0
    ShopPanel.Cooldown[8][ShopIndex]=0
  --初始单位库存 1光明小精灵 2黑暗小精灵 3萨满祭司 4巨魔巫医 5树妖 6花岗岩傀儡 7树魔影子牧师 8地精工兵
    ShopPanel.Kucun[1][ShopIndex]=1
    ShopPanel.Kucun[2][ShopIndex]=1
    ShopPanel.Kucun[3][ShopIndex]=3
    ShopPanel.Kucun[4][ShopIndex]=3
    ShopPanel.Kucun[5][ShopIndex]=0
    ShopPanel.Kucun[6][ShopIndex]=5
    ShopPanel.Kucun[7][ShopIndex]=1
    ShopPanel.Kucun[8][ShopIndex]=1
  end

  BuyGold = {7000,7000,400,600,1000,500,2000,2500}--购买金钱
  BuyCooldown = {600,600,30,30,460,20,60,60}--补货时间
  BuyKucun = {1,1,3,3,3,5,1,1}--最大库存

  BuyUnitName = {}
  BuyUnitName[3] = "npc_dota_creep_samanjisi"
  BuyUnitName[4] = "npc_dota_creep_jumowuyi"
  BuyUnitName[5] = "npc_dota_creep_huagangyankuilei"
  BuyUnitName[6] = "npc_creep_dijinggongbing"
  BuyUnitName[7] = "item_jiqiangshouwei"
  BuyUnitName[8] = "item_huoyanshouwei"

end
--==============================[[购买精灵]]
function ShopPanel:OnBuyElf( keys )
  
  local _type = keys.type
  BuyUnit(_type,keys)
end

--==============================[[购买雇佣兵]]
function ShopPanel:BuyMercenaries( keys )
  -- 购买雇佣兵
  --local _type=keys.type + 2   -- 购买的雇佣兵
  -- CustomGameEventManager:Send_ServerToAllClients("unitshop_updateStock", { Index = GameRules.HeroTavernEntityID, Item = UnitShopItem, playerID = playerID, Tier=tier, Altar=hasAltar})

  BuyUnitByName( keys.UnitName, keys )
end

function  BuyUnit( _type, keys )
  local playerID=keys.PlayerID
  --print(_type,BuyKucun[_type])
  local player=PlayerResource:GetPlayer(playerID)
  local team=PlayerResource:GetTeam(playerID)
  local steamid=PlayerResource:GetSteamAccountID(playerID)
  local gold = PlayerResource:GetGold(playerID)
  
  --test
  local ShopIndex = 1

  local buyer = keys.unit


 
  if buyer then
    buyer = EntIndexToHScript(keys.unit)
  end
  if ShopPanel.Kucun[_type][ShopIndex] == 0 then
    SendErrorMessage(playerID, "#dota_hud_error_item_out_of_stock")
    --Notifications:Top(0, {text="没有库存了！", duration=1, style={color="red"}, continue=true})
  elseif gold < BuyGold[_type] then
    SendErrorMessage(playerID, "#dota_hud_error_not_enough_gold")
    --Notifications:Top(0, {text="金币不足！", duration=1, style={color="red"}, continue=true})
  else
    PlayerResource:SpendGold(playerID,BuyGold[_type],4)
    Containers:EmitSoundOnClient(playerID,"General.Buy")
    ShopPanel.Kucun[_type][ShopIndex]=ShopPanel.Kucun[_type][ShopIndex]-1
    if _type <=2 then
      MultiHeroControlSystem:Create_elf({playerID=playerID,type=_type})
    else
      local unit = CreateUnitByName(BuyUnitName[_type], buyer:GetAbsOrigin(), true, player, player,team)
      unit:SetOwner(buyer:GetOwner())
      unit:SetControllableByPlayer(playerID, true)
      FindClearSpaceForUnit(unit,unit:GetAbsOrigin(), false)
    end
  end

end

function  BuyUnitByName( UnitName, keys )
  local playerID=keys.PlayerID
  --print(_type,BuyKucun[_type])
  local player=PlayerResource:GetPlayer(playerID)
  local team=PlayerResource:GetTeam(playerID)
  local steamid=PlayerResource:GetSteamAccountID(playerID)
  local gold = PlayerResource:GetGold(playerID)
  
  --test
  local ShopIndex = keys.Shop

  local buyer = keys.unit

  local _type = GetUnitType(UnitName)
 
  if buyer then
    buyer = EntIndexToHScript(keys.unit)
  end
  local UnitShop = EntIndexToHScript(ShopIndex)
  local Item = UnitShop.Items[_type]

  if not buyer:HasInventory() then
    SendErrorMessage(playerID, "#dota_hud_error_target_cant_take_items")
  elseif Item.CurrentStock == 0 then
    SendErrorMessage(playerID, "#dota_hud_error_item_out_of_stock")
    --Notifications:Top(0, {text="没有库存了！", duration=1, style={color="red"}, continue=true})
  elseif not IsInShopRange(buyer,UnitShop) then
    SendErrorMessage(playerID, "#dota_hud_error_target_out_of_range")
  elseif gold < BuyGold[_type] then
    SendErrorMessage(playerID, "#dota_hud_error_not_enough_gold")
    --Notifications:Top(0, {text="金币不足！", duration=1, style={color="red"}, continue=true})
  else
    if _type <=2 then
      MultiHeroControlSystem:Create_elf({playerID=playerID,type=_type})
    elseif _type > 6 then
      local item = CreateItem(BuyUnitName[_type], buyer, buyer)
      local iname = item:GetAbilityName()
      local exists = false
      local full = true
      for i=0,5 do
        local it = buyer:GetItemInSlot(i)
        if not it then
          full = false
        elseif it:GetAbilityName() == iname then
          exists = true
        end
      end
      if not full or (full and item:IsStackable() and exists) or item:IsCastOnPickup() then
        buyer:AddItem(item)
      else
        CreateItemOnPositionSync(buyer:GetAbsOrigin() + RandomVector(10), item)
      end
    else
      local unit = CreateUnitByName(BuyUnitName[_type], buyer:GetAbsOrigin(), true, player, player,team)
      unit:SetOwner(buyer:GetOwner())
      --print("owner",player,buyer:GetOwner(),buyer:GetPlayerOwner(),unit:GetOwner(),unit:GetPlayerOwner(),unit:GetPlayerOwnerID())
      unit:SetControllableByPlayer(playerID, true)
      FindClearSpaceForUnit(unit,unit:GetAbsOrigin(), false)
    end
    PlayerResource:SpendGold(playerID,Item.GoldCost,4)
    Containers:EmitSoundOnClient(playerID,"General.Buy")
    Item.CurrentStock=Item.CurrentStock-1
  end

end

--==============================[[买活]]
function ShopPanel:BuyBack( keys )
  -- 买活
  local playerID=keys.PlayerID
  local entIndex=keys.entIndex   --要复活的英雄实体index
  local hero=EntIndexToHScript(entIndex)  --要复活的英雄
  
  local gold = BuyBackCost[entIndex]

  local buyer = keys.unit
  if buyer then
    buyer = EntIndexToHScript(keys.unit)
  end
  -- print("BuyBack",buyer)

--[[
  if buyer:GetPlayerOwnerID() and buyer:GetPlayerOwnerID() == playerID and not (IsValidEntity(buyer) and buyer:IsAlive()) then
    SendErrorMessage(playerID, "#dota_hud_error_cant_sell_shop_not_in_range")
    --Notifications:Top(0, {text="商店守卫附近必须有有效的购买单位", duration=1, style={color="red"}, continue=true})
    return
  end
--]]
  local jiqiang = FindUnitsInRadius(DOTA_TEAM_BADGUYS, buyer:GetAbsOrigin(), nil, 300, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)

  if jiqiang then
    if PlayerResource:GetGold(playerID) < gold then
      SendErrorMessage(playerID, "#dota_hud_error_not_enough_gold")
      --Notifications:Top(0, {text="金钱不足", duration=1, style={color="red"}, continue=true})
    else
      hero.IsBuyBacked = true
      hero:SetRespawnPosition(jiqiang[1]:GetAbsOrigin())
      hero:SetTimeUntilRespawn(0)
      FindClearSpaceForUnit(hero, hero:GetAbsOrigin(), true)
      PlayerResource:SpendGold(playerID,gold,4)
      EmitGlobalSound("valve_dota_001.stinger.buy_back")
    end
  else
    SendErrorMessage(playerID, "#dota_hud_error_cant_sell_shop_not_in_range")
    --Notifications:Top(0, {text="商店守卫附近必须有有效的购买单位", duration=1, style={color="red"}, continue=true})
  end

  --要做什么判断自己加了，比如什么离机枪多远什么的
  --判断金币什么的模仿购买精灵，然后直接对hero进行复活操作，在什么地方复活，你懂的，很简单啦
end

function CreateUnitShop( ShopID ,IsHome,playerID)
  -- print("CreateUnitShop", ShopID,IsHome)
  local UnitShop = EntIndexToHScript(ShopID)
  if UnitShop.Items == nil then
  -- initialise Shop item table
    UnitShop.Items = {}
  end
  local ShopName = UnitShop.headerText
  local ShopItemTable = {}
  local shopList = {}
  if IsHome then
    shopList = {
      "npc_light_elf",
      "npc_dark_elf",
      "npc_dota_creep_samanjisi",
      "npc_dota_creep_jumowuyi",
      "npc_dota_creep_huagangyankuilei",
      "npc_creep_dijinggongbing",
      "item_jiqiangshouwei",
      "item_huoyanshouwei",
    }
  else
    shopList = {
      "npc_dota_creep_samanjisi",
      "npc_dota_creep_jumowuyi",
      "npc_dota_creep_huagangyankuilei",
    }
  end

  for order,itemname in pairs(shopList) do
    --unit_shops:print("Creating timer for "..itemname.." new unit shop: "..shop_name)
    local key = itemname

    local UnitType = GetUnitType(key)
    -- set all variables

    -- print("UnitShop.Items[UnitType]",UnitShop.Items[UnitType])
    if not UnitShop.Items[UnitType] then
      UnitShop.Items[UnitType] = {}
      UnitShop.Items[UnitType].ItemName = key
      UnitShop.Items[UnitType].CurrentRefreshTime = 1
    
      local Item = UnitShop.Items[UnitType]
    
    -- Tavern heroes initially cost only food
      Item.CurrentStock = ShopPanel.Kucun[UnitType][ShopID]
      Item.MaxStock = BuyKucun[UnitType]
      Item.RequiredTier = 0
      Item.GoldCost = BuyGold[UnitType]
      Item.LumberCost = 0
      Item.FoodCost = 0
      Item.RestockRate = BuyCooldown[UnitType]
      Item.StockStartDelay = 0
      Item.ShopID = ShopID
    
      local isGlobal = true

      --print("StockUpdater",key,Item.CurrentStock,Item.MaxStock,Item.GoldCost,Item.RestockRate)
      ShopPanel:StockUpdater(Item, ShopID, isGlobal)

    end
    -- save item into table using it's sort index, this is send once at the beginning to initialise the shop
    ShopItemTable[order] = UnitShop.Items[UnitType]
  end

  local player = PlayerResource:GetPlayer(playerID)
  -- print("Shops_Create", ShopID,ShopItemTable)
  CustomGameEventManager:Send_ServerToPlayer(player,"Shops_Create", {Index = ShopID, Shop = ShopItemTable, Tier=0, ShopName=ShopName, Neutral = "goblin_merchant"})

end

function ShopPanel:OpenUnitShop(keys)
  -- print("OpenUnitShop")
  local player=PlayerResource:GetPlayer(keys.PlayerID)
  local ShopIndex = nil
  local unit = nil

  if keys.ShopIndex then
    ShopIndex = keys.ShopIndex
  else
    unit = EntIndexToHScript(keys.unitIndex) 
    if keys.PlayerID == unit:GetPlayerOwnerID() then
      ShopIndex = GetCurrentRangeUnitShopIndex(unit)
    end
  end

  if not ShopIndex or not EntIndexToHScript(ShopIndex).IsUnitShopOpen then
    if unit:GetTeam() == DOTA_TEAM_GOODGUYS then
      ShopIndex = contShopRadEnt:GetEntityIndex()
    elseif unit:GetTeam() == DOTA_TEAM_BADGUYS then
      ShopIndex = contShopDireEnt:GetEntityIndex()
    else
      print("unit NOTeam!!",unit:GetTeam())
    end
  end

  
  CustomGameEventManager:Send_ServerToPlayer(player, "Shops_Open", {Shop=ShopIndex})
  --EmitSoundOnClient("Shop.PanelUp", player)
  EntIndexToHScript(ShopIndex).IsUnitShopOpen[keys.PlayerID] = true
end

function ShopPanel:CloseUnitShop(keys)
  -- print("CloseUnitShop")
  local player=PlayerResource:GetPlayer(keys.PlayerID)
  local ShopIndex = keys.ShopIndex

  CustomGameEventManager:Send_ServerToPlayer(player, "Shops_Close", {Shop=ShopIndex})
  --EmitSoundOnClient("Shop.PanelDown", player)
  EntIndexToHScript(ShopIndex).IsUnitShopOpen[keys.PlayerID] = false
end

function ShopPanel:CloseAllUnitShop(keys)
  -- print("CloseAllUnitShop")
  local player=PlayerResource:GetPlayer(keys.PlayerID)

  CustomGameEventManager:Send_ServerToPlayer(player, "shop_force_hide", {})
  for _,ShopIndex in pairs(ShopIndexs) do
    EntIndexToHScript(ShopIndex).IsUnitShopOpen[keys.PlayerID] = false
  end
end

function ShopPanel:SetAllIsOpenFalse(keys)
  -- print("SetIsUnitShopOpen")
  local player=PlayerResource:GetPlayer(keys.PlayerID)
  for _,ShopIndex in pairs(ShopIndexs) do
    EntIndexToHScript(ShopIndex).IsUnitShopOpen[keys.PlayerID] = false
  end
end

function ShopPanel:IsOpen(shopEnt,playerID)
  if shopEnt.IsUnitShopOpen == nil then
    return nil
  end
  return shopEnt.IsUnitShopOpen[playerID]
end

function ShopPanel:SetupShops(keys)
  print("SetupShops")
  ---[[
  local playerID = keys.PlayerID
  for k,ShopIndex in pairs(ShopIndexs) do
    if ShopIndex == contShopRadEnt:GetEntityIndex() or  ShopIndex == contShopDireEnt:GetEntityIndex() then
      local IsHome = true
      CreateUnitShop(ShopIndex,IsHome,playerID)
    elseif  ShopIndex == SideShopEnt1:GetEntityIndex() or ShopIndex == SideShopEnt2:GetEntityIndex() then
      local IsHome = false
      CreateUnitShop(ShopIndex,IsHome,playerID)
    end
  end
  --]]
end

function ShopPanel:StockUpdater(UnitShopItem, ShopID, isGlobal)
  local UnitID = ShopID
  --local team = unit:GetTeam()
  Timers:CreateTimer(1, function()
    --local playerID = unit:GetPlayerOwnerID()  
    --local tier = playerID and Players:GetCityLevel(playerID) or 9000

    --[[
    if not IsValidEntity(unit) or not unit:IsAlive() then
      -- send command to kill shop panel
      print("Shop identified not valid, terminating timer")
      return
    end
    --]]

    ShopPanel:Stock_Management(UnitShopItem)
    if isGlobal then
      CustomGameEventManager:Send_ServerToAllClients("unitshop_updateStock", { Index = UnitID, Item = UnitShopItem})    
    end
    
    return 1
  end)
end

function ShopPanel:Stock_Management(UnitShopItem)
  -- if the item is not at max stock start a counter until it's restocked
  
  if UnitShopItem.CurrentStock < UnitShopItem.MaxStock then
    --print("CurrentStock",UnitShopItem.CurrentStock,UnitShopItem.MaxStock)
    if UnitShopItem.StockStartDelay ~= 0 and UnitShopItem.CurrentRefreshTime == UnitShopItem.StockStartDelay then
      -- this is might need altering, currently its abit hardcoded
      UnitShopItem.CurrentStock = UnitShopItem.MaxStock
      
      -- set to 0 to stop condition from being met so that it resumes normal restocking rates
      UnitShopItem.StockStartDelay = 0
      
      -- reset counter for next stock
      UnitShopItem.CurrentRefreshTime = 1
      --print("Increasing stock count by 1 for global shop")
    elseif UnitShopItem.CurrentRefreshTime == UnitShopItem.RestockRate then
      -- increase stock by 1 when the currentrefreshtime == the refreshrate
      UnitShopItem.CurrentStock = UnitShopItem.CurrentStock + 1
      
      -- reset counter for next stock
      UnitShopItem.CurrentRefreshTime = 1
      --print("Increasing stock count by 1")
    else
      --unit_shops:print("Incrementing counter to restock")
      -- increment the time counter
      UnitShopItem.CurrentRefreshTime = UnitShopItem.CurrentRefreshTime + 1
    end
  
  end
end

function GetUnitType( UnitName )
  -- body
  if UnitName == "npc_light_elf" then
    UnitType = 1
  elseif UnitName == "npc_dark_elf" then
    UnitType = 2
  elseif UnitName == "npc_dota_creep_samanjisi" then
    UnitType = 3
  elseif UnitName == "npc_dota_creep_jumowuyi" then
    UnitType = 4
  elseif UnitName == "npc_dota_creep_huagangyankuilei"  then
    UnitType = 5
  elseif UnitName == "npc_creep_dijinggongbing" then
    UnitType = 6
  elseif UnitName == "item_jiqiangshouwei" then
    UnitType = 7
  elseif UnitName == "item_huoyanshouwei" then
    UnitType = 8
  end

  return UnitType
end

function InShopRange( keys )
  --print("InShopRange",keys.activator)
  for k,v in pairs(keys) do
    --print(k,v)
  end
  local activator=keys.activator  
  local caller=keys.caller
  if not activator then
    return
  end
  local player = keys.activator:GetOwner() 
  --print("InShopRange2",player)
  if not activator or not activator:HasInventory() or not player then
    return
  end

  if activator.ValidShop == nil then
    activator.ValidShop = {}
  end

  local shopEnt = GetShopEntityByTrigger( caller )
  if shopEnt then
    if (shopEnt == JiqiangRadiant or shopEnt == JiqiangDire) then
      -- print("DeathMatch",DeathMatch,DeathMatch~=true)
      if not DeathMatch then
        CustomGameEventManager:Send_ServerToPlayer(player, "SetBuyBacker", {unit=activator:GetEntityIndex(),Buyable=true})
      end
    elseif shopEnt:GetTeam() == DOTA_TEAM_GOODGUYS and activator:GetTeam() == DOTA_TEAM_BADGUYS then
      return
    elseif shopEnt:GetTeam() == DOTA_TEAM_BADGUYS and activator:GetTeam() == DOTA_TEAM_GOODGUYS then
      return
    elseif shopEnt.IsUnitShopOpen then
      CustomGameEventManager:Send_ServerToPlayer(player, "ActivateShopButton", {unit=activator:GetEntityIndex(),Activate=true})
      CustomGameEventManager:Send_ServerToPlayer(player, "ActivateUnitShopButton", {unit=activator:GetEntityIndex(),Activate=true})
      activator.ValidShop[shopEnt:GetEntityIndex()] = true
    else
      CustomGameEventManager:Send_ServerToPlayer(player, "ActivateShopButton", {unit=activator:GetEntityIndex(),Activate=true})
      activator.ValidShop[shopEnt:GetEntityIndex()] = true
    end
  end
  
end

function OutShopRange( keys )
  
  for k,v in pairs(keys) do
    --print(k,v)
  end
  local activator=keys.activator  
  local caller=keys.caller
  if not activator then
    return
  end
  local player = activator:GetOwner() 
  if not activator or not activator:HasInventory() or not player then
    return
  end

  if activator.ValidShop == nil then
    activator.ValidShop = {}
  end

  local IsLeaveShop = true
  local IsLeaveUnitShop = true
  local shopEnt = GetShopEntityByTrigger( caller )
  if shopEnt then
    if (shopEnt == JiqiangRadiant or shopEnt == JiqiangDire) then
      -- print("DeathMatch",DeathMatch,DeathMatch~=true)
      if not DeathMatch then
        CustomGameEventManager:Send_ServerToPlayer(player, "SetBuyBacker", {unit=activator:GetEntityIndex(),Buyable=false})
      end
    else
      activator.ValidShop[shopEnt:GetEntityIndex()] = false
      
      for k,v in pairs(activator.ValidShop) do
        --print("activator.ValidShop",k,v)
        if v then
          if EntIndexToHScript(k).IsUnitShopOpen then
            IsLeaveUnitShop = false
          end
          IsLeaveShop = false
        end
      end
      if IsLeaveShop then
        CustomGameEventManager:Send_ServerToPlayer(player, "ActivateShopButton", {unit=activator:GetEntityIndex(),Activate=false})
      end
      if IsLeaveUnitShop then
        CustomGameEventManager:Send_ServerToPlayer(player, "ActivateUnitShopButton", {unit=activator:GetEntityIndex(),Activate=false})
      end
    end
  end
  --print("OutShopRange")
end

function IsInShopRange( unit,shop )

  if unit.ValidShop == nil then
    unit.ValidShop = {}
  end

  return unit.ValidShop[shop:GetEntityIndex()]
end

--获取当前所处商店最近一个
function GetCurrentRangeShopIndex(unit)

  if unit.ValidShop == nil then
    unit.ValidShop = {}
    return nil
  end

  local RangeShop = {}
  for k,v in pairs(unit.ValidShop) do 
    if v then
      table.insert(RangeShop,k)
    end
  end
  if #RangeShop == 1 then
    return RangeShop[1]
  elseif #RangeShop > 1 then
    local shortest = 99999
    local ShopIndex = -1
    for _,k in pairs(RangeShop) do 
      local length = (unit:GetAbsOrigin() - EntIndexToHScript(k):GetAbsOrigin()):Length2D()
      if length < shortest then
        shortest = length
        ShopIndex = k
      end
    end
    return ShopIndex
  else
    return nil
  end
end

--获取当前所处雇佣兵商店最近一个
function GetCurrentRangeUnitShopIndex(unit)
  if unit.ValidShop == nil then
    unit.ValidShop = {}
    return nil
  end

  local RangeShop = {}
  for k,v in pairs(unit.ValidShop) do 
    if v and k ~= SecretShopEnt1:GetEntityIndex() and k~= SecretShopEnt1:GetEntityIndex() then
      table.insert(RangeShop,k)
    end
  end
  if #RangeShop == 1 then
    return RangeShop[1]
  elseif #RangeShop > 1 then
    local shortest = 99999
    local ShopIndex = -1
    for _,k in pairs(RangeShop) do 
      local length = (unit:GetAbsOrigin() - EntIndexToHScript(k):GetAbsOrigin()):Length2D()
      if length < shortest then
        shortest = length
        ShopIndex = k
      end
    end
    return ShopIndex
  else
    return nil
  end
end

function GetShopEntityByTrigger( trigger )
  local triggerName = trigger:GetName()
  -- print("GetShopEntityByTrigger",trigger,triggerName )
  if triggerName == "trigger_shop_radiant" then
    return contShopRadEnt
  elseif triggerName == "trigger_shop_dire" then
    return contShopDireEnt
  elseif triggerName == "trigger_shop_side1" then
    return SideShopEnt1
  elseif triggerName == "trigger_shop_side2" then
    return SideShopEnt2
  elseif triggerName == "trigger_shop_secret1" then
    return SecretShopEnt1
  elseif triggerName == "trigger_shop_secret2" then
    return SecretShopEnt2
  elseif triggerName == "trigger_jiqiang_radiant" then
    return JiqiangRadiant
  elseif triggerName == "trigger_jiqiang_dire" then
    return JiqiangDire
  else
    return nil
  end
end

function ShopPanel:OnShopClick( keys )
  -- print("OnShopClick")
  local unitIndex = keys.unitIndex
  local ShopIndex = GetCurrentRangeShopIndex(EntIndexToHScript(unitIndex))
  local playerID = keys.PlayerID
  if not ShopIndex then
    if PlayerResource:GetTeam(playerID) == DOTA_TEAM_GOODGUYS then
      ShopIndex = contShopRadEnt:GetEntityIndex()
    elseif PlayerResource:GetTeam(playerID) == DOTA_TEAM_BADGUYS then
      ShopIndex = contShopDireEnt:GetEntityIndex()
    else
      print("unit NOTeam!!",PlayerResource:GetTeam(playerID))
    end
  end
  local container = EntIndexToHScript(ShopIndex).container

  if not container:IsOpen(playerID) then
    container:Open(ShopIndex,playerID)
    Containers:EmitSoundOnClient(playerID,"Shop.PanelUp")
  else
    container:Close(ShopIndex,playerID)
    Containers:EmitSoundOnClient(playerID,"Shop.PanelDown")
  end

end

function ShopPanel:OnUnitShopClick( keys )
  -- print("OnUnitShopClick")
  local ShopIndex = GetCurrentRangeUnitShopIndex(EntIndexToHScript(keys.unitIndex))
  local playerID = keys.PlayerID
  if not ShopIndex or not EntIndexToHScript(ShopIndex).IsUnitShopOpen then
    if PlayerResource:GetTeam(playerID) == DOTA_TEAM_GOODGUYS then
      ShopIndex = contShopRadEnt:GetEntityIndex()
    elseif PlayerResource:GetTeam(playerID) == DOTA_TEAM_BADGUYS then
      ShopIndex = contShopDireEnt:GetEntityIndex()
    else
      print("unit NOTeam!!",PlayerResource:GetTeam(playerID))
    end
  end
  if ShopPanel:IsOpen(EntIndexToHScript(ShopIndex),playerID) then
    ShopPanel:CloseAllUnitShop(keys)
  else
    ShopPanel:CloseAllUnitShop(keys)
    ShopPanel:OpenUnitShop(keys)
  end
end
function ShopPanel:OnShopKeyDown(keys)
  -- print("OnShopKeyDown")
  ShopPanel:OnShopClick( keys )
  ShopPanel:OnUnitShopClick( keys )
end
function ShopPanel:OnFlyingMountClick( keys )
  -- print("OnFlyingMountClick")
  local playerID = keys.PlayerID
  local player=PlayerResource:GetPlayer(playerID)
  
  if player.FlyingMountIndex and EntIndexToHScript(player.FlyingMountIndex) and IsValidEntity(EntIndexToHScript(player.FlyingMountIndex)) and EntIndexToHScript(player.FlyingMountIndex):IsAlive() then
    --CustomGameEventManager:Send_ServerToPlayer(player,"select_unit",{unitIndex=player.FlyingMountIndex:GetEntityIndex()})
  else
    local gold = PlayerResource:GetGold(playerID)
    if gold < 1000 then
      SendErrorMessage(playerID, "#dota_hud_error_not_enough_gold")
      return
    else
      PlayerResource:SpendGold(playerID,1000,4)
      Containers:EmitSoundOnClient(playerID,"General.Buy")
    end

    local team = PlayerResource:GetTeam(playerID)
    local spawnLoc
    if team == DOTA_TEAM_GOODGUYS then 
      spawnLoc = Entities:FindByName(nil, "hero_spawn_1"):GetAbsOrigin()
    elseif team == DOTA_TEAM_BADGUYS then
      spawnLoc = Entities:FindByName(nil, "hero_spawn_2"):GetAbsOrigin()
    end
    local courier = CreateUnitByName( "npc_dota_courier", spawnLoc, true, player, player, team )
    courier:UpgradeToFlyingCourier()
    local model = courier:GetModelName()
    courier:SetAbsOrigin(Vector(8000,8000,-300))
    courier:AddNoDraw()
    courier:AddAbility("courier_out_of_world")
    -- courier:RemoveSelf()
    
    local FlyingMount = CreateUnitByName( "npc_flying_mount", spawnLoc, true, player, player, team )
    FlyingMount:SetModel(model)
    FlyingMount:SetOriginalModel(model)
    FlyingMount:SetControllableByPlayer(playerID, true)
    player.FlyingMountIndex = FlyingMount:GetEntityIndex()
    -- CustomGameEventManager:Send_ServerToPlayer(player,"select_unit",{unitIndex=player.FlyingMountIndex:GetEntityIndex()})
    MultiHeroControlSystem:CameraMoveToUnit(playerID,FlyingMount)
    CustomGameEventManager:Send_ServerToPlayer(player, "SetFlyingMountButton", {FlyingMount=player.FlyingMountIndex})
  end
end

function ShopPanel:OnCourierSelected( keys )
  -- print("OnCourierSelected")
  local playerID = keys.PlayerID
  local player=PlayerResource:GetPlayer(playerID)
  if player.FlyingMountIndex and EntIndexToHScript(player.FlyingMountIndex) and IsValidEntity(EntIndexToHScript(player.FlyingMountIndex)) and EntIndexToHScript(player.FlyingMountIndex):IsAlive() then
    CustomGameEventManager:Send_ServerToPlayer(player,"select_unit",{unitIndex=player.FlyingMountIndex:GetEntityIndex()})
  end
end

function ShopPanel:SetUnitShopOpenState( keys )
  -- print("SetUnitShopOpenState")
  -- PrintTable(keys)
  local playerID = keys.PlayerID
  local ShopID = tonumber(keys.ShopID)
  local shopEnt = EntIndexToHScript(ShopID)
  if shopEnt.IsUnitShopOpen == nil then
    return nil
  end
  shopEnt.IsUnitShopOpen[playerID] = keys.IsOpen == 1
end

function ShopPanel:SetContainerOpenState( keys )
  -- print("SetContainerOpenState")
  -- PrintTable(keys)
  local playerID = keys.PlayerID
  local containerID = tonumber(keys.containerID)
  local container = Containers.containers[containerID]
  if container then
    if keys.IsOpen == 1 then
      container:SetOpen( playerID, true )
    else
      container:SetOpen( playerID, nil )
    end
  end
end