-- Copyright (c) 2015-2016 野猪大大(zllang)
--================================================================================================================
--=======================================||外建服务器||===========================================================
--================================================================================================================
--[[服务器数据表结构：			
			Database={---------------------------数据库根节点
				3c={-----------------------------3c节点
					user={-----------------------用户节点
						steamID={----------------单个用户节点，steamID为key
							Coin=123,------------------3c币
							IsVIP=1,-------------------是否为VIP
							Score=1589,----------------积分
							Tittle={-------------------称号，可以多个
								tittle1,
								tittle2
							}
						}
					}
				}
			}

]]
--=======================================||Http处理类||===========================================================
	print('Encrypted File part2 Start Init...................')	
	local Bit = require("libraries.lockbox.util.bit");
	local String = require("string");
	local Stream = require("libraries.lockbox.util.stream");
	local Array = require("libraries.lockbox.util.array");
	local Queue = require("libraries.lockbox.util.queue");
	local HMAC = require("libraries.lockbox.mac.hmac");
	local SHA2_256 = require("libraries.lockbox.digest.sha2_256");
	local Base64 = require("libraries.lockbox.util.base64");
	local Json = require("libraries.lockbox.util.dkjson");
	--[[POST方法：新建k-v]]
	function HttpMethod:POST( directory, param, data ,callback)
		local url=self.Setting.host..directory..'.json?'..'auth='..Token()
		if param then url=url..'&'..param end
		local req = CreateHTTPRequest("POST", url)
	   	--req:SetHTTPRequestGetOrPostParameter('application/json;charset=UTF-8', json.encode(data))
	   	req:SetHTTPRequestRawPostBody('application/json;charset=UTF-8', json.encode(data))       	
		req:Send(function (result)
			local result=json.decode(result["Body"])
			if callback then 
				if not result then 
					print('GET Field!') callback(false) 
					return false 
				else
					callback(result)
				end
			end				
		end)
	end
	--[[PUT方法：重写k-v]]
	function HttpMethod:PUT( directory , param, data ,callback)
		local url=self.Setting.host..directory..'.json?'..'auth='..Token()
		url=url..'&'..'x-http-method-override=PUT'
		if param then url=url..'&'..param end
		local req = CreateHTTPRequest("POST", url)
	   	req:SetHTTPRequestRawPostBody('application/json;charset=UTF-8', json.encode(data))       	
		req:Send(function (result)
			if not result then print('PUT Field!') callback(false) return false end
			local result=json.decode(result["Body"])
			if callback then 
				if not result then 
					print('GET Field!') callback(false) 
					return false 
				else
					callback(result)
				end
			end				
		end)
	end
	--[[PATCH方法：更新数据]]
	function HttpMethod:PATCH( directory , param, data ,callback)
		local url=self.Setting.host..directory..'.json?'..'auth='..Token()
		url=url..'&'..'x-http-method-override=PATCH'
		if param then url=url..'&'..param end
		local req = CreateHTTPRequest("POST", url)
	   	req:SetHTTPRequestRawPostBody('application/json;charset=UTF-8', json.encode(data))       	
		req:Send(function (result)
			local StatusCode=json.decode(result["StatusCode"])		
			local result=json.decode(result["Body"])
			if callback then 
				if StatusCode ~= 200 then 
					print('PATCH Field!') callback(false,StatusCode) 
					return false 
				else
					callback(result,StatusCode)
				end
			end							
		end)
	end
	--[[DELETE方法：删除k-v]]
	function HttpMethod:DELETE( directory , param, data,callback)
		local url=self.Setting.host..directory..'.json?'..'auth='..Token()
		url=url..'&'..'x-http-method-override=DELETE'
		if param then url=url..'&'..param end
		local req = CreateHTTPRequest("POST", url)
	   	req:SetHTTPRequestRawPostBody('application/json;charset=UTF-8', json.encode(data))       	
		req:Send(function (result)
			local result=json.decode(result["Body"])
			if callback then 
				if not result then 
					print('GET Field!') callback(false) 
					return false 
				else
					callback(result)
				end
			end				
		end)
	end
	--[[GET方法：查询数据]]
	function HttpMethod:GET(  directory , param, callback )
		local url=self.Setting.host..directory..'.json?'..'auth='..Token()
		if param then url=url..'&'..param end
		local req = CreateHTTPRequest("GET", url)       	
		req:Send(function (result)
			local StatusCode=json.decode(result["StatusCode"])	
			print('StatusCode GET:',StatusCode)
			local result=json.decode(result["Body"])
			if callback then 
				if StatusCode ~= 200 then 
					print('GET Field!') callback(false,StatusCode) 
					return false 
				else
					--print("Result GET:")
					--DeepPrintTable(result)
					callback(result,StatusCode)
				end
			end			
		end)
	end
--=======================================||服务器读写处理类||===========================================================

	if Server==nil then Server=class({},{},HttpMethod) _G.Server=Server end

	--[[创建新用户]]

	function Server:CreateUser( SteamID ,callback)
		local directory="3c/user"
		local data={}
		data[SteamID]={
						Coin=8,
						IsVIP=false,
						Score=1000,
						ScoreD=1000,
						Title={},
						IsBetaPlayer=true
					}	
						
		self:PATCH(directory,nil,data,callback)
	end

	--[[获取用户信息]]

	function Server:GetUserInfo( SteamID ,callback)
		local directory="3c/user/"..SteamID
		--调用GET方法获取信息
		self:GET(directory,nil,function ( result ,StatusCode)
			--如果查询为空，则代表新玩家，先在服务器建立用户
			local SteamID=SteamID
			if StatusCode==200 then
				if not result then 
					self:CreateUser( SteamID ,function (_result,_StatusCode)
							GameRules:GetGameModeEntity().DB.AskForUpdate(SteamID)
					end)
				else
					if callback then callback(result) end
				end
			else
				print("Result GetUserInfo:")
				DeepPrintTable(result)
				if callback then callback(false) end									 
			end
		end)
	end

	--[[更新玩家数据]]
	--[[
		param:data={Coin=XXX,IsVIP=XXX,Score=XXXX,Tittle=XXXX}
	]]

	function Server:UpdataData( SteamID ,data,callback)
		local directory="3c/user/"..SteamID
		self:PATCH(directory,nil,data,callback)
	end

	--[[查询排行榜]]

	function Server:GetRankings(callback)
		local param='orderBy="Score"&limitToLast=20&print=pretty'
		if _G.DeathMatch==true then 
			param='orderBy="ScoreD"&limitToLast=20&print=pretty'
		end  
		self:GET('3c/user',param,function ( result )
			if not result then print('GET Field!') callback(false) return false end		
			local function bubble_sort(arr)    
			    local c=0
			    local tt={}
			    for k,v in pairs(arr) do
			    	c=c+1
			    	tt[c]={k,v}
			    end
			    local tmp = 0  
			    for i=1,c-1 do  
			        for j=1,c-1 do  
			        	if _G.DeathMatch then 

				            if tonumber( tt[j][2].ScoreD ) < tonumber(tt[j+1][2].ScoreD) then  
				                tmp = tt[j]
				                tt[j] = tt[j+1]
				                tt[j+1] = tmp 
				            end  
				        else
				        	if tonumber( tt[j][2].Score ) < tonumber(tt[j+1][2].Score) then  
				                tmp = tt[j]
				                tt[j] = tt[j+1]
				                tt[j+1] = tmp 
				            end 
				        end
			        end  
			    end 
			    return tt 
			end
			local bb=bubble_sort(result)
			if callback then callback(bb) end
		end)
	end
	print("Init ServerMethod success!")

