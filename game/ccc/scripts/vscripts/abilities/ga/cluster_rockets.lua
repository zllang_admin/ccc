-- Keep track of the targeted point to make the rockets
function StartClusterRockets( event )
	-- Variables
	local caster = event.caster
	local ability = event.ability
	--ability.point = event.target_points[1]
	local point = event.target_points[1]
	local radius =  ability:GetLevelSpecialValueFor( "radius" , ability:GetLevel() - 1  )
	--local projectile_count =  ability:GetLevelSpecialValueFor( "projectile_count" , ability:GetLevel() - 1  )
	local projectile_speed =  ability:GetLevelSpecialValueFor( "projectile_speed" , ability:GetLevel() - 1  )
	local projectile_radius =  ability:GetLevelSpecialValueFor( "projectile_radius" , ability:GetLevel() - 1  )
	local chixu =  ability:GetLevelSpecialValueFor( "chixu" , ability:GetLevel() - 1  )
	local projectile_count =  ability:GetLevelSpecialValueFor( "projectile_count" , ability:GetLevel() - 1  )
	local particleName = "particles/units/heroes/hero_tinker/tinker_missile.vpcf"
	local daodanjiange = chixu / projectile_count
	local Count = 0


	local casterTeam = caster:GetTeamNumber()
	local casterForward = caster:GetForwardVector()
	local casterLoc = caster:GetAbsOrigin()

	Timers:CreateTimer(daodanjiange,function()

		local random_position = point + RandomVector(RandomInt(0,radius))

		--[[马甲法
		local dummy = CreateUnitByName("npc_dummy_rocket", random_position, false, caster, caster, DOTA_UNIT_TARGET_TEAM_ENEMY)
		local projTable = {
			EffectName = particleName,
			Ability = ability,
			Target = dummy,
			Source = caster,
			bDodgeable = false,
			bProvidesVision = false,	
			vSpawnOrigin = caster:GetAbsOrigin(),
			iMoveSpeed = projectile_speed,
			iVisionRadius = 0,
			iVisionTeamNumber = caster:GetTeamNumber(),
			iSourceAttachment = DOTA_PROJECTILE_ATTACHMENT_ATTACK_3
		}

		ProjectileManager:CreateTrackingProjectile( projTable )
		Timers:CreateTimer(2,function() dummy:RemoveSelf() end)
		]]

		--[[project库
		local projectile = {
  			EffectName = particleName,
  			vSpawnOrigin = {unit=caster, attach="attach_attack3", offset=Vector(0,0,0)},
  			fDistance = (casterLoc - random_position):Length2D(),
  			fStartRadius = 0,
  			fEndRadius = 0,
  			Source = caster,
  			fExpireTime = 8.0,
  			vVelocity = (casterLoc - random_position):Normalized() * projectile_speed, -- RandomVector(1000),
  			UnitBehavior = PROJECTILES_DESTROY,
  			bMultipleHits = false,
  			bIgnoreSource = true,
  			TreeBehavior = PROJECTILES_NOTHING,
  			bCutTrees = false,
  			bTreeFullCollision = false,
  			WallBehavior = PROJECTILES_NOTHING,
  			GroundBehavior = PROJECTILES_NOTHING,
  			fGroundOffset = 80,
  			nChangeMax = 1,
  			bRecreateOnChange = true,
  			bZCheck = false,
  			bGroundLock = true,
  			bProvidesVision = false,
 	 		iVisionRadius = 350,
  			iVisionTeamNumber = casterTeam,
  			bFlyingVision = false,
  			fVisionTickTime = .1,
  			fVisionLingerDuration = 1,
  			draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
  			--iPositionCP = 0,
  			--iVelocityCP = 1,
  			ControlPoints = {[0]=random_position,[1]=random_position,[2]=Vector(projectile_speed,0,0),[4]=random_position},
  			--ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
  			--ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
  			--[[ControlPointEntityAttaches = {[0]={
  	  		--unit = hero,
  	  		--pattach = PATTACH_ABSORIGIN_FOLLOW,
  	  		--attachPoint = "attach_attack1", -- nil
  	  		--origin = Vector(0,0,0)
  			--}},
  			--fRehitDelay = .3,
  			--fChangeDelay = 1,
  			--fRadiusStep = 10,
  			--bUseFindUnitsInRadius = false,

  			UnitTest = function(self, unit) return unit:GetUnitName() ~= "npc_dummy_unit" and unit:GetTeamNumber() ~= hero:GetTeamNumber() end,
  			--OnUnitHit = function(self, unit) ... end,
    			
  			
  			--OnTreeHit = function(self, tree) ... end,
  			--OnWallHit = function(self, gnvPos) ... end,
  			--OnGroundHit = function(self, groundPos) ... end,
  			OnFinish = function(self, pos) 
  				print ('OnFinish: ',pos)
  				StartSoundEventFromPosition("Hero_Gyrocopter.Rocket_Barrage.Impact", pos)
  				local pFinish = ParticleManager:CreateParticle("particles/units/heroes/hero_tinker/tinker_missle_explosion.vpcf", PATTACH_CUSTOMORIGIN, nil)

  				local  targets = FindUnitsInRadius(casterTeam, pos, nil, projectile_radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
  				for _,target in pairs(targets) do
  					print(target)
  					event.target = target
  					hit(event)
  				end		
  			end,
		}
		Projectiles:CreateProjectile(projectile)

		--]]
		---[[随心所欲法

			local projectile = ParticleManager:CreateParticle(particleName, PATTACH_CUSTOMORIGIN, nil)
			local projectileControlPoints = {[0]=casterLoc+Vector(0,0,220),[1]=random_position,[2]=Vector(projectile_speed,0,0),[4]=random_position}
			for k,v in pairs(projectileControlPoints) do
    			ParticleManager:SetParticleControl(projectile, k, v)
  			end
  			local finishTime = (random_position - casterLoc):Length2D()/projectile_speed
  			Timers:CreateTimer(finishTime,function() 
  				ParticleManager:DestroyParticle(projectile,false)
  				--print ('OnFinish',finishTime)
  				StartSoundEventFromPosition("Hero_Gyrocopter.Rocket_Barrage.Impact", random_position)
  				local pFinish = ParticleManager:CreateParticle("particles/units/heroes/hero_tinker/tinker_missle_explosion.vpcf", PATTACH_CUSTOMORIGIN, nil)
  				ParticleManager:SetParticleControl(pFinish, 0, random_position)
  				local  targets = FindUnitsInRadius(casterTeam, random_position, nil, projectile_radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
  				for _,target in pairs(targets) do
  					--print(target)
  					event.target = target
  					hit(event)
  				end		 
  				end)
		--]]

		Count = Count + 1
		if Count >= projectile_count or not caster:IsChanneling() then
			return nil
		else
			return daodanjiange
		end
	end)
end

function hit(event)
	local caster = event.caster
	local ability = event.ability
	local target = event.target
	local shanghai =  ability:GetLevelSpecialValueFor( "shanghai" , ability:GetLevel() - 1  )
	local jianzhu_shanghai =  ability:GetLevelSpecialValueFor( "jianzhu_shanghai" , ability:GetLevel() - 1  ) / 100
	if target:HasModifier("modifier_lianjinshushi_cluster_rockets") then
		shanghai = 0
	else
		ability:ApplyDataDrivenModifier(caster, target, "modifier_lianjinshushi_cluster_rockets", nil)
	end
	if target:IsHero() then
		local damageTable = {
			victim = target,
			attacker = caster,
			damage = shanghai,
			damage_type = DAMAGE_TYPE_MAGICAL,
		}
		ApplyDamage(damageTable)
		ability:ApplyDataDrivenModifier(caster, target, "modifier_lianjinshushi_cluster_rockets_hero", nil)
	elseif target:IsTower() then
		local damageTable = {
			victim = target,
			attacker = caster,
			damage = shanghai * jianzhu_shanghai,
			damage_type = DAMAGE_TYPE_MAGICAL,
		}
		ApplyDamage(damageTable)
	else
		local damageTable = {
			victim = target,
			attacker = caster,
			damage = shanghai,
			damage_type = DAMAGE_TYPE_MAGICAL,
		}
		ApplyDamage(damageTable)
		ability:ApplyDataDrivenModifier(caster, target, "modifier_lianjinshushi_cluster_rockets_creep", nil)
	end
end