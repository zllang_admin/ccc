function chenlunzhiyuan(keys)
	local caster = keys.caster
	local ability = keys.ability
	ability.point = keys.target_points[1]
	local chixu = ability:GetSpecialValueFor("chixu")
	local dummy = CreateUnitByName( "npc_dummy_unit", ability.point, false, caster, caster:GetOwner(), caster:GetTeamNumber() )
	dummy:AddNewModifier(caster, ability, "modifier_kill", {duration = chixu})
	ability:ApplyDataDrivenModifier( caster, dummy, "modifier_lianjinshushi_chenlunzhiyuan_in", {duration = chixu} )
	ability:ApplyDataDrivenModifier( caster, dummy, "modifier_lianjinshushi_chenlunzhiyuan_out", {duration = chixu} )
	local particleName = "particles/ch3c/lianjin/enigma_blackhole.vpcf"
	local particle = ParticleManager:CreateParticle(particleName, PATTACH_CUSTOMORIGIN, dummy)
	ParticleManager:SetParticleControl(particle, 0, ability.point)
	ParticleManager:SetParticleControl(particle, 3, ability.point+Vector(0,0,150))
	ParticleManager:SetParticleControl(particle, 5, ability.point+Vector(0,-200,400))
	StartSoundEvent("Chenlun.Loop",dummy)
	Timers:CreateTimer(chixu,function() 
		ParticleManager:DestroyParticle(particle, false)

		end)

end

function pull(keys)
	local target = keys.target
	local ability = keys.ability
	Timers:CreateTimer(0.01,function() 
		if not target:HasModifier("modifier_lianjinshushi_chenlunzhiyuan_1") then
			return
		end
		local chufa = ability:GetSpecialValueFor("chufa")
		local newLoc = ability.point + RandomVector(RandomFloat(0, chufa))
		local particleName = "particles/units/heroes/hero_morphling/morphling_adaptive_strike.vpcf"
		local particle = ParticleManager:CreateParticle(particleName, PATTACH_CUSTOMORIGIN, target)
		StartSoundEventFromPosition("Hero_Morphling.AdaptiveStrike", newLoc)
		ParticleManager:SetParticleControl(particle, 1, newLoc)
		FindClearSpaceForUnit(target, newLoc, true)
		target:AddNewModifier(caster, ability, "modifier_phased", {duration = 0.03})
	end)
end

function pubu(keys)
	local caster = keys.caster
	local ability = keys.ability
	local chufa = ability:GetSpecialValueFor("chufa")
	local newLoc = ability.point + RandomVector(RandomFloat(0, chufa))
	local particleName = "particles/ch3c/lianjin/kunkka_spell_torrent_2splash_a.vpcf"
	local particle = ParticleManager:CreateParticle(particleName, PATTACH_CUSTOMORIGIN, nil)
	ParticleManager:SetParticleControl(particle, 0, newLoc)
end

function end_sound(keys)
    StopSoundEvent("Chenlun.Loop",keys.target)
end