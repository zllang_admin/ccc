function HealingSprayStart( event )
	local caster = event.caster
	local point = event.target_points[1]

	local healing_spray_dummy = CreateUnitByName("npc_dummy_rocket", point, false, caster, caster, caster:GetTeam())
	caster.healing_spray_dummyIndex = healing_spray_dummy:GetEntityIndex()
	event.ability:ApplyDataDrivenModifier(caster, healing_spray_dummy, "modifier_lianjinshushi_yiliaoqiwu_thinker", nil)
end


function HealingSprayWave( event )
	local caster = event.caster
	local point = event.target:GetAbsOrigin()
	local particleName = "particles/ch3c/lianjin/alchemist_acid_spray_cast.vpcf"
	local particle = ParticleManager:CreateParticle(particleName, PATTACH_CUSTOMORIGIN, caster)
	if caster:IsChanneling() == false then
		return nil
	end
	local ability = event.ability
	local radius = ability:GetSpecialValueFor("banjing")
	local zhiliao = ability:GetLevelSpecialValueFor("zhiliao", ability:GetLevel()-1) 
	local zhiliaojianzhu = ability:GetLevelSpecialValueFor("zhiliaojianzhu", ability:GetLevel()-1) 
	ParticleManager:SetParticleControlEnt(particle, 0, caster, PATTACH_POINT_FOLLOW, "attach_hitloc", caster:GetAbsOrigin(), true)
	ParticleManager:SetParticleControl(particle, 1, point)
	--ParticleManager:SetParticleControl(particle, 15, Vector(255,255,0))
	--ParticleManager:SetParticleControl(particle, 16, Vector(255,255,0))

	local units = FindUnitsInRadius( caster:GetTeam(), point, caster, radius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false )	
	local buildings = FindUnitsInRadius( caster:GetTeam(), point, caster, radius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_BUILDING, DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false )	
	for k,v in pairs(units) do
		if not IsDummyUnit(v) then
			v:Heal(zhiliao,caster)
			local pfxPath = string.format("particles/msg_fx/msg_%s.vpcf",  "heal")
			local pidx = ParticleManager:CreateParticle(pfxPath, PATTACH_ABSORIGIN_FOLLOW, v) -- target:GetOwner()
			local digits = #tostring(math.ceil(zhiliao))
			ParticleManager:SetParticleControl(pidx, 1, Vector(tonumber(0), tonumber(zhiliao), tonumber(nil)))
			ParticleManager:SetParticleControl(pidx, 2, Vector(1.5, digits+1, 0))
			ParticleManager:SetParticleControl(pidx, 3, Vector(0, 255, 0))
		elseif v:GetUnitName() == "npc_building_shangdianshouwei" then
			table.insert(v, buildings)
		end
	end
	for k,v in pairs(buildings) do
		v:Heal(zhiliaojianzhu,caster)
		local pfxPath = string.format("particles/msg_fx/msg_%s.vpcf",  "heal")
		local pidx = ParticleManager:CreateParticle(pfxPath, PATTACH_ABSORIGIN_FOLLOW, v) -- target:GetOwner()
		local digits = #tostring(math.ceil(zhiliaojianzhu))
		ParticleManager:SetParticleControl(pidx, 1, Vector(tonumber(0), tonumber(zhiliaojianzhu), tonumber(nil)))
		ParticleManager:SetParticleControl(pidx, 2, Vector(1.5, digits+1, 0))
		ParticleManager:SetParticleControl(pidx, 3, Vector(0, 255, 0))
	end

end

function HealingSprayEnd( event )
	local caster = event.caster
	if caster.healing_spray_dummyIndex then
		local dummy = EntIndexToHScript(caster.healing_spray_dummyIndex)
		if dummy then
			dummy:RemoveSelf()
		end
		caster.healing_spray_dummyIndex = nil
	end
end