function dianjinshu(keys)
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	local ratio = ability:GetSpecialValueFor("ratio")/100
	target:SetMinimumGoldBounty(target:GetMinimumGoldBounty() * ratio)
	target:SetMaximumGoldBounty(target:GetMaximumGoldBounty() * ratio)
	target:Kill(ability,caster)
	target:EmitSound("DOTA_Item.Hand_Of_Midas")
	local midas_particle = ParticleManager:CreateParticle("particles/items2_fx/hand_of_midas.vpcf", PATTACH_ABSORIGIN_FOLLOW, target)	
	ParticleManager:SetParticleControlEnt(midas_particle, 1, caster, PATTACH_POINT_FOLLOW, "attach_hitloc", caster:GetAbsOrigin(), false)
end