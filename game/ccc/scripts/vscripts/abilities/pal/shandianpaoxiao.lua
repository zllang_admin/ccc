function shandian_spawn( keys )--生成一圈球
	-- Variables
	local caster = keys.caster
	local ability = keys.ability
	local casterLoc = caster:GetAbsOrigin()
	local duration = ability:GetLevelSpecialValueFor( "duration", ability:GetLevel() - 1 )
	local shuliang = ability:GetLevelSpecialValueFor( "shuliang", ability:GetLevel() - 1 )
	local meiquanjuli = ability:GetLevelSpecialValueFor( "meiquanjuli", ability:GetLevel() - 1 )
	local angel_cha = 360/shuliang
	local angel = 90
	local count = 1
	local radiu = 0

	while count <= 3 do
		radiu = radiu +	meiquanjuli
		for i=1,shuliang,1 do
			angel = angel + angel_cha
			local spawn_location = casterLoc + Vector(math.cos(math.rad(angel)),math.sin(math.rad(angel)),0)*radiu
			local dummy = CreateUnitByName( "npc_dota_shandianxinxing", spawn_location, true, caster, caster, caster:GetTeamNumber() )
			--dummy:SetMaxSpeed(0)
			dummy:AddNewModifier(caster, ability, "modifier_kill", {duration = duration})
			--添加特效
			local p = ParticleManager:CreateParticle( "particles/ch3c/pal/shandianxinxing.vpcf", PATTACH_OVERHEAD_FOLLOW, dummy )
        	ParticleManager:SetParticleControl( p, 1, casterLoc )
        	ParticleManager:SetParticleControl( p, 0, spawn_location )
        	Timers:CreateTimer(duration, function()
        		ParticleManager:DestroyParticle(p, false)
        		end)
		end
		count = count + 1
	end
	
	local mainP = ParticleManager:CreateParticle( "particles/ch3c/pal/stormspirit_static_remnant.vpcf", PATTACH_POINT, caster )
	Timers:CreateTimer(duration, function()
        ParticleManager:DestroyParticle(mainP, false)
        end)
end