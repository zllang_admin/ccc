function shenshengzhiguang( keys )
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability

	StartSoundEvent("Hero_Omniknight.Purification", target)

	local particleName1 = "particles/units/heroes/hero_omniknight/omniknight_purification_cast.vpcf"
	local particleName2 = "particles/units/heroes/hero_omniknight/omniknight_purification.vpcf"
	local particle1 = ParticleManager:CreateParticle(particleName1, PATTACH_ABSORIGIN, caster)
   	ParticleManager:SetParticleControl(particle1, 0, target:GetAbsOrigin())
   	ParticleManager:SetParticleControl(particle1, 1, target:GetAbsOrigin())
   	local particle2 = ParticleManager:CreateParticle(particleName2, PATTACH_ABSORIGIN, caster)
   	ParticleManager:SetParticleControl(particle2, 0, target:GetAbsOrigin())
   	ParticleManager:SetParticleControl(particle2, 1, Vector(100,100,100))
end