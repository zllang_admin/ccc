function siwangjianta_start( keys )
	local caster = keys.caster
	local point = keys.target_points[1]
	local ability = keys.ability
	local feixingshijian = ability:GetSpecialValueFor("feixingshijian")
	local banjing = ability:GetSpecialValueFor("banjing")
	local chixu = ability:GetSpecialValueFor("chixu")

	local fDistance = ( caster:GetOrigin() - point ):Length2D() - 50
	local fXiang = ( point - caster:GetOrigin() ):Normalized()

	-- print(caster:GetOrigin(),point,fDistance,fXiang,feixingshijian)
	caster:SetForwardVector( fXiang ) 
	--击退函数：击退者，被击退者，方向，距离，持续时间，落地高度，最高高度，是否破坏树，破坏树的范围
    KnockBack(caster,caster,fXiang,fDistance,feixingshijian,point.z,200,true,150)
    StartAnimation(caster, {duration=feixingshijian, activity=ACT_DOTA_ATTACK_EVENT, rate=0.2})
    
    Timers:CreateTimer(feixingshijian, function()
    	-- caster:AddNoDraw()
    	local enemies = FindUnitsInRadius( caster:GetTeamNumber(),
                                      point,
                                      nil,
                                      banjing,
                                      DOTA_UNIT_TARGET_TEAM_ENEMY ,
                                      DOTA_UNIT_TARGET_HERO+DOTA_UNIT_TARGET_BASIC,
                                      DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
                                      FIND_ANY_ORDER,
                                      false )

    	for k,v in pairs(enemies) do
    		if v:GetUnitName() ~= "npc_building_shangdianshouwei" then
    			ability:ApplyDataDrivenModifier(caster, v, "modifier_shenyuanlingzhu_siwangjianta_2", {duration = chixu-feixingshijian})
    		end
    	end
    end)
end

function siwangjianta_interval( keys )
	-- print("interval")
	local caster = keys.caster
	local ability = keys.ability
	local target = keys.target
	local banjing = ability:GetSpecialValueFor("banjing")

	local newLoc = caster:GetAbsOrigin() + RandomVector(RandomFloat(0, 256))

	target:SetAbsOrigin(newLoc)
	
end

function siwangjianta_end( keys )
	-- print("interval")
	local caster = keys.caster
	-- caster:RemoveNoDraw()
	
end