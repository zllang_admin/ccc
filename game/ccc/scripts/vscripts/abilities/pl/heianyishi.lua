function heianyishi_interval( keys )
	local caster = keys.caster
	
	local ability = keys.ability
	if not ability:IsChanneling() then
		caster:RemoveModifierByName("modifier_shenyuanlingzhu_heianyishi")
	end
end

function heianyishi_animation( keys )
	local caster = keys.caster
	Timers:CreateTimer(1, function()
		if not caster:HasModifier("modifier_shenyuanlingzhu_heianyishi") then
			return
		end
		StartAnimation(caster, {duration=0.9, activity=ACT_DOTA_CAST_ABILITY_2, rate=1})
		return 1
	end)
end