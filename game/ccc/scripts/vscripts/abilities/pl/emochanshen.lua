function emochanshen_death( keys )
	local caster = keys.caster
	local unit = keys.unit
	local point = unit:GetAbsOrigin()
	local ability = keys.ability
	if not unit or unit.chongsheng then
		return
	end

	local chixu = ability:GetLevelSpecialValueFor("chixu",ability:GetLevel()-1)
	local emo = CreateUnitByName("npc_creep_morishizhe",point,true,caster, caster, caster:GetTeamNumber())
	emo:SetControllableByPlayer(caster:GetPlayerID(), true)
    emo:AddNewModifier(caster, keys.ability, "modifier_kill", {duration = chixu})
    FindClearSpaceForUnit(emo, point, true)
end