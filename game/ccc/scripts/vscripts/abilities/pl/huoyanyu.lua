function huoyanyu_Start( event )
	-- Variables
	local caster = event.caster
	local point = event.target_points[1]

	local ability = event.ability

	ability.count = 0
	caster.huoyanyu_dummy = CreateUnitByName("npc_dummy_rocket", point, false, caster, caster, caster:GetTeam())
	event.ability:ApplyDataDrivenModifier(caster, caster.huoyanyu_dummy, "modifier_shenyuanlingzhu_huoyanyu_thinker", nil)
end

function huoyanyu_interval( event )
	local caster = event.caster
	local target = event.target

	local point = target:GetAbsOrigin()
	local ability = event.ability
	local boshu = ability:GetLevelSpecialValueFor("boshu", ability:GetLevel()-1)
	local radius = ability:GetLevelSpecialValueFor("banjing", ability:GetLevel()-1)

	StartSoundEventFromPosition("Hero_AbyssalUnderlord.Firestorm", point)
	local n = 0
	Timers:CreateTimer(0,function()--不同时落下来更自然，在0.16秒内完成一次伤害
		if n>3 then
			return nil
		end
		for i = 1,2 do
			local rp = random_point(point,radius)
			local p = ParticleManager:CreateParticle("particles/units/heroes/heroes_underlord/abyssal_underlord_firestorm_wave.vpcf",PATTACH_ABSORIGIN,caster)
			ParticleManager:SetParticleControl(p,0,rp)
		end
		n = n+1
		return 0.02
	end)

	ability.count = ability.count + 1
	if ability.count >= boshu then
		caster:RemoveModifierByName("modifier_shenyuanlingzhu_huoyanyu_channeling")
		return
	end
end

function huoyanyu_End( event )
	local caster = event.caster
	if caster.huoyanyu_dummy and IsValidEntity(caster.huoyanyu_dummy) and caster.huoyanyu_dummy:IsAlive() then
		caster.huoyanyu_dummy:RemoveSelf()
	end
end