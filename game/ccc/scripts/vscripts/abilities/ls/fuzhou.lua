function fuzhou( keys )
	local caster = keys.caster
	local target = keys.target
	local caster_team = caster:GetTeamNumber()
	local player = caster:GetPlayerOwnerID()
	local ability = keys.ability

	-- Change the ownership of the unit and restore its mana to full
	target:SetTeam(caster_team)
	target:SetOwner(caster)
	target:SetControllableByPlayer(player, true)
	target:GiveMana(target:GetMaxMana())
	target:Interrupt()
end

function fuzhou_pre(keys)
	local caster = keys.caster
	local pID = caster:GetPlayerID()

	if keys.target:GetOwner() then
		if keys.target:GetPlayerOwnerID() == pID then	
			caster:Interrupt()
			SendErrorMessage(pID, "#error_self_unit")
		end
	end
end