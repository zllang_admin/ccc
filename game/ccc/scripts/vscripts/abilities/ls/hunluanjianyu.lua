function jian_spawn( keys )--生成一圈球
	-- Variables
	local caster = keys.caster
	local ability = keys.ability
	local casterLoc = caster:GetAbsOrigin()
	local targetLoc = keys.target_points[1]
	local duration = ability:GetLevelSpecialValueFor( "duration", ability:GetLevel() - 1 )
	local radiu = ability:GetLevelSpecialValueFor( "radiu", ability:GetLevel() - 1 )
	local ballNum = ability:GetLevelSpecialValueFor( "ballNum", ability:GetLevel() - 1 )
	local team = caster:GetTeamNumber()
	
	local dummyModifierName = "modifier_ls_hunluanjianyu_wudi"
	
	-- Find forward vector
	local forwardVec = targetLoc - casterLoc
	forwardVec = forwardVec:Normalized()

	local angel_cha = 360/ballNum
	local angel = RandomInt( 0, angel_cha )

	--ability.SourceLoc = {}
	local spawn_location = targetLoc + Vector(math.cos(math.rad(angel)),math.sin(math.rad(angel)),0)*500+Vector(0,0,1000)

	for i=1,ballNum,1 do	
		--[[三角函数法
			angel = angel + angel_cha
			spawn_location = targetLoc + Vector(math.cos(math.rad(angel)),math.sin(math.rad(angel)),0)*500+Vector(0,0,1000)
		--]]
		---[[旋转法
		spawn_location = RotatePosition(targetLoc, QAngle( 0, angel_cha, 0 ), spawn_location)
		--]]

		--print(spawn_location)
	--[[马甲法
		local dummy = CreateUnitByName( "npc_dota_hunluanjianyu", spawn_location, true, caster, caster:GetOwner(), caster:GetTeamNumber() )
		dummy:AddNewModifier(caster, ability, "modifier_kill", {duration = 4})
		dummy:SetAbsOrigin(spawn_location)
		ability:ApplyDataDrivenModifier( caster, dummy, dummyModifierName, {} )
		--]]
    ---[[随心所欲法
    	--SourceLoc[i] = spawn_location
    	local SourceLoc = spawn_location

    	local target = FindJianyuTarget(team,SourceLoc)
    	SheJianProjectile( keys,target,SourceLoc )
    	local cishu = 1
    	Timers:CreateTimer(1.5,function()
            target = FindJianyuTarget(team,SourceLoc)
        	SheJianProjectile( keys,target,SourceLoc )
        	cishu = cishu + 1
        	if cishu == 3 then
        		return nil
        	else
        		return 1.5
        	end
    	end)
    --]]
	end

	
end

function shejian( keys )--前摇后ls往天上射
	local caster = keys.caster
	local casterLoc = caster:GetAbsOrigin()
	local targetLoc = keys.target_points[1]
	local ability = keys.ability
	local spawn_location = targetLoc + Vector(0,0,1500)

	--特效法
	local projectile_speed = 2000
	local projectile = ParticleManager:CreateParticle("particles/ch3c/ls/hunluanshejian.vpcf", PATTACH_CUSTOMORIGIN, nil)
	local projectileControlPoints = {[0]=casterLoc+Vector(0,0,100),[1]=spawn_location,[2]=Vector(projectile_speed,0,0),[4]=spawn_location}
	for k,v in pairs(projectileControlPoints) do
   		ParticleManager:SetParticleControl(projectile, k, v)
  	end
 	local finishTime = (spawn_location - casterLoc):Length()/projectile_speed
 	--print(finishTime)
  	Timers:CreateTimer(finishTime,function() 
  		ParticleManager:DestroyParticle(projectile,false)
 		--print ('OnFinish',finishTime)
 	end)

  local dummy = CreateUnitByName( "npc_dota_hunluanjianyu", spawn_location, false, caster, caster:GetOwner(), caster:GetTeamNumber() )
  dummy:SetAbsOrigin(spawn_location)
  dummy:AddNewModifier(caster, ability, "modifier_kill", {duration = 4})

	--[[马甲法
	local dummyModifierName = "modifier_ls_hunluanjianyu_yindao"
	local dummy = CreateUnitByName( "npc_dota_hunluanjianyu", spawn_location, false, caster, caster:GetOwner(), caster:GetTeamNumber() )
	dummy:SetAbsOrigin(spawn_location)
	dummy:AddNewModifier(caster, ability, "modifier_kill", {duration = 2})
	ability:ApplyDataDrivenModifier( caster, dummy, dummyModifierName, {} )
	local info = 
	{
		Target = dummy,
		Source = caster,
		Ability = ability,	
		EffectName = "particles/ch3c/ls/hunluanshejian.vpcf",
        iMoveSpeed = 2000,
		vSourceLoc= caster:GetAbsOrigin(),                -- Optional (HOW)
		bDrawsOnMinimap = false,                          -- Optional
        bDodgeable = false,                               -- Optional
        bIsAttack = false,                                -- Optional
        bVisibleToEnemies = true,                         -- Optional
        bReplaceExisting = false,                         -- Optional
        flExpireTime = GameRules:GetGameTime() + 10,      -- Optional but recommended
		bProvidesVision = true,                           -- Optional
		iVisionRadius = 400,                              -- Optional
		iVisionTeamNumber = caster:GetTeamNumber()        -- Optional
	}
	projectile = ProjectileManager:CreateTrackingProjectile(info)--]]
	--]]

	
end

function xiayu(keys)--每个球往下射
	local unit = keys.target
	local teamId= keys.caster:GetTeamNumber()
	local ability = keys.ability
	local max_speed = ability:GetSpecialValueFor( "max_speed" )
	local min_speed = ability:GetSpecialValueFor( "min_speed" )
	local enemies = FindUnitsInRadius( unit:GetTeamNumber(),
                                      unit:GetAbsOrigin(),
                                      nil,
                                      600,
                                      DOTA_UNIT_TARGET_TEAM_ENEMY ,
                                      DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING,
                                      DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_NOT_ATTACK_IMMUNE,
                                      FIND_ANY_ORDER,
                                      false )
	local zuiyouxian = 0
	local target = nil
	for _,v in pairs(enemies) do
        if v and IsValidEntity(v) and v:IsAlive() then
            -- Cast a fake net
            local youxianji = 6
            if v:HasFlyMovementCapability() then
            	youxianji = youxianji+6
            end
            if v:IsLowAttackPriority() then
            	youxianji = youxianji-6
            end
            local AggroTarger = v:GetAggroTarget()
            if AggroTarger ~= nil then
            	if AggroTarger:GetTeamNumber() == team then
            		youxianji =youxianji + 5
            		if AggroTarger:IsHero() then
            			youxianji =youxianji + 5
            		end
          	  	end
          	end
          	youxianji = youxianji + RandomInt( 0, 4 )
            youxianji = youxianji + (v:GetAbsOrigin()-unit:GetAbsOrigin()):Length2D()/100
            if youxianji>zuiyouxian then
            	zuiyouxian=youxianji
            	target = v
            end
        end
    end
    if target == nil  then
    	return
    elseif target:IsAlive() ~= true then
    	return
    end
    --print(zuiyouxian,target:GetName())
    ---[[
    ProjectileManager:CreateTrackingProjectile( {
                Target = target,
                Source = unit,
                Ability = ability,  
                EffectName = "particles/error/null.vpcf",
                vSourceLoc= caster:GetAbsOrigin(), 
                bDodgeable = false,
                bIsAttack = false,
                bProvideVision = false,
                iMoveSpeed = RandomInt( min_speed, max_speed ),
            } )--]]

end

function FindJianyuTarget( team,SourceLoc )
	-- body

	local enemies = FindUnitsInRadius( team,
                                      SourceLoc,
                                      nil,
                                      600,
                                      DOTA_UNIT_TARGET_TEAM_ENEMY ,
                                      DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING,
                                      DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_NOT_ATTACK_IMMUNE,
                                      FIND_ANY_ORDER,
                                      false )
	local zuiyouxian = 0
	local target = nil
	for _,v in pairs(enemies) do
        if v and IsValidEntity(v) and v:IsAlive() then
            -- Cast a fake net
            local youxianji = 6
            if v:HasFlyMovementCapability() then
            	youxianji = youxianji+6
            end
            if v:IsLowAttackPriority() then
            	youxianji = youxianji-6
            end
            local AggroTarger = v:GetAggroTarget()
            if AggroTarger ~= nil then
            	if AggroTarger:GetTeam() == team then
            		youxianji =youxianji + 5
            		if AggroTarger:IsHero() then
            			youxianji =youxianji + 5
            		end
          	  	end
          	end
          	youxianji = youxianji + RandomInt( 0, 4 )
            youxianji = youxianji + (v:GetAbsOrigin()-SourceLoc):Length2D()/100
            if youxianji>zuiyouxian then
            	zuiyouxian=youxianji
            	target = v
            end
        end
  end
  if target then
  	return target
  else
  	return nil
  end

end

function  SheJianProjectile( keys,target,SourceLoc )
	-- body
	if not (target and IsValidEntity(target) and target:IsAlive()) then
		return
	end

	local caster = keys.caster
	local ability = keys.ability
	local max_speed = ability:GetSpecialValueFor( "max_speed" )
	local min_speed = ability:GetSpecialValueFor( "min_speed" )
	ProjectileManager:CreateTrackingProjectile( {
                Target = target,
                Ability = ability,  
                EffectName = "particles/ch3c/ls/hunluanxiayu.vpcf",
                vSourceLoc= SourceLoc, 
                bDodgeable = true,
                bIsAttack = true,
                bProvideVision = false,
                iMoveSpeed = RandomInt( min_speed, max_speed ),
            } )--]]
end