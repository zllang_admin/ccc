modifier_ls_life_drain_lua = class({})


--------------------------------------------------------------------------------

function modifier_ls_life_drain_lua:OnCreated( kv )
	caster = self:GetCaster()
	target = self:GetParent()
	ability = self:GetAbility()
	self.tick_rate = self:GetAbility():GetSpecialValueFor( "tick_rate" )
	local targetTeam = target:GetTeamNumber()
    local casterTeam = caster:GetTeamNumber()
    local jiaxue
    local diaoxue
    EmitSoundOn("Hero_Pugna.LifeDrain.Target", caster)
    if targetTeam == casterTeam then
    	jiaxue = target
    	diaoxue = caster
    else
    	jiaxue = caster
    	diaoxue = target
    end
	if IsServer() then
		local particleName = "particles/units/heroes/hero_pugna/pugna_life_drain.vpcf"
    	caster.LifeDrainParticle = ParticleManager:CreateParticle(particleName, PATTACH_ABSORIGIN_FOLLOW, jiaxue)
    	ParticleManager:SetParticleControlEnt(caster.LifeDrainParticle, 1, diaoxue, PATTACH_POINT_FOLLOW, "attach_hitloc", diaoxue:GetAbsOrigin(), true)
		self:OnIntervalThink()
		self:StartIntervalThink( self.tick_rate )	
	end
end

--------------------------------------------------------------------------------

function modifier_ls_life_drain_lua:OnDestroy()
	if IsServer() then
		caster:StopSound("Hero_Pugna.LifeDrain.Target")
		caster:InterruptChannel()
    	ParticleManager:DestroyParticle(caster.LifeDrainParticle,false)
	end
end

--------------------------------------------------------------------------------

function modifier_ls_life_drain_lua:OnIntervalThink()
	if IsServer() then
		local targetTeam = target:GetTeamNumber()
    	local casterTeam = caster:GetTeamNumber()

		local tick_rate = ability:GetLevelSpecialValueFor( "tick_rate" , ability:GetLevel() - 1 )
		local HP_drain = ability:GetLevelSpecialValueFor( "health_drain" , ability:GetLevel() - 1 )

		-- HP drained depends on the actual damage dealt. This is for MAGICAL damage type
		local HP_gain = HP_drain * ( 1 - target:GetMagicalArmorValue())



		-- If its an illusion then kill it
		if target:IsIllusion() then
			target:ForceKill(true)
			ability:OnChannelFinish(false)
			caster:Stop()
			return
		else
			-- Location variables
			local caster_location = caster:GetAbsOrigin()
			local target_location = target:GetAbsOrigin()

			-- Distance variables
			local distance = (target_location - caster_location):Length2D()
			local break_distance = ability:GetSpecialValueFor( "break_distance" )
			local direction = (target_location - caster_location):Normalized()
			-- If the leash is broken then stop the channel
			if distance >= break_distance  or (not target:IsAlive()) or target:IsInvisible() then
				print("break")
				ability:OnChannelFinish(false)
				caster:Stop()
				return
			end

			-- Make sure that the caster always faces the target
			caster:SetForwardVector(direction)
		end

		if targetTeam == casterTeam then
        	-- Health Transfer Caster->Ally
        	ApplyDamage({ victim = caster, attacker = caster, damage = HP_drain, damage_type = DAMAGE_TYPE_MAGICAL })
        	target:Heal( HP_gain, caster)
        	--TODO: Check if this damage transfer should be lethal
        
        	-- Set the particle control color as green
        	ParticleManager:SetParticleControl(caster.LifeDrainParticle, 10, Vector(0,0,0))
        	ParticleManager:SetParticleControl(caster.LifeDrainParticle, 11, Vector(0,0,0))
		else
			-- Health Transfer Enemy->Caster
			ApplyDamage({ victim = target, attacker = caster, damage = HP_drain, damage_type = DAMAGE_TYPE_MAGICAL })
			caster:Heal( HP_gain, caster)

			-- Set the particle control color as green
			ParticleManager:SetParticleControl(caster.LifeDrainParticle, 10, Vector(0,0,0))
			ParticleManager:SetParticleControl(caster.LifeDrainParticle, 11, Vector(0,0,0))
		end
	end
end