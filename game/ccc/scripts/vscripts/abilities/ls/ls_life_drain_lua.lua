ls_life_drain_lua = class({})
LinkLuaModifier( "modifier_ls_life_drain_lua", "abilities/ls/modifier_ls_life_drain_lua",LUA_MODIFIER_MOTION_NONE )


function ls_life_drain_lua:OnAbilityPhaseStart()
	if IsServer() then
		self.hVictim = self:GetCursorTarget()
		local hTarget = self.hVictim
	end

	return true
end

function ls_life_drain_lua:CastFilterResultTarget( hTarget )
	if self:GetCaster() == hTarget then
		return UF_FAIL_CUSTOM
	end

	if hTarget:IsAncient() then
		return UF_FAIL_CUSTOM
	end

	if hTarget:IsMagicImmune() then
		return UF_FAIL_CUSTOM
	end

	if hTarget:IsTower() or hTarget:GetClassname()=="npc_dota_fort" then
		return UF_FAIL_CUSTOM
	end
	return UF_SUCCESS
end

function ls_life_drain_lua:GetCustomCastErrorTarget( hTarget )
	if self:GetCaster() == hTarget then
		return "#dota_hud_error_cant_cast_on_self"
	end

	if hTarget:IsAncient() then
		return "#dota_hud_error_cant_cast_on_ancient"
	end

	if hTarget:IsMagicImmune() then
		return "#dota_hud_error_target_magic_immune"
	end

	if hTarget:IsTower() or hTarget:GetClassname()=="npc_dota_fort" then
		return "#dota_hud_error_cant_cast_on_building"
	end

	return ""
end

function ls_life_drain_lua:OnSpellStart()
	--print("start")
	if self.hVictim == nil then
		return
	end

	if self.hVictim:TriggerSpellAbsorb( self ) then
		self.hVictim = nil
		self:GetCaster():Interrupt()
	else
		self.hVictim:AddNewModifier( self:GetCaster(), self, "modifier_ls_life_drain_lua", { duration = self:GetChannelTime() } )
		--print(self:GetChannelTime())
	end
end

function ls_life_drain_lua:GetChannelTime()
	self.creep_duration = self:GetSpecialValueFor( "creep_duration" )
	self.hero_duration = self:GetSpecialValueFor( "hero_duration" )

	if IsServer() then
		if self.hVictim ~= nil then
			if self.hVictim:IsConsideredHero() then
				--print("hero")
				return self.hero_duration
			else
				--print("creep")
				return self.creep_duration
			end
		end

	return 0.0
	end

	return self.hero_duration
end

function ls_life_drain_lua:OnChannelFinish( bInterrupted )
	if self.hVictim and IsValidEntity(self.hVictim) and self.hVictim:IsAlive() then
		print("Finish")
		self.hVictim:RemoveModifierByName( "modifier_ls_life_drain_lua" )
	end
end