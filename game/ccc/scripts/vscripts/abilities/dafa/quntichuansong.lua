function chuansong( keys )
	local caster = keys.caster
	local point_ori = keys.target_points[1]
	local ori_c   = caster:GetOrigin() 
	local radius = keys.abilityradius
	local point_youjun = ori_c
    local target = keys.target
    local c_owner = caster:GetPlayerOwner() or caster:GetOwner() 
    local c_team = caster:GetTeam()

    if target == caster then
        if c_team == DOTA_TEAM_GOODGUYS then
            point_ori = Entities:FindByName(nil, "ent_dota_fountain_good"):GetAbsOrigin()
        elseif c_team == DOTA_TEAM_BADGUYS then
            point_ori = Entities:FindByName(nil, "ent_dota_fountain_bad"):GetAbsOrigin()
        end
    end

	local ability = keys.ability
    local yanchi = ability:GetSpecialValueFor("yanchi") 
    --local yanchi = ability:GetLevelSpecialValueFor("yanchi", (ability:GetLevel() - 1))
  
    local t_target =  DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC 
                   
    local t_team   = DOTA_UNIT_TARGET_TEAM_FRIENDLY
                   --DOTA_UNIT_TARGET_TEAM_ENEMY 
                   --DOTA_UNIT_TARGET_TEAM_BOTH                    
    
    ---[[全扫法
    local youjun = FindUnitsInRadius( c_team, point_ori, caster, 20000, t_team, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_CLOSEST, false )	
	
    if youjun[1] then
        local i = 1
        --不能以飞行单位和守卫作为传送点
        while youjun[i]:HasFlyMovementCapability() or youjun[i]:GetClassname() == "npc_dota_ward_base" or youjun[i]:GetClassname() == "npc_dota_ward_base_truesight" or youjun[i]:GetUnitName()=="npc_dota_shadow_shaman_ward_1" do
            i = i + 1
        end
        point_youjun=youjun[i]:GetAbsOrigin()
        if youjun[i]:IsBuilding() and ( point_ori - point_youjun ):Length2D() <=300 then 
            point_youjun=point_ori
        end
    end
    --]]
    --[[递进法
    for dist=100,1000000,100 do
    	local youjun = FindUnitsInRadius( c_team, point_ori, caster, dist, t_team, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false )	
		for n=1,#youjun do
			if youjun[n]:IsAncient() or youjun[n]:IsOther()==true then
				youjun[n]=nil
				--print("delete")
			end
		end
		--print(dist,youjun[1])
		if youjun[1] ~=nil then
			local shortest=math.huge
			for _,t in pairs(youjun) do
				local x = t: GetAbsOrigin().x
             	local y = t: GetAbsOrigin().y
             	local len = (point_ori.x-x)*(point_ori.x-x) + (point_ori.y-y)*(point_ori.y-y)
				if   len < shortest  then
                   	shortest = len
                   	point_youjun=t:GetOrigin()
             	end
             	if t:IsBuilding() == true and dist<=100 then
             		point_youjun=point_ori
             	end
			end
			break
		end
    end
    --]]
    
    local ti  = FindUnitsInRadius( c_team, ori_c, caster, radius, t_team, t_target, DOTA_UNIT_TARGET_FLAG_PLAYER_CONTROLLED + DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false )
    --FindUnitsInRadius(int a, Vector b, handle c, float d, int e, int f, int g, int h, bool i) 
    --Finds the units in a given radius with the given flags. 
    --( iTeamNumber, vPosition, hCacheUnit, flRadius, iTeamFilter, iTypeFilter, iFlagFilter, iOrder, bCanGrowCache )
    for _,t in pairs(ti) do          
        local t_owner = t:GetPlayerOwner() or t:GetOwner()   
        if t:HasMovementCapability() and t_owner == c_owner and not t:HasModifier("modifier_queenofpain_qunmoluanwu") and not IsDummyUnit(t) then 
            local ori_r = t:GetOrigin()

            --Timers:CreateTimer(yanchi, function()
            		
            --local ori = Vector(point_youjun.x + ori_r.x - ori_c.x, point_youjun.y + ori_r.y - ori_c.y, point_youjun.z )
            local cha = Vector(ori_r.x-ori_c.x,ori_r.y-ori_c.y,0):Normalized()*200
		    local ori = point_youjun + cha

            --print(t,"2")
            --被吹风状态时被传送不会运行吹风的下降功能
            t.uped = nil

            t:Purge(true,true,false,true,true)
            t:RemoveModifierByName("modifier_ensnare_datadriven")

            t:Interrupt()

            FindClearSpaceForUnit(t, ori, true)
            ProjectileManager:ProjectileDodge(t)

            --drain particle
            local pa = tye or PATTACH_ABSORIGIN_FOLLOW
                    --PATTACH_WORLDORIGIN
                    --PATTACH_OVERHEAD_FOLLOW
            Timers:CreateTimer(yanchi, function()
                local p2 = ParticleManager:CreateParticle( "particles/ch3c/chuansong/lion_spell_mana_drain_demon.vpcf", pa, caster )
                --ParticleManager:SetParticleControlEnt( p2, 0, caster, pa, "attach_hitloc", ori_c, true)
                ParticleManager:SetParticleControl( p2, 0, ori_c )
                ParticleManager:SetParticleControlEnt( p2, 1, t, pa, "attach_hitloc", ori, true)
            
                --ParticleManager:SetParticleControl( p2, 1, ori + Vector(0,0,100) )
                Timers:CreateTimer(0.3, function()
                    ParticleManager:DestroyParticle(p2, false)
                    ParticleManager:ReleaseParticleIndex(p2)
                end)
            end)        
        end
    end
    local p3 = ParticleManager:CreateParticle( "particles/abilities/dafa/quntichuansong/teleport_end_2.vpcf", PATTACH_ABSORIGIN_FOLLOW, caster )
    ParticleManager:SetParticleControl( p3, 1, point_youjun )
    ParticleManager:DestroyParticle(p3, false)
    
    local p4 = ParticleManager:CreateParticle( "particles/econ/events/league_teleport_2014/teleport_start_l_league.vpcf", PATTACH_CUSTOMORIGIN, caster )
    ParticleManager:SetParticleControl( p4, 0, ori_c )

end


function  pull( keys )
    local caster = keys.caster
    local ori_c   = caster:GetOrigin() 
    local radius = keys.abilityradius
    --local yanchi = ability:GetLevelSpecialValueFor("yanchi", (ability:GetLevel() - 1)) 
    local c_team   = caster:GetTeam()
    local c_owner = caster:GetPlayerOwner() or caster:GetOwner() 
    local ability = keys.ability
    local ti  = FindUnitsInRadius( c_team, ori_c, caster, radius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC , DOTA_UNIT_TARGET_FLAG_PLAYER_CONTROLLED + DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false )
    --FindUnitsInRadius(int a, Vector b, handle c, float d, int e, int f, int g, int h, bool i) 
    --Finds the units in a given radius with the given flags. 
    --( iTeamNumber, vPosition, hCacheUnit, flRadius, iTeamFilter, iTypeFilter, iFlagFilter, iOrder, bCanGrowCache )
    -- print("keys.yanchi",keys.yanchi)
    for _,t in pairs(ti) do 
        local t_owner = t:GetPlayerOwner() or t:GetOwner()   
        if t:HasMovementCapability() and t_owner == c_owner and not t:HasModifier("modifier_queenofpain_qunmoluanwu") and not IsDummyUnit(t) then          
            local ori_r = t:GetOrigin()
            local p = ParticleManager:CreateParticle( "particles/ch3c/chuansong/lion_spell_mana_drain_demon.vpcf", PATTACH_CUSTOMORIGIN_FOLLOW, t )
            ParticleManager:SetParticleControl( p, 0, ori_r )
            ParticleManager:SetParticleControl( p, 1, ori_c )
            Timers:CreateTimer(keys.yanchi - 0.01, function()
                ParticleManager:DestroyParticle(p, false)
                ParticleManager:ReleaseParticleIndex(p)
            end)
        end
    end
end