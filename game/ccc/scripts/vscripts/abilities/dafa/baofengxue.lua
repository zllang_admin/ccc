function baofengxue_damage( keys )
	local caster = keys.caster
	local point = keys.target_points[1]
	local ability = keys.ability
	local radius = keys.radius
	local damage = keys.damage
	local interval = keys.interval
	Timers:CreateTimer(0,function()
		if not caster:HasModifier("modifier_baofengxue_channelling") and not caster:HasModifier("modifier_duyezhizhu_baofengxue_channel") then
			return nil
		end
		baofengxue_damage_interval( caster,ability,point,radius,damage )
		return interval
	end)
end

function baofengxue_damage_interval( caster,ability,point,radius,damage )
	Timers:CreateTimer(0.5,function()
		lazy_damage_aoe(caster,ability,point,radius,damage)
		end)
	StartSoundEventFromPosition("Hero_CrystalMaiden.Pick", point)
	local n = 0
	Timers:CreateTimer(0,function()--不同时落下来更自然，在0.16秒内完成一次伤害
		if n>5 then
			return nil
		end
		for i = 1,3 do
			local rp = random_point(point,radius)
			local p = ParticleManager:CreateParticle("particles/abilities/dafa/baofengxue/bfx_attack.vpcf",PATTACH_ABSORIGIN,caster)
			ParticleManager:SetParticleControl(p,0,rp)
		end
		n = n+1
		return 0.02
	end)
	--for i=1,15 do--范围内随机15个落冰 感觉随机会让有时候的落点很奇怪
		--local rp = random_point(point,radius)
		--local p = ParticleManager:CreateParticle("particles/econ/items/crystal_maiden/crystal_maiden_maiden_of_icewrack/maiden_freezing_field_explosion_arcana1.vpcf",PATTACH_ABSORIGIN,caster)
		--ParticleManager:SetParticleControl(p,0,rp)
	--end
end