function zisha_start( keys )
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	caster:Kill(ability, caster)
end

function zisha_attack( keys )
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	if caster:GetTeam() == target:GetTeam() then
		return 
	end
	if ability:GetAutoCastState() then
		caster:Kill(ability, caster)
	end
end

function zisha_boom( keys )
	local caster = keys.caster
	local ability = keys.ability
	local ability_level = ability:GetLevel()
	local quan_shanghai = ability:GetLevelSpecialValueFor("quan_shanghai", ability_level-1) 
	local quan_banjing = ability:GetLevelSpecialValueFor("quan_banjing", ability_level-1)
	local xiao_shanghai = ability:GetLevelSpecialValueFor("xiao_shanghai", ability_level-1)
	local xiao_banjing = ability:GetLevelSpecialValueFor("xiao_banjing", ability_level-1)
	local jianzhu = ability:GetLevelSpecialValueFor("jianzhu", ability_level-1)
	local xishu = 1
	local enemies = FindUnitsInRadius( caster:GetTeamNumber(), caster:GetAbsOrigin(), caster, quan_banjing, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, 0, false )
	if #enemies > 0 then
		for _,enemy in pairs(enemies) do
			if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
				if enemy:IsBuilding() then
					xishu = jianzhu / 100
				end
				local damage = {
					victim = enemy,
					attacker = caster,
					damage = (quan_shanghai - xiao_shanghai) * xishu,
					damage_type = DAMAGE_TYPE_PHYSICAL,
					ability = ability
				}

				ApplyDamage( damage )
			end
		end
	end
	local big_enemies = FindUnitsInRadius( caster:GetTeamNumber(), caster:GetAbsOrigin(), caster, xiao_banjing, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, 0, false )
	if #big_enemies > 0 then
		for _,big_enemy in pairs(big_enemies) do
			if big_enemy ~= nil and ( not big_enemy:IsInvulnerable() ) then
				if big_enemy:IsBuilding() then
					xishu = jianzhu / 100
				end
				local damage = {
					victim = big_enemy,
					attacker = caster,
					damage = ( xiao_shanghai ) * xishu,
					damage_type = DAMAGE_TYPE_PHYSICAL,
					ability = ability
				}

				ApplyDamage( damage )
			end
		end
	end

	StartSoundEvent("Hero_Techies.Pick",caster)

	local particle = "particles/units/heroes/hero_techies/techies_suicide_base.vpcf"
	local pfx = ParticleManager:CreateParticle( particle, PATTACH_ABSORIGIN, caster )
end

function jishengzhadan( keys )
	local caster = keys.caster
	local target = keys.unit
	local ability = keys.ability
	local ability_level = ability:GetLevel()
	local quan_shanghai = ability:GetLevelSpecialValueFor("quan_shanghai", ability_level-1) 
	local quan_banjing = ability:GetLevelSpecialValueFor("quan_banjing", ability_level-1)
	local xiao_shanghai = ability:GetLevelSpecialValueFor("xiao_shanghai", ability_level-1)
	local xiao_banjing = ability:GetLevelSpecialValueFor("xiao_banjing", ability_level-1)


	local enemies = FindUnitsInRadius( target:GetTeamNumber(), target:GetAbsOrigin(), target, quan_banjing, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, 0, false )

	if #enemies > 0 then
		for _,enemy in pairs(enemies) do
			if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
				local damage = {
					victim = enemy,
					attacker = caster,
					damage = (quan_shanghai - xiao_shanghai),
					damage_type = DAMAGE_TYPE_PHYSICAL,
					ability = ability
				}

				ApplyDamage( damage )
			end
		end
	end
	local big_enemies = FindUnitsInRadius( target:GetTeamNumber(), target:GetAbsOrigin(), target, xiao_banjing, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, 0, false )
	if #big_enemies > 0 then
		for _,big_enemy in pairs(big_enemies) do
			if big_enemy ~= nil and ( not big_enemy:IsInvulnerable() ) then
				local damage = {
					victim = big_enemy,
					attacker = caster,
					damage = ( xiao_shanghai ),
					damage_type = DAMAGE_TYPE_PHYSICAL,
					ability = ability
				}

				ApplyDamage( damage )
			end
		end
	end

	StartSoundEvent("Hero_Techies.Pick",target)

	local particle = "particles/units/heroes/hero_techies/techies_suicide_base.vpcf"
	local pfx = ParticleManager:CreateParticle( particle, PATTACH_ABSORIGIN, target )
end