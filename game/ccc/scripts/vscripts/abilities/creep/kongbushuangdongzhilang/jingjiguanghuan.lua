function jingjiguanghuan(keys)
	local caster = keys.caster
	local attacker = keys.attacker
	local fantan = keys.ability:GetSpecialValueFor("fantan")
	local damage = keys.Damage
	if attacker:IsRangedAttacker() then
		return
	end
	local damageTable = {
		victim = attacker,
		attacker = caster,
		damage = damage*fantan/100,
		damage_type = DAMAGE_TYPE_PHYSICAL,
	}
 
	ApplyDamage(damageTable)
end