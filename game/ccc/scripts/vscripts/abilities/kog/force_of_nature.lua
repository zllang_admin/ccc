function ForceOfNature( event )
	local caster = event.caster
	local player = event.caster:GetPlayerID()
	local ability = event.ability
	local radius = ability:GetLevelSpecialValueFor( "radius", ability:GetLevel() - 1 )
	local treant_count = ability:GetLevelSpecialValueFor( "treant_count", ability:GetLevel() - 1 )
	local duration = ability:GetLevelSpecialValueFor( "duration", ability:GetLevel() - 1 )
	local unit_name = "npc_kog_treant"
	local casterLoc = caster:GetAbsOrigin()
	local forward = caster:GetForwardVector()
	local targetLoc = casterLoc + forward * 250
	local attack_damage_min = ability:GetLevelSpecialValueFor("damage_min", ability:GetLevel() - 1 )
	local attack_damage_max = ability:GetLevelSpecialValueFor("damage_max", ability:GetLevel() - 1 )
	local MaxHealth = ability:GetLevelSpecialValueFor("MaxHealth", ability:GetLevel() - 1 )
	local speed = ability:GetLevelSpecialValueFor("speed", ability:GetLevel() - 1 )
	local hujia = ability:GetLevelSpecialValueFor("hujia", ability:GetLevel() - 1 )
	local gold_min = ability:GetLevelSpecialValueFor("gold_min", ability:GetLevel() - 1 )
	local gold_max = ability:GetLevelSpecialValueFor("gold_max", ability:GetLevel() - 1 )
	local abilitylevel = ability:GetLevel()

	-- Play the particle
	local particleName = "particles/units/heroes/hero_furion/furion_force_of_nature_cast.vpcf"
	local particle1 = ParticleManager:CreateParticle( particleName, PATTACH_CUSTOMORIGIN, caster )
	ParticleManager:SetParticleControl( particle1, 0, targetLoc )
	ParticleManager:SetParticleControl( particle1, 1, targetLoc )
	ParticleManager:SetParticleControl( particle1, 2, Vector(radius,0,0) )

	-- Create the units on the next frame
	Timers:CreateTimer(0.03,
		function() 
			-- Spawn as many treants as possible
			for i=1,treant_count do
				local treant = CreateUnitByName(unit_name, targetLoc, true, caster, caster, caster:GetTeamNumber())
				treant:SetControllableByPlayer(player, true)
				treant:AddNewModifier(caster, ability, "modifier_kill", {duration = duration})
				treant:AddNewModifier(caster, ability, "modifier_phased", {duration = 0.03})
				ability:ApplyDataDrivenModifier(caster, treant, "modifier_summoned", nil)
				treant:SetBaseDamageMax(attack_damage_max)
				treant:SetBaseDamageMin(attack_damage_min)
				treant:SetBaseMaxHealth(MaxHealth)
				treant:SetBaseMoveSpeed(speed)
				treant:SetMaximumGoldBounty(gold_max)
				treant:SetMinimumGoldBounty(gold_min)
				treant:SetPhysicalArmorBaseValue(hujia)
				treant:SetForwardVector(forward)
				if abilitylevel == 9 or abilitylevel == 10 then
					local abilityEx = treant:AddAbility("zhenshishiyu")
    				abilityEx:SetLevel(1)
    				abilityEx = treant:AddAbility("shengwu_momian")
    				abilityEx:SetLevel(1)
    			end
			end
		end
	)

end