function leitinglieyan( keys )
	local caster = keys.caster

	local startPos = caster:GetAbsOrigin()

	-- Create particle effect
	local particleName = "particles/units/heroes/hero_phoenix/phoenix_supernova_reborn.vpcf"
	local pfx = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN, caster )
	ParticleManager:SetParticleControl( pfx, 0, startPos )
	--ParticleManager:SetParticleControl( pfx, 1, startPos )
	--ParticleManager:SetParticleControl( pfx, 2, Vector( 1, 0, 0 ) )
	--ParticleManager:SetParticleControl( pfx, 3, startPos )

	if caster:IsHero() then
		local ability = keys.ability
		local abilitylevel = ability:GetLevel()
		local casterForward = caster:GetForwardVector()
		local julijiange = ability:GetLevelSpecialValueFor("julijiange", abilitylevel-1)
		local spawnLoc = startPos
		local shijianjiange = ability:GetLevelSpecialValueFor("shijianjiange", abilitylevel-1)
		local shuliang = ability:GetLevelSpecialValueFor("shuliang", abilitylevel-1)
		Timers:CreateTimer(shijianjiange,function()
			spawnLoc = casterForward * julijiange + spawnLoc

			---[[随心所欲法
			leitingyiji(ability, spawnLoc, caster, abilitylevel)
			--]]

			--[[马甲法
			local dummy = CreateUnitByName( "npc_mk_leitinglieyan", spawnLoc, false, caster, caster, caster:GetTeamNumber() )
			dummy:SetControllableByPlayer(caster:GetPlayerID(), true)
			local abilityEx = dummy:FindAbilityByName("shanqiuzhiwang_leitingyiji")
    		if abilityEx then
    			abilityEx:SetLevel(1)
    			dummy:CastAbilityImmediately(abilityEx,caster:GetPlayerOwnerID())
    		end
    		dummy:AddNewModifier(caster, ability, "modifier_kill", {duration = 1})
			--]]
    		if shuliang > 1 then
    			shuliang = shuliang - 1
    			return shijianjiange
    		end
    		return nil
		end)
	end
end

function leitingyiji(ability, spawnLoc, caster, abilitylevel)

	---[[
	local particleName = "particles/units/heroes/hero_jakiro/jakiro_macropyre.vpcf"
	local pfx = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN, caster )
	ParticleManager:SetParticleControl( pfx, 0, spawnLoc )
	ParticleManager:SetParticleControl( pfx, 1, spawnLoc )
	ParticleManager:SetParticleControl( pfx, 2, Vector( 1, 0, 0 ) )
	ParticleManager:SetParticleControl( pfx, 3, spawnLoc )
	--]]

	local particle2 = "particles/units/heroes/hero_ursa/ursa_earthshock.vpcf"
	local pfx = ParticleManager:CreateParticle( particle2, PATTACH_ABSORIGIN, caster )
	ParticleManager:SetParticleControl( pfx, 0, spawnLoc )

	StartSoundEventFromPosition("Hero_Jakiro.LiquidFire",spawnLoc)

	local kuosan_shanghai = ability:GetLevelSpecialValueFor("kuosan_shanghai", abilitylevel-1)
	local kuosan_banjing = ability:GetLevelSpecialValueFor("kuosan_banjing", abilitylevel-1)
	local kuosan_hero = ability:GetLevelSpecialValueFor("kuosan_hero", abilitylevel-1)
	local kuosan_creep = ability:GetLevelSpecialValueFor("kuosan_creep", abilitylevel-1)

	local enemies = FindUnitsInRadius( caster:GetTeamNumber(), spawnLoc, caster, kuosan_banjing, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING, 0, 0, false )
	if #enemies > 0 then
		for _,enemy in pairs(enemies) do
			if enemy ~= nil and ( not enemy:IsMagicImmune() ) and ( not enemy:IsInvulnerable() ) then

				local damage = {
					victim = enemy,
					attacker = caster,
					damage = kuosan_shanghai,
					damage_type = DAMAGE_TYPE_MAGICAL,
					ability = ability
				}

				ApplyDamage( damage )
				if enemy:IsHero() then
					true_duration = kuosan_hero
				else
					true_duration = kuosan_creep
				end
				ability:ApplyDataDrivenModifier( caster, enemy, "modifier_shanqiuzhiwang_leitinglieyan_yiji", { duration = true_duration} )
			end
		end
	end
end

function baozhashenqu( keys )
	local caster = keys.caster
	local startPos = caster:GetAbsOrigin()
	local ability = keys.ability
	local abilitylevel = ability:GetLevel()

	local particleName = "particles/units/heroes/hero_techies/techies_suicide.vpcf"
	local pfx = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN, caster )
	ParticleManager:SetParticleControl( pfx, 0, startPos )

	StopSoundEvent("Hero_Batrider.Firefly.loop",caster)

	local kuosan_zhongxinjuli = ability:GetLevelSpecialValueFor("kuosan_zhongxinjuli", abilitylevel-1)
	local kuosan_shuliang = ability:GetLevelSpecialValueFor("kuosan_shuliang", abilitylevel-1)
	local angel_cha = 360/kuosan_shuliang
	local angel = RandomInt( 0, angel_cha )
	for i = 1, kuosan_shuliang, 1 do
		angel = angel + angel_cha
		local spawnLoc = startPos + Vector(math.cos(math.rad(angel)),math.sin(math.rad(angel)),0)*kuosan_zhongxinjuli

		leitingyiji(ability, spawnLoc, caster, abilitylevel)
	end
end

function baozhashenqu_stop( keys )
	local caster = keys.caster
	StopSoundEvent("Hero_Batrider.Firefly.loop",keys.caster)
end