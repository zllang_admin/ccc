function lieyanfengbao_start( keys )
	local caster = keys.caster
	local point = keys.target_points[1]
	local ability = keys.ability

	local quan_chixu = ability:GetLevelSpecialValueFor("quan_chixu", ability:GetLevel() - 1)
	local xiao_chixu = ability:GetLevelSpecialValueFor("xiao_chixu", ability:GetLevel() - 1)
	local duration = quan_chixu+xiao_chixu 

	local pathLength = ability:GetLevelSpecialValueFor("changdu", ability:GetLevel() - 1)
	local pathRadius = ability:GetLevelSpecialValueFor("banjing", ability:GetLevel() - 1)
	local half_pathLength = pathLength / 2
	local startPos = point + Vector(0,half_pathLength,0)
	local endPos = point - Vector(0,half_pathLength,0)

	ability.macropyre_startPos	= startPos
	ability.macropyre_endPos	= endPos
	ability.macropyre_expireTime = GameRules:GetGameTime() + duration

	-- Create particle effect
	local particleName = "particles/units/heroes/hero_jakiro/jakiro_macropyre.vpcf"
	local pfx = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN, caster )
	ParticleManager:SetParticleControl( pfx, 0, startPos )
	ParticleManager:SetParticleControl( pfx, 1, endPos )
	ParticleManager:SetParticleControl( pfx, 2, Vector( duration, 0, 0 ) )
	ParticleManager:SetParticleControl( pfx, 3, startPos )

	pathRadius = math.max( pathRadius, 64 )
	local numProjectiles = 5
	local stepLength = pathLength / ( numProjectiles - 1 )
	local dummyModifierName = "modifier_macropyre_destroy_tree_datadriven"

	print(numProjectiles,stepLength)
	for i=1, numProjectiles do
		local projectilePos = startPos + (endPos-startPos):Normalized() * (i-1) * stepLength
		print(projectilePos)
		-- Create dummy to destroy trees
		if i~=1 and GridNav:IsNearbyTree( projectilePos, pathRadius, true ) then
			local dummy = CreateUnitByName( "npc_dummy_rocket", projectilePos, false, caster, caster, caster:GetTeamNumber() )
			
			ability:ApplyDataDrivenModifier( caster, dummy, dummyModifierName, {} )
			print("dummy",dummy:HasModifier(dummyModifierName))
		end
	end


	local damage_type = ability:GetAbilityDamageType()--在kv没写好的返回的是空值，会出错，下面一样
    local target_type = ability:GetAbilityTargetType()
    local target_team = ability:GetAbilityTargetTeam()
    local target_flag = ability:GetAbilityTargetFlags()
    local tick = ability:GetLevelSpecialValueFor("tick", ability:GetLevel() - 1)
    local pass_time = 0
    local jianruo = false
    local quan_shanghai = ability:GetLevelSpecialValueFor("quan_shanghai", ability:GetLevel() - 1)
    local xiao_shanghai = ability:GetLevelSpecialValueFor("xiao_shanghai", ability:GetLevel() - 1)
    local jianzhu = ability:GetLevelSpecialValueFor("jianzhu", ability:GetLevel() - 1)

    Timers:CreateTimer(tick,function()

		local enemies = FindUnitsInLine(caster:GetTeamNumber(),
			startPos , 
			endPos , 
			dummy, 
			pathRadius, 
			target_team, 
			target_type, 
			target_flag)
		local xishu = 1
		for k,enemy in pairs(enemies) do
			if enemy:IsBuilding() then
				xishu = jianzhu
			end
			local shanghai = quan_shanghai * tick
			if jianruo then
				shanghai = xiao_shanghai * tick
			end
			local damage = {
				victim = enemy,
				attacker = caster,
				damage = shanghai * xishu,
				damage_type = damage_type,
				ability = ability
			}
			ApplyDamage( damage )

		end
		pass_time = pass_time + tick
		if pass_time > duration then
			return
		elseif pass_time > quan_chixu then
			jianruo = true
			return tick
		else
			return tick
		end
	end)
end