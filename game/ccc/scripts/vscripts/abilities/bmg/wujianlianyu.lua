function WujianlianyuStart( event )
	-- Variables
	local caster = event.caster
	local point = event.target_points[1]

	caster.wujianlianyu_dummy = CreateUnitByName("npc_dummy_rocket", point, false, caster, caster, caster:GetTeam())
	event.ability:ApplyDataDrivenModifier(caster, caster.wujianlianyu_dummy, "modifier_wujianlianyu_thinker", nil)
end

function WujianlianyuEnd( event )
	local caster = event.caster

	caster.wujianlianyu_dummy:RemoveSelf()
end