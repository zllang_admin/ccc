--[[
	Author: Noya
	Date: 20.01.2015.
	Gets the summoning location for the new units
]]
function GetSummonPoints( event )
    local caster = event.caster
    local fv = caster:GetForwardVector()
    local origin = caster:GetAbsOrigin()
    local distance = event.distance

    --[[ Gets 2 points facing a distance away from the caster origin and separated from each other at 30 degrees left and right
  	ang_right = QAngle(0, -30, 0)
    ang_left = QAngle(0, 30, 0)
	
	point_left = RotatePosition(origin, ang_left, front_position)
    point_right = RotatePosition(origin, ang_right, front_position)
    --]]

    local front_position = origin + fv * distance

    local result = { }
    table.insert(result, front_position)
    --table.insert(result, point_left)

    return result
end

-- Set the units looking at the same point of the caster
function SetUnitsMoveForward( event )
	local caster = event.caster
	local target = event.target
    local fv = caster:GetForwardVector()
    local origin = caster:GetAbsOrigin()
	local player = event.caster:GetPlayerID()
	local ability = event.ability
	local attack_damage_min = ability:GetLevelSpecialValueFor("damage_min", ability:GetLevel() - 1 )
	local attack_damage_max = ability:GetLevelSpecialValueFor("damage_max", ability:GetLevel() - 1 )
	local MaxHealth = ability:GetLevelSpecialValueFor("MaxHealth", ability:GetLevel() - 1 )
	local speed = ability:GetLevelSpecialValueFor("speed", ability:GetLevel() - 1 )
	local hujia = ability:GetLevelSpecialValueFor("hujia", ability:GetLevel() - 1 )
	local gold_min = ability:GetLevelSpecialValueFor("gold_min", ability:GetLevel() - 1 )
	local gold_max = ability:GetLevelSpecialValueFor("gold_max", ability:GetLevel() - 1 )
	local scale = ability:GetLevelSpecialValueFor("scale", ability:GetLevel() - 1 )
	local xp = ability:GetLevelSpecialValueFor("xp", ability:GetLevel() - 1 )
	local abilitylevel = ability:GetLevel()
	
	target:SetForwardVector(fv)
	if not ability.wolvesIndex then
		ability.wolvesIndex  = {}
	end
	-- Add the target to a table on the caster handle, to find them later
	table.insert(ability.wolvesIndex, target:GetEntityIndex())
	target:SetBaseDamageMax(attack_damage_max)
	target:SetBaseDamageMin(attack_damage_min)
	target:SetBaseMaxHealth(MaxHealth)
	target:SetBaseMoveSpeed(speed)
	target:SetMaximumGoldBounty(gold_max)
	target:SetMinimumGoldBounty(gold_min)
	target:SetPhysicalArmorBaseValue(hujia)
	target:SetModelScale(scale)
	target:SetDeathXP(xp)

	if abilitylevel > 1 then
		local abilityEx = target:AddAbility("alpha_wolf_critical_strike")
    	abilityEx:SetLevel(1)
    end

    if abilitylevel > 2 then
		local abilityEx = target:AddAbility("lycan_summon_wolves_invisibility")
    	abilityEx:SetLevel(1)
    end
    	
	-- Leave no corpse
	target.no_corpse = true
end

--[[
	Kill wolves on resummon
	Author: Noya
	Date: 20.01.2015.
]]
function KillWolves( event )
	local caster = event.caster
	local ability = event.ability
	local targets = ability.wolvesIndex or {}
	-- print("KillWolves",#ability.wolvesIndex)
	for _,unitIndex in pairs(targets) do
		if unitIndex then
			local unit = EntIndexToHScript(unitIndex)	
	   		if unit and IsValidEntity(unit) and unit:IsAlive() then
    	  		unit:ForceKill(true) 
    		end
    	end
	end

	-- Reset table
	ability.wolvesIndex = {}
end
