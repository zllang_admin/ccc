function kuangbaohongzha( keys )
	local caster = keys.caster

	local startPos = keys.target_points[1]

	-- Create particle effect
	local particleName = "particles/units/heroes/hero_phoenix/phoenix_supernova_reborn.vpcf"
	local pfx = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN, caster )
	ParticleManager:SetParticleControl( pfx, 0, startPos )
	--ParticleManager:SetParticleControl( pfx, 1, startPos )
	--ParticleManager:SetParticleControl( pfx, 2, Vector( 1, 0, 0 ) )
	--ParticleManager:SetParticleControl( pfx, 3, startPos )

	local ability = keys.ability
	local abilitylevel = ability:GetLevel()
	local casterForward = caster:GetForwardVector()
	local banjing = ability:GetLevelSpecialValueFor("banjing", abilitylevel-1)
	--local spawnLoc = startPos
	local shijianjiange = ability:GetLevelSpecialValueFor("baozha_jiange", abilitylevel-1)
	local shuliang = ability:GetLevelSpecialValueFor("shuliang", abilitylevel-1)
	Timers:CreateTimer(shijianjiange,function()	

		local spawnLoc = startPos + RandomVector(RandomInt(0, banjing))

		---[[随心所欲法
		hongzhadaodan(ability, spawnLoc, caster, abilitylevel)
		--]]

		--[[马甲法
		local dummy = CreateUnitByName( "npc_mk_leitinglieyan", spawnLoc, false, caster, caster, caster:GetTeamNumber() )
		dummy:SetControllableByPlayer(caster:GetPlayerID(), true)
		local abilityEx = dummy:FindAbilityByName("shanqiuzhiwang_leitingyiji")
   		if abilityEx then
   			abilityEx:SetLevel(1)
   			dummy:CastAbilityImmediately(abilityEx,caster:GetPlayerOwnerID())
   		end
   		dummy:AddNewModifier(caster, ability, "modifier_kill", {duration = 1})
		--]]
   		if shuliang > 1 then
   			shuliang = shuliang - 1
   			return shijianjiange
   		end
   		return nil
	end)
end

function hongzhadaodan(ability, spawnLoc, caster, abilitylevel)

	---[[
	local particleName = "particles/units/heroes/hero_jakiro/jakiro_macropyre.vpcf"
	local pfx = ParticleManager:CreateParticle( particleName, PATTACH_ABSORIGIN, caster )
	ParticleManager:SetParticleControl( pfx, 0, spawnLoc )
	ParticleManager:SetParticleControl( pfx, 1, spawnLoc )
	ParticleManager:SetParticleControl( pfx, 2, Vector( 1, 0, 0 ) )
	ParticleManager:SetParticleControl( pfx, 3, spawnLoc )
	--]]

	local particle2 = "particles/units/heroes/hero_ursa/ursa_earthshock.vpcf"
	local pfx = ParticleManager:CreateParticle( particle2, PATTACH_ABSORIGIN, caster )
	ParticleManager:SetParticleControl( pfx, 0, spawnLoc )

	StartSoundEventFromPosition("Ability.LightStrikeArray",spawnLoc)

	local shanghai = ability:GetLevelSpecialValueFor("shanghai", abilitylevel-1)
	local shanghai_fanwei = ability:GetLevelSpecialValueFor("shanghai_fanwei", abilitylevel-1)
	local xuanyun_fanwei = ability:GetLevelSpecialValueFor("xuanyun_fanwei", abilitylevel-1)
	local hero_xuanyun = ability:GetLevelSpecialValueFor("hero_xuanyun", abilitylevel-1)
	local creep_xuanyun = ability:GetLevelSpecialValueFor("creep_xuanyun", abilitylevel-1)

	local enemies = FindUnitsInRadius( caster:GetTeamNumber(), spawnLoc, caster, shanghai_fanwei, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, 0, false )
	if #enemies > 0 then
		for _,enemy in pairs(enemies) do
			if enemy ~= nil and ( not enemy:IsInvulnerable() ) then

				local damage = {
					victim = enemy,
					attacker = caster,
					damage = shanghai,
					damage_type = DAMAGE_TYPE_PHYSICAL,
					ability = ability
				}

				ApplyDamage( damage )
			end
		end
	end

	local enemies_xuanyun = FindUnitsInRadius( caster:GetTeamNumber(), spawnLoc, caster, xuanyun_fanwei, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_BUILDING, 0, 0, false )
	if #enemies_xuanyun > 0 then
		for _,enemy in pairs(enemies_xuanyun) do
			if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
				if enemy:IsHero() then
					true_duration = hero_xuanyun
				else
					true_duration = creep_xuanyun
				end
				enemy:AddNewModifier(caster, ability, "modifier_stunned", {duration = true_duration})
			end
		end
	end
end