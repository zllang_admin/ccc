function heijian(keys)
	local caster = keys.caster
	local point = keys.unit:GetAbsOrigin()
	local kulou = CreateUnitByName("npc_dota_creep_kulouzhanshi",point,true,caster, caster, caster:GetTeamNumber())
	kulou:SetControllableByPlayer(caster:GetPlayerID(), true)
    kulou:AddNewModifier(caster, keys.ability, "modifier_kill", {duration = 10})
    FindClearSpaceForUnit(kulou, point, true)
end