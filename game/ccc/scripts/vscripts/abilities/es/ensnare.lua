

function Ensnare_Catch( event )
    local caster = event.caster
    local target = event.target
    local ability = event.ability
    local projectile_speed = ability:GetLevelSpecialValueFor( "net_speed", ( ability:GetLevel() - 1 ) )
    local enemies = FindUnitsInRadius( caster:GetTeamNumber(),
                                      target:GetAbsOrigin(),
                                      nil,
                                      event.radiu,
                                      DOTA_UNIT_TARGET_TEAM_ENEMY ,
                                      DOTA_UNIT_TARGET_ALL,
                                      DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
                                      FIND_ANY_ORDER,
                                      false )
    for _,v in pairs(enemies) do
        if v and IsValidEntity(v) and v:IsAlive() then
            -- Cast a fake net

            ProjectileManager:CreateTrackingProjectile( {
                Target = v,
                Source = caster,
                Ability = ability,  -- Don't let it call "OnProjectileHitUnit"
                EffectName = "particles/units/heroes/hero_siren/siren_net_projectile.vpcf",
                bDodgeable = true,
                bProvideVision = true,
                iMoveSpeed = projectile_speed,
                iVisionRadius = 0,
                iVisionTeamNumber = v:GetTeamNumber(),
                iSourceAttachment = DOTA_PROJECTILE_ATTACHMENT_ATTACK_1,
            } )
        end
    end
end

function Ensnare_Hit( keys )
  local caster = keys.caster
  local target = keys.target
  local caster_position = caster:GetAbsOrigin()
  local target_position = target:GetAbsOrigin()
  local caster_height = GetGroundHeight(caster_position, caster)
  local target_height = GetGroundHeight(target_position, target)

  target:Interrupt()
  target.CanFly = target:HasFlyMovementCapability()

  if target.CanFly then
    target:SetMoveCapability(DOTA_UNIT_CAP_MOVE_GROUND)
    if target_height > caster_height then
      FindClearSpaceForUnit(target, caster_position, true)
    else
      target:SetAbsOrigin(target_position - Vector(0,0,100))
    end
  end
end

function Ensnare_Destroy( keys )
  local target = keys.target
  if target.CanFly then
    target:SetMoveCapability(DOTA_UNIT_CAP_MOVE_FLY)
    FindClearSpaceForUnit(target, target:GetAbsOrigin(), true)
  end
end