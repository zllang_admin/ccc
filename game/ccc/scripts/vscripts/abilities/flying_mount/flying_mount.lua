function FlyingMountDeath( keys )
	local caster = keys.caster
	local player = caster:GetOwner()
	-- local player=PlayerResource:GetPlayer(playerID)
	player.FlyingMountIndex = nil
    CustomGameEventManager:Send_ServerToPlayer(player, "SetFlyingMountButton", {FlyingMount=-1})
end

function FlyingMountLoad( keys )
	local caster = keys.caster
	local target = keys.target
	if caster.passenger == nil then
		caster.passenger = {}
	end
	if caster.passengerItem == nil then
		caster.passengerItem = {}
	end
	local player = caster:GetOwner()
	local playerID
	if player then
		playerID = player:GetPlayerID()
	end
	
	local targetOwner = target:GetPlayerOwner() or target:GetOwner()
	local passengerIndex = GetNilSeat(caster)

	if targetOwner~=player then
		SendErrorMessage(playerID, "#error_load_self_unit")
		return
	elseif target:HasFlyMovementCapability() then
		SendErrorMessage(playerID, "#error_load_flying_unit")
		return
	elseif target:IsAncient() then
		SendErrorMessage(playerID, "#dota_hud_error_cant_cast_on_ancient")
		return
	elseif passengerIndex == 0 then
		SendErrorMessage(playerID, "#DOTA_PartyFull_Message")
		return
	end

	-- print("load",passengerIndex)
	caster.passenger[passengerIndex] = target:GetEntityIndex()

	local item = CreateItem("item_mount", caster, caster)
	local itemID = item:GetEntityIndex()
	caster:AddItem(item)
	item.passengerIndex = passengerIndex

	caster.passengerItem[passengerIndex] = itemID

	if target:IsRealHero() then
		local HeroName = target:GetUnitName()
		CustomGameEventManager:Send_ServerToAllClients("set_ItemImageName", {itemID=itemID, ItemName=HeroName} )
	end

	EmitSoundOn("DOTA_Item.PhaseBoots.Activate", caster)
	target:Interrupt()
	ProjectileManager:ProjectileDodge(target)
	keys.ability:ApplyDataDrivenModifier(caster, target, "modifier_flying_mount_load_out_of_world", nil)
	target:AddNoDraw()

end

function FlyingMountUnLoad( keys )
	-- print("FlyingMountUnLoad")
	local caster = keys.caster
	if caster.passenger == nil then
		caster.passenger = {}
	end
	if caster.passengerItem == nil then
		caster.passengerItem = {}
	end
	local passengerIndex = GetFirstPassenger(caster)
	UnloadPassenger( caster,passengerIndex )
	Timers:CreateTimer(0.4, function() 
			passengerIndex = GetFirstPassenger(caster)
			-- print("unload",passengerIndex)
			if passengerIndex == 0 then
				caster:Interrupt()
				return nil
			elseif not keys.ability:IsChanneling() then
				return nil
			else
				UnloadPassenger( caster,passengerIndex )
				return 0.4
			end
		end)
end

function FlyingMountMove( keys )
	local caster = keys.caster
	local target = keys.target
    --Check if the host still exists
    if caster == nil or not caster:IsAlive() then -- CHANGE THIS PLEASE?
    	FindClearSpaceForUnit(target, caster:GetAbsOrigin(), true)
    	target:RemoveModifierByName("modifier_flying_mount_load_out_of_world")
    	target:RemoveNoDraw()
    else
        target:SetAbsOrigin(caster:GetAbsOrigin())
    end
end

function UsePassengerItem( keys )
	local mount = keys.caster
	local item = keys.ability
	local passengerIndex = item.passengerIndex
	UnloadPassenger( mount,passengerIndex )
end

function UnloadPassenger( mount,passengerIndex )
	-- print("UnloadPassenger",passengerIndex,mount.passenger[passengerIndex],mount.passengerItem[passengerIndex])
	local item = EntIndexToHScript(mount.passengerItem[passengerIndex])
	local passenger = EntIndexToHScript(mount.passenger[passengerIndex])
	if item and item:GetAbilityName() == "item_mount" then
		CustomGameEventManager:Send_ServerToAllClients("set_ItemImageName", {itemID=item:GetEntityIndex(), ItemName=nil} )
		item:RemoveSelf()
		if passenger and IsValidEntity(passenger) then
			passenger:RemoveModifierByName("modifier_flying_mount_load_out_of_world")
			passenger:RemoveNoDraw()
		end
		mount.passengerItem[passengerIndex] = nil
		mount.passenger[passengerIndex] = nil
		EmitSoundOn("DOTA_Item.Tango.Activate", mount)
	end
end

function GetLastPassenger( mount )
	for i=1,6 do
		local passengerIndex = 7- i
		if mount.passengerItem[passengerIndex] then
			return passengerIndex
		end
	end
	return 0
end

function GetFirstPassenger( mount )
	for i=1,6 do
		local passengerIndex = i
		if mount.passengerItem[passengerIndex] then
			return passengerIndex
		end
	end
	return 0
end

function GetNilSeat( mount )
	for i=1,6 do
		if mount.passengerItem[i] == nil then
			return i 
		end
	end
	return 0
end