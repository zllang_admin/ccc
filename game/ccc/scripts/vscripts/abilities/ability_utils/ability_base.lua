function KnockBackFunction(t,sheight,mheight,eheight,T)
    return (2*eheight+2*sheight-4*mheight)/(T*T)*(t*t)+(4*mheight-eheight-3*sheight)/T*t+sheight
end
--击退函数：击退者，被击退者，方向，距离，持续时间，落地高度，最高高度，是否破坏树，破坏树的范围
function KnockBack(hCaster,hTarget,vAngle,fDistance,fDuration,fHeight,fMaxHeight,bKilltree,fKilltreeRange)
    local period = 0.03125
    local g = -9.8
    local vVectorStart = hTarget:GetAbsOrigin()
    local vVectorEnd = vVectorStart+vAngle*fDistance
    fHeight = GetGroundPosition(vVectorEnd,hTarget).z + fHeight
    fMaxHeight = vVectorStart.z + fMaxHeight
    local Velocity = (vVectorEnd - vVectorStart) / fDuration * period
    local time = 0
    local TVector = hTarget:GetAbsOrigin()
    local last_time = GameRules:GetGameTime()-period
    if fDuration > 0 then
        hTarget:AddNewModifier(nil,nil,"modifier_rooted",nil) 
        hTarget:SetContextThink("KnckBack",
            function()
                if not hTarget:HasModifier("modifier_rooted") then
                    local forward = hTarget:GetForwardVector() 
                    hTarget:SetForwardVector(Vector(forward.x,forward.y,0)) 
                    return nil
                end
                local now = GameRules:GetGameTime()
                if now - last_time >= period then
                    last_time = now
                    time = time + period
                    TVector = TVector + Velocity
                    TVector.z = KnockBackFunction(time,vVectorStart.z,fMaxHeight,fHeight,fDuration)
                    hTarget:SetAbsOrigin(TVector)
                    if bKilltree then
                        GridNav:DestroyTreesAroundPoint( TVector, fKilltreeRange, false)
                    end
                    if time > fDuration then
                        hTarget:SetAbsOrigin(vVectorEnd)
                        hTarget:RemoveModifierByName("modifier_rooted")
                        hTarget:AddNewModifier(nil,nil,"modifier_phased",{duration=0.1}) 
                    end
                end
                return 0
            end
        , 0)
    end
end

function create_illusion(keys, illusion_origin, illusion_incoming_damage, illusion_outgoing_damage, illusion_duration)  
    local player_id = keys.caster:GetPlayerID()
    local caster_team = keys.caster:GetTeam()
    
    local illusion = CreateUnitByName(keys.caster:GetUnitName(), illusion_origin, true, keys.caster, nil, caster_team)  --handle_UnitOwner needs to be nil, or else it will crash the game.
    illusion:SetOwner(keys.caster:GetOwner())

    illusion:SetPlayerID(player_id)
    illusion:SetControllableByPlayer(player_id, true)

    --Level up the illusion to the caster's level.
    local caster_level = keys.caster:GetLevel()
    for i = 1, caster_level - 1 do
        illusion:HeroLevelUp(false)
    end

    --Set the illusion's available skill points to 0 and teach it the abilities the caster has.
    illusion:SetAbilityPoints(0)
    for ability_slot = 0, 15 do
        local individual_ability = keys.caster:GetAbilityByIndex(ability_slot)
        if individual_ability ~= nil then 
            local illusion_ability = illusion:FindAbilityByName(individual_ability:GetAbilityName())
            if illusion_ability ~= nil then
                illusion_ability:SetLevel(individual_ability:GetLevel())
            end
        end
    end

    --Recreate the caster's items for the illusion.
    for item_slot = 0, 5 do
        local individual_item = keys.caster:GetItemInSlot(item_slot)
        if individual_item ~= nil then
            local illusion_duplicate_item = CreateItem(individual_item:GetName(), illusion, illusion)
            illusion:AddItem(illusion_duplicate_item)
        end
    end
    
    -- modifier_illusion controls many illusion properties like +Green damage not adding to the unit damage, not being able to cast spells and the team-only blue particle 
    illusion:AddNewModifier(keys.caster, keys.ability, "modifier_illusion", {duration = illusion_duration, outgoing_damage = illusion_outgoing_damage, incoming_damage = illusion_incoming_damage})
    
    illusion:MakeIllusion()  --Without MakeIllusion(), the unit counts as a hero, e.g. if it dies to neutrals it says killed by neutrals, it respawns, etc.  Without it, IsIllusion() returns false and IsRealHero() returns true.

    return illusion
end

--aoe伤害函数，施法者，伤害圆心，半径，伤害大小，伤害类型，目标类型，队伍，flag
function damage_aoe(caster,center,radius,damage,damage_type,target_type,target_team,target_flag)
    
    if units[1] then
        for _,unit in pairs(units) do
            ApplyDamage(
            {
                victim = unit,
                attacker = caster,
                damage = damage,
                damage_type = damage_type
            }
            )
        end
    end
end

--偷懒点的写法,注意使用这个的话在kv里面必须写好以下这四个四个技能属性
function lazy_damage_aoe(caster,ability,center,radius,damage)
    local damage_type = ability:GetAbilityDamageType()--在kv没写好的返回的是空值，会出错，下面一样
    local target_type = ability:GetAbilityTargetType()
    local target_team = ability:GetAbilityTargetTeam()
    local target_flag = ability:GetAbilityTargetFlags()
    local units = FindUnitsInRadius(caster:GetTeam(),center,nil,radius,target_team,target_type,target_flag,0,false)
    if units[1] then
        for _,unit in pairs(units) do
            ApplyDamage(
            {
                victim = unit,
                attacker = caster,
                damage = damage,
                damage_type = damage_type
            }
            )
        end
    end
end

--获取center附近radius范围内一个随机点
function random_point( center,radius )
    return (center + RandomVector(RandomFloat(0,radius))) 
end

--在表中查找是否存在val
function table_find( t,val)
    for k,v in pairs(t) do
        if v == val then
            return true
        end
    end
    return false
end

--复制一个表并返回复制好的表
function table_copy( t1 )
    local t2 = {}
    for k,v in pairs(t1)do
        table.insert(t2,v)
    end
    return t2
end

--创建带有攻击弹道的假单位
function CreateAttackUnit( position, interval, duration, dandao, range)
    -- body
end

function HeroModelInit( keys )
    local caster = keys.caster
    local casterID = caster:GetEntityIndex()
    if _G.ModelAnimationGroup == nil then
        _G.ModelAnimationGroup = {}
    end
    -- print("ModelAnimationGroup",_G.ModelAnimationGroup)
    table.insert(_G.ModelAnimationGroup, casterID)
end