function shouwei( keys ) 
	local caster = keys.caster
	local ability = keys.ability
	local shouwei = keys.shouwei
	local casterLoc = caster:GetAbsOrigin()
	local juli = ability:GetSpecialValueFor("juli")
	if ability.unit == nil then
		ability.unit = {}
	end
	local angel = 0
	local angel_cha = 360 / shouwei
	for i=1,shouwei,1 do
		angel = angel + angel_cha
		local spawn_location = casterLoc + Vector(math.cos(math.rad(angel)),math.sin(math.rad(angel)),0)*juli
		ability.unit[i] = CreateUnitByName( "npc_dota_creep_xiaojiqiang", spawn_location, true, caster, caster, caster:GetTeamNumber() )
		ability.unit[i]:AddNewModifier(caster, ability, "modifier_phased",{})
	end
end

function shouwei_kill( keys )
	local caster = keys.caster
	local ability = keys.ability
	for i=1,#ability.unit,1 do
		if IsValidEntity(ability.unit[i]) and ability.unit[i]:IsAlive() then
			ability.unit[i]:ForceKill(false)
		end
		ability.unit[i] = nil
	end
	caster:RemoveModifierByName("modifier_anyinglieshou_wudu")
	FindClearSpaceForUnit(caster, caster:GetAbsOrigin(), true)
end

function wudu_check( keys )
	local caster = keys.caster
	local ability = keys.ability
	if not ability:IsChanneling() then
		caster:RemoveModifierByName("modifier_anyinglieshou_wudu")
	end

	local caster_position = caster:GetAbsOrigin()

	local banjing = ability:GetLevelSpecialValueFor("banjing", (ability:GetLevel() - 1))

	local teammates = FindUnitsInRadius( caster:GetTeamNumber(),
                                      caster_position,
                                      nil,
                                      banjing,
                                      DOTA_UNIT_TARGET_TEAM_FRIENDLY ,
                                      DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO,
                                      DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_NOT_ANCIENTS,
                                      FIND_ANY_ORDER,
                                      false )

	for k,v in pairs(teammates) do
		-- print("teammate",v:GetUnitName(),v:IsChanneling(),v:HasModifier("modifier_anyinglieshou_wudu_2"))
		if v:IsChanneling() then
			v:RemoveModifierByName("modifier_anyinglieshou_wudu_2")
		else
			ability:ApplyDataDrivenModifier(caster, v, "modifier_anyinglieshou_wudu_2", {duration = 0.1})
		end
	end
end