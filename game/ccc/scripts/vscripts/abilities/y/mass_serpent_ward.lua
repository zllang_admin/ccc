function SetWardDamage( event )
	--local target = event.target
	local ability = event.ability
	local caster = event.caster
	local targetLoc = event.target_points[1]
	local attack_damage_min = ability:GetLevelSpecialValueFor("damage_min", ability:GetLevel() - 1 )
	local attack_damage_max = ability:GetLevelSpecialValueFor("damage_max", ability:GetLevel() - 1 )
	local MaxHealth = ability:GetLevelSpecialValueFor("MaxHealth", ability:GetLevel() - 1 )
	local duration = ability:GetLevelSpecialValueFor( "duration", ability:GetLevel() - 1 )
	local ward_count = ability:GetLevelSpecialValueFor( "ward_count", ability:GetLevel() - 1 )

	for i=1,ward_count,1 do
		local target = CreateUnitByName( "npc_dota_shadow_shaman_ward_1", targetLoc, true, caster, caster, caster:GetTeamNumber() )
		target:SetControllableByPlayer(caster:GetPlayerID(), true)
		target:AddNewModifier(caster, ability, "modifier_magicimmune", {duration = duration})
		target:AddNewModifier(caster, ability, "modifier_kill", {duration = duration})
		target:AddNewModifier(caster, ability, "modifier_phased", {duration = 0.03})
		target:SetBaseDamageMax(attack_damage_max)
		target:SetBaseDamageMin(attack_damage_min)
		target:SetBaseMaxHealth(MaxHealth)
	end
end