LinkLuaModifier("modifier_voodoo_lua", "abilities/y/modifiers/modifier_voodoo_lua.lua", LUA_MODIFIER_MOTION_NONE)

--[[Author: Pizzalol
	Date: 27.09.2015.
	Checks if the target is an illusion, if true then it kills it
	otherwise it applies the hex modifier to the target]]
function voodoo_start( keys )
	local caster = keys.caster
	local ability = keys.ability
	local ability_level = ability:GetLevel() - 1
	local target_entities = keys.target_entities
	local maintarget = keys.target

	local duration = keys.duration
	
	for _,target in pairs(target_entities) do
		if target:IsIllusion() then
			target:ForceKill(true)
		elseif maintarget ~= target then
			target:AddNewModifier(caster, ability, "modifier_voodoo_lua", {duration = duration})
		end
	end
end