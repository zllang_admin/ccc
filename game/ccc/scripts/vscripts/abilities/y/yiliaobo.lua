function yiliaobo_start( keys )
	local caster = keys.caster
	local target = keys.target
	local ability = keys.ability
	local range = keys.range
	local heal = keys.heal
	local reduce = keys.reduce
	local max_num = keys.max_num

	heal_loop(caster,target,ability,range,heal,reduce,max_num)
end

function heal_loop( caster,target,ability,range,heal,reduce,max_num)--若是30的治疗，调用这个函数，注意此时第二个参数也就是自动选择的第一个目标，接下来所有的目标会自动选择的
	local t1 = caster
	local t2 = target
	local healed_units = {}
	heal_per(caster,t1,t2,heal)
	table.insert(healed_units,target)

	Timers:CreateTimer(0.3,function()
		local units = FindUnitsInRadius(caster:GetTeam(),t2:GetAbsOrigin(),nil,range,DOTA_UNIT_TARGET_TEAM_FRIENDLY,ability:GetAbilityTargetType(),ability:GetAbilityTargetFlags(),0,false)
		if units[1] then
			for _,unit in pairs(units) do
				if not table_find(healed_units,unit) then --是否已经治疗过
					while true do
						--"continue"条件
						if (#healed_units == 1 and unit == caster) then --如果第一个目标是施法者的话就"continue"，防止有时候加血只是施法者->目标1->施法者
							break
						end
						if unit:GetHealthPercent()==100 then --满血的跳过
							break
						end
						--=============while内的真实代码
						t1 = t2
						t2 = unit
						heal = heal*(1-reduce/100)--治疗量递减
						heal_per(caster,t1,t2,heal)
						table.insert(healed_units,unit)
						if #healed_units >= max_num then
							return nil
						end
						return 0.3
						--================
						--break--其实可有可无的break
					end
				end
			end
			return nil
		end
		return nil
	end)
end

function heal_per(caster,t1,t2,heal)--治疗、特效、音效
	local p = ParticleManager:CreateParticle("particles/abilities/y/yiliaobo/wave.vpcf",PATTACH_CUSTOMORIGIN,caster)
	ParticleManager:SetParticleControlEnt(p,0,t1,PATTACH_POINT_FOLLOW,"attach_hitloc",t1:GetAbsOrigin(), true)
	ParticleManager:SetParticleControlEnt(p,1,t2,PATTACH_POINT_FOLLOW,"attach_hitloc",t2:GetAbsOrigin(), true)
	t2:Heal(heal,caster)
	t2:EmitSound("Hero_Huskar.Inner_Vitality")
end

function damage_loop( caster,target,ability,range,damage,reduce,max_num,elf)--若是30的治疗，调用这个函数，注意此时第二个参数也就是自动选择的第一个目标，接下来所有的目标会自动选择的
	local t1 = elf
	local t2 = target
	local damage_units = {}
	damage_per(caster,t1,t2,damage)
	table.insert(damage_units,target)
	Timers:CreateTimer(0.3,function()
		local units = FindUnitsInRadius(caster:GetTeam(),t2:GetAbsOrigin(),nil,range,DOTA_UNIT_TARGET_TEAM_ENEMY,ability:GetAbilityTargetType(),ability:GetAbilityTargetFlags(),0,false)
		if units[1] then
			for _,unit in pairs(units) do
				if not table_find(damage_units,unit) then --是否已经治疗过
					while true do
						--"continue"条件
						if (#damage_units == 1 and unit == caster) then --如果第一个目标是施法者的话就"continue"，防止有时候加血只是施法者->目标1->施法者
							break
						end
						--=============while内的真实代码
						t1 = t2
						t2 = unit
						damage = damage*(1-reduce/100)--治疗量递减
						damage_per(caster,t1,t2,damage)
						table.insert(damage_units,unit)
						if #damage_units >= max_num then
							return nil
						end
						return 0.3
						--================
						--break--其实可有可无的break
					end
				end
			end
			return nil
		end
		return nil
	end)
end

function damage_per(caster,t1,t2,damage)--伤害、特效、音效
	local p = ParticleManager:CreateParticle("particles/units/heroes/hero_zuus/zuus_arc_lightning_.vpcf",PATTACH_CUSTOMORIGIN,caster)
	ParticleManager:SetParticleControlEnt(p,0,t1,PATTACH_POINT_FOLLOW,"attach_hitloc",t1:GetAbsOrigin(), true)
	ParticleManager:SetParticleControlEnt(p,1,t2,PATTACH_POINT_FOLLOW,"attach_hitloc",t2:GetAbsOrigin(), true)
	local damageTable = {
		victim = t2,
		attacker = caster,
		damage = damage,
		damage_type = DAMAGE_TYPE_MAGICAL,
	}
	ApplyDamage(damageTable)
	t2:EmitSound("Hero_Zuus.ArcLightning.Cast")
end


function create_elf(keys)--创建五个精灵
	local caster = keys.caster
	local ability = keys.ability
	local casterLoc = caster:GetAbsOrigin()
	local juli = ability:GetSpecialValueFor("elf_juli")
	local jingling = ability:GetSpecialValueFor("jingling")
	local angel = 90
	local angel_cha = 360 / jingling
	local chixu = ability:GetSpecialValueFor("chixu")

	caster:SetAbsOrigin(casterLoc+Vector(0,0,200))
	if ability.unit == nil then
		ability.unit = {}
	end
	for i=1,jingling,1 do
		angel = angel + angel_cha
		local location = casterLoc + Vector(math.cos(math.rad(angel)),math.sin(math.rad(angel)),0)*juli
		ability.unit[i] = CreateUnitByName( "npc_dummy_jinglingzhufu", location, true, caster, caster, caster:GetTeamNumber() )
		ability:ApplyDataDrivenModifier(caster, ability.unit[i], "modifier_anyinglieshou_jinglingzhufu", nil)	
	end
	if p_jingling == nil then
		p_jingling = {}
	end
	for i=1,#ability.unit,1 do
		local nextIndex = 0
		if i < 4 then
			nextIndex = i+2
		else
			nextIndex = i-3
		end
			--print(nextIndex)
		local t2 = ability.unit[i]
		p_jingling[i] = ParticleManager:CreateParticle("particles/abilities/y/yiliaobo/batrider_flaming_lasso.vpcf",PATTACH_POINT_FOLLOW,t2)
		ParticleManager:SetParticleControlEnt(p_jingling[i],0,ability.unit[nextIndex],PATTACH_POINT_FOLLOW,"attach_hitloc",ability.unit[nextIndex]:GetAbsOrigin(), true)
		ParticleManager:SetParticleControlEnt(p_jingling[i],1,t2,PATTACH_POINT_FOLLOW,"attach_hitloc",t2:GetAbsOrigin(), true)
		--ParticleManager:SetParticleControlEnt(p,2,t2,PATTACH_POINT_FOLLOW,"attach_hitloc",t2:GetAbsOrigin(), true)
	end
end
k=0
function move(keys)--移动
	local caster = keys.caster
	local ability = keys.ability
	local jingling = ability:GetSpecialValueFor("jingling")
	if not ability:IsChanneling() then
		caster:RemoveModifierByName("modifier_anyinglieshou_jinglingzhufu")
		return
	end
	for i=1,#ability.unit,1 do
		local t2 = ability.unit[i]
		local p = ParticleManager:CreateParticle("particles/abilities/y/yiliaobo/wave.vpcf",PATTACH_CUSTOMORIGIN,caster)
		ParticleManager:SetParticleControlEnt(p,0,caster,PATTACH_POINT_FOLLOW,"attach_hitloc",caster:GetAbsOrigin(), true)
		ParticleManager:SetParticleControlEnt(p,1,t2,PATTACH_POINT_FOLLOW,"attach_hitloc",t2:GetAbsOrigin(), true)
		heal_search(keys,i)
		
	end
	damage_search(keys,caster)
	caster:EmitSound("Hero_Huskar.Inner_Vitality")
	Timers:CreateTimer(0.8,function()
		for i=1,#ability.unit,1 do
			local nextIndex = 0
			if i < 4 then
				nextIndex = i+2
			else
				nextIndex = i-3
			end
			--print(nextIndex)
			ability.unit[i]:MoveToPosition(ability.unit[nextIndex]:GetAbsOrigin())
		end
	end)
end

function damage_search(keys,i)--寻找伤害目标
	local caster = keys.caster
	local ability = keys.ability
	local damage = ability:GetSpecialValueFor("shanghai")
	local range = ability:GetSpecialValueFor("shanghai_tiaoyue")
	local reduce = ability:GetSpecialValueFor("shanghai_shuaijian")
	local max_num = ability:GetSpecialValueFor("shanghai_mubiao")
	local juli = ability:GetSpecialValueFor("shanghai_juli")
	local target = FindUnitsInRadius(caster:GetTeam(),
									caster:GetAbsOrigin(),
									nil,
									juli,
									DOTA_UNIT_TARGET_TEAM_ENEMY ,
                                    ability:GetAbilityTargetType(),
                                    ability:GetAbilityTargetFlags(),
									FIND_CLOSEST,
									false)
	if target[1] then
		damage_loop(caster,target[1],ability,range,damage,reduce,max_num,caster)
	else
		return
	end
end

function heal_search(keys,i)--寻找治疗目标
	local caster = keys.caster
	local ability = keys.ability
	local heal = ability:GetSpecialValueFor("zhiliao")
	local range = ability:GetSpecialValueFor("zhiliao_tiaoyue")
	local reduce = ability:GetSpecialValueFor("zhiliao_shuaijian")
	local max_num = ability:GetSpecialValueFor("zhiliao_mubiao")
	local juli = ability:GetSpecialValueFor("zhiliao_juli")
	local target = FindUnitsInRadius(caster:GetTeam(),
									ability.unit[i]:GetAbsOrigin(),
									nil,
									juli,
									DOTA_UNIT_TARGET_TEAM_FRIENDLY,
                                    ability:GetAbilityTargetType(),
                                    ability:GetAbilityTargetFlags(),
									FIND_CLOSEST,
									false)
	for t=1,#target,1 do
		if target[t]:GetHealthPercent()==100 then --满血的跳过
		else
			heal_loop(ability.unit[i],target[t],ability,range,heal,reduce,max_num)
			return
		end
	end
end

function jingling_kill(keys)
	local ability = keys.ability
	for i=1,#ability.unit,1 do
		if IsValidEntity(ability.unit[i]) and ability.unit[i]:IsAlive() then
			ability.unit[i]:ForceKill(false)
		end
		ability.unit[i] = nil
		ParticleManager:DestroyParticle(p_jingling[i], false)
	end
	p_jingling = nil
end