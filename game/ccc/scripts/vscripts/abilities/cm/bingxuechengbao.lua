function BingXueChengBao_start( keys )
	local caster = keys.caster
	local point = keys.target_points[1]
	local ability = keys.ability
	local banjing = ability:GetSpecialValueFor("banjing")
	local chixu = ability:GetSpecialValueFor("chixu")
	ability.particle = ParticleManager:CreateParticle("particles/units/heroes/hero_crystalmaiden/maiden_freezing_field_snow.vpcf", PATTACH_ABSORIGIN, caster)
	ParticleManager:SetParticleControl(ability.particle, 0, point)
    ParticleManager:SetParticleControl(ability.particle, 1, Vector(banjing,banjing,banjing))

    local enemies = FindUnitsInRadius( caster:GetTeamNumber(),
                                      point,
                                      nil,
                                      banjing,
                                      DOTA_UNIT_TARGET_TEAM_ENEMY ,
                                      DOTA_UNIT_TARGET_BASIC,
                                      DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
                                      FIND_ANY_ORDER,
                                      false )

    for k,v in pairs(enemies) do
    	if v:GetUnitName() == "npc_building_shangdianshouwei" then
    		ability:ApplyDataDrivenModifier(caster, v, "modifier_crystal_maiden_bingxuechengbao", {duration = chixu})
    	end
    end
end

function BingXueChengBao_damage( keys )
	local caster = keys.caster
	local ability = keys.ability
	local target = keys.target

	if not caster:HasModifier("modifier_crystal_maiden_bingxuechengbao_channeling") then
		target:RemoveModifierByName("modifier_crystal_maiden_bingxuechengbao")
	end

	local shanghai = ability:GetSpecialValueFor("shanghai")
	local target_maxhealth = target:GetMaxHealth()
	local damage = target_maxhealth * shanghai / 100
	local dmg_table_target = {
                                victim = target,
                                attacker = caster,
                                damage = damage,
                                damage_type = DAMAGE_TYPE_PURE
                            }
    ApplyDamage(dmg_table_target)
end

function BingXueChengBao_end( keys )
	local caster = keys.caster
	local ability = keys.ability
    ParticleManager:DestroyParticle(ability.particle, false)
end

function BingXueChengBao_animation( keys )
	local caster = keys.caster

    StartAnimation(caster, {duration=6, activity=ACT_DOTA_TELEPORT, rate=1})
end