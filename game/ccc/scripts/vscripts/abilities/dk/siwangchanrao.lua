function siwangchanrao(keys)
	local caster = keys.caster
	local target = keys.target
	local zhiliao = keys.ability:GetSpecialValueFor("zhiliao")
	local shanghai = keys.ability:GetSpecialValueFor("shanghai")
	local targetTeam = target:GetTeamNumber()
    local casterTeam = caster:GetTeamNumber()
    if targetTeam == casterTeam then
    	target:Heal(zhiliao, caster)
    else
    	ApplyDamage({ victim = target, attacker = caster, damage = shanghai, damage_type = DAMAGE_TYPE_MAGICAL })
    end
end