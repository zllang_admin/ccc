function wuyanzhangqi(keys)
	local caster = keys.caster
	local ability = keys.ability
	local casterLoc = caster:GetAbsOrigin()
	local targetLoc = keys.target_points[1]
	local duration = ability:GetSpecialValueFor("chixu")
	local distance = ability:GetSpecialValueFor( "distance")
	local radius = ability:GetSpecialValueFor( "radius")
	local collision_radius = ability:GetLevelSpecialValueFor( "collision_radius", ability:GetLevel() - 1 )
	local projectile_speed = ability:GetLevelSpecialValueFor( "speed", ability:GetLevel() - 1 )
	local machines_per_sec = ability:GetLevelSpecialValueFor ( "machines_per_sec", ability:GetLevel() - 1 )
	local dummyModifierName = "modifier_siwangqishi_wuyanzhangqi"
	-- Find forward vector
	local forwardVec = targetLoc - casterLoc
	forwardVec = forwardVec:Normalized()
	
	-- Find backward vector
	local backwardVec = casterLoc - targetLoc
	backwardVec = backwardVec:Normalized()
	
	-- Find middle point of the spawning line
	local middlePoint = casterLoc + ( radius * backwardVec )
	
	-- Find perpendicular vector
	local v = middlePoint - casterLoc
	local dx = -v.y
	local dy = v.x
	local perpendicularVec = Vector( dx, dy, v.z )
	perpendicularVec = perpendicularVec:Normalized()

	-- Create dummy to store data in case of multiple instances are called
	local dummy = CreateUnitByName( "npc_dummy_unit", caster:GetAbsOrigin(), false, caster, caster, caster:GetTeamNumber() )
	ability:ApplyDataDrivenModifier( caster, dummy, dummyModifierName, {} )
	dummy.march_of_the_machines_num = 0
	
	-- Create timer to spawn projectile
	Timers:CreateTimer( function()
		if caster:IsChanneling() == false then
			dummy:Destroy()
			return nil
		end

			-- Get random location for projectile
			local random_distance = RandomInt( -radius, radius )
			local spawn_location = Vector(middlePoint.x,middlePoint.y,casterLoc.z+250) + Vector(perpendicularVec.x * random_distance,perpendicularVec.y * random_distance,0)
			local velocityVec = Vector( forwardVec.x, forwardVec.y, 0 )
			
			-- Spawn projectiles
			local projectileTable = {
				Ability = ability,
				EffectName = "particles/ch3c/dk/wuyanzhangqi.vpcf",
				vSpawnOrigin = spawn_location,
				fDistance = distance,
				fStartRadius = collision_radius,
				fEndRadius = collision_radius,
				Source = caster,
				bHasFrontalCone = false,
				bReplaceExisting = false,
				bProvidesVision = false,
				iUnitTargetTeam = DOTA_UNIT_TARGET_TEAM_ENEMY,
				iUnitTargetType = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
				vVelocity = velocityVec * projectile_speed
			}
			ProjectileManager:CreateLinearProjectile( projectileTable )
			StartSoundEventFromPosition("Hero_Huskar.Burning_Spear.Cast", spawn_location)
			-- Increment the counter
			dummy.march_of_the_machines_num = dummy.march_of_the_machines_num + 1
			
			-- Check if the number of machines have been reached
			if dummy.march_of_the_machines_num == machines_per_sec * duration then
				dummy:Destroy()
				return nil
			else
				return 1 / machines_per_sec
			end
		end
	)
end