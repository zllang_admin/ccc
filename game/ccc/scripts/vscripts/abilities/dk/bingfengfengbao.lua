function bingfengfengbao(keys)
	local caster = keys.caster 
	local startPos = keys.target_points[1]
	local yanchi = keys.ability:GetSpecialValueFor("yanchi")
	local particle = ParticleManager:CreateParticle("particles/econ/items/ancient_apparition/aa_blast_ti_5/ancient_apparition_ice_blast_final_ti5.vpcf", PATTACH_ABSORIGIN_FOLLOW, caster)
	ParticleManager:SetParticleControl(particle, 0, caster:GetAbsOrigin())
	ParticleManager:SetParticleControl(particle, 1, startPos-caster:GetAbsOrigin())
	ParticleManager:SetParticleControl(particle, 3, startPos)
	ParticleManager:SetParticleControl(particle, 5, Vector(yanchi,0,0))
	ParticleManager:ReleaseParticleIndex(particle)
	--print(particle)

	ParticleManager:DestroyParticle(particle, false)
end