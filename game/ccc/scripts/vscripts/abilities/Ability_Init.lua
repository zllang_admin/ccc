if CustomAbility == nil then CustomAbility=class({}) end 

function CustomAbility:Ability_Precache( context )
	--预加载特效、模型、音效等。
	--例如：
	--PrecacheResource("particle", "particles/econ/generic/generic_aoe_explosion_sphere_1/generic_aoe_explosion_sphere_1.vpcf", context)
  	--PrecacheResource("particle_folder", "particles/test_particle", context)  
	--PrecacheResource("model_folder", "particles/heroes/antimage", context)
	--PrecacheResource("model", "particles/heroes/viper/viper.vmdl", context)
	PrecacheResource("particle","particles/abilities/dafa/baofengxue/bfx_attack.vpcf", context)--大法暴风雪特效（暂用）
	PrecacheResource("particle","particles/abilities/y/yiliaobo/wave.vpcf", context)--小y医疗波
end
function CustomAbility:Ability_Init(  )
	--初始化技能相关脚本,功能等同于addon_game_mode.lua中的Init。
	--例如：
		--监听事件：ListenToGameEvent("player_reconnected", Dynamic_Wrap(ccc, 'OnPlayerReconnect'), self)
		--初始化某函数：Init()
	ListenToGameEvent("dota_player_used_ability", Dynamic_Wrap(CustomAbility, 'OnAbilityUsed'), self)
	LinkLuaModifier("modifier_tianyouwudi","sys/modifiers/modifier_tianyouwudi.lua",LUA_MODIFIER_MOTION_NONE)
    ListenToGameEvent("dota_player_learned_ability", Dynamic_Wrap(CustomAbility, 'On_dota_player_learned_ability'), self)
    ListenToGameEvent("dota_unit_event", Dynamic_Wrap(CustomAbility, 'On_dota_unit_event'), self)
end
function CustomAbility:Require( )
	-- 加载脚本文件至主系统，功能等同与在addon_game_mode.lua 中require。
	--例如：require('xxxx.xxx.lua')
    require('abilities/ability_utils/ability_base')


	
end

function CustomAbility:On_dota_player_learned_ability( keys )
    -- PrintTable(keys)
    local player = EntIndexToHScript(keys.player)
    local playerID = player:GetPlayerID()
    local steamid=PlayerResource:GetSteamAccountID(playerID)
    local abilityname = keys.abilityname
    if abilityname == "nevermore_requiem" then
        local hero_entindex = GameRules:GetGameModeEntity().DB.player[steamid].hero_entityindex["npc_dota_hero_nevermore"]
        local hero = EntIndexToHScript(hero_entindex)
        local ability = hero:FindAbilityByName("nevermore_requiem")
        if not hero.RequiemCheck then
            local RequiemTimes = 10
            hero.RequiemCheck = true
            local RequiemStart = false
            local RequiemCount = 0
            local RequiemCasting = false
            local RequiemOtherOrderCount = 0
            Timers:CreateTimer(0.035, function()
                if ability:IsInAbilityPhase() and not RequiemStart then
                    --前摇第一帧
                    print("phase")
                    RequiemStart = true
                    RequiemCasting = true
                    RequiemOtherOrderCount = 0
                    
                elseif not ability:IsInAbilityPhase() and RequiemStart and ability:IsCooldownReady() then
                    --前摇时打断
                    RequiemStart = false
                    print("interrupt",RequiemCount)
                    if RequiemCount > 0 then
                        --第二次后前摇时打断
                        RequiemCount = 0
                        RequiemCasting = false
                        ability:StartCooldown(ability:GetCooldown(ability:GetLevel()))
                    end
                    hero:Interrupt()
                    hero:RemoveModifierByName("modifier_tianyouwudi")
                elseif RequiemStart and not ability:IsCooldownReady() then
                    --施法结束第一帧
                    print("finish",RequiemCount)
                    if RequiemCount == 0 then
                        --第一次施法结束第一帧
                        hero:AddNewModifier(hero,nil,"modifier_tianyouwudi",{duration = 3})
                    end
                    RequiemCount = RequiemCount + 1
                    ability:EndCooldown()
                    if RequiemCount < RequiemTimes then
                        -- Timers:CreateTimer(0.5, function()
                            hero:CastAbilityNoTarget(ability, playerID)
                            RequiemStart = false
                        -- end)
                    else
                        --第n次施法结束第一帧
                        RequiemCount = 0
                        RequiemCasting = false
                        ability:StartCooldown(ability:GetCooldown(ability:GetLevel() - 1))
                    end
                elseif RequiemCasting and RequiemCount > 0 and (RequiemStart==false) and (ability:IsInAbilityPhase()==false) then
                    print("other orders")
                    if RequiemOtherOrderCount > 0 then
                        RequiemCount = 0
                        RequiemCasting = false
                        ability:StartCooldown(ability:GetCooldown(ability:GetLevel() - 1))
                    else
                        RequiemOtherOrderCount = RequiemOtherOrderCount + 1
                    end
                else
                    -- print("else",RequiemCasting,RequiemCount,RequiemStart,ability:IsInAbilityPhase())
                    RequiemOtherOrderCount = 0
                end
                return 0.035
            end)
        end 
    end
end

function CustomAbility:OnAbilityUsed( keys )
    -- PrintTable(keys)
	local player = PlayerResource:GetPlayer(keys.PlayerID)
	local playerID = keys.PlayerID
	local steamid=PlayerResource:GetSteamAccountID(playerID)
  	local abilityname = keys.abilityname
  	-- print("OnAbilityUsed",abilityname)
  	if abilityname == "rubick_spell_steal" then
  		local hero_entindex = GameRules:GetGameModeEntity().DB.player[steamid].hero_entityindex["npc_dota_hero_rubick"]
  		local hero = EntIndexToHScript(hero_entindex)
        
        

  		--[[
  		for i = 0, 15 do
        	local ability_slot = hero:GetAbilityByIndex(i)
        	if ability_slot then
        		print("ability_slot",i,ability_slot,ability_slot:GetAbilityName(),ability_slot:IsHidden())
        	end
        end
        Timers:CreateTimer(3, function()
			for i = 0, 15 do
        		local ability_slot = hero:GetAbilityByIndex(i)
        		if ability_slot then
        			print("ability_slot2",i,ability_slot,ability_slot:GetAbilityName(),ability_slot:IsHidden())
        		end
        	end
        end)
        --]]
        
    elseif abilityname == "crystal_maiden_freezing_field" then
    	local hero_entindex = GameRules:GetGameModeEntity().DB.player[steamid].hero_entityindex["npc_dota_hero_crystal_maiden"]
  		local hero = EntIndexToHScript(hero_entindex)
    	hero:AddNewModifier(hero,nil,"modifier_tianyouwudi",{duration = 5})
    
    end
end

function IsDummyUnit( unit )
    local unit_name = unit:GetUnitName()
    if unit_name == "npc_dummy_shop" then
        return true
    elseif unit_name == "npc_dummy_unit" then
        return true
    elseif unit_name == "npc_dota_shandianxinxing" then
        return true
    elseif unit_name == "npc_dota_hunluanjianyu" then
        return true
    elseif unit_name == "npc_dota_mofajiejie" then
        return true
    elseif unit_name == "npc_dummy_corpse" then
        return true
    elseif unit_name == "npc_dummy_rocket" then
        return true
    elseif unit_name == "npc_dummy_jinglingzhufu" then
        return true
    elseif unit_name == "npc_dummy_xianshi" then
        return true
    elseif unit_name == "npc_ccc_hero_wisp" then
        return true
    elseif unit_name == "npc_kog_chenai" then
        return true
    elseif unit_name == "npc_mk_leitinglieyan" then
        return true
    elseif unit_name == "npc_dummy_moon_glaive" then
        return true
    elseif unit_name == "npc_building_shangdianshouwei" then
        return true
    elseif unit_name == "npc_dota_shadow_shaman_ward_1" then
        return true
    elseif unit:HasAbility("dummy_unit_ability") then
        return true
    elseif unit:HasAbility("courier_out_of_world") then
        return true
    elseif unit:HasAbility("dummy_unit") then
        return true
    elseif unit:HasAbility("dummy_elf") then
        return true
    elseif unit:HasAbility("dummy_corpse") then
        return true
    elseif unit:HasModifier("modifier_flying_mount_load_out_of_world") then
        return true
    else
        return false
    end    
end

function animation_death_fix( keys )
    -- print("death_animation")
    local caster = keys.caster
    local particleName = "particles/units/heroes/hero_dragon_knight/dragon_knight_transform_red.vpcf"
    local particle = ParticleManager:CreateParticle(particleName, PATTACH_ABSORIGIN, caster)
    -- ParticleManager:SetParticleControl(particle, 0, caster:GetAbsOrigin())

    caster:SetModelScale(0)
    -- caster:AddNoDraw()
    StartAnimation(caster, {duration=5, activity=ACT_DOTA_DIE, rate=1})
end

function animation_attack_fix( keys )
    -- print("attack_animation")
    local caster = keys.caster
    StartAnimation(caster, {duration=5, activity=ACT_DOTA_CAST_ABILITY_1, rate=1.5})
end

function CustomAbility:On_dota_unit_event(data)
    print("On_dota_unit_event",data.eventtype)
    if data.eventtype == 20 then
        print("dota_unit_event 20")
        PrintTable(data)
        local hero = EntIndexToHScript(data.victim)
        local old_spell_steal = hero:FindAbilityByName("rubick_spell_steal")
        if old_spell_steal and old_spell_steal:GetLevel() > 0 then
            hero.SpellStealLevel = old_spell_steal:GetLevel()
        elseif not old_spell_steal then
            return
        elseif not hero:GetClassname() == "npc_dota_hero_rubick" then
            return
        end
        Timers:CreateTimer(0.035, function()
            ---[[删技能法
            local ability5 = hero:GetAbilityByIndex(5)
            hero:RemoveAbility(ability5:GetAbilityName())
            local new_spell_steal = hero:AddAbility("rubick_spell_steal")
            print("new_spell_steal1",new_spell_steal,hero.SpellStealLevel)
            new_spell_steal:SetLevel(hero.SpellStealLevel)
            --]]
            --[[交换法
            local ability5 = hero:GetAbilityByIndex(5)
            
            local new_spell_steal = hero:AddAbility("rubick_spell_steal")
            print("new_spell_steal1",new_spell_steal,hero.SpellStealLevel)
            new_spell_steal:SetLevel(hero.SpellStealLevel)
            hero:SwapAbilities("rubick_spell_steal", ability5:GetAbilityName() , true, false)
            hero:RemoveAbility(ability5:GetAbilityName())
            --]]
        end)

        -- hero.SpellStealCheck = true

        if not hero.SpellStealCheck then
            hero.SpellStealCheck = true
            -- print("SpellStealCheck")
            Timers:CreateTimer(0.04, function()
                local ability5 = hero:GetAbilityByIndex(5)
                if not ability5 then
                    return 0.04
                end
                -- if ability5:GetAbilityName() ~= "rubick_spell_steal" then
                --     -- print("~=")
                --     -- local old_spell_steal = hero:FindAbilityByName("rubick_spell_steal")
                --     hero:RemoveAbility(ability5:GetAbilityName())
                --     local new_spell_steal = hero:AddAbility("rubick_spell_steal")
                --     print("new_spell_steal2",new_spell_steal,hero.SpellStealLevel)
                --     new_spell_steal:SetLevel(hero.SpellStealLevel)
                --     -- hero:SwapAbilities("rubick_spell_steal", "rubick_empty2" , true, false)
                    
                -- end
                if not hero:HasModifier("modifier_rubick_spell_steal") then
                    local ability4 = hero:GetAbilityByIndex(4)
                    if ability4:GetAbilityName() ~= "rubick_empty1" then
                        hero:RemoveAbility(ability4:GetAbilityName())
                        hero:AddAbility("rubick_empty1")
                    end
                end
                return 0.04
            end)
        end
    end

  
end