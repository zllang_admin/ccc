function modaofengyin( keys )
	local caster = keys.caster
	local ability = keys.ability
	local spell_steal = caster:FindAbilityByName("rubick_spell_steal")
	local chixu = ability:GetSpecialValueFor( "chixu" )
	local shanghai = ability:GetSpecialValueFor( "shanghai" )
	local ability4 = caster:GetAbilityByIndex(4)
	local ability4Name = ability4:GetAbilityName()
	if ability4Name == "rubick_empty1" or ability4Name == "rubick_modaofengyin_empty" then
		local pID = caster:GetPlayerID()
        caster:Interrupt()
		SendErrorMessage(pID, "#error_no_spell_stolen")
		return
    end

    caster:RemoveAbility(ability4Name)
    local caster_fengyin_empty = caster:AddAbility("rubick_modaofengyin_empty")
	caster_fengyin_empty:SetLevel(1)
	caster_fengyin_empty:StartCooldown(chixu)

    local heroes = HeroList:GetAllHeroes()
    for k,target in pairs(heroes) do
		local ability_fengyin = target:FindAbilityByName(ability4Name)
		if ability_fengyin then
			local target_fengyin_empty = target:FindAbilityByName("rubick_modaofengyin_empty")
			if not target_fengyin_empty then
				target_fengyin_empty = target:AddAbility("rubick_modaofengyin_empty")
				target_fengyin_empty:SetLevel(1)
				target_fengyin_empty:StartCooldown(chixu)
			end
			target:SwapAbilities(ability4Name, "rubick_modaofengyin_empty",false, true)
			local ability4Level = ability_fengyin:GetLevel()
			local damage = {
            	victim = target,
            	attacker = caster,
            	damage = shanghai * ability4Level,
            	damage_type = DAMAGE_TYPE_MAGICAL,
            	ability = ability
        	}
    		ApplyDamage(damage)
    		StartSoundEvent("Hero_Visage.SoulAssumption.Target", target)
			local particleName = "particles/units/heroes/hero_undying/undying_tower_destruction.vpcf"
			local particle = ParticleManager:CreateParticle(particleName, PATTACH_ABSORIGIN_FOLLOW, target)
		end
	end

	Timers:CreateTimer(chixu, function()
		local new_ability4Name = caster:GetAbilityByIndex(4):GetAbilityName()
		if new_ability4Name == "rubick_modaofengyin_empty" then
			caster:RemoveAbility(new_ability4Name)
    		caster:AddAbility("rubick_empty1")
    	end
    	for k,target in pairs(heroes) do
			local ability_fengyin_empty = target:FindAbilityByName("rubick_modaofengyin_empty")
			if ability_fengyin_empty then
				target:SwapAbilities("rubick_modaofengyin_empty", ability4Name, false, true)
			end
		end
	end)
end

function modaofengyin_phase( keys )
	local caster = keys.caster
	local ability = keys.ability
	local spell_steal = caster:FindAbilityByName("rubick_spell_steal")
	local chixu = ability:GetSpecialValueFor( "chixu" )
	local ability4 = caster:GetAbilityByIndex(4)
	local ability4Name = ability4:GetAbilityName()
	if ability4Name == "rubick_empty1" or ability4Name == "rubick_modaofengyin_empty" then
		local pID = caster:GetPlayerID()
        caster:Interrupt()
		SendErrorMessage(pID, "#error_no_spell_stolen")
		return
    end
end