function jianrenshanghai( keys )
	local caster = keys.caster
	local target = keys.target
	
	local damage = {
            victim = target,
            attacker = caster,
            damage = keys.jianren_damage,
            damage_type = DAMAGE_TYPE_MAGICAL,
            ability = keys.ability
        }
    if not target:HasModifier(keys.modifier_damage) then 
        damage.damage = damage.damage *2
    end
    ApplyDamage(damage) 
end
function GenerateRing( event )
	local caster = event.caster
	local fv = caster:GetForwardVector()
	local origin = caster:GetAbsOrigin()
	local distance = event.Radius
    -- Gets 2 points facing a distance away from the caster origin and separated from each other at 30 degrees left and right
	ang_1 = QAngle(0, RandomInt(0, 90), 0)
	ang_2 = QAngle(0, RandomInt(90, 180), 0)
	ang_3 = QAngle(0, RandomInt(180, 270), 0)
    ang_4 = QAngle(0, RandomInt(270,360), 0)

    local front_position = origin + fv * distance
	point_1 = RotatePosition(origin, ang_1, front_position)
	point_2 = RotatePosition(origin, ang_2, front_position)
	point_3 = RotatePosition(origin, ang_3, front_position)
    point_4 = RotatePosition(origin, ang_4, front_position)

    local result = {
         point_1,
         point_2,
         point_3,
         point_4,

    }
    return result
end

function start_sound(keys)
    local caster = keys.caster
    StartSoundEvent("Hero_Juggernaut.BladeFuryStart",caster)
end

function end_sound(keys)
    local caster = keys.caster
    StopSoundEvent("Hero_Juggernaut.BladeFuryStart",caster)
end