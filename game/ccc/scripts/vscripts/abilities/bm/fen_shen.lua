
--[[ ============================================================================================================
    Author: Rook
    Date: February 2, 2015
    Called when Manta Style is cast.  Performs the first part of Manta Style's active, where the caster becomes
    invulnerable and disappears briefly.
    Additional parameters: keys.CooldownMelee, keys.InvulnerabilityDuration, keys.VisionRadius
================================================================================================================= ]]
function manta_datadriven_on_spell_start(keys)
    --杀死之前的幻象
    if keys.caster.illusionIndex == nil then
        keys.caster.illusionIndex = {}
    end
    for _,v in pairs(keys.caster.illusionIndex) do
        if v then
            local illusion = EntIndexToHScript(v)
            if illusion and IsValidEntity(illusion) and illusion:IsAlive() then
                --使用控制台命令script_reload后再添加新英雄主身会被remove
                illusion:RemoveSelf() 
                v = nil
            else
                v = nil
            end
        end
    end
    
    local manta_particle = ParticleManager:CreateParticle("particles/items2_fx/manta_phase.vpcf", PATTACH_ABSORIGIN_FOLLOW, keys.caster)
    Timers:CreateTimer({  --Start a timer that stops the particle after a short time.
        endTime = keys.InvulnerabilityDuration, --When this timer will first execute
        callback = function()
            ParticleManager:DestroyParticle(manta_particle, false)
        end
    })
    
    keys.caster:EmitSound("DOTA_Item.Manta.Activate")
    
    --Purge(bool RemovePositiveBuffs, bool RemoveDebuffs, bool BuffsCreatedThisFrameOnly, bool RemoveStuns, bool RemoveExceptions) 
    keys.caster:Purge(false, true, false, false, false)
    
    ProjectileManager:ProjectileDodge(keys.caster)  --Disjoints disjointable incoming projectiles.
    
    --The caster is briefly made invulnerable and disappears, while ground vision is supplied nearby.
    keys.ability:CreateVisibilityNode(keys.caster:GetAbsOrigin(), keys.VisionRadius, keys.InvulnerabilityDuration)
    -- keys.caster:AddNoDraw()
    keys.ability:ApplyDataDrivenModifier(keys.caster, keys.caster, "modifier_manta_datadriven_invulnerability", nil)
end


--[[ ============================================================================================================
    Author: Rook
    Date: February 2, 2015
    Called after Manta Style's brief invulnerability period ends.  Creates some illusions.
    Additional parameters: keys.IllusionIncomingDamageMelee, keys.IllusionOutgoingDamageMelee, 
        keys.IllusionIncomingDamageRanged, keys.IllusionOutgoingDamageRanged, keys.IllusionDuration,
================================================================================================================= ]]
function modifier_manta_datadriven_invulnerability_on_destroy(keys)

    -- keys.caster:RemoveNoDraw()
    keys.caster:Interrupt()
    --Set the health and mana values to those of the real hero.
    local caster_health = keys.caster:GetHealth()
    local caster_mana = keys.caster:GetMana()

    local caster_origin = keys.caster:GetAbsOrigin()
    
    --Illusions are created to the North, South, East, or West of the hero (obviously, both cannot be created in the same direction).
    local illusion1_direction = RandomInt(1, 4)
    local illusion2_direction = (RandomInt(1, 3) + illusion1_direction) % 4  --This will ensure that the illusions will spawn in different directions.
    
    local illusion1_origin = nil
    local illusion2_origin = nil
    
    if illusion1_direction == 1 then  --North
        illusion1_origin = caster_origin + Vector(0, 200, 0)
    elseif illusion1_direction == 2 then  --South
        illusion1_origin = caster_origin + Vector(0, -100, 0)
    elseif illusion1_direction == 3 then  --East
        illusion1_origin = caster_origin + Vector(200, 0, 0)
    else  --West
        illusion1_origin = caster_origin + Vector(-200, 0, 0)
    end
    
    if illusion2_direction == 1 then  --North
        illusion2_origin = caster_origin + Vector(0, 200, 0)
    elseif illusion2_direction == 2 then  --South
        illusion2_origin = caster_origin + Vector(0, -200, 0)
    elseif illusion2_direction == 3 then  --East
        illusion2_origin = caster_origin + Vector(200, 0, 0)
    else  --West
        illusion2_origin = caster_origin + Vector(-200, 0, 0)
    end
    
    --Create the illusions.
    local illusion1 = nil
    local illusion2 = nil

    --[[
    if keys.caster.illusion_one  then
        if not keys.caster.illusion_one:IsNull() then
            if keys.caster.illusion_one:IsAlive() then
                keys.caster.illusion_one:RemoveSelf() 
                keys.caster.illusion_one = nil
            else
                keys.caster.illusion_one = nil
            end
        end
    else
        keys.caster.illusion_one = nil
    end
    if keys.caster.illusion_two  then
        if not keys.caster.illusion_two:IsNull() then
            if keys.caster.illusion_two:IsAlive() then
                keys.caster.illusion_two:RemoveSelf() 
                keys.caster.illusion_two = nil
            else
                keys.caster.illusion_two = nil
            end
        end
    else
        keys.caster.illusion_two = nil
    end
    if keys.ability:GetLevel() >= 6 then
        OutgoingDamage = keys.IllusionOutgoingDamage
    else
        OutgoingDamage = -100
    end
    ]]
    illusion1 = create_illusion(keys, illusion1_origin, keys.IllusionIncomingDamage, OutgoingDamage, keys.IllusionDuration)
    illusion2 = create_illusion(keys, illusion2_origin, keys.IllusionIncomingDamage, OutgoingDamage, keys.IllusionDuration)

    keys.caster.illusionIndex[1] = illusion1:GetEntityIndex()
    keys.caster.illusionIndex[2] = illusion2:GetEntityIndex()

    Timers:CreateTimer(0.5, function()
        keys.caster:EmitSound("DOTA_Item.Manta.Activate")
        --print(keys.IllusionIncomingDamage, keys.IllusionOutgoingDamageHuan)
        for i=1,3 do
            local origin = illusion1:GetOrigin() + RandomVector(i*100) 
            local illusion = create_illusion(keys, origin, keys.IllusionIncomingDamage, keys.IllusionOutgoingDamageHuan, 5)
            keys.caster.illusionIndex[i+2] = illusion:GetEntityIndex()
            illusion:SetHealth(caster_health)
            illusion:SetMana(caster_mana)
        end
    end)
     
    --Reset our illusion origin variables because CreateUnitByName might have slightly changed the origin so that the unit won't be stuck.
    illusion1_origin = illusion1:GetAbsOrigin()
    illusion2_origin = illusion2:GetAbsOrigin()
    
    --Make it so all of the units are facing the same direction.
    local caster_forward_vector = keys.caster:GetForwardVector()
    illusion1:SetForwardVector(caster_forward_vector)
    illusion2:SetForwardVector(caster_forward_vector)
    
    --Randomize the positions of the illusions and the real hero.
    local hero_random_origin = RandomInt(1, 3)
    local illusion1_random_origin = (RandomInt(1, 2) + hero_random_origin) % 3  --This will ensure that this variable will be different from hero_random_origin.
    
    if hero_random_origin == 1 then
        keys.caster:SetAbsOrigin(caster_origin)
        if illusion1_random_origin == 2 then
            illusion1:SetAbsOrigin(illusion1_origin)
            illusion2:SetAbsOrigin(illusion2_origin)
        else  --illusion1_random_origin == 3
            illusion1:SetAbsOrigin(illusion2_origin)
            illusion2:SetAbsOrigin(illusion1_origin)
        end
    elseif hero_random_origin == 2 then
        keys.caster:SetAbsOrigin(illusion1_origin)
        if illusion1_random_origin == 1 then
            illusion1:SetAbsOrigin(caster_origin)
            illusion2:SetAbsOrigin(illusion2_origin)
        else  --illusion1_random_origin == 3
            illusion1:SetAbsOrigin(illusion2_origin)
            illusion2:SetAbsOrigin(caster_origin)
        end
    else  --hero_random_origin == 3
        keys.caster:SetAbsOrigin(illusion2_origin)
        if illusion1_random_origin == 1 then
            illusion1:SetAbsOrigin(caster_origin)
            illusion2:SetAbsOrigin(illusion1_origin)
        else  --illusion1_random_origin == 2
            illusion1:SetAbsOrigin(illusion1_origin)
            illusion2:SetAbsOrigin(caster_origin)
        end
    end

    
    illusion1:SetHealth(caster_health)
    illusion1:SetMana(caster_mana)
    illusion2:SetHealth(caster_health)
    illusion2:SetMana(caster_mana)
end