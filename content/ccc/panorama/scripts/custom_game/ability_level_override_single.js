"use strict";

function SetLevel( ability, query_unit)
{
	
	var controlable = Entities.IsControllableByPlayer( query_unit , Players.GetLocalPlayer() );
	var query_unit_team = Entities.GetTeamNumber( query_unit );
	var local_player = Players.GetLocalPlayer();
	var local_player_team = Players.GetTeam( local_player );
	var isTeammate = query_unit_team == local_player_team;
	// $.Msg("SetLevel ",query_unit," ",query_unit_team," ",local_player," ",local_player_team," ",isTeammate );
	var canUpgrade = ( Abilities.CanAbilityBeUpgraded( ability ) == AbilityLearnResult_t.ABILITY_CAN_BE_UPGRADED );
	var nRemainingPoints = Entities.GetAbilityPoints( query_unit );
	var currentLevel = Abilities.GetLevel( ability );
	var maxLevel = Abilities.GetMaxLevel( ability );
	$.GetContextPanel().SetHasClass("no_ability", (!isTeammate || ability == -1) );
	$.GetContextPanel().SetHasClass("can_be_upgrade", ( canUpgrade && nRemainingPoints > 0 ) );
	$( "#LVNum" ).text = currentLevel + "/" + maxLevel;
}

(function()
{
	$.GetContextPanel().SetLevel = SetLevel;
})();
