var Root = $.GetContextPanel()
var LocalPlayerID = Game.GetLocalPlayerID()


function Buy_Item(){	

	var event;

	event = "BuyMercenaries";
		
	EnoughStock = Root.ItemInfo.CurrentStock > 0;
		
	GameEvents.SendCustomGameEventToServer( event, { "PlayerID" : LocalPlayerID, "Shop" : Root.Entity, "UnitName" : Root.ItemName,unit:Players.GetLocalPlayerPortraitUnit()} );

}

function ShowToolTip(){ 
	var abilityButton = $( "#ItemButton" );
	//$.DispatchEvent( "DOTAShowAbilityTooltip", abilityButton, Root.ItemName );
}

function HideToolTip(){
	var abilityButton = $( "#ItemButton" );
	//$.DispatchEvent( "DOTAHideAbilityTooltip", abilityButton );
}

function Setup_Panel(){	

	//$.Msg("Setup_Panel")

	GameEvents.Subscribe( "unitshop_updateStock", Update_Central);
	
	var FullItemName = Root.ItemName;

	if (FullItemName.substring(0,5) == "item_")
		FullItemName = "DOTA_Tooltip_ability_" + FullItemName;
	
	var image_path = "url('file://{images}/custom_game/items/"+Root.ItemName+".png');"
	$("#ItemImage").style["background-image"] = image_path;
	
	$( "#GoldCost" ).text = Root.ItemInfo.GoldCost;  


	$( "#Stock").text = Root.ItemInfo.CurrentStock;
	
	$( "#ItemName").text = $.Localize(FullItemName);
	/*
	$.Msg("LumberCost--",Root.ItemInfo.LumberCost)
	if(Root.ItemInfo.LumberCost != 0){
		$( "#LumberCost" ).text = Root.ItemInfo.LumberCost;
	}else{
		$( "#LumberCost" ).visible = false;
	}
	if(Root.ItemInfo.FoodCost != null && Root.ItemInfo.FoodCost != 0)
		$("#Food").text = Root.ItemInfo.FoodCost; 	
	else
		$("#FoodPanel").visible = false;

	
	if(Root.ItemInfo.RequiredTier==9000){
		$( "#RequiredTier").text = "You need to upgrade your main building"
		Update_Tier_Required_Panels(Root.Tier)
	}
	else if(Root.ItemInfo.RequiredTier != 1){ 
		$( "#RequiredTier").text = "Requires: "+$.Localize(Root.Race+"_tier_"+Root.ItemInfo.RequiredTier);
		Update_Tier_Required_Panels(Root.Tier)
	}else if(Root.Hero){
		$( "#RequiredTier").text = "Revive this Hero instantly"
	}
	*/
}

function Update_Central(data){
	//$.Msg("Update_Central",data)
	// this checks that update is the correct entity shop based on EntityIndex
	if(data.Index != Root.Entity || data.Item.ItemName != Root.ItemName || data.Item.ShopID != Root.Entity){ 
		//$.Msg(Key+" is not "+Root.Entity) 
		return;
	}

	if(data.PlayerID != null){
		if(data.PlayerID != LocalPlayerID){
			$.Msg("Incorrect local PlayerID, returning");
			return;
		}
	}
	
	var item = Root.ItemName;
	var ItemValues = data.Item;

	if(ItemValues.CurrentStock != null){
		Root.ItemInfo.CurrentStock = ItemValues.CurrentStock;
		$("#Stock").text = ItemValues.CurrentStock;
	}
	
	if(ItemValues.RestockRate != null){
		if(ItemValues.CurrentRefreshTime > 1){
				$("#ItemMask").visible = true;
				//$.Msg(((100 / ItemValues.RestockRate) * ItemValues.CurrentRefreshTime)+"%")
				if( ItemValues.StockStartDelay != null){
					if( ItemValues.StockStartDelay != 0 ){
						$("#ItemMask").style["width"] = 100 - ((100 / ItemValues.StockStartDelay) * ItemValues.CurrentRefreshTime)+"%";
					}else{
						$("#ItemMask").style["width"] = 100 - ((100 / ItemValues.RestockRate) * ItemValues.CurrentRefreshTime)+"%";
					};
				}else{
					$("#ItemMask").style["width"] = 100 - ((100 / ItemValues.RestockRate) * ItemValues.CurrentRefreshTime)+"%";
				};
				$( "#ItemStockTimer" ).text = Math.ceil( ItemValues.RestockRate-ItemValues.CurrentRefreshTime );
				$( "#ItemStockTimer" ).visible = true;
		}
		else{
				$("#ItemMask").style["width"] = "0px";
				$("#ItemMask").visible = false;
				$("#ItemStockTimer").visible = false;
		}
	}
	
	/*
	if ($( "#ItemStockTimer" ).text){
		$.Msg("Update_Central"," ",$( "#ItemStockTimer" ).text," ",data.Index)
	}
	*/
	

	//$("#ItemImage").visible = true
	//$("#ItemMask").visible = false
	
	// gold update
	if(ItemValues.GoldCost != null || ItemValues.GoldCost != "0"){
		$( "#GoldCost" ).visible = true;
		$( "#GoldCost" ).text = ItemValues.GoldCost;
		Root.ItemInfo.GoldCost = ItemValues.GoldCost;
	}else{
		$( "#GoldCost" ).visible = false;
	}
	// lumber
	// 
	/*
	if(ItemValues.LumberCost != null || ItemValues.LumberCost != "0"){
		$( "#LumberCost" ).visible = true
		$( "#LumberCost" ).text = ItemValues.LumberCost;
		Root.ItemInfo.LumberCost = ItemValues.LumberCost
	}else{
		$( "#LumberCost" ).visible = false
	}

	
	if(ItemValues.RequiredTier != null){
		Root.ItemInfo.RequiredTier = ItemValues.RequiredTier
	}
	
	if(Root.Tavern){
		if(!data.Altar){
			$("#RequiredTier").text = "Requires: Altar"
			Update_Tier_Required_Panels(0)
		}else{
			$("#RequiredTier").text = "Upgrade your Main Hall"
		}
			
		if(data.Altar || data.Altar == null){
			Update_Tier_Required_Panels(data.Tier)
		}
	}else{
		Update_Tier_Required_Panels(data.Tier)
	}
	*/
}

function Update_Tier_Required_Panels(tier){
	
	if(tier >= Root.ItemInfo.RequiredTier)
	{
		$("#RequiredTier").visible = false;
		$("#ItemButton").enabled = true;
	}else{
		$("#ItemButton").enabled = false;
		$("#RequiredTier").visible = true;
	}
}

function Open_Shop_item(tier){
	
	//$.Msg("Open_Shop_item",entIndex);
}



(function () { 
	$.Schedule( 0.1, Setup_Panel); 
	//GameEvents.Subscribe( "Shops_Open", Open_Shop_item)
	//$.Schedule( 0.1, UpdateItem );	
})();