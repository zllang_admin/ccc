"use strict";
//=====================================
//               常用
//=====================================
var DebugPrint=true;

function print (arg) {
	$.Msg(arg);
}
function DPrint (name,content) {
	if (DebugPrint===false)return;
	$.Msg("-----------------JS:"+name+"-------------------");
	$.Msg(content);
	$.Msg("-----------------------------------------------");
}
function Error (content) {
	$.Msg("-----------------JS ERROR:-------------------");
	$.Msg(content);
	$.Msg("-----------------------------------------------");
}

function checkRate(num)  //判断正整数
{   
     var re = /^[1-9]+[0-9]*]*$/;

     if (!re.test(num))
    {
        return false;
    }else{
    	return true;
    }
}

//=====================================
//               面向对象
//=====================================
/*
	例如：
		var Base={    };
		var child=$.class(Base,{
			//成员变量


			//成员方法


		})
*/
/* 例如：
		//定义父类：
		var obj1 = {
		  'a': 's1',
		  'b': [1,2,3,{'a':'s2'}],
		  'c': {'a':'s3', 'b': [4,5,6]}
		}
		//继承父类：浅拷贝：$.extend({}, obj1); 深拷贝：$.extend(true, {}, obj1);
		var obj2 = $.extend(true, {}, obj1);
		//子类对象:
		obj2.a='s1s1';
		obj2.b[0]=100;
		obj2.c.b[0]=400;
		//输出测试：
		print(obj1);
		print(obj2);
*/
$.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !$.isFunction(target) ) {
		target = {};
	}

	// Extend $ itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( $.isPlainObject(copy) || (copyIsArray = $.isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && $.isArray(src) ? src : [];

					} else {
						clone = src && $.isPlainObject(src) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = $.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
}
$.isFunction=function( obj ) {
		return $.type(obj) === "function";
	}
$.isPlainObject=function( obj ) {
		if ( $.type( obj ) !== "object" ) {
			return false;
		}	
		return true;
	}
$.type=function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}
		return typeof obj;
	}
$.isArray = Array.isArray || function( obj ) {
    return $.type(obj) === "array";
}
$.class=function(base,constructor) {
	// 类
	return $.extend({},base,constructor);
}
//基类
var class_Base={
	//成员变量
	_self:undefined,
	//成员方法

};
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX







function CreateCounter() {
	// 计数器
	var count=0;
	var re=new Object();
	re.Add=function() {
		//计数加
		count+=1;
	}
	re.Sub=function () {
		// 计数减
		if (count>0) {
			count-=1;
		}
	}
	re.GetCount=function () {
		//获取当前计数
		return count;
	}
	return re; 
}
//=====================================
//               通讯响应
//=====================================
function TwoWayCommunicate (data) {
	// 双向通讯，需要返回数据
	//DPrint("Js Receive",data);
	if (data.body.test==1) {
		var cc=CreateCommunicateWithServer();
		cc.Send({a:"js test commu"},function  (data) {
			//测试
			print("js to lua test success");
		})
	
	};
	Feedback (data);
}
function ReceiveOnly (data) {
	// 只接收
	//DPrint("Js Receive",data);
	if(data.body.msg=="OnHeroInGame"){
		ViewOnHeroInGame();
	}
}

//=====================================
//               通讯核心
//=====================================
/*例如：
		var cc=CreateCommunicateWithServer();
		cc.Send({a:"js test commu"},function  (data) {
			//通讯成功后执行
			print("js to lua test success")
		})
*/

//Communicate type:

var GET=0;    //需要PUI返回数据
var SEND=1;	 //仅发送数据
var JSGET=2;   //js需要返回
var JSSEND=3;//js仅发送
var TaskStack;
var CMCStack;

var Feedback=function (data) {
	// 通讯反馈
	GameEvents.SendCustomGameEventToServer("SendToLua", data);
}
function ReceiveHandle (data) {
	//通讯交换器
	//print(data);
	if (data.head==GET) {
		var cmcStack=CMCStack.GetAllItem();
		for(var v in cmcStack){
			if(data.body.listener==cmcStack[v].listener){
				cmcStack[v].func(data,Feedback);
			}
		}
	}else if(data.head==SEND) {
		//ReceiveOnly (data);
		var cmcStack=CMCStack.GetAllItem();
		for(var v in cmcStack){
			if(data.body.listener==cmcStack[v].listener){
				cmcStack[v].func(data);
			}
		}
		
	}else if (data.head==JSGET) {
		var Callback=TaskStack.PopByIndex(data.back);
		Callback(data);
	}else if (data.head==JSSEND) {
		//无需任何处理
	};	
}
function AddCommunicateListener( name,func ) {
	// 添加交互监听
	CMCStack.Push({listener:name,func:func});
}
function CreateCommunicate() {
	// 与lua server通讯
	var cc=new Object();
	cc.Send=function  (Data,Callback) {
		if (typeof Callback != "function" && Callback != undefined) {
			Error("Callback's type is wrrong.");
		};
		if (Data==undefined) {
			Error("Data can't be undefined.");
		};
		var data=new Object();
		if (Callback != undefined) {
			data.head=JSGET;
			data.back=TaskStack.Push(Callback);
		}else{
			data.back=null;
			data.head=JSSEND;
		};
		data.body=Data;
		GameEvents.SendCustomGameEventToServer("SendToLua", data);
	}
	return cc;
}
function CreateTaskStack () {
	//PUI 任务堆
	var _Stack = new Object();
	var top = 1;
	var cc = new Object();
	cc.Push=function  (Item) {
		_Stack[top]=Item;
		var Index=top;
		top=top+1;
		return Index;
	}
	cc.PopByIndex=function  (Index) {
		var back=_Stack[Index];
		_Stack[Index]=null;
		return back;
	}
	cc.GetAllItem=function (){
		return _Stack;
	}	
	return cc;
}

//=====================================
//               初始化
//=====================================
function UtilsInit () {
	print("UtilsInit success!")
	//监听lua通讯
	GameEvents.Subscribe("SendToJs", ReceiveHandle);
	//初始化任务堆
	TaskStack=CreateTaskStack();
	CMCStack=CreateTaskStack();
}