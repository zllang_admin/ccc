"use strict";

var lv_panels = [];

function updata_list()
{	
	var list_panel = $("#List");
	if (!list_panel)
		return;
	var query_unit = Players.GetLocalPlayerPortraitUnit();
	var ability_num = Entities.GetAbilityCount( query_unit );

	var used_panel = 0;
	for (var i = 0; i < ability_num; i++)
	{
		var ability = Entities.GetAbility( query_unit, i );
		if ( ability == -1 )
			continue;

		if ( !Abilities.IsDisplayedAbility(ability) )
			continue;

		if (used_panel >= lv_panels.length)
		{
			var lv_panel = $.CreatePanel( "Panel", list_panel, "" );
			lv_panel.BLoadLayout( "file://{resources}/layout/custom_game/ability_level_override_single.xml", false, false );
			lv_panels.push( lv_panel );
		}
		var lv_panel = lv_panels[used_panel];
		lv_panel.SetLevel( ability,query_unit );
		used_panel++;
	};


	if (used_panel == 0)
		$.GetContextPanel().SetHasClass( "no_ability", true );
	else
	{	
		//$.Msg("used_panel num is "+used_panel);
		$.GetContextPanel().SetHasClass( "no_ability", false );
		for (var i = 1; i <= 6; i++)
		{
			var num_class = "num"+i;
			$.GetContextPanel().SetHasClass( num_class, (used_panel == i) );
		}	
	}

	for ( var i = used_panel; i < lv_panels.length; ++i )
	{
		var single_panel = lv_panels[ i ];
		single_panel.SetLevel( -1, -1 );
	}

	
}

(function (){
	GameEvents.Subscribe( "dota_portrait_ability_layout_changed", updata_list );
	GameEvents.Subscribe( "dota_player_update_selected_unit", updata_list );
	GameEvents.Subscribe( "dota_player_update_query_unit", updata_list );
	GameEvents.Subscribe( "dota_ability_changed", updata_list );
	GameEvents.Subscribe( "dota_hero_ability_points_changed", updata_list );
	GameEvents.Subscribe( "dota_player_learned_ability", updata_list );
	updata_list();
})();