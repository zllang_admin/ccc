"use strict";

var IsInShopRange = {};
var IsInUnitShopRange = {};
var IsShopActived = false;
var IsUnitShopActived = false;
var ShopButtonIcon = $("#ShopButtonIcon");
var UnitShopButtonIcon = $("#UnitShopButtonIcon");
var FlyingMountButtonIcon = $("#FlyingMountButtonIcon");
var lastSel = null;
var KeybindHint = $("#KeybindHint");
GameUI.FlyingMount = -1;


function OnShopClick() {
	var sel = Players.GetLocalPlayerPortraitUnit();
	GameEvents.SendCustomGameEventToServer("OnShopClick", {unitIndex:sel});
}

function OnUnitShopClick() {
	var sel = Players.GetLocalPlayerPortraitUnit();
	GameEvents.SendCustomGameEventToServer("OnUnitShopClick", {unitIndex:sel});
}

function OnFlyingMountClick() {
	if(GameUI.FlyingMount != -1){
		// $.Msg("Click")
		GameUI.SelectUnit( GameUI.FlyingMount, false );
	}else{
		GameEvents.SendCustomGameEventToServer("OnFlyingMountClick", {});
	}	
}

function OnFlyingMountDoubleClick(){
	if(GameUI.FlyingMount!=-1){
		// $.Msg("FlyingMount")
		// GameUI.SetCameraTarget(GameUI.FlyingMount);
		// $.Schedule(0.035, function() {
		// 	// $.Msg("-1")
		// 	GameUI.SetCameraTarget(-1);
		// 	});
		var playerid=Game.GetLocalPlayerID();
		GameEvents.SendCustomGameEventToServer( "center_hero_camera_unit", { "hero_entindex" : GameUI.FlyingMount, "playerid" : playerid} );
	}
}

function SetFlyingMountButton(arg) {
	GameUI.FlyingMount = arg.FlyingMount;
	// $.Msg("FlyingMount",FlyingMount)
	// $.Msg("FlyingMountButtonIcon",$("#FlyingMountButtonIcon"))
	if (FlyingMountButtonIcon!=null){
		if (GameUI.FlyingMount==-1){
			FlyingMountButtonIcon.SetImage("file://{resources}/images/custom_game/Shop/flying_mount_buy.png");
			$("#FlyingMountButtonLabel").text = "";
		}else{
			$("#FlyingMountButtonIcon").SetImage("file://{resources}/images/custom_game/Shop/flying_mount.png");
			$("#FlyingMountButtonLabel").text = $.Localize("#flying_mount");
			GameUI.SelectUnit( GameUI.FlyingMount, false );
		}
	}
	
}

function UpdateButtons(){
	var sel = Players.GetLocalPlayerPortraitUnit();
	//$.Msg("UpdateButtons--",sel,"--",IsInShopRange[sel],"--",IsShopActived,"--",IsUnitShopActived)
    if(IsInShopRange[sel] && !IsShopActived){
    	ShopAvailable();
    }else if(!IsInShopRange[sel] && IsShopActived){
    	ShopUnavailable();
    }
    if(IsInUnitShopRange[sel] && !IsUnitShopActived){
    	UnitShopAvailable();
    }else if(!IsInUnitShopRange[sel] && IsUnitShopActived){
    	UnitShopUnavailable();
    }
    
    //*/
	$.Schedule( 0.1, UpdateButtons );
}

function ShopAvailable() {
	// $.Msg("ShopAvailable");
	Game.EmitSound("Shop.Available");
	if(ShopButtonIcon!=null){
		ShopButtonIcon.SetImage("file://{resources}/images/custom_game/Shop/shop1.png");
	}
	IsShopActived = true
}

function ShopUnavailable() {
	// $.Msg("ShopUnavailable");
	Game.EmitSound("Shop.Unavailable");
	if(ShopButtonIcon!=null){
		ShopButtonIcon.SetImage("file://{resources}/images/custom_game/Shop/shop.png");
	}
	IsShopActived = false;
}

function UnitShopAvailable() {
	// $.Msg("UnitShopAvailable");
	Game.EmitSound("Shop.Available");
	if(ShopButtonIcon!=null){
		UnitShopButtonIcon.SetImage("file://{resources}/images/custom_game/Shop/unitshop.png");
	}
	IsUnitShopActived = true;
}

function UnitShopUnavailable() {
	// $.Msg("UnitShopUnavailable");
	Game.EmitSound("Shop.Unavailable");
	if(ShopButtonIcon!=null){
		UnitShopButtonIcon.SetImage("file://{resources}/images/custom_game/Shop/unitshop0.png");
	}
	IsUnitShopActived = false;
}

function ActivateShopButton(arg) {
	// $.Msg("ActivateShopButton",arg.Activate);
	IsInShopRange[arg.unit] = arg.Activate;
}

function ActivateUnitShopButton(arg) {
	// $.Msg("ActivateUnitShopButton",arg.Activate);
	IsInUnitShopRange[arg.unit] = arg.Activate;
}

function BuyElf(arg) {
	// 购买小精灵 arg=1 光明  arg=2 黑暗
	// $.Msg(arg);
	GameEvents.SendCustomGameEventToServer("BuyElf", {type:arg});
}
function BuyMercenaries(arg) {
	//雇佣兵
	// $.Msg(arg);
	GameEvents.SendCustomGameEventToServer("BuyMercenaries", {type:arg,unit:Players.GetLocalPlayerPortraitUnit()});
}

function UpdateSelectUnit(){
	var query_unit = Players.GetLocalPlayerPortraitUnit();
	if (KeybindHint == null){
		return;
	}
	// $.Msg(Entities.GetUnitName( query_unit ) )
	if (Entities.GetUnitName( query_unit ) == "npc_dota_hero_wisp")	{
		// $.Msg("KeybindHint",KeybindHint);
		KeybindHint.SetHasClass("Hidden", false);
	}else{
		// $.Msg("else",KeybindHint);
		KeybindHint.SetHasClass("Hidden", true);
	}

}


(function(){
  var panel = $.GetContextPanel();


  //containerPanel = $.CreatePanel( "Panel", panel, "" );
  //containerPanel.BLoadLayout("file://{resources}/layout/custom_game/containers/container.xml", false, false);

  	GameEvents.Subscribe( "ActivateShopButton", ActivateShopButton); 
  	GameEvents.Subscribe( "ActivateUnitShopButton", ActivateUnitShopButton); 
  	GameEvents.Subscribe( "SetFlyingMountButton", SetFlyingMountButton); 

  	GameEvents.Subscribe( "dota_player_update_selected_unit", UpdateSelectUnit );
	GameEvents.Subscribe( "dota_player_update_query_unit", UpdateSelectUnit );

	$.Schedule( 0.1, UpdateButtons );  
})();





