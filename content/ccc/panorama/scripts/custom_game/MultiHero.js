"use strict";
/*=============================================================================================================*/
/*==============================================工具函数=======================================================*/
function print (arg) {
	$.Msg(arg);
}
function dprint (name,content) {
	// body...
	$.Msg("==============="+name+"===================");
	$.Msg(content);
	$.Msg("==========================================");
}
/*=============================================================================================================*/
/*==============================================测试函数=======================================================*/
function test01() {
	// body...
	dprint("js test function","");
	dprint("child",$("#elf_image_panel").GetChild(0));
	$("#elf_image_panel").GetChild(0).select_call();
}
function button_test_click() {
	var id=Game.GetLocalPlayerID();
	var ids=Game.GetAllPlayerIDs();
	var info = new Object();
	//print(ids);

	for(var playerid of ids);
	{
		var plyinfo=Game.GetPlayerInfo(playerid);
		info.playerid=plyinfo;
	}
	GameEvents.SendCustomGameEventToServer("test", info);
}
function button_test2_click() {

	var id=Game.GetLocalPlayerID();
	var info = new Object();
	info.playerid=id;
	GameEvents.SendCustomGameEventToServer("create_hero_lua", info);
}
function button_test1_click() {
	var id=Game.GetLocalPlayerID();
	var ids=Game.GetAllPlayerIDs();
	var info = new Object();
	//print(ids);
	for(var playerid of ids);
	{
		var plyinfo=Game.GetPlayerInfo(playerid);
		info.playerid=plyinfo;
	}
	GameEvents.SendCustomGameEventToServer("test1", info);
}

/*=============================================================================================================*/
/*==============================================通用函数=======================================================*/
//[[添加来至Lua的单位选择命令监听]]
AddCommunicateListener("OnSelectUnit",function (data,callback) {
	//选择单位
	GameUI.SelectUnit( data.body.entindex,false);

})
//[[英雄头像点击回调]]
var	HeroImage_clickcallback=function (arg) {
	if (arg.active_ability == -1){
		UnitSelect(arg.heroindex,arg.playerid,arg.heroname);
	}else{
		var caster = Players.GetLocalPlayerPortraitUnit();
		var target = arg.heroindex;
		var ability = arg.active_ability;
		GameEvents.SendCustomGameEventToServer("cast_on_heroimage",{caster:caster,target:target,ability:ability} );
		// Abilities.ExecuteAbility( -1, caster, true );
	}	
}
//[小精灵头像点击回调]]
var	ElfImage_clickcallback=function (arg) {
	UnitSelect(arg.heroindex,arg.playerid,arg.heroname);
}
//[[返回对象长度]]
var	Ocount=function (o) {
	    var t = typeof o;
	    if(t == 'string'){
	            return o.length;
	    }else if(t == 'object'){
	            var n = 0;
	            for(var i in o){
	                    n++;
	            }
	            return n;
	    }
	    return false;
	}
var	UnitSelect=function (heroindex, playerid, heroname) {		
		clicked_heropanel(heroindex, playerid, heroname);
	}
//[[判断双击]]
function PressCount() {
	var count = 0;
	function _Press () {
		count=count+1;
		if (count >= 2){
			count=0;
			return true;
		}else{
			$.Schedule(0.31, function() {
				count = 0;
			})
			return false;
		}
		
	}
	return _Press;
}
var IsDoubleClick=PressCount();

var double_clicked = [];
function clicked_heropanel(hero, playerid, heroname){
	if (GameUI.IsShiftDown() == true) {
		GameUI.SelectUnit( hero, true )	;
	}
	else{
		GameUI.SelectUnit( hero, false );			
	}	
	double_clicked[hero] = double_clicked[hero] + 1;
	if (double_clicked[hero] >= 2){
		if (heroname == "npc_dota_hero_wisp")
		{
			// GameUI.SetCameraTarget(hero);
			// $.Schedule(0.035, function() {
			// 	GameUI.SetCameraTarget(-1);
			// });
			
			GameEvents.SendCustomGameEventToServer( "center_hero_camera_unit", { "hero_entindex" : hero, "playerid" : playerid} );
		}else{
			// GameUI.SetCameraTarget(hero);
			// $.Schedule(1, function() {
			// 	GameUI.SetCameraTarget(-1);
			// });
			// $.Msg(hero,Entities.IsIllusion(hero),Entities.GetUnitName(hero));
			GameEvents.SendCustomGameEventToServer( "center_hero_camera", { "heroname" : heroname, "playerid" : playerid} );			
		}
	}
	$.Schedule(0.3, function() {
		double_clicked[hero] = 0;
	});
}
/*=============================================================================================================*/
/*==============================================英雄相关=======================================================*/
var HeroList=new Array();
var CHeroHandle=$.class({},{
	//成员变量
	

	//成员方法
	New:function () {
		//构造函数
		var re=$.class(CHeroHandle,{});
		return re;
	},
	CreateHero:function (data) {
		//[[创建英雄]]
		var self=this;
		var HeroPanel = $.CreatePanel( "Panel", $("#image_panel"), "" );
		data.click_callback=HeroImage_clickcallback;
		HeroPanel.data = data;
		HeroPanel.BLoadLayout("file://{resources}/layout/custom_game/HeroImagePanel.xml", false, false);
		HeroList.push(data.entindex);
	},
	HeroRevive:function(heroindex, playerid, heroname) {
		this.test("aaaa#####################sadsas");
	},
	OnKeyPress:function (arg) {
		// [[按键响应F1——F8]]
		var HeroNum=HeroList.length;	
		if (arg<=HeroNum && HeroNum != 0){
			$("#image_panel").GetChild(arg-1).data.select_call();
		}
	},
	HeroInit:function () {
		AddCommunicateListener("OnCreateHero",function (data,callback) {
			//选择单位
			$.Msg("OnCreateHero",data);
			CHeroHandle.CreateHero(data.body);
		});
	}
});
var HeroHandle=CHeroHandle.New();
/*=============================================================================================================*/
/*==============================================精灵相关=======================================================*/

var Elf=new Array();
var LastIndex=0;
var	NowIndex=0;
var CElfHandle=$.class({},{
	//成员变量
	

	//成员方法
	New:function () {
		//构造函数
		var re=$.class(CElfHandle,{});
		return re;
	},
	CreateElf:function (data) {
		// 创建
		var self=this;
		var ElfPanel = $.CreatePanel( "Panel", $("#elf_image_panel"), "" );
		data.click_callback=ElfImage_clickcallback;
		ElfPanel.data =data;
		ElfPanel.BLoadLayout("file://{resources}/layout/custom_game/ElfImagePanel.xml", false, false);
		Elf.push(data.entindex);
	},
	RemoveElf:function (arg) {
		// 移除精灵面板
		var elf_panel=$("#elf_image_panel");
		var child_count=elf_panel.GetChildCount();
		for (var i = 0; i < child_count; i++) {
			var child=elf_panel.GetChild(i);
			if (child.data.entindex == arg.entindex)
			{
				Elf.pop(arg.entindex);
				child.RemoveAndDeleteChildren();
				child.DeleteAsync(0);
				return;
			};
		};
	},
	OnF8Select:function () {
		//[[F8按键响应]]
		var ElfNum=Elf.length; 
		var doubleclick =IsDoubleClick();
		if (ElfNum==0){ return; }
		if(NowIndex < ElfNum-1){
			if(doubleclick){
				if(LastIndex==0){
					NowIndex=LastIndex;
					LastIndex=ElfNum-1;
				}else{
					NowIndex=LastIndex;
					LastIndex=LastIndex-1;
				}
				$("#elf_image_panel").GetChild(LastIndex).select_call();
				$("#elf_image_panel").GetChild(LastIndex).select_call();
			}else{
				$("#elf_image_panel").GetChild(NowIndex).select_call();
				LastIndex=NowIndex;
				NowIndex=NowIndex+1;
			} 		
		}else{
			if(doubleclick){
				if(LastIndex==0){
					NowIndex=LastIndex;
					LastIndex=ElfNum-1;
				}else{
					NowIndex=LastIndex;
					LastIndex=LastIndex-1;
				}
				$("#elf_image_panel").GetChild(LastIndex).select_call();
				$("#elf_image_panel").GetChild(LastIndex).select_call();
			}else{
				$("#elf_image_panel").GetChild(NowIndex).select_call();
				LastIndex=NowIndex;
				NowIndex=0;
			}		
		}
	},
	ElfInit:function () {
		AddCommunicateListener("OnCreateElf",function (data,callback) {
			//[[创建小精灵面板]]
			CElfHandle.CreateElf(data.body);
		});
		AddCommunicateListener("OnRemoveElf",function (data,callback) {
			//[[移除小精灵面板]]
			CElfHandle.RemoveElf(data.body);
		});
	}

});
var ElfHandle=CElfHandle.New();

/*===================================================================================*/
/*                                       商店实体点击事件                             */
/*====================================================================================*/
// 获取鼠标的焦点单位
function GetMouseTarget ( heroIndex ) {
	var mouseEntities = GameUI.FindScreenEntities( GameUI.GetCursorPosition() );

	mouseEntities = mouseEntities.filter( function (e) { return Entities.IsShop(e.entityIndex);} );

	for( var ent of mouseEntities )
	{
		if( !ent.accurateCollision )continue;

		return ent.entityIndex;
	}

	for( var ent of mouseEntities )
	{
		return ent.entityIndex;
	}
	
	return -1;
}
/*
// 鼠标事件
GameUI.SetMouseCallback(function ( eventName, arg ) {
	var nMouseButton = arg;
	var CONSUME_EVENT = true;
	var CONTINUE_PROCESSING_EVENT = false;
	if ( GameUI.GetClickBehaviors() !== CLICK_BEHAVIORS.DOTA_CLICK_BEHAVIOR_NONE )
		return CONTINUE_PROCESSING_EVENT;

	if ( eventName === "pressed" )
	{
		// Right-click is use ability #2
		if ( arg === 1 )
		{
			var entIndex = Players.GetLocalPlayerPortraitUnit();
			var shops = GetMouseTarget(entIndex);

			if( shops != -1 && Entities.IsShop(box) )
			{
				shop.Onclick();
			}

			return CONTINUE_PROCESSING_EVENT;
		}
	}

	return CONTINUE_PROCESSING_EVENT;
});
*/

function OnF1(arg) {HeroHandle.OnKeyPress(1);}
function OnF2(arg) {HeroHandle.OnKeyPress(2);}
function OnF3(arg) {HeroHandle.OnKeyPress(3);}
function OnF4(arg) {HeroHandle.OnKeyPress(4);}
function OnF5(arg) {HeroHandle.OnKeyPress(5);}
function OnF6(arg) {HeroHandle.OnKeyPress(6);}
function OnF7(arg) {HeroHandle.OnKeyPress(7);}
function OnF8(arg) {HeroHandle.OnKeyPress(8);}
// function OnF8(arg) {ElfHandle.OnF8Select();}
function OnCapsLock(arg) {
	// $.Msg("CapsLock");
	var sel = Players.GetLocalPlayerPortraitUnit();
	GameEvents.SendCustomGameEventToServer( "OnShopKeyDown", {unitIndex:sel} );
}
/*=============================================================================================================*/
/*==============================================初始化========================================================*/
function MultiInit() {
    HeroHandle.HeroInit();
    ElfHandle.ElfInit();
}

/*-------------------------------------------------------------*/
/*-------------------------------------------------------------*/
(function(){
	$.Msg("MultiHero load success");
    MultiInit();   
    Game.AddCommand("F1", OnF1, "" , 0);
	Game.AddCommand("F2", OnF2, "" , 0);
	Game.AddCommand("F3", OnF3, "" , 0);
	Game.AddCommand("F4", OnF4, "" , 0);
	Game.AddCommand("F5", OnF5, "" , 0);
	Game.AddCommand("F6", OnF6, "" , 0);
	Game.AddCommand("F7", OnF7, "" , 0);
	Game.AddCommand("F8", OnF8, "" , 0);
	Game.AddCommand("CapsLock", OnCapsLock, "" , 0);
  
    //GameEvents.Subscribe( "create_elf",  ElfHandle.CreateElf);
	//GameEvents.Subscribe( "remove_elf", ElfHandle.removeElf);
})();
		