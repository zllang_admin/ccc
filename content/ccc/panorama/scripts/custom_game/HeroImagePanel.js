"use strict";

var _data = new Object();
function heroimage_click () {
	var playerid=Game.GetLocalPlayerID();
	var active_ability = Abilities.GetLocalPlayerActiveAbility()
	var arg={heroindex:_data.entindex,playerid:playerid,heroname:_data.heroname,active_ability:active_ability};
	// $.Msg("active_ability  ",active_ability);
	_data.click_callback(arg);
}
var select_call = heroimage_click;

function Init(data) {
	$('#DOTAHeroImage_id').heroname=data.heroname;
	$.GetContextPanel().data.select_call = select_call;
	_data=data;
	updateHero();
}
function print (arg) {
	$.Msg(arg);
}
function dprint (name,content) {
	// body...
	$.Msg("---------------"+name+"-------------------");
	$.Msg(content);
	$.Msg("------------------------------------------");
}
/*---------------------------------------------*/
var healthPanel =$("#heamlstrand");
var manaPanel=$("#manastrand");
var deadPanel=$("#layout_die");
var injuryPanel=$("#layout_injury");
var abilityPointLabel = $("#AbilityPoint");
var IsDead = false;
var lasthealth=0;
var IsinjuryPanelOpen=false;
var BuyBacker = {};
var IsRespawn = {};
var BuyBackCost = {};

function updateHero() {
	$.Schedule(0.1,function () {
		var health=Entities.GetHealth(_data.entindex);
		var healthmax =Entities.GetMaxHealth(_data.entindex);
		var healthPercent=(health/healthmax)*100;
	    var mana=Entities.GetMana(_data.entindex);
	    var manamax=Entities.GetMaxMana(_data.entindex);
	    var manaPercent=(mana/manamax)*100;
	    var sel = Players.GetLocalPlayerPortraitUnit();
	    healthPanel.style['width']=(86*healthPercent/100)+'px';
	    manaPanel.style['width']=(86*manaPercent/100)+'px';	
	    var abilityPoints = Entities.GetAbilityPoints( _data.entindex )
	    if(abilityPoints == 0){
	    	abilityPointLabel.SetHasClass("Hidden", true);
	    }else{
	    	abilityPointLabel.text = abilityPoints;
	    	abilityPointLabel.SetHasClass("Hidden", false);
	    }

	    var hero_team = Entities.GetTeamNumber( _data.entindex );
	    var sel_team = Entities.GetTeamNumber( sel );
	    var isTeammate = hero_team == sel_team;
	    // $.Msg("BuyBack",isTeammate,hero_team,sel_team)
	    if(healthPercent == 0 && isTeammate){
	    	if (IsDead==false && IsRespawn[_data.entindex]!=true){
	    		deadPanel.SetHasClass("Hidden" , false);    		
	    		if(GameUI.IsDeathMatch!=true){
	    			if(BuyBacker[sel]==1){
	    				if($("#CBuyBack").BHasClass("Hidden")){
	    					$("#CBuyBack").SetHasClass("Hidden", false);
	    					$("#HeroReviveButtonCost").text= BuyBackCost[_data.entindex].toString();
	    					Game.EmitSound("Shop.Available");
	    				}	
	    			}else if(Entities.GetUnitName( sel ) == "npc_building_shangdianshouwei"){
	    				if($("#CBuyBack").BHasClass("Hidden")){
	    					$("#CBuyBack").SetHasClass("Hidden", false);
	    					$("#HeroReviveButtonCost").text= BuyBackCost[_data.entindex].toString();
	    					Game.EmitSound("Shop.Available");
	    				}
	    			}else{
	    				if(!$("#CBuyBack").BHasClass("Hidden")){
	    					$("#CBuyBack").SetHasClass("Hidden", true);
	    					Game.EmitSound("Shop.Unavailable");
	    				}
	    			}
	    		}
	    		IsDead==true;
	    	}
	    }else{
				deadPanel.SetHasClass("Hidden" , true);
				$("#CBuyBack").SetHasClass("Hidden", true);
				
				IsDead=false;
	    }
	    if((healthPercent-lasthealth) < 0) {
	    	if(IsinjuryPanelOpen==false){
	    		injury ();
	    		IsinjuryPanelOpen=true;
	    	}
	    }
	    lasthealth=healthPercent;
	    updateHero();
	});
}
function injury () {
	injuryPanel.SetHasClass("Hidden" , false);
	$.Schedule(0.3,function  () {
		injuryPanel.SetHasClass("Hidden" , true);
		IsinjuryPanelOpen=false;
	});

}
function BuyBack() {
	//买活
	GameEvents.SendCustomGameEventToServer("BuyBack", {entIndex:_data.entindex,unit:Players.GetLocalPlayerPortraitUnit()});
}

function OnDragEnter( panelId, dragCallbacks )
{
	// $.Msg("OnDragEnter");
	//$.Msg(panelId,_data.entindex, dragCallbacks);
	GameUI.ItemDragTarget = _data.entindex;
}

function OnDragLeave( panelId, dragCallbacks )
{
	// $.Msg("OnDragLeave");
	//$.Msg(panelId,_data.entindex, dragCallbacks);
	GameUI.ItemDragTarget = null;
}

function OnDragDrop( panelId, dragCallbacks )
{
	// $.Msg("OnDragDrop");
	$.Msg(panelId,_data.entindex, dragCallbacks);
	
	if (GameUI.ItemDragTarget !== null){
		GameUI.TrueItemDragTarget = GameUI.ItemDragTarget ;
	}
}

function SetBuyBacker( arg )
{
	// $.Msg("SetBuyBacker",arg);
	BuyBacker[arg.unit] = arg.Buyable;

}

function SetRespawn( arg )
{
	// $.Msg("SetRespawn",arg);
	IsRespawn[arg.unit] = arg.Respawn;

}

function SetBuyBackCost( arg )
{
	//$.Msg("SetuyBackCost",arg);
	BuyBackCost[arg.unit] = arg.BuyBackCost;

}

(function () {
	print("SUBPanel success!");


    Init($.GetContextPanel().data);

    $.RegisterEventHandler( 'DragEnter', $.GetContextPanel(), OnDragEnter );
	$.RegisterEventHandler( 'DragDrop', $.GetContextPanel(), OnDragDrop );
	$.RegisterEventHandler( 'DragLeave', $.GetContextPanel(), OnDragLeave );
	//$.RegisterEventHandler( 'DragEnd', $.GetContextPanel(), OnDragEnd );
	//
	GameEvents.Subscribe( "SetBuyBacker", SetBuyBacker );
	GameEvents.Subscribe( "SetRespawn", SetRespawn );
	GameEvents.Subscribe( "SetBuyBackCost", SetBuyBackCost );
})();

/*
function OnDragEnd( panelId, dragCallbacks )
{
	$.Msg("OnDragEnd");
	$.Msg(panelId,_data.entindex, dragCallbacks);
	//GameEvents.SendCustomGameEventToServer( "Containers_OnDragWorld", {unit:Players.GetLocalPlayerPortraitUnit(), contID:-1, itemID:m_Item, slot:m_ItemSlot, position:position, entity:_data.entindex} );
}
*/