"use strict";

function DismissMenu()
{
	$.DispatchEvent( "DismissAllContextMenus" );
}

function OnSell()
{
	//*
	var itemID = $.GetContextPanel().Item;
	var selectID = Players.GetLocalPlayerPortraitUnit();
	// GameUI.SellingItem = itemID;
	$.Msg("OnSell",itemID,selectID);
	GameEvents.SendCustomGameEventToServer("Containers_OnSell", {itemID:itemID, selectID:selectID});
	DismissMenu();
}

function OnDisassemble()
{
	Items.LocalPlayerDisassembleItem( $.GetContextPanel().Item );
	DismissMenu();
}

function OnShowInShop()
{
	var itemName = Abilities.GetAbilityName( $.GetContextPanel().Item );
	//$.Msg("OnSell",$.GetContextPanel().Item,itemName);

	var itemClickedEvent = {
		"link": ( "dota.item." + itemName ),
		"shop": 0,
		"recipe": 0
	};
	GameEvents.SendEventClientSide( "dota_link_clicked", itemClickedEvent );
	DismissMenu();
}

function OnDropFromStash()
{
	Items.LocalPlayerDropItemFromStash( $.GetContextPanel().Item );
	DismissMenu();
}

function OnMoveToStash()
{
	Items.LocalPlayerMoveItemToStash( $.GetContextPanel().Item );
	DismissMenu();
}

function OnAlert()
{
	Items.LocalPlayerItemAlertAllies( $.GetContextPanel().Item );
	DismissMenu();
}
