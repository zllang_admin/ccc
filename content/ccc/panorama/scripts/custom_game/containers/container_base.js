"use strict";
/*
function OpenInventory()
{
  var args=new Object();
  args.unitIndex=Players.GetLocalPlayerPortraitUnit();
  GameEvents.SendCustomGameEventToServer( "OpenInventory", args );
}

function DefaultInventory()
{
  var args=new Object();
  args.unitIndex=Players.GetLocalPlayerPortraitUnit();
  GameEvents.SendCustomGameEventToServer( "DefaultInventory", args );
}

*/
var containers = {};
var ContainersOpenState = {};

var lastUnit = null;

GameUI.StockTime = {};
GameUI.ItemImage = {};


function DisableFocus(panel)
{
  panel.SetDisableFocusOnMouseDown(true);
  //panel.SetAcceptsFocus(false);
  
  for (var i=0; i<panel.GetChildCount(); i++){
    DisableFocus(panel.GetChild(i));
  }
}

function CreateContainer(msg)
{
  //$.Msg("CreateContainer -- ", msg);
  var panel = $.GetContextPanel();
  var id = msg.id; 
  var containerPanel = containers[id];

  if (!containerPanel){
    containerPanel = $.CreatePanel( "Panel", panel, "" );
    
    containerPanel.BLoadLayout("file://{resources}/layout/custom_game/containers/container.xml", false, false);
    containers[id] = containerPanel;
    
    containerPanel.NewContainer(id);

    containerPanel.unitIndex=msg.unitIndex;
    DisableFocus(panel);
    containerPanel.visible = false;
  }
}

function OpenContainer(msg)
{
  
  //$.Msg("OpenContainer -- ", msg);
  var panel = $.GetContextPanel();
  var id = msg.id; 
  var containerPanel = containers[id];

  if (!containerPanel){
    //$.Msg("containerPanel","false");
    containerPanel = $.CreatePanel( "Panel", panel, "" );
    
    containerPanel.BLoadLayout("file://{resources}/layout/custom_game/containers/container.xml", false, false);
    containers[id] = containerPanel;
    
    containerPanel.NewContainer(id);

    containerPanel.unitIndex=msg.unitIndex;
    DisableFocus(panel);
    //*/
  }
  else{
    //$.Msg("containerPanel","true");
    containerPanel.OpenContainer();
  }
  //*/
}


var CloseAllContainer = function ()
{
  // $.Msg("CloseAllContainer -- ");
  for(var id in containers){
    // $.Msg("id -- ",id);
    var containerPanel = containers[id];
    if (containerPanel){
      containerPanel.CloseContainer();
    }
    else{
      $.Msg("[container_base.js] Close container for id '" + id + "' unable to find existing container.");
    }
  }
  
}

function CloseContainer(msg)
{
  var id = msg.id;
  var containerPanel = containers[id];
  $.Msg("CloseContainer -- ", id);
  // $.Msg(containerPanel)
  if (containerPanel){
    containerPanel.CloseContainer();
  }
  else{
    $.Msg("[container_base.js] Close container for id '" + id + "' unable to find existing container.");
  }
}

function DeleteContainer(msg)
{
   $.Msg("DeleteContainer -- ", msg); 
   var id = msg.id;
   var containerPanel = containers[id];
   if (containerPanel){
      containerPanel.DeleteContainer();
      containerPanel.DeleteAsync(1);
      delete containers[id];
   }
   else{
    //$.Msg("[container_base.js] Delete container for id '" + id + "' unable to find existing container.")
   } 
}
 

function ExecuteProxy(msg)
{
  //$.Msg("ExecuteProxy");
  var localHeroIndex = Players.GetPlayerHeroEntityIndex( Players.GetLocalPlayer());
  var abil = Entities.GetAbilityByName( localHeroIndex , "containers_lua_targeting" );
  if (abil === -1)
     abil = Entities.GetAbilityByName( localHeroIndex , "containers_lua_targeting_tree" );
  
  if (abil !== -1){
    Abilities.ExecuteAbility( abil, localHeroIndex, false );

    var fun = function(){
      var behaviors = GameUI.GetClickBehaviors();
      if (behaviors != CLICK_BEHAVIORS.DOTA_CLICK_BEHAVIOR_CAST){
        GameEvents.SendCustomGameEventToServer( "Containers_HideProxy", {abilID:abil} );
        return;
      }
      $.Schedule(1/60, fun);
    };

    $.Schedule(1/60, fun);    
  }
  else{
    $.Msg("'containers_lua_targeting' ability not found.");
  }
}

var EntityShops = {};
var lastSel = null; 
var double_clicked = 0;

function CheckShop()
{
  var sel = Players.GetLocalPlayerPortraitUnit();
  //*$.Msg("CheckShop ", sel, ' -- ', lastSel,Entities.GetUnitName( lastSel ));
  if (sel !== lastSel){   
    if(Entities.GetUnitName( sel ) == "npc_dummy_shop" ){
      // $.Msg("!==SelectLastSel ", ' -- ', Entities.GetUnitName( sel ), ' -- ',Entities.GetUnitName( lastSel )); 
      GameEvents.SendCustomGameEventToServer( "Containers_Select", {entity:sel,lastentity:lastSel} );
      GameUI.SelectUnit(lastSel, false);

    }else if(Entities.GetUnitName( sel ) == "npc_dota_courier" ){
      if(GameUI.FlyingMount != -1){
        GameUI.SelectUnit( GameUI.FlyingMount, false );
        lastSel = GameUI.FlyingMount;

        double_clicked = double_clicked + 1;
        if(double_clicked>=2){
          // GameUI.SetCameraTarget(GameUI.FlyingMount);
          // $.Schedule(0.035, function() {
          //   GameUI.SetCameraTarget(-1);
          //   });
          var playerid=Game.GetLocalPlayerID();
          GameEvents.SendCustomGameEventToServer( "center_hero_camera_unit", { "hero_entindex" : GameUI.FlyingMount, "playerid" : playerid} );
        }
        $.Schedule(0.3, function() {
          double_clicked = 0;
          });
      }else{
        GameUI.SelectUnit(lastSel, false);
      }
    }else{
      // $.Msg("==SelectLastSel ", ' -- ', Entities.GetUnitName( sel ), ' -- ',Entities.GetUnitName( lastSel ));
      // GameEvents.SendCustomGameEventToServer( "Containers_Select", {entity:sel,lastentity:lastSel} );     
      if(Entities.IsControllableByPlayer( sel, Players.GetLocalPlayer() )){
        lastSel = sel;
      }  
    }
    //*/
  }

  for(var id in containers){
    var containerPanel = containers[id];
    if (containerPanel){
      if (ContainersOpenState[id] !== containerPanel.visible){
        ContainersOpenState[id] = containerPanel.visible;
        GameEvents.SendCustomGameEventToServer( "SetContainerOpenState", {containerID:id, IsOpen:containerPanel.visible} );
        return;
      }
    }
  }
}

function Containers_Precache() {
  //$.Msg("Containers_Precache");
  GameEvents.SendCustomGameEventToServer( "Containers_Precache", {} );
}

function select_unit(arg) {
  //$.Msg("select_unit ",arg.unitIndex)
	GameUI.SelectUnit(arg.unitIndex, false);
}
function CheckShopSchedule()
{
  CheckShop();
  $.Schedule(1/10, CheckShopSchedule);
}

function CheckCouriers()
{
  var cours = Entities.GetAllEntitiesByClassname("npc_dota_courier");
  var info = Game.GetLocalPlayerInfo();

  for (var i=0; i<cours.length; i++){
    var cour = cours[i];
    if (info.player_team_id == Entities.GetTeamNumber(cour)){

      var shop = 0;
      shop += Entities.IsInRangeOfShop( cour, 0, true) ? 1 : 0;
      shop += Entities.IsInRangeOfShop( cour, 1, true) ? 2 : 0; 
      shop += Entities.IsInRangeOfShop( cour, 2, true) ? 4 : 0;

      var oldShop = EntityShops[cour];
      if (oldShop !== shop){
        GameEvents.SendCustomGameEventToServer( "Containers_EntityShopRange", {unit:cour, shop:shop} );
      }

      EntityShops[cour] = shop;
    }
  }

  $.Schedule(1/10, CheckCouriers);
}

function CreateErrorMessage(msg)
{
  var reason = msg.reason || 80;
  if (msg.message){
    GameEvents.SendEventClientSide("dota_hud_error_message", {"splitscreenplayer":0,"reason":reason ,"message":msg.message} );
  }
  else{
    GameEvents.SendEventClientSide("dota_hud_error_message", {"splitscreenplayer":0,"reason":reason} );
  }
}

function EmitClientSound(msg)
{
  if (msg.sound){
    Game.EmitSound(msg.sound);
  }
}

function UsePanoramaInventory(msg)
{
  var use = msg.use;
  var panInv = $("#PanoramaInventory");
  if (use === 0){
    panInv.visible = false;
    GameUI.SetDefaultUIEnabled( DotaDefaultUIElement_t.DOTA_DEFAULT_UI_INVENTORY_ITEMS, true );
  }
  else{
    panInv.visible = true;
    GameUI.SetDefaultUIEnabled( DotaDefaultUIElement_t.DOTA_DEFAULT_UI_INVENTORY_ITEMS, false );
  }
}

function ScreenHeightWidth()
{
  var panel = $.GetContextPanel();

  GameUI.CustomUIConfig().screenwidth = panel.actuallayoutwidth;
  GameUI.CustomUIConfig().screenheight = panel.actuallayoutheight;

  $.Schedule(1/4, ScreenHeightWidth);
}

function SetStockTime(arg)
{
  
  GameUI.StockTime[arg.itemID] = arg.RemainTime;
}

function SetItemImageName(arg)
{
  GameUI.ItemImage[arg.itemID] = arg.ItemName;
}

(function(){
  var panel = $.GetContextPanel();

  ScreenHeightWidth();

  //containerPanel = $.CreatePanel( "Panel", panel, "" );
  //containerPanel.BLoadLayout("file://{resources}/layout/custom_game/containers/container.xml", false, false);

  GameEvents.Subscribe( "cont_create_container", CreateContainer); 
  GameEvents.Subscribe( "cont_open_container", OpenContainer); 
  GameEvents.Subscribe( "cont_close_container", CloseContainer);
  GameEvents.Subscribe( "cont_delete_container", DeleteContainer);

  GameEvents.Subscribe( "cont_execute_proxy", ExecuteProxy);
  GameEvents.Subscribe( "cont_create_error_message", CreateErrorMessage);
  GameEvents.Subscribe( "cont_emit_client_sound", EmitClientSound);
  GameEvents.Subscribe( "cont_use_panorama_inventory", UsePanoramaInventory);
  GameEvents.Subscribe( "select_unit", select_unit);

  GameEvents.Subscribe( "dota_player_update_selected_unit", CheckShop );
  GameEvents.Subscribe( "dota_player_update_query_unit", CheckShop );

  GameEvents.Subscribe( "set_stocktime", SetStockTime );
  GameEvents.Subscribe( "set_ItemImageName", SetItemImageName );
  
  

  var use = CustomNetTables.GetTableValue( "containers_lua", "use_panorama_inventory" );
  if (use)
    UsePanoramaInventory({use:use.value});
  else
    UsePanoramaInventory({use:false});

  CheckShopSchedule();
  // CheckCouriers();

  //$.Msg("container_base: ", panel); 

  if (panel.initialized){
    containers = panel.containers || {};
    for (var key in containers){
      containers[key].DeleteContainer();
      //containers[keys[key]].DeleteAsync(1);
      delete containers[key]; 
    }
    return;  
  }

  panel.containers = containers; 
  panel.initialized = true;  

  GameUI.CloseAllContainer = CloseAllContainer;
  // $.Msg("GameUI.CloseAllContainer",GameUI.CloseAllContainer())
  
  Containers_Precache();
})();

