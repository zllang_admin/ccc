"use strict"

// var attackTable = CustomNetTables.GetAllTableValues( "attacks_enabled" )

function GetMouseTarget()
{
    var mouseEntities = GameUI.FindScreenEntities( GameUI.GetCursorPosition() )
    var localHeroIndex = Players.GetPlayerHeroEntityIndex( Players.GetLocalPlayer() )

    for ( var e of mouseEntities )
    {
        if ( !e.accurateCollision )
            continue
        return e.entityIndex
    }

    for ( var e of mouseEntities )
    {
        return e.entityIndex
    }

    return 0
}

// Handle Right Button events
function OnRightButtonPressed()
{
    // $.Msg("OnRightButtonPressed")
    // $.Msg(arg)

    var iPlayerID = Players.GetLocalPlayer()
    var selectedEntities = Players.GetSelectedEntities( iPlayerID )
    var mainSelected = Players.GetLocalPlayerPortraitUnit() 
    var mainSelectedName = Entities.GetUnitName( mainSelected )    
    var targetIndex = GetMouseTarget()
    var cursor = GameUI.GetCursorPosition()
    var pressedShift = GameUI.IsShiftDown()
    var bMessageShown = false
    var CURRENT_CLICK_BEHAVIOR = GameUI.GetClickBehaviors()

    if (targetIndex)
    {
        if (Entities.IsInvulnerable(targetIndex))
        {
            GameEvents.SendCustomGameEventToServer( "move_to_invulnerable", {selectedEntities: selectedEntities, targetIndex: targetIndex })
            return false
        }
    }

    if (CURRENT_CLICK_BEHAVIOR == CLICK_BEHAVIORS.DOTA_CLICK_BEHAVIOR_CAST ){
        return false
    }

    return false
}

// Handle Left Button events
function OnLeftButtonPressed() {
    // $.Msg("OnLeftButtonPressed")
    // $.Msg(arg)

    var iPlayerID = Players.GetLocalPlayer()
    var mainSelected = Players.GetLocalPlayerPortraitUnit() 
    var mainSelectedName = Entities.GetUnitName( mainSelected )
    var targetIndex = GetMouseTarget()
    var ability = Abilities.GetLocalPlayerActiveAbility()
    var CURRENT_CLICK_BEHAVIOR = GameUI.GetClickBehaviors()
    var pressedCtrl = GameUI.IsControlDown()
    var abilityName = Abilities.GetAbilityName( ability )

    Hide_All_Shops()
    // $.Msg("GameUI.CloseAllContainer()",GameUI.CloseAllContainer())
    GameUI.CloseAllContainer()

    // if (targetIndex && pressedCtrl && abilityName == "item_chuansong")
    // {
    //     GameEvents.SendCustomGameEventToServer( "pass_chuansong", {mainSelected: mainSelected, targetIndex: targetIndex })
    // }

    return false
}

function OnAttacksEnabledChanged (args) {
    attackTable = CustomNetTables.GetAllTableValues( "attacks_enabled" )
}

function UnitCanAttackTarget (unit, target) {
    var attacks_enabled = GetAttacksEnabled(unit)
    var target_type = GetMovementCapability(target)
  
    return (Entities.CanAcceptTargetToAttack(unit, target) || (attacks_enabled.indexOf(target_type) != -1))
}

function GetMovementCapability (entIndex) {
    return Entities.HasFlyMovementCapability( entIndex ) ? "air" : "ground"
}

function GetAttacksEnabled (unit) {
    var unitName = Entities.GetUnitName(unit)
    var attackTypes = CustomNetTables.GetTableValue( "attacks_enabled", unitName)
    return attackTypes ? attackTypes.enabled : "ground"
}

function UnitCanPurchase(entIndex) {
    return (Entities.IsRealHero(entIndex) || 
            Entities.GetAbilityByName(entIndex, "human_backpack") != -1 || 
            Entities.GetAbilityByName(entIndex, "orc_backpack") != -1 || 
            Entities.GetAbilityByName(entIndex, "nightelf_backpack") != -1 || 
            Entities.GetAbilityByName(entIndex, "undead_backpack") != -1)
}

function IsBuilder(entIndex) {
    var tableValue = CustomNetTables.GetTableValue( "builders", entIndex.toString())
    return (tableValue !== undefined) && (tableValue.IsBuilder == 1)
}

function IsCustomBuilding(entIndex) {
    return (Entities.GetAbilityByName( entIndex, "ability_building") != -1 || Entities.GetAbilityByName( entIndex, "ability_tower") != -1)
}

function IsMechanical(entIndex) {
    return (Entities.GetAbilityByName( entIndex, "ability_siege") != -1)
}

function IsShop(entIndex) {
    return (IsCustomBuilding(entIndex) && Entities.GetAbilityByName( entIndex, "ability_shop") != -1)
}

function IsTavern(entIndex) {
    return (Entities.GetUnitLabel( entIndex ) == "tavern")
}

function IsGlobalShop(entIndex) {
	var entityName = Entities.GetUnitLabel( entIndex );
    return ( entityName == "goblin_merchant" || entityName == "goblin_lab" || entityName == "mercenary" )
}

function IsAlliedUnit(entIndex, targetIndex) {
    return (Entities.GetTeamNumber(entIndex) == Entities.GetTeamNumber(targetIndex))
}

function IsNeutralUnit(entIndex) {
    return (Entities.GetTeamNumber(entIndex) == DOTATeam_t.DOTA_TEAM_NEUTRALS)
}

(function () {
    // $.Msg("click.js")
    // CustomNetTables.SubscribeNetTableListener( "attacks_enabled", OnAttacksEnabledChanged );
})();

// Main mouse event callback
GameUI.SetMouseCallback( function( eventName, arg ) {
    var CONSUME_EVENT = true
    var CONTINUE_PROCESSING_EVENT = false
    var LEFT_CLICK = (arg === 0)
    var RIGHT_CLICK = (arg === 1)
    var CURRENT_CLICK_BEHAVIOR = GameUI.GetClickBehaviors()
    // $.Msg(eventName, arg)
    // $.Msg(GameUI.GetClickBehaviors())

    if ( CURRENT_CLICK_BEHAVIOR !== CLICK_BEHAVIORS.DOTA_CLICK_BEHAVIOR_NONE ){
        return CONTINUE_PROCESSING_EVENT
    }

    var mainSelected = Players.GetLocalPlayerPortraitUnit()

    if ( eventName === "pressed" || eventName === "doublepressed")
    {
        /*
        // Builder Clicks
        if (IsBuilder(mainSelected))
            if (LEFT_CLICK) 
                return (state == "active") ? SendBuildCommand() : OnLeftButtonPressed()
            else if (RIGHT_CLICK) 
                return OnRightButtonPressed()
        */
        if (LEFT_CLICK) 
            return OnLeftButtonPressed()
        else if (RIGHT_CLICK) 
            return OnRightButtonPressed() 
        
    }
    return CONTINUE_PROCESSING_EVENT
} )